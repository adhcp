--
--  Copyright (C) 2011-2014 secunet Security Networks AG
--  Copyright (C) 2011-2014 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011-2014 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ahven.Text_Runner;
with Ahven.Framework;

with DHCP.Logger;
with DHCP.Utils.Tests;
with DHCP.OS.Tests;
with DHCP.Socket_Callbacks.Tests;

with DHCPv4.Transactions.Tests;
with DHCPv4.Transmission.Tests;
with DHCPv4.Database.Tests;
with DHCPv4.ACD.Tests;

with DHCP_Tests;
with DHCP_Message_Tests;
with DHCP_Options_Tests;
with DHCP_Net_Tests;
with DHCP_Timing_Events_Tests;
with DHCP_Timer_Tests;
with DHCP_Random_Tests;
with DHCP_Notify_Tests;
with DHCP_Notify_Simple_Tests;
with DHCP_States_Tests.Init;
with DHCP_States_Tests.Selecting;
with DHCP_States_Tests.Requesting;
with DHCP_States_Tests.Bound;
with DHCP_States_Tests.Renewing;
with DHCP_States_Tests.Rebinding;
with DHCP_Termination_Tests;
with DHCP_Observers_Tests;
with DHCP_Cmdline_Tests;

with DHCPv6.Tests;

procedure Test_Runner is
   use Ahven.Framework;

   S : constant Test_Suite_Access := Create_Suite (Suite_Name => "DHCP tests");
begin
   DHCP.Logger.Use_Stdout;

   Add_Test (Suite => S.all,
             T     => new DHCPv4.Transactions.Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCPv4.Transmission.Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCPv4.Database.Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCPv4.ACD.Tests.Testcase);

   Add_Test (Suite => S.all,
             T     => new DHCP_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_States_Tests.Init.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_States_Tests.Selecting.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_States_Tests.Requesting.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_States_Tests.Bound.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_States_Tests.Renewing.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_States_Tests.Rebinding.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_Message_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_Options_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_Net_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_Timing_Events_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_Timer_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_Random_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_Notify_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_Notify_Simple_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_Termination_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_Observers_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP_Cmdline_Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP.Utils.Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP.OS.Tests.Testcase);
   Add_Test (Suite => S.all,
             T     => new DHCP.Socket_Callbacks.Tests.Testcase);

   Ahven.Text_Runner.Run (Suite => S);
   Release_Suite (T => S);

   DHCPv6.Tests.Run;

   DHCP.Logger.Stop;
end Test_Runner;
