--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Command_Line;

with DHCP.Termination;

package body DHCP_Termination_Tests is

   use Ahven;
   use DHCP;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for termination package");
      T.Add_Test_Routine
        (Routine => Signal_Termination'Access,
         Name    => "Signal termination");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Signal_Termination
   is
      use type Ada.Command_Line.Exit_Status;

      Code : Ada.Command_Line.Exit_Status := Ada.Command_Line.Failure;

      task Terminator;

      task body Terminator is
      begin
         Code := Termination.Wait;
      end Terminator;
   begin
      Termination.Signal (Exit_Status => Termination.Success);

      for I in 1 .. 200 loop
         exit when Terminator'Terminated;
         delay 0.01;
      end loop;

      if not Terminator'Terminated then
         abort Terminator;
         Fail (Message => "No signal fired");
      end if;

      Assert (Condition => Code = Ada.Command_Line.Success,
              Message   => "Exit code not Success");
   end Signal_Termination;

end DHCP_Termination_Tests;
