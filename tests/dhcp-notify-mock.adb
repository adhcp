--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCP.Notify.Mock
is

   Last_Reason : Reason_Type := Timeout;

   procedure Set_Reason (R : Reason_Type);
   --  Set last reason.

   -------------------------------------------------------------------------

   procedure Finalize (N : in out Notify_Cleaner)
   is
      pragma Unreferenced (N);
   begin
      Notify.Clear;
   end Finalize;

   -------------------------------------------------------------------------

   function Get_Last_Reason return Reason_Type
   is
   begin
      return Last_Reason;
   end Get_Last_Reason;

   -------------------------------------------------------------------------

   procedure Install
   is
   begin
      Register (Reasons => (Bound, Preinit, Rebind, Renew, Timeout),
                Handler => Set_Reason'Access);
   end Install;

   -------------------------------------------------------------------------

   procedure Set_Reason (R : Reason_Type)
   is
   begin
      Last_Reason := R;
   end Set_Reason;

end DHCP.Notify.Mock;
