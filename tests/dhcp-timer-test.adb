--
--  Copyright (C) 2011-2015 secunet Security Networks AG
--  Copyright (C) 2011-2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011-2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Timing_Events.Mock;

package body DHCP.Timer.Test
is

   -------------------------------------------------------------------------

   procedure Finalize (C : in out Cleaner_Type)
   is
   begin
      Timer.Stop;
      Initialize (C => C);
   end Finalize;

   -------------------------------------------------------------------------

   function Get_Next_Event return Timing_Events.Event_Type'Class
   is
   begin
      return Event_Queue.Get_Next_Event;
   end Get_Next_Event;

   -------------------------------------------------------------------------

   procedure Initialize (C : in out Cleaner_Type)
   is
      pragma Unreferenced (C);
   begin
      Event_Queue.Clear;
      Timing_Events.Mock.Counter.Clear;
   end Initialize;

end DHCP.Timer.Test;
