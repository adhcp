--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Strings.Unbounded;

with Anet;

with DHCP.Net;

package body DHCP_Net_Tests is

   use Ahven;
   use DHCP;
   use DHCP.Net;

   -------------------------------------------------------------------------

   procedure Discover_System_Ifaces
   is
   begin
      Assert (Condition => Get_Iface_Count = 0,
              Message   => "Iface count not 0 before discover");

      Assert (Condition => not Is_Local_Iface
              (Address => Anet.Loopback_Addr_V4),
              Message   => "Found local iface prior to discover");

      begin
         declare
            Dummy : Iface_Type := Get_Iface
              (Name     => "nonexistent",
               Addrtype => IPv4_Address);
         begin
            Fail (Message => "Expected network error");
         end;

      exception
         when Network_Error => null;
      end;

      Discover_Ifaces;

      declare
         use type Anet.IPv4_Addr_Type;

         Iface : Iface_Type (Addrtype => IPv4_Address);
      begin

         --  We assume that the local system has at least one loopback
         --  interface.

         Assert (Condition => Get_Iface_Count > 0,
                 Message   => "Could not discover any ifaces");

         Assert (Condition => Is_Local_Iface
                 (Address => Anet.Loopback_Addr_V4),
                 Message   => "Local loopback iface missing");

         Iface := Get_Iface (Name     => "lo",
                             Addrtype => IPv4_Address);

         Assert (Condition => Ada.Strings.Unbounded.To_String (Iface.Name)
                 = "lo",
                 Message   => "Name mismatch (1)");
         Assert (Condition => Iface.Address = Anet.Loopback_Addr_V4,
                 Message   => "Addr not 127.0.0.1");
         Assert (Condition => Iface.Netmask = Anet.To_IPv4_Addr
                 (Str => "255.0.0.0"),
                 Message   => "Netmask not 255.0.0.0");
      end;

      declare
         use type Anet.IPv6_Addr_Type;

         Iface6 : constant Iface_Type := Get_Iface
           (Name     => "lo",
            Addrtype => IPv6_Address);
      begin
         Assert (Condition => Ada.Strings.Unbounded.To_String
                 (Iface6.Name) = "lo",
                 Message   => "Name mismatch (2)");
         Assert (Condition => Iface6.Address6 = Anet.Loopback_Addr_V6,
                 Message   => "Addr not ::1");
         Assert (Condition => Iface6.Netmask6 = Anet.To_IPv6_Addr
                 (Str => "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff"),
                 Message   => "Netmask not /128");
      end;

      declare
         use type Anet.Hardware_Addr_Type;

         Iface_Ll : constant Iface_Type := Get_Iface
           (Name     => "lo",
            Addrtype => Link_Layer);
      begin
         Assert (Condition => Ada.Strings.Unbounded.To_String
                 (Iface_Ll.Name) = "lo",
                 Message   => "Name mismatch (3)");
         Assert (Condition => Iface_Ll.Mac_Addr = (0, 0, 0, 0, 0, 0),
                 Message   => "MAC mismatch");
      end;
   end Discover_System_Ifaces;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Net package");
      T.Add_Test_Routine
        (Routine => Discover_System_Ifaces'Access,
         Name    => "Discover network interfaces");
   end Initialize;

end DHCP_Net_Tests;
