--
--  Copyright (C) 2011, 2015 secunet Security Networks AG
--  Copyright (C) 2011, 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCP.Timing_Events.Mock
is

   -------------------------------------------------------------------------

   procedure Trigger (Event : in out Failing_Event_Type)
   is
   begin
      Event.Process.all;
   end Trigger;

   -------------------------------------------------------------------------

   procedure Trigger (Event : in out Mock_Event_Type)
   is
      pragma Unreferenced (Event);
   begin
      Counter.Increment;
   end Trigger;

   -------------------------------------------------------------------------

   protected body Counter_Type
   is

      ----------------------------------------------------------------------

      procedure Clear
      is
      begin
         Count       := 0;
         Incremented := False;
      end Clear;

      ----------------------------------------------------------------------

      procedure Increment
      is
      begin
         Count       := Count + 1;
         Incremented := True;
      end Increment;

      ----------------------------------------------------------------------

      function Value return Natural is (Count);

      ----------------------------------------------------------------------

      entry Wait_For_Inc when Incremented
      is
      begin
         if Wait_For_Inc'Count = 0 then
            Incremented := False;
         end if;
      end Wait_For_Inc;

   end Counter_Type;

end DHCP.Timing_Events.Mock;
