--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.States;

package DHCPv4.States.Mock is

   type Test_State_Type is new State_Type with private;

   function Get_Name (State : not null access Test_State_Type) return String;

   function State return DHCP.States.State_Handle;

   function Get_Start_Count return Natural;

   function Get_Offer_Count return Natural;

   function Get_Ack_Count return Natural;

   function Get_Nack_Count return Natural;

   function Get_T1_Count return Natural;

   function Get_T2_Count return Natural;

   function Get_Lease_Expiry_Count return Natural;

   procedure Clear;

private

   type Test_State_Type is new State_Type with null record;

   overriding
   procedure Start
     (State       : access Test_State_Type;
      Transaction : in out DHCP.States.Root_Context_Type'Class);

   overriding
   procedure Process_Offer
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type);

   overriding
   procedure Process_Ack
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type);

   overriding
   procedure Process_Nack
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type);

   overriding
   procedure Process_T1_Expiry
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class);

   overriding
   procedure Process_T2_Expiry
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class);

   overriding
   procedure Process_Lease_Expiry
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class);

end DHCPv4.States.Mock;
