--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCPv4.Transmission.Mock
is

   type UDPv4_Socket_Type is new
     Anet.Sockets.Inet.UDPv4_Socket_Type with null record;

   overriding
   procedure Send
     (Socket   : UDPv4_Socket_Type;
      Item     : Ada.Streams.Stream_Element_Array;
      Dst_Addr : Anet.IPv4_Addr_Type;
      Dst_Port : Anet.Port_Type);

   type Packet_Socket_Type is new
     Anet.Sockets.Packet.UDP_Socket_Type with null record;

   overriding
   procedure Send
     (Socket : Packet_Socket_Type;
      Item   : Ada.Streams.Stream_Element_Array;
      To     : Anet.Hardware_Addr_Type;
      Iface  : Anet.Types.Iface_Name_Type);

   US : aliased UDPv4_Socket_Type;
   PS : aliased Packet_Socket_Type;

   -------------------------------------------------------------------------

   procedure Clear
   is
   begin
      Send_Count    := 0;
      Last_Dst_IP   := Anet.Any_Addr;
      Last_Dst_Port := 0;
      Last_Item     := (others => 0);
      Last_Idx      := Last_Item'First;
   end Clear;

   -------------------------------------------------------------------------

   procedure Finalize (C : in out Sock_Cleaner)
   is
      pragma Unreferenced (C);
   begin
      Clear;
      Sock_Ucast := UDP_Socket'Access;
      Sock_Bcast := Packet_Socket'Access;
      Iface_Info := (Addrtype => DHCP.Net.Link_Layer,
                     others => <>);
   end Finalize;

   -------------------------------------------------------------------------

   function Get_Last_Iface return String
   is
   begin
      return Ada.Strings.Unbounded.To_String (Last_Iface);
   end Get_Last_Iface;

   -------------------------------------------------------------------------

   procedure Initialize (C : in out Sock_Cleaner)
   is
      pragma Unreferenced (C);
   begin
      Sock_Ucast := US'Access;
      Sock_Bcast := PS'Access;
   end Initialize;

   -------------------------------------------------------------------------

   procedure Send
     (Socket   : UDPv4_Socket_Type;
      Item     : Ada.Streams.Stream_Element_Array;
      Dst_Addr : Anet.IPv4_Addr_Type;
      Dst_Port : Anet.Port_Type)
   is
      pragma Unreferenced (Socket);
   begin
      Last_Item (Last_Item'First .. Item'Length) := Item;

      Send_Count    := Send_Count + 1;
      Last_Dst_IP   := Dst_Addr;
      Last_Dst_Port := Dst_Port;
      Last_Idx      := Item'Length;
   end Send;

   -------------------------------------------------------------------------

   procedure Send
     (Socket : Packet_Socket_Type;
      Item   : Ada.Streams.Stream_Element_Array;
      To     : Anet.Hardware_Addr_Type;
      Iface  : Anet.Types.Iface_Name_Type)
   is
      use type Ada.Streams.Stream_Element_Offset;

      pragma Unreferenced (Socket);
   begin
      Last_Dst_IP := (1 => Anet.Byte (Item (17)),
                      2 => Anet.Byte (Item (18)),
                      3 => Anet.Byte (Item (19)),
                      4 => Anet.Byte (Item (20)));

      Last_Item (Last_Item'First .. Item'Length) := Item;

      Send_Count  := Send_Count + 1;
      Last_Idx    := Item'Length;
      Last_Dst_HW := To;
      Last_Iface  := Ada.Strings.Unbounded.To_Unbounded_String
        (String (Iface));
   end Send;

end DHCPv4.Transmission.Mock;
