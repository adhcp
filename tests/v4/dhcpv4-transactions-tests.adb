--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Tags;
with Ada.Finalization;
with Ada.Real_Time;

with DHCP.States;
with DHCP.Timer.Test;
with DHCP.Timing_Events.Start;
with DHCP.Timing_Events.Timer_Expiry;
with DHCP.Types;
with DHCP.Notify.Mock;

with DHCPv4.Options;
with DHCPv4.States.Mock;
with DHCPv4.Transmission.Mock;

package body DHCPv4.Transactions.Tests is

   use Ahven;

   use type Anet.Word32;
   use type Anet.HW_Addr_Len_Type;
   use type Anet.Hardware_Addr_Type;
   use type Anet.IPv4_Addr_Type;

   type Test_Cleaner is new Ada.Finalization.Controlled with record
      Sock_Cleaner : DHCPv4.Transmission.Mock.Sock_Cleaner;
   end record;

   overriding
   procedure Finalize (C : in out Test_Cleaner);
   --  Clear the mock socket counters and timer etc.

   -------------------------------------------------------------------------

   procedure Finalize (C : in out Test_Cleaner)
   is
      pragma Unreferenced (C);
   begin
      States.Mock.Clear;
      DHCP.Timer.Clear;
   end Finalize;

   -------------------------------------------------------------------------

   procedure Increase_Retry_Delay
   is
      T : Transactions.Transaction_Type;
   begin
      Assert (Condition => T.Retry_Delay = Initial_Retry_Delay,
              Message   => "Initial retry delay mismatch");

      T.Increase_Retry_Delay;
      Assert (Condition => T.Retry_Delay = 8.0,
              Message   => "Retry delay not 8");

      T.Increase_Retry_Delay;
      Assert (Condition => T.Retry_Delay = 16.0,
              Message   => "Retry delay not 16");

      T.Increase_Retry_Delay;
      Assert (Condition => T.Retry_Delay = 32.0,
              Message   => "Retry delay not 32");

      T.Increase_Retry_Delay;
      Assert (Condition => T.Retry_Delay = 64.0,
              Message   => "Retry delay not 64");

      T.Increase_Retry_Delay;
      Assert (Condition => T.Retry_Delay = 64.0,
              Message   => "Retry delay should still be 64");

      T.Reset_Retry_Delay;
      T.Set_Exp_Backoff (New_State => False);

      T.Increase_Retry_Delay;
      Assert (Condition => T.Retry_Delay = Initial_Retry_Delay,
              Message   => "Delay increased with exp backoff disabled");
   end Increase_Retry_Delay;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DHCP transactions");
      T.Add_Test_Routine (Routine => Set_Get_Transaction_ID'Access,
                          Name    => "XID setter/getter");
      T.Add_Test_Routine (Routine => Set_Get_Server_Addr'Access,
                          Name    => "Server address setter/getter");
      T.Add_Test_Routine (Routine => Reset_Retry_Delay'Access,
                          Name    => "Reset retry delay");
      T.Add_Test_Routine (Routine => Increase_Retry_Delay'Access,
                          Name    => "Increase retry delay");
      T.Add_Test_Routine (Routine => Prepare_Message'Access,
                          Name    => "Prepare message");
      T.Add_Test_Routine (Routine => Set_Transaction_State'Access,
                          Name    => "Set state");
      T.Add_Test_Routine (Routine => Start_Transaction'Access,
                          Name    => "Start transaction");
      T.Add_Test_Routine (Routine => Process_Message'Access,
                          Name    => "Process message");
      T.Add_Test_Routine (Routine => Process_Timer_Expiry'Access,
                          Name    => "Process timer expiry");
      T.Add_Test_Routine (Routine => Send_Message'Access,
                          Name    => "Send message");
      T.Add_Test_Routine (Routine => Set_Get_Start_Time'Access,
                          Name    => "Start time setter/getter");
      T.Add_Test_Routine (Routine => Timeout_Processing'Access,
                          Name    => "Binding timeout");
      T.Add_Test_Routine (Routine => Reset_Transaction'Access,
                          Name    => "Reset transaction");
      T.Add_Test_Routine (Routine => Set_Exp_Backoff'Access,
                          Name    => "Exp backoff strategy setter");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Prepare_Message
   is
      M : Message.Message_Type;
      T : Transactions.Transaction_Type;
   begin
      T.Set_ID (XID => 1234);

      T.Prepare_Message (Msg => M);

      Assert (Condition => M.Get_Transaction_ID = 1234,
              Message   => "XID mismatch");
      Assert (Condition => M.Get_HW_Addr_Length = 6,
              Message   => "Hardware address length not 6");
      Assert (Condition => M.Get_Hardware_Address = (0, 0, 0, 0, 0, 0),
              Message   => "Hardware address mismatch");
      Assert (Condition => M.Get_Options.Get_Count = 1,
              Message   => "Options count not 1");
      Assert (Condition => M.Get_Options.Contains (Options.Parameter_List),
              Message   => "Missing parameter request list option");
   end Prepare_Message;

   -------------------------------------------------------------------------

   procedure Process_Message
   is
      use DHCPv4.States.Mock;

      M : Message.Message_Type := Message.Create (Kind => Message.Offer);
      T : Transactions.Transaction_Type;
      C : Test_Cleaner;
      pragma Unreferenced (C);
   begin
      T.Set_State (State => State);

      T.Process_Message (Msg => M);
      Assert (Condition => Get_Offer_Count = 1
              and then Get_Ack_Count = 0
              and then Get_Nack_Count = 0,
              Message   => "Offer processing failed");

      M := Message.Create (Kind => Message.Acknowledge);
      T.Process_Message (Msg => M);
      Assert (Condition => Get_Offer_Count = 1
              and then Get_Ack_Count = 1
              and then Get_Nack_Count = 0,
              Message   => "Ack processing failed");

      M := Message.Create (Kind => Message.Not_Acknowledge);
      T.Process_Message (Msg => M);
      Assert (Condition => Get_Offer_Count = 1
              and then Get_Ack_Count = 1
              and then Get_Nack_Count = 1,
              Message   => "Nack processing failed");

      M := Message.Create (Kind => Message.Discover);
      T.Process_Message (Msg => M);
      Assert (Condition => Get_Offer_Count = 1
              and then Get_Ack_Count = 1
              and then Get_Nack_Count = 1,
              Message   => "Other message processing failed");
   end Process_Message;

   -------------------------------------------------------------------------

   procedure Process_Timer_Expiry
   is
      use DHCPv4.States.Mock;

      T : Transactions.Transaction_Type;
      C : Test_Cleaner;
      pragma Unreferenced (C);
   begin
      T.Set_State (State => State);

      T.Process_Timer_Expiry (Timer_Kind => DHCP.Types.T1);
      Assert (Condition => Get_T1_Count = 1
              and then Get_T2_Count = 0
              and then Get_Lease_Expiry_Count = 0,
              Message   => "T1 expiry processing failed");

      T.Process_Timer_Expiry (Timer_Kind => DHCP.Types.T2);
      Assert (Condition => Get_T1_Count = 1
              and then Get_T2_Count = 1
              and then Get_Lease_Expiry_Count = 0,
              Message   => "T2 expiry processing failed");

      T.Process_Timer_Expiry (Timer_Kind => DHCP.Types.Lease_Expiry);
      Assert (Condition => Get_T1_Count = 1
              and then Get_T2_Count = 1
              and then Get_Lease_Expiry_Count = 1,
              Message   => "Lease expiry processing failed");

      declare
         use Ada.Real_Time;

         Dst    : constant Anet.IPv4_Addr_Type
           := Anet.To_IPv4_Addr (Str => "192.168.0.1");
         Before : constant Time := Clock;
      begin
         T.Last_Msg := Message.Create (Kind => Message.Request);
         T.Last_Dst := Dst;
         T.Process_Timer_Expiry (Timer_Kind => DHCP.Types.Retransmit);

         Assert (Condition => Transmission.Mock.Send_Count = 1,
                 Message   => "Retransmit event did not call send msg");
         Assert (Condition => Transmission.Mock.Last_Dst_IP = Dst,
                 Message   => "Retransmit destination mismatch");
         Assert (Condition => Before <= T.Get_Start_Time
                 and then T.Get_Start_Time <= Clock,
                 Message   => "Start time not set");
      end;

      declare
         use Ada.Real_Time;

         Dst   : constant Anet.IPv4_Addr_Type
           := Anet.To_IPv4_Addr (Str => "192.168.0.1");
         Stamp : constant Time := Clock;
      begin
         T.Last_Msg := Message.Create (Kind => Message.Discover);
         T.Last_Dst := Dst;
         T.Set_Start_Time (Time => Stamp);
         T.Process_Timer_Expiry (Timer_Kind => DHCP.Types.Retransmit);

         Assert (Condition => T.Get_Start_Time = Stamp,
                 Message   => "Start time changed");
      end;
   end Process_Timer_Expiry;

   -------------------------------------------------------------------------

   procedure Reset_Retry_Delay
   is
      T : Transactions.Transaction_Type;
   begin
      T.Retry_Delay := 1.0;

      T.Reset_Retry_Delay;
      Assert (Condition => T.Retry_Delay = Initial_Retry_Delay,
              Message   => "Retry delay not reset");
   end Reset_Retry_Delay;

   -------------------------------------------------------------------------

   procedure Reset_Transaction
   is
      use type Ada.Real_Time.Time;

      T    : Transactions.Transaction_Type;
      Opts : Options.Inst4.Option_List;
   begin
      Opts.Append (New_Item => Options.Inst4.Default_Param_Req_List);

      T.ID          := 5;
      T.Retry_Delay := 32.0;
      T.Set_Start_Time (Time => Ada.Real_Time.Clock);
      T.Cur_Server_Addr := Anet.Loopback_Addr_V4;
      T.Reset;

      Assert (Condition => T.ID = 0,
              Message   => "ID not reset");
      Assert (Condition => T.Retry_Delay = Initial_Retry_Delay,
              Message   => "Retry delay not reset");
      Assert (Condition => T.Get_Start_Time = Ada.Real_Time.Time_First,
              Message   => "Start time not reset");
      Assert (Condition => T.Cur_Server_Addr = Anet.Any_Addr,
              Message   => "Current server address not reset");
   end Reset_Transaction;

   -------------------------------------------------------------------------

   procedure Send_Message
   is
      use type DHCPv4.Message.Message_Type;
      use DHCP.Types;

      Dst : constant Anet.IPv4_Addr_Type
        := Anet.To_IPv4_Addr (Str => "192.168.0.1");
      M   : constant Message.Message_Type
        := Message.Create (Kind => Message.Discover);
      T   : Transactions.Transaction_Type;
      C   : Test_Cleaner;
      pragma Unreferenced (C);
   begin
      T.Retry_Delay := 32.0;

      T.Send_Message (Msg => M,
                      Dst => Dst);

      Assert (Condition => Transmission.Mock.Send_Count = 1,
              Message   => "Socket.Send was not called");
      Assert (Condition => Transmission.Mock.Last_Dst_IP = Dst,
              Message   => "Destination address mismatch");
      Assert (Condition => T.Last_Msg = M,
              Message   => "Transaction last msg mismatch");
      Assert (Condition => T.Last_Dst = Dst,
              Message   => "Transaction last dst mismatch");
      Assert (Condition => T.Retry_Delay = 64.0,
              Message   => "Retry delay mismatch");
      Assert (Condition => DHCP.Timer.Event_Count = 1,
              Message   => "Missing retry event");
      Assert (Condition => DHCP.Timing_Events.Timer_Expiry.Expiry_Type
              (DHCP.Timer.Test.Get_Next_Event).Timer_Kind = Retransmit,
              Message   => "Scheduled event not retransmit event");
   end Send_Message;

   -------------------------------------------------------------------------

   procedure Set_Exp_Backoff
   is
      T : Transactions.Transaction_Type;
   begin
      Assert (Condition => T.Exp_Backoff,
              Message   => "Exponential backoff not enabled by default");

      T.Set_Exp_Backoff (New_State => False);
      Assert (Condition => not T.Exp_Backoff,
              Message   => "Exponential backoff not disabled");

      T.Set_Exp_Backoff (New_State => True);
      Assert (Condition => T.Exp_Backoff,
              Message   => "Exponential backoff not re-enabled");
   end Set_Exp_Backoff;

   -------------------------------------------------------------------------

   procedure Set_Get_Server_Addr
   is
      T : Transactions.Transaction_Type;
   begin
      Assert (Condition => T.Get_Server_Address = Anet.Any_Addr,
              Message   => "Default server addr mismatch");

      T.Set_Server_Address (Address => Anet.Loopback_Addr_V4);
      Assert (Condition => T.Get_Server_Address =
                Anet.Loopback_Addr_V4,
              Message   => "Address mismatch");
   end Set_Get_Server_Addr;

   -------------------------------------------------------------------------

   procedure Set_Get_Start_Time
   is
      use type Ada.Real_Time.Time;

      T : Transactions.Transaction_Type;
      C : Test_Cleaner;
      pragma Unreferenced (C);
   begin
      Assert (Condition => T.Get_Start_Time = Ada.Real_Time.Time_First,
              Message   => "Default start time not time first");

      T.Set_Start_Time (Time => Ada.Real_Time.Time_Last);
      Assert (Condition => T.Get_Start_Time <= Ada.Real_Time.Time_Last,
              Message   => "Start time not changed");
   end Set_Get_Start_Time;

   -------------------------------------------------------------------------

   procedure Set_Get_Transaction_ID
   is
      T : Transactions.Transaction_Type;
   begin
      Assert (Condition => T.Get_ID = 0,
              Message   => "Default XID not 0");

      T.Set_ID (XID => 1234);
      Assert (Condition => T.Get_ID = 1234,
              Message   => "XID mismatch");
   end Set_Get_Transaction_ID;

   -------------------------------------------------------------------------

   procedure Set_Transaction_State
   is
      use type DHCP.States.State_Handle;

      T : Transactions.Transaction_Type;
   begin
      Assert (Condition => T.State = States.Init.State,
              Message   => "Initial state mismatch");

      T.Set_State (State => States.Mock.State);
      Assert (Condition => T.State = States.Mock.State,
              Message   => "State mismatch");
   end Set_Transaction_State;

   -------------------------------------------------------------------------

   procedure Start_Transaction
   is
      T : Transactions.Transaction_Type;
      C : Test_Cleaner;
      pragma Unreferenced (C);
   begin
      T.Set_State (State => States.Mock.State);
      T.Start;

      Assert (Condition => States.Mock.Get_Start_Count = 1,
              Message   => "Start count not 1");
   end Start_Transaction;

   -------------------------------------------------------------------------

   procedure Timeout_Processing
   is
      use type Ada.Tags.Tag;
      use type DHCP.States.State_Handle;
      use type DHCP.Notify.Reason_Type;

      T : Transactions.Transaction_Type;
      C : DHCP.Notify.Mock.Notify_Cleaner;
      pragma Unreferenced (C);
   begin
      DHCP.Notify.Mock.Install;
      T.Process_Bind_Timeout;

      Assert (Condition => T.State = States.Init.State,
              Message   => "Transaction state not Init");
      Assert (Condition => T.Retry_Delay = Initial_Retry_Delay,
              Message   => "Retry delay mismatch");
      Assert (Condition => DHCP.Timer.Event_Count = 1,
              Message   => "Missing retry event");
      Assert (Condition => DHCP.Timer.Test.Get_Next_Event'Tag =
                DHCP.Timing_Events.Start.Start_Type'Tag,
              Message   => "Scheduled event not start event");
      Assert (Condition => DHCP.Notify.Mock.Get_Last_Reason
              = DHCP.Notify.Timeout,
              Message   => "Notification reason not timeout");
   end Timeout_Processing;

end DHCPv4.Transactions.Tests;
