--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet.Util;

with DHCP.OS;

with DHCPv4.Database.IO;
with DHCPv4.Database.Mock;

with Test_Utils;

package body DHCPv4.Database.Tests
is

   use Ahven;

   Ref_Addr : constant Anet.IPv4_Addr_Type := (192, 168, 111, 12);
   Ref_DB   : constant String              := "data/database-v4-"
     & Test_Utils.Get_Word_Size & ".ref";

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Database package");
      T.Add_Test_Routine
        (Routine => Set_Get_Fixed_Address'Access,
         Name    => "Fixed address setter/getter");
      T.Add_Test_Routine
        (Routine => Set_Get_Options'Access,
         Name    => "Options setter/getter");
      T.Add_Test_Routine
        (Routine => Write'Access,
         Name    => "Write database to file");
      T.Add_Test_Routine
        (Routine => Load'Access,
         Name    => "Load database from file");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Load
   is
      Dummy : DHCPv4.Database.Mock.Cleaner;
   begin
      IO.Load (Filename => Ref_DB);

      Assert (Condition => Instance.Fixed_Address = Ref_Addr,
              Message   => "Database mismatch (1)");
      Assert (Condition => Instance.Opts.Contains
              (Name => Options.Forward_On_Off),
              Message   => "Database mismatch (2)");
   end Load;

   -------------------------------------------------------------------------

   procedure Set_Get_Fixed_Address
   is
      Dummy    : DHCPv4.Database.Mock.Cleaner;
      Ref_Addr : Anet.IPv4_Addr_Type := (192, 168, 10, 2);
   begin
      Assert (Condition => Instance.Fixed_Address = Anet.Any_Addr,
              Message   => "Address not anyaddr (1)");

      Set_Fixed_Address (Addr => Ref_Addr);
      Assert (Condition => Instance.Fixed_Address = Ref_Addr,
              Message   => "Address mismatch (1)");

      Ref_Addr (Ref_Addr'Last) := 4;
      Instance.Fixed_Address := Ref_Addr;
      Assert (Condition => Get_Fixed_Address = Ref_Addr,
              Message   => "Address mismatch (2)");

      Reset;

      Assert (Condition => Instance.Fixed_Address = Anet.Any_Addr,
              Message   => "Address not anyaddr (2)");
   end Set_Get_Fixed_Address;

   -------------------------------------------------------------------------

   procedure Set_Get_Options
   is
      use type DHCPv4.Options.Inst4.Option_List;

      Dummy    : DHCPv4.Database.Mock.Cleaner;
      Ref_Opts : Options.Inst4.Option_List;
   begin
      Assert (Condition => Instance.Opts.Is_Empty,
              Message   => "List not empty (1)");

      Ref_Opts.Append
        (New_Item => Options.Inst.Create
           (Name => Options.Forward_On_Off,
            Data => (1 => 1)));
      Ref_Opts.Append
        (New_Item => Options.Inst.Create
           (Name => Options.Forward_On_Off,
            Data => (1 => 0)));
      Assert (Condition => Ref_Opts.Get_Count = 2,
              Message   => "Option count not 2");

      Set_Options (Opts => Ref_Opts);
      Assert (Condition => Instance.Opts = Ref_Opts,
              Message   => "Options mismatch (1)");

      Ref_Opts.Append
        (New_Item => Options.Inst.Create
           (Name => Options.Forward_On_Off,
            Data => (1 => 0)));
      Assert (Condition => Ref_Opts.Get_Count = 3,
              Message   => "Option count not 3");

      Instance.Opts := Ref_Opts;
      Assert (Condition => Get_Options = Ref_Opts,
              Message   => "Options mismatch (2)");

      Reset;

      Assert (Condition => Instance.Opts.Is_Empty,
              Message   => "List not empty (2)");
   end Set_Get_Options;

   -------------------------------------------------------------------------

   procedure Write
   is
      Dummy  : DHCPv4.Database.Mock.Cleaner;
      F_Name : constant String := "/tmp/database-v4-"
        & Anet.Util.Random_String (Len => 8);
   begin
      Instance.Fixed_Address := Ref_Addr;
      Instance.Opts.Append
        (New_Item => Options.Inst.Create
           (Name => Options.Forward_On_Off,
            Data => (1 => 1)));
      IO.Write (Filename => F_Name);
      Reset;

      Assert (Condition => Test_Utils.Equal_Files
              (Filename1 => F_Name,
               Filename2 => Ref_DB),
              Message   => "Written database mismatch");
      DHCP.OS.Delete_File (Filename => F_Name);
   end Write;

end DHCPv4.Database.Tests;
