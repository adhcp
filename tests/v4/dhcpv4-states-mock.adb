--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCPv4.States.Mock is

   Instance          : aliased Test_State_Type;
   Start_Count       : Natural := 0;
   Proc_Off_Count    : Natural := 0;
   Proc_Ack_Count    : Natural := 0;
   Proc_Nack_Count   : Natural := 0;
   Proc_L_Exp_Count  : Natural := 0;
   Proc_T1_Exp_Count : Natural := 0;
   Proc_T2_Exp_Count : Natural := 0;

   -------------------------------------------------------------------------

   procedure Clear
   is
   begin
      Start_Count       := 0;
      Proc_Off_Count    := 0;
      Proc_Ack_Count    := 0;
      Proc_Nack_Count   := 0;
      Proc_L_Exp_Count  := 0;
      Proc_T1_Exp_Count := 0;
      Proc_T2_Exp_Count := 0;
   end Clear;

   -------------------------------------------------------------------------

   function Get_Ack_Count return Natural
   is
   begin
      return Proc_Ack_Count;
   end Get_Ack_Count;

   -------------------------------------------------------------------------

   function Get_Lease_Expiry_Count return Natural
   is
   begin
      return Proc_L_Exp_Count;
   end Get_Lease_Expiry_Count;

   -------------------------------------------------------------------------

   function Get_Nack_Count return Natural
   is
   begin
      return Proc_Nack_Count;
   end Get_Nack_Count;

   -------------------------------------------------------------------------

   function Get_Name (State : not null access Test_State_Type) return String
   is
      pragma Unreferenced (State);
   begin
      return "Test state";
   end Get_Name;

   -------------------------------------------------------------------------

   function Get_Offer_Count return Natural
   is
   begin
      return Proc_Off_Count;
   end Get_Offer_Count;

   -------------------------------------------------------------------------

   function Get_Start_Count return Natural
   is
   begin
      return Start_Count;
   end Get_Start_Count;

   -------------------------------------------------------------------------

   function Get_T1_Count return Natural
   is
   begin
      return Proc_T1_Exp_Count;
   end Get_T1_Count;

   -------------------------------------------------------------------------

   function Get_T2_Count return Natural
   is
   begin
      return Proc_T2_Exp_Count;
   end Get_T2_Count;

   -------------------------------------------------------------------------

   procedure Process_Ack
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      pragma Unreferenced (State, Transaction, Msg);
   begin
      Proc_Ack_Count := Proc_Ack_Count + 1;
   end Process_Ack;

   -------------------------------------------------------------------------

   procedure Process_Lease_Expiry
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class)
   is
      pragma Unreferenced (State, Transaction);
   begin
      Proc_L_Exp_Count := Proc_L_Exp_Count + 1;
   end Process_Lease_Expiry;

   -------------------------------------------------------------------------

   procedure Process_Nack
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      pragma Unreferenced (State, Transaction, Msg);
   begin
      Proc_Nack_Count := Proc_Nack_Count + 1;
   end Process_Nack;

   -------------------------------------------------------------------------

   procedure Process_Offer
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      pragma Unreferenced (State, Transaction, Msg);
   begin
      Proc_Off_Count := Proc_Off_Count + 1;
   end Process_Offer;

   -------------------------------------------------------------------------

   procedure Process_T1_Expiry
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class)
   is
      pragma Unreferenced (State, Transaction);
   begin
      Proc_T1_Exp_Count := Proc_T1_Exp_Count + 1;
   end Process_T1_Expiry;

   -------------------------------------------------------------------------

   procedure Process_T2_Expiry
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class)
   is
      pragma Unreferenced (State, Transaction);
   begin
      Proc_T2_Exp_Count := Proc_T2_Exp_Count + 1;
   end Process_T2_Expiry;

   -------------------------------------------------------------------------

   procedure Start
     (State       : access Test_State_Type;
      Transaction : in out DHCP.States.Root_Context_Type'Class)
   is
      pragma Unreferenced (State, Transaction);
   begin
      Start_Count := Start_Count + 1;
   end Start;

   -------------------------------------------------------------------------

   function State return DHCP.States.State_Handle
   is
   begin
      return Instance'Access;
   end State;

end DHCPv4.States.Mock;
