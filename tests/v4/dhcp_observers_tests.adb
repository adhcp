--
--  Copyright (C) 2011, 2015 secunet Security Networks AG
--  Copyright (C) 2011, 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Command_Line;

with Anet.OS;
with Anet.Types;

with DHCP.Termination;
with DHCP.Notify;
with DHCP.Observers;
with DHCP.Cmd_Line;

package body DHCP_Observers_Tests is

   use Ahven;
   use DHCP;

   function Get_Iface
     return Anet.Types.Iface_Name_Type
   is (Anet.Types.Iface_Name_Type'("wlan12"));
   --  Return test interface name.

   -------------------------------------------------------------------------

   procedure Call_External_Notify_False
   is
      procedure Do_Nothing (Destination : String) is null;

      procedure Call_External_Notify is new DHCP.Observers.Call_External_Notify
        (Get_Interface_Name => Get_Iface,
         Write_Database     => Do_Nothing);

      Unused_Reason : DHCP.Notify.Reason_Type;
   begin
      Cmd_Line.Set_Ext_Notify_Binary (B => "/bin/false");

      begin
         Call_External_Notify (R => Unused_Reason);
         Fail (Message => "Exception expected");

      exception
         when Anet.OS.Command_Failed => null;
      end;

   exception
      when others =>
         Cmd_Line.Set_Ext_Notify_Binary (B => "");
         raise;
   end Call_External_Notify_False;

   -------------------------------------------------------------------------

   procedure Call_External_Notify_True
   is
      Called : Boolean := False;

      procedure Set_Called (Destination : String);
      --  Set called flag to True.

      ----------------------------------------------------------------------

      procedure Set_Called (Destination : String)
      is
         pragma Unreferenced (Destination);
      begin
         Called := True;
      end Set_Called;

      procedure Call_External_Notify is new DHCP.Observers.Call_External_Notify
        (Get_Interface_Name => Get_Iface,
         Write_Database     => Set_Called);

      Unused_Reason : DHCP.Notify.Reason_Type;
   begin
      Cmd_Line.Set_Ext_Notify_Binary (B => "/bin/true");
      Call_External_Notify (R => Unused_Reason);

      Assert (Condition => Called,
              Message   => "Called flag not True");
      Cmd_Line.Set_Ext_Notify_Binary (B => "");

   exception
      when others =>
         Cmd_Line.Set_Ext_Notify_Binary (B => "");
         raise;
   end Call_External_Notify_True;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for observers");
      T.Add_Test_Routine
        (Routine => One_Shot_Observer'Access,
         Name    => "One-shot observer");
      T.Add_Test_Routine
        (Routine => Call_External_Notify_True'Access,
         Name    => "Call external notifier (bin/true)");
      T.Add_Test_Routine
        (Routine => Call_External_Notify_False'Access,
         Name    => "Call external notifier (bin/false)");
   end Initialize;

   -------------------------------------------------------------------------

   procedure One_Shot_Observer
   is
      use Ada.Command_Line;
   begin
      Observers.One_Shot_Mode (R => Notify.Bound);
      Assert (Condition => Termination.Wait = Success,
              Message   => "Exit code not Success");

      Observers.One_Shot_Mode (R => Notify.Timeout);
      Assert (Condition => Termination.Wait = Failure,
              Message   => "Exit code not Failure");
   end One_Shot_Observer;

end DHCP_Observers_Tests;
