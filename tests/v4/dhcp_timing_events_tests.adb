--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;
with Ada.Streams;
with Ada.Strings.Unbounded;

with Anet.ARP;

with DHCP.Net;
with DHCP.Timing_Events.Queues;
with DHCP.Timing_Events.Mock;
with DHCP.Timing_Events.Start;
with DHCP.Timing_Events.Timer_Expiry;
with DHCP.Types;

with DHCPv4.Timing_Events.Announce;
with DHCPv4.Timing_Events.Handle_Msg;
with DHCPv4.Timing_Events.Bind_Timeout;
with DHCPv4.Transactions.Mock;
with DHCPv4.ACD.Mock;
with DHCPv4.Message;

package body DHCP_Timing_Events_Tests is

   use Ahven;
   use DHCPv4;

   -------------------------------------------------------------------------

   procedure Event_Announce
   is
      use Ada.Strings.Unbounded;
      use DHCPv4.ACD;
      use type Ada.Streams.Stream_Element_Array;
      use type Anet.Hardware_Addr_Type;

      Ref_IP_Addr : constant Anet.IPv4_Addr_Type := (192, 168, 0, 129);
      Ref_HW_Addr : constant Anet.Hardware_Addr_Type (1 .. 6)
        := (16#be#, 16#ef#, 16#ca#, 16#fe#, 16#be#, 16#ef#);
      Ref_Data    : constant Anet.ARP.Header_Type
        := (Operation => Anet.ARP.ARP_Request,
            Src_Ether => Ref_HW_Addr,
            Src_IP    => Ref_IP_Addr,
            Dst_Ether => (others => 0),
            Dst_IP    => Ref_IP_Addr);

      E : Timing_Events.Announce.Announce_Type
        := Timing_Events.Announce.Create_Event (IP_Address => Ref_IP_Addr,
                                                Count      => 1);
      Dummy_Cleaner : Mock.Sock_Cleaner;
   begin
      ACD.Initialize (Iface => (Addrtype => DHCP.Net.Link_Layer,
                                Name     => To_Unbounded_String ("eth0"),
                                Mac_Addr => Ref_HW_Addr));

      E.Trigger;
      Assert (Condition => Mock.Send_Count = 1,
              Message   => "Announce send count mismatch");
      Assert (Condition => Anet.ARP.To_Stream (Header => Ref_Data)
              = Mock.Last_Item (Mock.Last_Item'First .. Mock.Last_Idx),
              Message   => "Announce data mismatch");
      Assert (Condition => Mock.Last_Dst_HW = Anet.Bcast_HW_Addr,
              Message   => "Announce destination hw address mismatch");
   end Event_Announce;

   -------------------------------------------------------------------------

   procedure Event_Bind_Timeout
   is
      T : aliased Transactions.Mock.Transaction_Type;
      E : Timing_Events.Bind_Timeout.Timeout_Type (T => T'Access);
   begin
      E.Trigger;
      Assert (Condition => T.Timeout_Count = 1,
              Message   => "Process bind timeout not called");
   end Event_Bind_Timeout;

   -------------------------------------------------------------------------

   procedure Event_Handle_Message
   is
      use type DHCPv4.Message.Message_Type;

      T : aliased Transactions.Mock.Transaction_Type;
      E : Timing_Events.Handle_Msg.Handle_Msg_Type (T => T'Access);
      M : Message.Message_Type := Message.Create (Kind => Message.Release);
   begin
      M.Set_Transaction_ID (XID => 123123);
      E.Set_Message (Msg => M);
      E.Trigger;
      Assert (Condition => T.Proc_Msg_Count = 1,
              Message   => "Process msg not called");
      Assert (Condition => T.Last_Proc_Msg = M,
              Message   => "Msg mismatch");
   end Event_Handle_Message;

   -------------------------------------------------------------------------

   procedure Event_Setters_Getters
   is
      use type Ada.Real_Time.Time;

      Ev  : DHCP.Timing_Events.Mock.Mock_Event_Type;
      Now : constant Ada.Real_Time.Time := Ada.Real_Time.Clock;
   begin
      Assert (Condition => Ev.Get_Time = Ada.Real_Time.Time_First,
              Message   => "Default time not Time_First");

      Ev.Set_Time (At_Time => Now);
      Assert (Condition => Ev.Get_Time = Now,
              Message   => "Error setting time");

      Ev.Set_Time (At_Time => Ada.Real_Time.Time_Last);
      Assert (Condition => Ev.Get_Time = Ada.Real_Time.Time_Last,
              Message   => "Error resetting time");
   end Event_Setters_Getters;

   -------------------------------------------------------------------------

   procedure Event_Start
   is
      T : aliased Transactions.Mock.Transaction_Type;
      E : DHCP.Timing_Events.Start.Start_Type (T => T'Access);
   begin
      E.Trigger;
      Assert (Condition => T.Start_Count = 1,
              Message   => "Start not called");
   end Event_Start;

   -------------------------------------------------------------------------

   procedure Event_Timer_Expiry
   is
      T : aliased Transactions.Mock.Transaction_Type;
      E : DHCP.Timing_Events.Timer_Expiry.Expiry_Type
        (T          => T'Access,
         Timer_Kind => DHCP.Types.T1);
   begin
      E.Trigger;
      Assert (Condition => T.Tim_Exp_Count = 1,
              Message   => "Timer expiry not called");
   end Event_Timer_Expiry;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for timing events and event queues");
      T.Add_Test_Routine
        (Routine => Event_Setters_Getters'Access,
         Name    => "Event setter/getters");
      T.Add_Test_Routine
        (Routine => Queue_Event_Insertion'Access,
         Name    => "Queue insertion");
      T.Add_Test_Routine
        (Routine => Queue_Event_Removal'Access,
         Name    => "Queue event removal");
      T.Add_Test_Routine
        (Routine => Queue_Next_Time_Getter'Access,
         Name    => "Queue next event time getter");
      T.Add_Test_Routine
        (Routine => Queue_Next_Event_Getter'Access,
         Name    => "Queue next event getter");
      T.Add_Test_Routine
        (Routine => Queue_Event_Time_Changed'Access,
         Name    => "Queue event time changed entry");
      T.Add_Test_Routine
        (Routine => Queue_Reset'Access,
         Name    => "Queue reset");
      T.Add_Test_Routine
        (Routine => Event_Start'Access,
         Name    => "Event start");
      T.Add_Test_Routine
        (Routine => Event_Handle_Message'Access,
         Name    => "Event handle message");
      T.Add_Test_Routine
        (Routine => Event_Timer_Expiry'Access,
         Name    => "Event timer expiry");
      T.Add_Test_Routine
        (Routine => Event_Bind_Timeout'Access,
         Name    => "Event bind timeout");
      T.Add_Test_Routine
        (Routine => Event_Announce'Access,
         Name    => "Event announce");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Queue_Event_Insertion
   is
      Queue : DHCP.Timing_Events.Queues.Protected_Event_Queue;
      Event : DHCP.Timing_Events.Mock.Mock_Event_Type;
   begin
      Assert (Condition => Queue.Length = 0,
              Message   => "New queue not empty");

      Queue.Insert (Event => Event);
      Assert (Condition => Queue.Length = 1,
              Message   => "Unable to insert event");

      --  Inserting the same element twice should not raise an exception.

      Queue.Insert (Event => Event);
   end Queue_Event_Insertion;

   -------------------------------------------------------------------------

   procedure Queue_Event_Removal
   is
      Queue : DHCP.Timing_Events.Queues.Protected_Event_Queue;
      Ev    : DHCP.Timing_Events.Mock.Mock_Event_Type;
   begin
      Queue.Insert (Event => Ev);
      Assert (Condition => Queue.Length = 1,
              Message   => "Insertion failed");

      Queue.Remove (Event => Ev);
      Assert (Condition => Queue.Length = 0,
              Message   => "Remove failed");

      --  Trying to remove an event again should not raise an exception.

      Queue.Remove (Event => Ev);
   end Queue_Event_Removal;

   -------------------------------------------------------------------------

   procedure Queue_Event_Time_Changed
   is
      Queue    : DHCP.Timing_Events.Queues.Protected_Event_Queue;
      Ev       : DHCP.Timing_Events.Mock.Mock_Event_Type;
      Now      : constant Ada.Real_Time.Time := Ada.Real_Time.Clock;
      Shutdown : Boolean                     := False;
      Ev_Delay : Duration;
   begin
      Ev.Set_Time (At_Time => Now);
      Assert (Condition => not Queue.Has_Runnable_Event,
              Message   => "Empty queue has runnable event");

      Queue.Insert (Event => Ev);
      Assert (Condition => Queue.Has_Runnable_Event,
              Message   => "No runnable event after insert");

      select
         delay 2.0;
         Fail (Message => "Blocked on event time changed entry1");
      then abort
         Queue.Event_Time_Changed (New_Delay => Ev_Delay,
                                   Stop      => Shutdown);
      end select;
      Assert (Condition => Ev_Delay = 0.0,
              Message   => "Event delay mismatch");
      Assert (Condition => not Shutdown,
              Message   => "Shutdown is true");

      Queue.Clear;
      select
         delay 2.0;
         Fail (Message => "Blocked on event time changed entry2");
      then abort
         Queue.Event_Time_Changed (New_Delay => Ev_Delay,
                                   Stop      => Shutdown);
      end select;
      Assert (Condition => Ev_Delay = Duration'Last,
              Message   => "Event delay not last");
      Assert (Condition => not Shutdown,
              Message   => "Shutdown is true");

      Queue.Signal_Stop;
      select
         delay 2.0;
         Fail (Message => "Blocked on event time changed entry3");
      then abort
         Queue.Event_Time_Changed (New_Delay => Ev_Delay,
                                   Stop      => Shutdown);
      end select;

      Assert (Condition => Shutdown,
              Message   => "Shutdown is false");
   end Queue_Event_Time_Changed;

   -------------------------------------------------------------------------

   procedure Queue_Next_Event_Getter
   is
      use type Ada.Real_Time.Time;
      use DHCP.Timing_Events;

      T1    : constant Ada.Real_Time.Time := Ada.Real_Time.Clock;
      T2    : constant Ada.Real_Time.Time
        := T1 - Ada.Real_Time.Seconds (S => 3);
      Queue : DHCP.Timing_Events.Queues.Protected_Event_Queue;
      Ev1   : DHCP.Timing_Events.Mock.Mock_Event_Type;
      Ev2   : DHCP.Timing_Events.Mock.Mock_Event_Type;
   begin
      Ev1.Set_Time (At_Time => T1);
      Ev2.Set_Time (At_Time => T2);

      begin
         declare
            E : DHCP.Timing_Events.Event_Type'Class := Queue.Get_Next_Event;

            pragma Unreferenced (E);
         begin
            Fail (Message => "Expected no events exception");
         end;

      exception
         when DHCP.Timing_Events.Queues.No_Events => null;
      end;

      Queue.Insert (Event => Ev1);
      Assert (Condition => Queue.Get_Next_Event = Event_Type'Class (Ev1),
              Message   => "Next event not Ev1");
      Queue.Insert (Event => Ev2);
      Assert (Condition => Queue.Get_Next_Event = Event_Type'Class (Ev2),
              Message   => "Next event not Ev2");
      Queue.Remove (Event => Ev2);
      Assert (Condition => Queue.Get_Next_Event = Event_Type'Class (Ev1),
              Message   => "Next event not E1 after remove");
   end Queue_Next_Event_Getter;

   -------------------------------------------------------------------------

   procedure Queue_Next_Time_Getter
   is
      use type Ada.Real_Time.Time;

      T1    : constant Ada.Real_Time.Time := Ada.Real_Time.Clock;
      T2    : constant Ada.Real_Time.Time
        := T1 - Ada.Real_Time.Seconds (S => 3);
      Queue : DHCP.Timing_Events.Queues.Protected_Event_Queue;
      Ev1   : DHCP.Timing_Events.Mock.Mock_Event_Type;
      Ev2   : DHCP.Timing_Events.Mock.Mock_Event_Type;
   begin
      Ev1.Set_Time (At_Time => T1);
      Ev2.Set_Time (At_Time => T2);

      Assert (Condition => Queue.Next_Event_Time = Ada.Real_Time.Time_Last,
              Message   => "Default next event time not Time_Last");

      Queue.Insert (Event => Ev1);
      Assert (Condition => Queue.Next_Event_Time = T1,
              Message   => "Next event time not T1");
      Queue.Insert (Event => Ev2);
      Assert (Condition => Queue.Next_Event_Time = T2,
              Message   => "Next event time not T2");
      Queue.Remove (Event => Ev2);
      Assert (Condition => Queue.Next_Event_Time = T1,
              Message   => "Next event time not reset after remove");

      Queue.Remove (Event => Ev1);
      Assert (Condition => Queue.Next_Event_Time = Ada.Real_Time.Time_Last,
              Message   => "Next event time not reset after delete");

      Queue.Insert (Event => Ev1);
      Queue.Insert (Event => Ev2);
      Queue.Clear;
      Assert (Condition => Queue.Next_Event_Time = Ada.Real_Time.Time_Last,
              Message   => "Next event time not reset after clear");
   end Queue_Next_Time_Getter;

   -------------------------------------------------------------------------

   procedure Queue_Reset
   is
      Queue : DHCP.Timing_Events.Queues.Protected_Event_Queue;
      Ev1   : DHCP.Timing_Events.Mock.Mock_Event_Type;
      Ev2   : DHCP.Timing_Events.Mock.Mock_Event_Type;
   begin
      Queue.Insert (Event => Ev1);
      Queue.Insert (Event => Ev2);
      Queue.Reset;
      Assert (Condition => Queue.Length = 0,
              Message   => "Queue not empty after reset");
   end Queue_Reset;

end DHCP_Timing_Events_Tests;
