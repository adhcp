--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCPv4.Transactions.Mock is

   -------------------------------------------------------------------------

   function Get_ID
     (Transaction : Transaction_Type)
      return Transaction_ID_Type
   is
   begin
      return Transaction.ID;
   end Get_ID;

   -------------------------------------------------------------------------

   function Get_Server_Address
     (Transaction : Transaction_Type)
      return Anet.IPv4_Addr_Type
   is
   begin
      return Transaction.Cur_Server_Addr;
   end Get_Server_Address;

   -------------------------------------------------------------------------

   function Get_Start_Time
     (Transaction : Transaction_Type)
      return Ada.Real_Time.Time
   is
   begin
      return Transaction.Start_Time;
   end Get_Start_Time;

   -------------------------------------------------------------------------

   procedure Process_Bind_Timeout (Transaction : in out Transaction_Type)
   is
   begin
      Transaction.Timeout_Count := Transaction.Timeout_Count + 1;
   end Process_Bind_Timeout;

   -------------------------------------------------------------------------

   procedure Process_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type)
   is
   begin
      Transaction.Last_Proc_Msg  := Msg;
      Transaction.Proc_Msg_Count := Transaction.Proc_Msg_Count + 1;
   end Process_Message;

   -------------------------------------------------------------------------

   procedure Process_Timer_Expiry
     (Transaction : in out Transaction_Type;
      Timer_Kind  :        DHCP.Types.DHCP_Timer_Kind)
   is
      pragma Unreferenced (Timer_Kind);
   begin
      Transaction.Tim_Exp_Count := Transaction.Tim_Exp_Count + 1;
   end Process_Timer_Expiry;

   -------------------------------------------------------------------------

   procedure Reset (Transaction : in out Transaction_Type)
   is
   begin
      Transaction.Reset_Count := Transaction.Reset_Count + 1;
   end Reset;

   -------------------------------------------------------------------------

   procedure Reset_Retry_Delay (Transaction : in out Transaction_Type)
   is
   begin
      Transaction.Retry_Reset_Count := Transaction.Retry_Reset_Count + 1;
   end Reset_Retry_Delay;

   -------------------------------------------------------------------------

   procedure Send_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type;
      Dst         :        Anet.IPv4_Addr_Type)
   is
   begin
      Transaction.Send_Count    := Transaction.Send_Count + 1;
      Transaction.Last_Sent_Msg := Msg;
      Transaction.Last_Dst      := Dst;
   end Send_Message;

   -------------------------------------------------------------------------

   procedure Set_ID
     (Transaction : in out Transaction_Type;
      XID         :        Transaction_ID_Type)
   is
   begin
      Transaction.ID := XID;
   end Set_ID;

   -------------------------------------------------------------------------

   procedure Set_Server_Address
     (Transaction : in out Transaction_Type;
      Address     :        Anet.IPv4_Addr_Type)
   is
   begin
      Transaction.Cur_Server_Addr := Address;
   end Set_Server_Address;

   -------------------------------------------------------------------------

   procedure Set_Start_Time
     (Transaction : in out Transaction_Type;
      Time     :        Ada.Real_Time.Time := Ada.Real_Time.Clock)
   is
   begin
      Transaction.Start_Time := Time;
   end Set_Start_Time;

   -------------------------------------------------------------------------

   procedure Start (Transaction : in out Transaction_Type)
   is
   begin
      Transaction.Start_Time  := Ada.Real_Time.Clock;
      Transaction.Start_Count := Transaction.Start_Count + 1;
   end Start;

end DHCPv4.Transactions.Mock;
