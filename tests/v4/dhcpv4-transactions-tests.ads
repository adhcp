--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ahven.Framework;

package DHCPv4.Transactions.Tests is

   type Testcase is new Ahven.Framework.Test_Case with null record;

   procedure Initialize (T : in out Testcase);
   --  Initialize testcase.

   procedure Set_Get_Transaction_ID;
   --  Test XID setter and getter.

   procedure Set_Get_Server_Addr;
   --  Test server address setter and getter.

   procedure Reset_Retry_Delay;
   --  Test retry delay reset.

   procedure Increase_Retry_Delay;
   --  Test retry delay increase.

   procedure Prepare_Message;
   --  Test message preparation.

   procedure Set_Transaction_State;
   --  Test transaction state setter.

   procedure Start_Transaction;
   --  Test transaction start.

   procedure Process_Message;
   --  Test message processing.

   procedure Process_Timer_Expiry;
   --  Test timer expiry processing.

   procedure Send_Message;
   --  Test transaction message sending.

   procedure Set_Get_Start_Time;
   --  Test transaction start time setter and getter.

   procedure Timeout_Processing;
   --  Test transaction bind timeout processing.

   procedure Reset_Transaction;
   --  Test transaction resetting.

   procedure Set_Exp_Backoff;
   --  Test setter for exponential backoff strategy.

end DHCPv4.Transactions.Tests;
