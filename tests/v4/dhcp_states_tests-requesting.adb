--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;
with Ada.Streams;
with Ada.Strings.Unbounded;
with Ada.Tags;

with Anet.ARP;

with DHCP.Net;
with DHCP.States;
with DHCP.Timer.Test;
with DHCP.Notify.Mock;
with DHCP.Timing_Events.Mock;
with DHCP.Timing_Events.Start;

with DHCPv4.ACD.Mock;
with DHCPv4.Options;
with DHCPv4.Message;
with DHCPv4.Transactions.Mock;
with DHCPv4.States.Requesting;
with DHCPv4.States.Init;
with DHCPv4.States.Bound;
with DHCPv4.Database;

package body DHCP_States_Tests.Requesting is

   use Ahven;
   use DHCPv4;
   use type DHCP.States.State_Handle;

   -------------------------------------------------------------------------

   procedure Infinity_Lease_Times
   is
      use Ada.Real_Time;

      T      : Transactions.Mock.Transaction_Type;
      Msg    : Message.Message_Type := Message.Create (Kind => Message.Offer);
      Srv_ID : constant Options.Inst.Option_Type'Class
        := Options.Inst.Create
          (Name => Options.DHCP_Server_Id,
           Data => (127, 0, 0, 1));

      Dummy_T : DHCP.Timer.Test.Cleaner_Type;
   begin
      T.ID := 123;
      Msg.Set_Your_IP (IP => (192, 168, 0, 100));
      Msg.Add_Option (Opt => Srv_ID);
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Address_Time,
                       Data => (255, 255, 255, 255)));
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Rebinding_Time,
                       Data => (255, 255, 255, 255)));
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Renewal_Time,
                       Data => (255, 255, 255, 255)));

      States.State_Type'Class (States.Requesting.State.all).Process_Ack
        (Transaction => T,
         Msg         => Msg);

      declare
         Ev  : constant DHCP.Timing_Events.Event_Type'Class
           := DHCP.Timer.Test.Get_Next_Event;
         Dur : constant Duration
           := Ada.Real_Time.To_Duration (TS => Ev.Get_Time - T.Get_Start_Time);
      begin
         Assert (Condition => Dur = 4294967295.0,
                 Message   => "T1 not 2^32-1");
         DHCP.Timer.Cancel (Event => Ev);
      end;

      declare
         Ev  : constant DHCP.Timing_Events.Event_Type'Class
           := DHCP.Timer.Test.Get_Next_Event;
         Dur : constant Duration
           := Ada.Real_Time.To_Duration (TS => Ev.Get_Time - T.Get_Start_Time);
      begin
         Assert (Condition => Dur = 4294967295.0,
                 Message   => "T2 not 2^32-1");
         DHCP.Timer.Cancel (Event => Ev);
      end;

      declare
         Ev  : constant DHCP.Timing_Events.Event_Type'Class
           := DHCP.Timer.Test.Get_Next_Event;
         Dur : constant Duration
           := Ada.Real_Time.To_Duration (TS => Ev.Get_Time - T.Get_Start_Time);
      begin
         Assert (Condition => Dur = 4294967295.0,
                 Message   => "LEASE_EXPIRY not 2^32-1");
         DHCP.Timer.Cancel (Event => Ev);
      end;
   end Infinity_Lease_Times;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DHCP client state 'Requesting'");
      T.Add_Test_Routine
        (Routine => Verify_Name_Getter'Access,
         Name    => "Name getter");
      T.Add_Test_Routine
        (Routine => Verify_Process_Ack'Access,
         Name    => "ACK handling");
      T.Add_Test_Routine
        (Routine => Verify_Process_Ack_ACD'Access,
         Name    => "ACK handling with ACD enabled");
      T.Add_Test_Routine
        (Routine => Verify_Process_Ack_Collision'Access,
         Name    => "ACK handling (Collision)");
      T.Add_Test_Routine
        (Routine => Verify_Process_Nack'Access,
         Name    => "NACK handling");
      T.Add_Test_Routine
        (Routine => Invalid_Lease_Times'Access,
         Name    => "Invalid lease times");
      T.Add_Test_Routine
        (Routine => Infinity_Lease_Times'Access,
         Name    => "Infinity lease times");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Invalid_Lease_Times
   is
      T      : Transactions.Mock.Transaction_Type;
      Msg    : Message.Message_Type         := Message.Create
        (Kind => Message.Offer);
      Srv_ID : constant Options.Inst.Option_Type'Class
        := Options.Inst.Create
          (Name => Options.DHCP_Server_Id,
           Data => (127, 0, 0, 1));
   begin
      T.ID := 123;
      Msg.Set_Your_IP (IP => (192, 168, 0, 100));
      Msg.Add_Option (Opt => Srv_ID);
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Address_Time,
                       Data => (0, 0, 0, 60)));
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Rebinding_Time,
                       Data => (0, 0, 0, 80)));
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Renewal_Time,
                       Data => (0, 0, 0, 120)));

      States.State_Type'Class (States.Requesting.State.all).Process_Ack
        (Transaction => T,
         Msg         => Msg);
      Fail (Message => "Expected invalid lease time exception");

   exception
      when States.Invalid_Lease_Time => null;
   end Invalid_Lease_Times;

   -------------------------------------------------------------------------

   procedure Verify_Name_Getter
   is
   begin
      Assert (Condition => States.Requesting.State.Get_Name = "Requesting",
              Message   => "Name mismatch");
   end Verify_Name_Getter;

   -------------------------------------------------------------------------

   procedure Verify_Process_Ack
   is
      use Ada.Real_Time;
      use type Anet.IPv4_Addr_Type;
      use type DHCP.Notify.Reason_Type;

      Ev     : DHCP.Timing_Events.Mock.Mock_Event_Type;
      T      : Transactions.Mock.Transaction_Type;
      Msg    : Message.Message_Type         := Message.Create
        (Kind => Message.Offer);
      Y_IP   : constant Anet.IPv4_Addr_Type := (192, 168, 0, 100);
      Srv_ID : constant Options.Inst.Option_Type'Class
        := Options.Inst.Create
          (Name => Options.DHCP_Server_Id,
           Data => (127, 0, 0, 1));

      Dummy_N : DHCP.Notify.Mock.Notify_Cleaner;
      Dummy_T : DHCP.Timer.Test.Cleaner_Type;
   begin
      DHCP.Notify.Mock.Install;

      T.ID := 123;
      Ev.Set_Time (At_Time => Ada.Real_Time.Time_Last);
      DHCP.Timer.Schedule (Event => Ev);
      Msg.Set_Your_IP (IP => Y_IP);
      Msg.Add_Option (Opt => Srv_ID);
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Address_Time,
                       Data => (0, 0, 0, 120)));
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Rebinding_Time,
                       Data => (0, 0, 0, 80)));
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Renewal_Time,
                       Data => (0, 0, 0, 60)));

      States.State_Type'Class (States.Requesting.State.all).Process_Ack
        (Transaction => T,
         Msg         => Msg);

      Assert (Condition => DHCP.Timer.Event_Count = 3,
              Message   => "Event count not 3");
      Assert (Condition => T.Retry_Reset_Count = 1,
              Message   => "Retry reset count not 1");
      Assert (Condition => T.Cur_Server_Addr = Anet.Loopback_Addr_V4,
              Message   => "Server address mismatch");

      declare
         Ev : constant DHCP.Timing_Events.Event_Type'Class
           := DHCP.Timer.Test.Get_Next_Event;
      begin
         Assert (Condition => Ev.Get_Time = T.Get_Start_Time
                 + Seconds (S => 60),
                 Message   => "T1 not 60");
         DHCP.Timer.Cancel (Event => Ev);
      end;

      declare
         Ev : constant DHCP.Timing_Events.Event_Type'Class
           := DHCP.Timer.Test.Get_Next_Event;
      begin
         Assert (Condition => Ev.Get_Time = T.Get_Start_Time
                 + Seconds (S => 80),
                 Message   => "T2 not 80");
         DHCP.Timer.Cancel (Event => Ev);
      end;

      declare
         Ev : constant DHCP.Timing_Events.Event_Type'Class
           := DHCP.Timer.Test.Get_Next_Event;
      begin
         Assert (Condition => Ev.Get_Time = T.Get_Start_Time
                 + Seconds (S => 120),
                 Message   => "LEASE_EXPIRY not 120");
         DHCP.Timer.Cancel (Event => Ev);
      end;

      Assert (Condition => T.State = States.Bound.State,
              Message   => "New state not 'Bound'");
      Assert (Condition => DHCP.Notify.Mock.Get_Last_Reason
              = DHCP.Notify.Bound,
              Message   => "Notify reason not Bound");
   end Verify_Process_Ack;

   -------------------------------------------------------------------------

   procedure Verify_Process_Ack_ACD
   is
      use Ada.Strings.Unbounded;
      use Ada.Real_Time;
      use type Ada.Streams.Stream_Element_Array;
      use type Anet.IPv4_Addr_Type;

      Ev       : DHCP.Timing_Events.Mock.Mock_Event_Type;
      T        : Transactions.Mock.Transaction_Type;
      Msg      : Message.Message_Type         := Message.Create
        (Kind => Message.Offer);
      Y_IP     : constant Anet.IPv4_Addr_Type := (192, 168, 0, 100);
      Srv_ID   : constant Options.Inst.Option_Type'Class
        := Options.Inst.Create
          (Name => Options.DHCP_Server_Id,
           Data => (127, 0, 0, 1));
      HW_Addr  : constant Anet.Ether_Addr_Type
        := (16#ca#, 16#fe#, 16#be#, 16#ef#, 16#ca#, 16#fe#);
      Ref_Data : constant Anet.ARP.Header_Type
        := (Operation => Anet.ARP.ARP_Request,
            Src_Ether => HW_Addr,
            Src_IP    => Y_IP,
            Dst_Ether => (others => 0),
            Dst_IP    => Y_IP);

      Dummy_N : DHCP.Notify.Mock.Notify_Cleaner;
      Dummy_T : DHCP.Timer.Test.Cleaner_Type;
      Dummy_A : ACD.Mock.Sock_Cleaner;
   begin
      DHCP.Notify.Mock.Install;

      ACD.Initialize (Iface => (Addrtype => DHCP.Net.Link_Layer,
                                Name     => To_Unbounded_String ("eth0"),
                                Mac_Addr => HW_Addr));

      T.ID := 123;
      Ev.Set_Time (At_Time => Ada.Real_Time.Time_Last);
      DHCP.Timer.Schedule (Event => Ev);
      Msg.Set_Your_IP (IP => Y_IP);
      Msg.Add_Option (Opt => Srv_ID);
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Address_Time,
                       Data => (0, 0, 0, 120)));
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Rebinding_Time,
                       Data => (0, 0, 0, 80)));
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Renewal_Time,
                       Data => (0, 0, 0, 60)));

      States.State_Type'Class (States.Requesting.State.all).Process_Ack
        (Transaction => T,
         Msg         => Msg);

      Assert (Condition => DHCP.Timer.Event_Count = 4,
              Message   => "Event count not 4");
      Assert (Condition => T.Retry_Reset_Count = 1,
              Message   => "Retry reset count not 1");
      Assert (Condition => T.Cur_Server_Addr = Anet.Loopback_Addr_V4,
              Message   => "Server address mismatch");
      Assert (Condition => ACD.Mock.Send_Count = 4,
              Message   => "ARP message send count mismatch");
      Assert (Condition => Anet.ARP.To_Stream (Header => Ref_Data)
              = ACD.Mock.Last_Item
                (ACD.Mock.Last_Item'First .. ACD.Mock.Last_Idx),
              Message   => "Announce data mismatch");
   end Verify_Process_Ack_ACD;

   -------------------------------------------------------------------------

   procedure Verify_Process_Ack_Collision
   is
      use Ada.Real_Time;
      use Ada.Strings.Unbounded;
      use type Options.Inst.Option_Type'Class;
      use type Message.Message_Kind;
      use type Ada.Tags.Tag;
      use type Anet.IPv4_Addr_Type;

      Ev      : DHCP.Timing_Events.Mock.Mock_Event_Type;
      T       : Transactions.Mock.Transaction_Type;
      Msg     : Message.Message_Type         := Message.Create
        (Kind => Message.Offer);
      Y_IP    : constant Anet.IPv4_Addr_Type := (192, 168, 0, 100);
      Srv_IP  : constant Anet.IPv4_Addr_Type := (127, 0, 0, 1);
      Srv_ID  : constant Options.Inst.Option_Type'Class
        := Options.Inst.Create
          (Name => Options.DHCP_Server_Id,
           Data => (127, 0, 0, 1));
      IP_Opt  : constant  Options.Inst.Option_Type'Class
        := Options.Inst4.Create
          (Name    => Options.Address_Request,
           Address => Y_IP);

      HW_Addr : constant Anet.Ether_Addr_Type
        := (16#ca#, 16#fe#, 16#be#, 16#ef#, 16#ca#, 16#fe#);

      Collision_Pkt : constant Anet.ARP.Header_Type
        := (Operation => Anet.ARP.ARP_Reply,
            Src_Ether => (16#be#, 16#ef#, 16#be#, 16#ef#, 16#be#, 16#ef#),
            Src_IP    => Y_IP,
            Dst_Ether => HW_Addr,
            Dst_IP    => Anet.Any_Addr);

      Packet : DHCPv4.ACD.Mock.Packet_Type;
      Buf    : constant Ada.Streams.Stream_Element_Array
        := Anet.ARP.To_Stream (Header => Collision_Pkt);

      Dummy_N : DHCP.Notify.Mock.Notify_Cleaner;
      Dummy_T : DHCP.Timer.Test.Cleaner_Type;
      Dummy_A : ACD.Mock.Sock_Cleaner;
   begin
      DHCP.Notify.Mock.Install;

      ACD.Initialize (Iface => (Addrtype => DHCP.Net.Link_Layer,
                                Name     => To_Unbounded_String ("eth0"),
                                Mac_Addr => HW_Addr));
      Packet.Data (Buf'Range) := Buf;
      Packet.Last_Idx         := Buf'Last;
      ACD.Mock.Enqueue (Packet => Packet);

      T.ID := 3201;
      Ev.Set_Time (At_Time => Ada.Real_Time.Time_Last);
      DHCP.Timer.Schedule (Event => Ev);
      Msg.Set_Your_IP (IP => Y_IP);
      Msg.Add_Option (Opt => Srv_ID);
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Address_Time,
                       Data => (0, 0, 0, 120)));
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Rebinding_Time,
                       Data => (0, 0, 0, 80)));
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Renewal_Time,
                       Data => (0, 0, 0, 60)));

      States.State_Type'Class (States.Requesting.State.all).Process_Ack
        (Transaction => T,
         Msg         => Msg);

      --  Check sent DECLINE message.

      Assert (Condition => T.Last_Sent_Msg.Get_Kind = Message.Decline,
              Message   => "Transaction last msg not decline");
      Assert (Condition => T.Last_Dst = Anet.Bcast_Addr,
              Message   => "Decline message not broadcasted");
      Assert (Condition => T.Last_Sent_Msg.Get_Options.Get
              (Name => Options.Address_Request) = IP_Opt,
              Message   => "Declined IP address mismatch");
      Assert (Condition => T.Last_Sent_Msg.Get_Server_ID = Srv_IP,
              Message   => "Declined server IP address mismatch");

      Assert (Condition => T.Reset_Count = 1,
              Message   => "Reset count not 1");
      Assert (Condition => T.State = States.Init.State,
              Message   => "New state not 'Init'");
      Assert (Condition => T.Start_Count = 0,
              Message   => "Start count not 0");
      Assert (Condition => DHCP.Timer.Event_Count = 1,
              Message   => "Event count not 1");
      Assert (Condition => DHCP.Timer.Test.Get_Next_Event'Tag =
                DHCP.Timing_Events.Start.Start_Type'Tag,
              Message   => "Event not start type");
      Assert (Condition => Database.Is_Clear,
              Message   => "Database not clear");
   end Verify_Process_Ack_Collision;

   -------------------------------------------------------------------------

   procedure Verify_Process_Nack
   is
      use type Ada.Tags.Tag;

      T   : aliased Transactions.Mock.Transaction_Type;
      Msg : Message.Message_Type;
      Ev  : DHCP.Timing_Events.Mock.Mock_Event_Type;
   begin
      Database.Set_Fixed_Address (Addr => (192, 168, 12, 1));
      Ev.Set_Time (At_Time => Ada.Real_Time.Time_Last);
      DHCP.Timer.Schedule (Event => Ev);
      States.State_Type'Class (States.Requesting.State.all).Process_Nack
        (Transaction => T,
         Msg         => Msg);

      Assert (Condition => T.Reset_Count = 1,
              Message   => "Reset count not 1");
      Assert (Condition => T.State = States.Init.State,
              Message   => "New state not 'Init'");
      Assert (Condition => T.Start_Count = 0,
              Message   => "Start count not 0");
      Assert (Condition => DHCP.Timer.Event_Count = 1,
              Message   => "Event count not 1");
      Assert (Condition => DHCP.Timer.Test.Get_Next_Event'Tag =
                DHCP.Timing_Events.Start.Start_Type'Tag,
              Message   => "Event not start type");
      Assert (Condition => Database.Is_Clear,
              Message   => "Database not clear");
   end Verify_Process_Nack;

end DHCP_States_Tests.Requesting;
