--
--  Copyright (C) 2014, 2015 secunet Security Networks AG
--  Copyright (C) 2014, 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2014, 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet.Util;

with DHCP.OS;
with DHCP.Notify;

with DHCPv4.Options;
with DHCPv4.Notify_Simple;
with DHCPv4.Database.Mock;

with Test_Utils;

package body DHCP_Notify_Simple_Tests
is

   use Ahven;
   use DHCP;
   use DHCPv4;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for simple notify package");
      T.Add_Test_Routine
        (Routine => Write_Lease'Access,
         Name    => "Write lease");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Write_Lease
   is
      Dummy : DHCPv4.Database.Mock.Cleaner;
      F     : constant String := "/tmp/test.simplelease-"
        & Anet.Util.Random_String (Len => 8);
      Opts  : Options.Inst4.Option_List;
   begin
      begin
         Notify_Simple.Write_Lease_File
           (State    => Notify.Preinit,
            Iface    => "lo",
            Filename => "./");
         Fail (Message => "Exception expected");

      exception
         when Notify_Simple.Lease_Error => null;
      end;

      Notify_Simple.Write_Lease_File
        (State    => Notify.Preinit,
         Iface    => "lo",
         Filename => F);
      Assert (Condition => Test_Utils.Equal_Files
              (Filename1 => "data/simple.lease.preinit",
               Filename2 => F),
              Message   => "Lease file incorrect (1)");
      OS.Delete_File (Filename => F);

      Database.Set_Fixed_Address (Addr => (192, 168, 231, 199));

      Opts.Append (New_Item => Options.Inst.Create
                   (Name => Options.NETBIOS_Name_Servers,
                    Data => (192, 168, 231, 10)));
      Opts.Append (New_Item => Options.Inst.Create
                   (Name => Options.Routers,
                    Data => (192, 168, 231, 1, 192, 168, 231, 254)));
      Database.Set_Options (Opts => Opts);

      Notify_Simple.Write_Lease_File
        (State    => Notify.Bound,
         Iface    => "lo",
         Filename => F);
      Assert (Condition => Test_Utils.Equal_Files
              (Filename1 => "data/simple.lease",
               Filename2 => F),
              Message   => "Lease file incorrect (2)");
      OS.Delete_File (Filename => F);
   end Write_Lease;

end DHCP_Notify_Simple_Tests;
