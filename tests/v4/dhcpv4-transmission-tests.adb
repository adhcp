--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;
with Ada.Strings.Unbounded;

with DHCPv4.Message;
with DHCPv4.Transmission.Mock;

package body DHCPv4.Transmission.Tests
is

   use Ada.Strings.Unbounded;
   use Ahven;

   -------------------------------------------------------------------------

   procedure Get_Iface_Mac
   is
      use type Anet.Hardware_Addr_Type;

      Ref_Mac : constant Anet.Hardware_Addr_Type
        := (16#ca#, 16#fe#, 16#be#, 16#ef#, 16#ca#, 16#fe#);
   begin
      Iface_Info.Mac_Addr := Ref_Mac;
      Assert (Condition => Get_Iface_Mac = Ref_Mac,
              Message   => "Interface MAC mismatch");
   end Get_Iface_Mac;

   -------------------------------------------------------------------------

   procedure Get_Iface_Name
   is
      use type Anet.Types.Iface_Name_Type;
   begin
      Assert (Condition => Get_Iface_Name = "",
              Message   => "Default interface name mismatch");

      Iface_Info.Name := To_Unbounded_String ("eth2");
      Assert (Condition => Get_Iface_Name = "eth2",
              Message   => "Interface name mismatch");
   end Get_Iface_Name;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Transmission package");
      T.Add_Test_Routine
        (Routine => Send'Access,
         Name    => "Send data");
      T.Add_Test_Routine
        (Routine => Get_Iface_Name'Access,
         Name    => "Get interface name");
      T.Add_Test_Routine
        (Routine => Get_Iface_Mac'Access,
         Name    => "Get interface MAC address");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Send
   is
      use type Ada.Streams.Stream_Element_Array;
      use type Anet.IPv4_Addr_Type;
      use type Anet.Double_Byte;

      Msg      : constant DHCPv4.Message.Message_Type
        := DHCPv4.Message.Create (Kind => DHCPv4.Message.Discover);
      Ref_Data : constant Ada.Streams.Stream_Element_Array
        := Msg.Serialize;

      procedure Send_Unicast;
      procedure Send_Broadcast;

      ----------------------------------------------------------------------

      procedure Send_Broadcast
      is
         use type Ada.Streams.Stream_Element_Offset;
         use type Anet.Hardware_Addr_Type;

         Dummy : Mock.Sock_Cleaner;

         Hdr_Size  : constant := 28;
         Ref_Iface : constant Unbounded_String := To_Unbounded_String ("eth1");
      begin
         Iface_Info.Name := Ref_Iface;

         Send (Message     => Msg,
               Destination => Anet.Bcast_Addr);
         Assert (Condition => Mock.Send_Count = 1,
                 Message   => "Send count mismatch (2)");
         Assert (Condition => Mock.Last_Dst_IP = Anet.Bcast_Addr,
                 Message   => "Send IP mismatch (2)");
         Assert (Condition => Mock.Last_Item
                 (Mock.Last_Item'First + Hdr_Size .. Mock.Last_Idx) = Ref_Data,
                 Message   => "Send data mismatch (2)");
         Assert (Condition => Mock.Last_Dst_HW = Anet.Bcast_HW_Addr,
                 Message   => "Send hw address mismatch");
         Assert (Condition => Mock.Last_Iface = Ref_Iface,
                 Message   => "Send interface name mismatch");
      end Send_Broadcast;

      ----------------------------------------------------------------------

      procedure Send_Unicast
      is
         Dummy : Mock.Sock_Cleaner;
      begin
         Send (Message     => Msg,
               Destination => Anet.Loopback_Addr_V4);
         Assert (Condition => Mock.Send_Count = 1,
                 Message   => "Send count mismatch (1)");
         Assert (Condition => Mock.Last_Dst_IP = Anet.Loopback_Addr_V4,
                 Message   => "Send IP mismatch (1)");
         Assert (Condition => Mock.Last_Dst_Port = Bootp_Server_Port,
                 Message   => "Send port mismatch (1)");
         Assert (Condition => Mock.Last_Item
                 (Mock.Last_Item'First .. Mock.Last_Idx) = Ref_Data,
                 Message   => "Send data mismatch (1)");
      end Send_Unicast;
   begin
      Send_Unicast;
      Send_Broadcast;
   end Send;

end DHCPv4.Transmission.Tests;
