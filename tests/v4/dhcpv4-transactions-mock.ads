--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

with DHCP.Types;

with DHCPv4.States.Mock;

package DHCPv4.Transactions.Mock is

   ----------------------
   -- Test transaction --
   ----------------------

   type Transaction_Type is new DHCPv4.States.Transaction_Type
     (Initial_State => States.Mock.State) with record
      ID                : Transaction_ID_Type  := 0;
      Last_Sent_Msg     : Message.Message_Type;
      Last_Proc_Msg     : Message.Message_Type;
      Last_Dst          : Anet.IPv4_Addr_Type  := Anet.Any_Addr;
      Cur_Server_Addr   : Anet.IPv4_Addr_Type  := Anet.Any_Addr;
      Send_Count        : Natural              := 0;
      Retry_Reset_Count : Natural              := 0;
      Reset_Count       : Natural              := 0;
      Start_Count       : Natural              := 0;
      Proc_Msg_Count    : Natural              := 0;
      Tim_Exp_Count     : Natural              := 0;
      Timeout_Count     : Natural              := 0;
      T1, T2, Exp_Dur   : Duration             := Duration'First;
      Start_Time        : Ada.Real_Time.Time   := Ada.Real_Time.Clock;
   end record;

   procedure Set_ID
     (Transaction : in out Transaction_Type;
      XID         :        Transaction_ID_Type);

   function Get_ID
     (Transaction : Transaction_Type)
      return Transaction_ID_Type;

   procedure Start (Transaction : in out Transaction_Type);

   procedure Prepare_Message
     (Transaction :        Transaction_Type;
      Msg         : in out Message.Message_Type) is null;

   procedure Process_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type);

   procedure Process_Timer_Expiry
     (Transaction : in out Transaction_Type;
      Timer_Kind  :        DHCP.Types.DHCP_Timer_Kind);

   procedure Process_Bind_Timeout (Transaction : in out Transaction_Type);

   procedure Send_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type;
      Dst         :        Anet.IPv4_Addr_Type);

   function Get_Server_Address
     (Transaction : Transaction_Type)
      return Anet.IPv4_Addr_Type;

   procedure Set_Server_Address
     (Transaction : in out Transaction_Type;
      Address     :        Anet.IPv4_Addr_Type);

   procedure Increase_Retry_Delay (Transaction : in out Transaction_Type)
   is null;

   procedure Reset_Retry_Delay (Transaction : in out Transaction_Type);

   function Get_Start_Time
     (Transaction : Transaction_Type)
      return Ada.Real_Time.Time;

   procedure Set_Start_Time
     (Transaction : in out Transaction_Type;
      Time        :        Ada.Real_Time.Time := Ada.Real_Time.Clock);

   procedure Reset (Transaction : in out Transaction_Type);

end DHCPv4.Transactions.Mock;
