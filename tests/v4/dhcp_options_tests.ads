--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ahven.Framework;

package DHCP_Options_Tests is

   type Testcase is new Ahven.Framework.Test_Case with null record;

   procedure Initialize (T : in out Testcase);
   --  Initialize testcase.

   procedure Create_Options;
   --  Create DHCP options.

   procedure Compare_Options;
   --  Compare DHCP options.

   procedure Append_Options_To_List;
   --  Append options to option list.

   procedure List_Option_Data_Size;
   --  Get data size from option list.

   procedure Deserialize_Options_From_Stream;
   --  Check DHCP option deserialization.

   procedure Deserialize_Option_Lists;
   --  Check DHCP option list deserialization.

   procedure Serialize_Options_To_Stream;
   --  Check DHCP option serialization.

   procedure Serialize_Option_Lists;
   --  Check DHCP option list serialization.

   procedure Get_Option_By_Name;
   --  Verify option getter function.

   procedure Get_Option_Data_As_String;
   --  Verify getter for option data string.

   procedure Get_Positive_Octet_Option_Data_As_String;
   --  Verify getter for positive octet data string.

   procedure Get_IP_Option_Data_As_String;
   --  Verify getter for IP address string.

   procedure Get_IP_Option_Value;
   --  Verify getter for IP option address.

   procedure Get_IP_Array_Option_Data_As_String;
   --  Verify getter for array of IP address string.

   procedure Get_IP_Pairs_Option_Data_As_String;
   --  Verify getter for array of IP address pairs string.

   procedure Get_Uint16_Array_Option_Data_As_String;
   --  Verify getter for array of 16-bit unsigned integers data string.

   procedure Get_Uint16_Option_Data_As_String;
   --  Verify getter for unsigned 16-bit integer data string.

   procedure Get_Uint32_Option_Data_As_String;
   --  Verify getter for unsigned 32-bit integer data string.

   procedure Get_Bool_Option_Data_As_String;
   --  Verify getter for boolean data string.

   procedure Get_Raw_Option_Data_As_String;
   --  Verify getter for raw data string.

   procedure Create_Positive_Octet_Option;
   --  Verify positive octet option creation.

   procedure Create_String_Option;
   --  Verify string option creation.

   procedure Create_IP_Option;
   --  Verify IP option creation.

   procedure Create_IP_Array_Option;
   --  Verify IP array option creation.

   procedure Create_IP_Pairs_Option;
   --  Verify IP pairs option creation.

   procedure Create_Uint16_Array_Option;
   --  Verify array of unsigned 16-bit integers option creation.

   procedure Create_Uint16_Option;
   --  Verify unsigned 16-bit option creation.

   procedure Create_Uint32_Option;
   --  Verify unsigned 32-bit option creation.

   procedure Create_Bool_Option;
   --  Verify boolean option creation.

   procedure Create_Iface_MTU_Option;
   --  Verify interface MTU option creation.

   procedure Create_DHCP_Msg_Kind_Option;
   --  Verify DHCP message kind option creation.

   procedure Create_Raw_Option;
   --  Verify raw option creation.

   procedure List_Option_Contains;
   --  Verify contains operation of option list.

   procedure List_Option_Clear;
   --  Verify clear operation of option list.

   procedure List_Option_Remove;
   --  Verify remove operation of option list.

end DHCP_Options_Tests;
