--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Notify.Mock;

package body DHCP_Notify_Tests is

   use Ahven;
   use DHCP;
   use DHCP.Notify;

   Counter : Natural := 0;

   procedure Inc_Counter (R : Reason_Type);
   --  Increment counter.

   -------------------------------------------------------------------------

   procedure Handler_Registration
   is
      C : Mock.Notify_Cleaner;
      pragma Unreferenced (C);
   begin
      Assert (Condition => Handler_Count (Reason => Preinit) = 0,
              Message   => "Handler count for 'Preinit' not 0");
      Assert (Condition => Handler_Count (Reason => Bound) = 0,
              Message   => "Handler count for 'Bound' not 0");

      Notify.Register (Reasons => (Preinit, Bound),
                       Handler => Inc_Counter'Access);
      Assert (Condition => Handler_Count (Reason => Preinit) = 1,
              Message   => "Handler count for 'Preinit' not 1");
      Assert (Condition => Handler_Count (Reason => Bound) = 1,
              Message   => "Handler count for 'Bound' not 1");

      Notify.Register (Reasons => (1 => Preinit),
                       Handler => Inc_Counter'Access);
      Assert (Condition => Handler_Count (Reason => Preinit) = 2,
              Message   => "Handler count for 'Preinit' not 2");
      Assert (Condition => Handler_Count (Reason => Bound) = 1,
              Message   => "Handler count for 'Bound' not 1");

      Notify.Clear;
      Assert (Condition => Handler_Count (Reason => Preinit) = 0,
              Message   => "Handler count not 0");
   end Handler_Registration;

   -------------------------------------------------------------------------

   procedure Inc_Counter (R : Reason_Type)
   is
      pragma Unreferenced (R);
   begin
      Counter := Counter + 1;
   end Inc_Counter;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for notify package");
      T.Add_Test_Routine
        (Routine => Handler_Registration'Access,
         Name    => "Register handler");
      T.Add_Test_Routine
        (Routine => Notification_Update'Access,
         Name    => "Notification update");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Notification_Update
   is
      C : Mock.Notify_Cleaner;
      pragma Unreferenced (C);
   begin
      Counter := 0;

      Notify.Register (Reasons => (Preinit, Renew),
                       Handler => Inc_Counter'Access);

      Notify.Update (Reason => Preinit);
      Assert (Condition => Counter = 1,
              Message   => "Counter not 1");

      Notify.Update (Reason => Renew);
      Assert (Condition => Counter = 2,
              Message   => "Counter not 2");

      Notify.Update (Reason => Bound);
      Assert (Condition => Counter = 2,
              Message   => "Counter should still be 2");
   end Notification_Update;

end DHCP_Notify_Tests;
