--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ahven.Framework;

package DHCP_Message_Tests is

   type Testcase is new Ahven.Framework.Test_Case with null record;

   procedure Initialize (T : in out Testcase);
   --  Initialize testcase.

   procedure Raw_Validate_Operation_Code;
   --  Verify raw message validation: Op code.

   procedure Raw_Validate_HType;
   --  Verify raw message validation: Hardware type.

   procedure Raw_Validate_Hlen;
   --  Verify raw message validation: Hardware address length.

   procedure Raw_Validate_Hops_Count;
   --  Verify raw message validation: Hops count.

   procedure Deserialize_Msg_From_Stream;
   --  Check DHCP message deserialization.

   procedure Deserialize_Msg_Invalid_Buffer;
   --  Verify exception behavior for invalid data buffers.

   procedure Deserialize_Msg_Invalid_Options;
   --  Verify exception behavior for invalid options in message.

   procedure Serialize_Msg_To_Stream;
   --  Serialize DHCP message to stream element array.

   procedure Set_Get_Gateway_IP;
   --  Test gateway IP setter/getter.

   procedure Increment_Hop_Count;
   --  Test hop count incrementation.

   procedure Create_Message;
   --  Test message create function.

   procedure Set_Get_Broadcast_Flag;
   --  Test broadcast flag setter/getter.

   procedure Set_Get_Hardware_Address;
   --  Test hardware address setter/getter.

   procedure Set_Get_XID;
   --  Test XID setter/getter.

   procedure Add_Option_To_Msg;
   --  Verify adding of options.

   procedure Set_Get_Client_IP;
   --  Test client IP setter/getter.

   procedure Set_Get_Your_IP;
   --  Test 'your IP' setter/getter.

end DHCP_Message_Tests;
