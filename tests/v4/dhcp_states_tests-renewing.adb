--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

with Anet;

with DHCP.States;
with DHCP.Timer.Test;
with DHCP.Notify.Mock;
with DHCP.Timing_Events.Mock;

with DHCPv4.Options;
with DHCPv4.Message;
with DHCPv4.Transactions.Mock;
with DHCPv4.States.Renewing;
with DHCPv4.States.Rebinding;
with DHCPv4.States.Init;
with DHCPv4.States.Bound;
with DHCPv4.Database.Mock;

package body DHCP_States_Tests.Renewing is

   use Ahven;
   use DHCPv4;
   use type DHCP.States.State_Handle;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DHCP client state 'Renewing'");
      T.Add_Test_Routine
        (Routine => Verify_Name_Getter'Access,
         Name    => "Name getter");
      T.Add_Test_Routine
        (Routine => Verify_Process_Ack'Access,
         Name    => "ACK handling");
      T.Add_Test_Routine
        (Routine => Verify_Process_Nack'Access,
         Name    => "NACK handling");
      T.Add_Test_Routine
        (Routine => Verify_Process_T2_Expiry'Access,
         Name    => "T2 expiration handling");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Verify_Name_Getter
   is
   begin
      Assert (Condition => States.Renewing.State.Get_Name = "Renewing",
              Message   => "Name mismatch");
   end Verify_Name_Getter;

   -------------------------------------------------------------------------

   procedure Verify_Process_Ack
   is
      use Ada.Real_Time;
      use type Anet.IPv4_Addr_Type;
      use type DHCP.Notify.Reason_Type;

      Ev     : DHCP.Timing_Events.Mock.Mock_Event_Type;
      T      : Transactions.Mock.Transaction_Type;
      Msg    : Message.Message_Type         := Message.Create
        (Kind => Message.Offer);
      Y_IP   : constant Anet.IPv4_Addr_Type := (192, 168, 0, 100);
      Srv_ID : constant Options.Inst.Option_Type'Class
        := Options.Inst.Create
          (Name => Options.DHCP_Server_Id,
           Data => (127, 0, 0, 1));

      Dummy_N : DHCP.Notify.Mock.Notify_Cleaner;
      Dummy_T : DHCP.Timer.Test.Cleaner_Type;
   begin
      DHCP.Notify.Mock.Install;

      T.ID := 123;
      Ev.Set_Time (At_Time => Ada.Real_Time.Time_Last);
      DHCP.Timer.Schedule (Event => Ev);
      Msg.Set_Your_IP (IP => Y_IP);
      Msg.Add_Option (Opt => Srv_ID);
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Address_Time,
                       Data => (0, 0, 0, 120)));
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Rebinding_Time,
                       Data => (0, 0, 0, 80)));
      Msg.Add_Option (Opt => Options.Inst.Create
                      (Name => Options.Renewal_Time,
                       Data => (0, 0, 0, 60)));

      States.State_Type'Class (States.Renewing.State.all).Process_Ack
        (Transaction => T,
         Msg         => Msg);

      Assert (Condition => DHCP.Timer.Event_Count = 3,
              Message   => "Event count not 3");
      Assert (Condition => T.Retry_Reset_Count = 1,
              Message   => "Retry reset count not 1");
      Assert (Condition => T.Cur_Server_Addr = Anet.Loopback_Addr_V4,
              Message   => "Server address mismatch");

      declare
         Ev : constant DHCP.Timing_Events.Event_Type'Class
           := DHCP.Timer.Test.Get_Next_Event;
      begin
         Assert (Condition => Ev.Get_Time = T.Get_Start_Time
                 + Seconds (S => 60),
                 Message   => "T1 not 60");
         DHCP.Timer.Cancel (Event => Ev);
      end;

      declare
         Ev : constant DHCP.Timing_Events.Event_Type'Class
           := DHCP.Timer.Test.Get_Next_Event;
      begin
         Assert (Condition => Ev.Get_Time = T.Get_Start_Time
                 + Seconds (S => 80),
                 Message   => "T2 not 80");
         DHCP.Timer.Cancel (Event => Ev);
      end;

      declare
         Ev : constant DHCP.Timing_Events.Event_Type'Class
           := DHCP.Timer.Test.Get_Next_Event;
      begin
         Assert (Condition => Ev.Get_Time = T.Get_Start_Time
                 + Seconds (S => 120),
                 Message   => "LEASE_EXPIRY not 120");
         DHCP.Timer.Cancel (Event => Ev);
      end;

      Assert (Condition => T.State = States.Bound.State,
              Message   => "New state not 'Bound'");
      Assert (Condition => DHCP.Notify.Mock.Get_Last_Reason
              = DHCP.Notify.Renew,
              Message   => "Notify reason not Renew");
   end Verify_Process_Ack;

   -------------------------------------------------------------------------

   procedure Verify_Process_Nack
   is
      T   : Transactions.Mock.Transaction_Type;
      Msg : Message.Message_Type;
      Ev  : DHCP.Timing_Events.Mock.Mock_Event_Type;
   begin
      Ev.Set_Time (At_Time => Ada.Real_Time.Time_Last);
      DHCP.Timer.Schedule (Event => Ev);
      States.State_Type'Class (States.Renewing.State.all).Process_Nack
        (Transaction => T,
         Msg         => Msg);

      Assert (Condition => T.Reset_Count = 1,
              Message   => "Reset count not 1");
      Assert (Condition => T.State = States.Init.State,
              Message   => "New state not 'Init'");
      Assert (Condition => T.Start_Count = 1,
              Message   => "Start count not 1");
      Assert (Condition => DHCP.Timer.Event_Count = 0,
              Message   => "Event count not 0");
   end Verify_Process_Nack;

   -------------------------------------------------------------------------

   procedure Verify_Process_T2_Expiry
   is
      use type Anet.IPv4_Addr_Type;
      use type DHCPv4.Message.Message_Kind;

      Dummy : DHCPv4.Database.Mock.Cleaner;
      T     : Transactions.Mock.Transaction_Type;
   begin
      Database.Set_Fixed_Address (Addr => (192, 168, 239, 1));
      States.State_Type'Class (States.Renewing.State.all).Process_T2_Expiry
        (Transaction => T);

      Assert (Condition => T.Retry_Reset_Count = 1,
              Message   => "Retry reset count not 1");
      Assert (Condition => T.State = States.Rebinding.State,
              Message   => "New state not 'Rebinding'");
      Assert (Condition => T.Last_Sent_Msg.Get_Kind = Message.Request,
              Message   => "Message kind not Request");
      Assert (Condition => T.Last_Sent_Msg.Get_Client_IP = (192, 168, 239, 1),
              Message   => "Client IP mismatch");
      Assert (Condition => T.Send_Count = 1,
              Message   => "Send count not 1");
   end Verify_Process_T2_Expiry;

end DHCP_States_Tests.Renewing;
