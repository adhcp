--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet;

with DHCP.States;

with DHCPv4.Options;
with DHCPv4.Message;
with DHCPv4.Transactions.Mock;
with DHCPv4.States.Selecting;
with DHCPv4.States.Requesting;

package body DHCP_States_Tests.Selecting is

   use Ahven;
   use DHCPv4;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DHCP client state 'Selecting'");
      T.Add_Test_Routine
        (Routine => Verify_Name_Getter'Access,
         Name    => "Name getter");
      T.Add_Test_Routine
        (Routine => Verify_Process_Offer'Access,
         Name    => "Offer processing");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Verify_Name_Getter
   is
   begin
      Assert (Condition => States.Selecting.State.Get_Name = "Selecting",
              Message   => "Name mismatch");
   end Verify_Name_Getter;

   -------------------------------------------------------------------------

   procedure Verify_Process_Offer
   is
      use type Anet.IPv4_Addr_Type;
      use type DHCP.States.State_Handle;
      use type DHCPv4.Message.Message_Kind;
      use type DHCPv4.Options.Inst.Option_Type'Class;

      T      : Transactions.Mock.Transaction_Type;
      Msg    : Message.Message_Type         := Message.Create
        (Kind => Message.Offer);
      Y_IP   : constant Anet.IPv4_Addr_Type := (192, 168, 0, 100);
      Srv_ID : constant Options.Inst.Option_Type'Class
        := Options.Inst.Create (Name => Options.DHCP_Server_Id,
                                Data => (255, 255, 255, 255));
   begin
      Msg.Set_Your_IP (IP => Y_IP);
      Msg.Add_Option (Opt => Srv_ID);

      States.State_Type'Class (States.Selecting.State.all).Process_Offer
        (Transaction => T,
         Msg         => Msg);

      Assert (Condition => T.State = States.Requesting.State,
              Message   => "New state not 'Requesting'");
      Assert (Condition => T.Last_Sent_Msg.Get_Kind = Message.Request,
              Message   => "Message kind not Request");
      Assert (Condition => T.Last_Sent_Msg.Get_Options.Get
              (Name => Options.DHCP_Server_Id) = Srv_ID,
              Message   => "Server ID option mismatch");
      Assert (Condition => Options.Inst4.IP_Option_Type
              (T.Last_Sent_Msg.Get_Options.Get
                 (Name => Options.Address_Request)).Get_Value = Y_IP,
              Message   => "Requested IP Address mismatch");
      Assert (Condition => T.Send_Count = 1,
              Message   => "Send count not 1");
      Assert (Condition => T.Retry_Reset_Count = 1,
              Message   => "Retry reset count not 1");
   end Verify_Process_Offer;

end DHCP_States_Tests.Selecting;
