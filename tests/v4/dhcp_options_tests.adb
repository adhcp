--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Interfaces;

with Ada.Streams;
with Ada.Exceptions;

with Anet;

with DHCPv4.Options;

package body DHCP_Options_Tests is

   use Ada.Streams;
   use Ahven;
   use DHCPv4;
   use DHCPv4.Options;
   use DHCPv4.Options.Inst;

   -------------------------------------------------------------------------

   procedure Append_Options_To_List
   is
      L  : Inst4.Option_List;
      Op : constant Option_Type'Class
        := Create (Name => Options.Domain_Name,
                   Data => (1 => 32));
   begin
      Assert (Condition => L.Is_Empty,
              Message   => "New list not empty");

      L.Append (New_Item => Op);
      L.Append (New_Item => Op);

      Assert (Condition => L.Get_Count = 2,
              Message   => "Option count not 2");
   end Append_Options_To_List;

   -------------------------------------------------------------------------

   procedure Compare_Options
   is
      Data1 : constant Stream_Element_Array := (16#35#, 16#01#, 16#05#);
      Data2 : constant Stream_Element_Array := (16#12#, 16#01#, 16#05#);
      Data3 : constant Stream_Element_Array := (16#35#, 16#01#, 16#06#);
      Data4 : constant Stream_Element_Array
        := (16#12#, 16#02#, 16#06#, 16#01#);

      Opt1 : constant Option_Type'Class := Deserialize (Buffer => Data1);
      Opt2 : constant Option_Type'Class := Deserialize (Buffer => Data2);
      Opt3 : constant Option_Type'Class := Deserialize (Buffer => Data3);
      Opt4 : constant Option_Type'Class := Deserialize (Buffer => Data4);
   begin
      Assert (Condition => Opt1 = Opt1,
              Message   => "Option1 not equal Option1");
      Assert (Condition => Opt1 /= Opt2,
              Message   => "Option1 equal Option2");
      Assert (Condition => Opt1 /= Opt3,
              Message   => "Option1 equal Option3");
      Assert (Condition => Opt1 /= Opt4,
              Message   => "Option1 equal Option4");
   end Compare_Options;

   -------------------------------------------------------------------------

   procedure Create_Bool_Option
   is
      Data : constant Stream_Element_Array := (1 => 0);
      Opt1 : constant Option_Type'Class    := Create
        (Name => Forward_On_Off,
         Data => Data);
   begin
      Assert (Condition => Opt1.Get_Data = Data,
              Message   => "Data mismatch");
      Assert (Condition => Opt1.Get_Data_Str = "0",
              Message   => "Data string mismatch");
      Assert (Condition => Opt1.Get_Name = Forward_On_Off,
              Message   => "Name mismatch");

      begin
         declare
            Opt2 : constant Option_Type'Class := Create
              (Name => Forward_On_Off,
               Data => (1 => 3));
            pragma Unreferenced (Opt2);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Boolean option FORWARD_ON_OFF: data 3, expected 0 or 1",
                    Message   => "Exception mismatch ");
      end;

      begin
         declare
            Opt3 : constant Option_Type'Class := Create
              (Name => Forward_On_Off,
               Data => (25, 25));
            pragma Unreferenced (Opt3);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Byte based option FORWARD_ON_OFF: data size 2, expected"
                    & " 1",
                    Message   => "Exception mismatch ");
      end;
   end Create_Bool_Option;

   -------------------------------------------------------------------------

   procedure Create_DHCP_Msg_Kind_Option
   is
      Data : constant Stream_Element_Array := (1 => 1);
      Opt1 : constant Option_Type'Class    := Create
        (Name => DHCP_Msg_Type,
         Data => Data);
   begin
      Assert (Condition => Opt1.Get_Data = Data,
              Message   => "Data mismatch");
      Assert (Condition => Opt1.Get_Name = DHCP_Msg_Type,
              Message   => "Name mismatch");

      begin
         declare
            Opt2 : constant Option_Type'Class := Create
              (Name => DHCP_Msg_Type,
               Data => (1 => 9));
            pragma Unreferenced (Opt2);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when Inst.Invalid_Option => null;
      end;
   end Create_DHCP_Msg_Kind_Option;

   -------------------------------------------------------------------------

   procedure Create_Iface_MTU_Option
   is
      Data : constant Stream_Element_Array := (255, 255);
      Opt1 : constant Option_Type'Class    := Create
        (Name => Interface_MTU,
         Data => Data);
   begin
      Assert (Condition => Opt1.Get_Data = Data,
              Message   => "Data mismatch");
      Assert (Condition => Opt1.Get_Name = Interface_MTU,
              Message   => "Name mismatch");

      begin
         declare
            Opt2 : constant Option_Type'Class := Create
              (Name => Interface_MTU,
               Data => (0, 67));
            pragma Unreferenced (Opt2);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when Inst.Invalid_Option => null;
      end;
   end Create_Iface_MTU_Option;

   -------------------------------------------------------------------------

   procedure Create_IP_Array_Option
   is
      Data : constant Stream_Element_Array := (255, 255, 255, 0);
      Opt1 : constant Option_Type'Class    := Create
        (Name => Routers,
         Data => Data);
   begin
      Assert (Condition => Opt1.Get_Data = Data,
              Message   => "Data mismatch");
      Assert (Condition => Opt1.Get_Name = Routers,
              Message   => "Name mismatch");
      Assert (Condition => IP_Array_Option_Type (Opt1).Get_Addrlen = 4,
              Message   => "Addrlen mismatch");

      begin
         declare
            Opt2 : constant Option_Type'Class := Create
              (Name => Routers,
               Data => (255, 255, 255, 0, 0, 0));
            pragma Unreferenced (Opt2);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "IP array option ROUTERS: data size 6, expected multiple"
                    & " of 4",
                    Message   => "Exception mismatch ");
      end;
   end Create_IP_Array_Option;

   -------------------------------------------------------------------------

   procedure Create_IP_Option
   is
      use type Anet.IPv4_Addr_Type;

      Data : constant Stream_Element_Array := (255, 255, 255, 0);
      Opt1 : constant Option_Type'Class    := Create
        (Name => Subnet_Mask,
         Data => Data);
      Opt2 : constant Inst4.IP_Option_Type := Inst4.Create
        (Name    => Swap_Server,
         Address => (192, 168, 10, 1));
   begin
      Assert (Condition => Opt1.Get_Data = Data,
              Message   => "Data mismatch");
      Assert (Condition => Opt1.Get_Name = Subnet_Mask,
              Message   => "Name mismatch");
      Assert (Condition => Opt2.Get_Value = (192, 168, 10, 1),
              Message   => "IP value mismatch");

      begin
         declare
            Opt2 : constant Option_Type'Class := Create
              (Name => Subnet_Mask,
               Data => (255, 255, 255, 0, 0));
            pragma Unreferenced (Opt2);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "IP option SUBNET_MASK: data size 5, expected 4",
                    Message   => "Exception mismatch ");
      end;
   end Create_IP_Option;

   -------------------------------------------------------------------------

   procedure Create_IP_Pairs_Option
   is
      Data : constant Stream_Element_Array := (10, 1, 1, 1, 255, 0, 0, 0);
      Opt1 : constant Option_Type'Class    := Create
        (Name => Static_Route,
         Data => Data);
   begin
      Assert (Condition => Opt1.Get_Data = Data,
              Message   => "Data mismatch");
      Assert (Condition => Opt1.Get_Name = Static_Route,
              Message   => "Name mismatch");

      begin
         declare
            Opt2 : constant Option_Type'Class := Create
              (Name => Policy_Filter,
               Data => (255, 255, 255, 0, 0));
            pragma Unreferenced (Opt2);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when Inst.Invalid_Option => null;
      end;
   end Create_IP_Pairs_Option;

   -------------------------------------------------------------------------

   procedure Create_Options
   is
      D1   : constant Stream_Element_Array := (192, 168, 10, 1);
      Opt1 : constant Option_Type'Class
        := Create (Name => NETBIOS_Name_Servers,
                   Data => D1);
   begin
      Assert (Condition => Opt1.Get_Data = D1,
              Message   => "Data mismatch1");
      Assert (Condition => Opt1.Get_Name = NETBIOS_Name_Servers,
              Message   => "Name mismatch1");

      begin
         declare
            L    : constant Stream_Element_Array
              (0 .. Inst.Size_Type'Last + 1)
              := (others => 12);
            Opt2 : constant Option_Type'Class
              := Create (Name => NETBIOS_Node_Type,
                         Data => L);
            pragma Unreferenced (Opt2);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when Inst.Invalid_Option => null;
      end;
   end Create_Options;

   -------------------------------------------------------------------------

   procedure Create_Positive_Octet_Option
   is
      Data : constant Stream_Element_Array := (1 => 12);
      Opt1 : constant Option_Type'Class    := Create
        (Name => Default_IP_TTL,
         Data => Data);
   begin
      Assert (Condition => Opt1.Get_Data = Data,
              Message   => "Data mismatch");
      Assert (Condition => Opt1.Get_Name = Default_IP_TTL,
              Message   => "Name mismatch");

      begin
         declare
            Opt2 : constant Option_Type'Class := Create
              (Name => Default_TCP_TTL,
               Data => (255, 255, 128));
            pragma Unreferenced (Opt2);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when Inst.Invalid_Option => null;
      end;

      begin
         declare
            Opt3 : constant Option_Type'Class := Create
              (Name => Default_TCP_TTL,
               Data => (1 => 0));
            pragma Unreferenced (Opt3);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when Inst.Invalid_Option => null;
      end;
   end Create_Positive_Octet_Option;

   -------------------------------------------------------------------------

   procedure Create_Raw_Option
   is
      Data : constant Stream_Element_Array := (65, 66, 67);
      Opt1 : constant Option_Type'Class    := Create
        (Name => PXE_Undefined_1,
         Data => Data);
   begin
      Assert (Condition => Opt1.Get_Data = Data,
              Message   => "Data mismatch");
      Assert (Condition => Opt1.Get_Name = PXE_Undefined_1,
              Message   => "Name mismatch");
   end Create_Raw_Option;

   -------------------------------------------------------------------------

   procedure Create_String_Option
   is
      Data  : constant Stream_Element_Array          := (65, 66, 67);
      Empty : constant Stream_Element_Array (1 .. 0) := (others => 0);
      Opt1  : constant Option_Type'Class             := Create
        (Name => Domain_Name,
         Data => Data);
   begin
      Assert (Condition => Opt1.Get_Data = Data,
              Message   => "Data mismatch");
      Assert (Condition => Opt1.Get_Name = Domain_Name,
              Message   => "Name mismatch");

      begin
         declare
            Opt2 : constant Option_Type'Class := Create
              (Name => Domain_Name,
               Data => Empty);
            pragma Unreferenced (Opt2);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when Inst.Invalid_Option => null;
      end;
   end Create_String_Option;

   -------------------------------------------------------------------------

   procedure Create_Uint16_Array_Option
   is
      Data : constant Stream_Element_Array := (255, 255, 255, 255);
      Opt1 : constant Option_Type'Class    := Create
        (Name => Plateau_MTU,
         Data => Data);
   begin
      Assert (Condition => Opt1.Get_Data = Data,
              Message   => "Data mismatch");
      Assert (Condition => Opt1.Get_Name = Plateau_MTU,
              Message   => "Name mismatch");

      begin
         declare
            Opt2 : constant Option_Type'Class := Create
              (Name => Plateau_MTU,
               Data => (255, 255, 128));
            pragma Unreferenced (Opt2);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Unsigned 16-bit array option PLATEAU_MTU: data size 3"
                    & ", expected multiple of 2",
                    Message   => "Exception mismatch ");
      end;
   end Create_Uint16_Array_Option;

   -------------------------------------------------------------------------

   procedure Create_Uint16_Option
   is
      use type Interfaces.Unsigned_16;

      Data : constant Stream_Element_Array := (255, 255);
      Opt1 : constant Option_Type'Class    := Create
        (Name => Interface_MTU,
         Data => Data);
      Opt2 : constant U_Int16_Option_Type  := Create
          (Name  => Boot_File_Size,
           Value => 6246);
   begin
      Assert (Condition => Opt1.Get_Data = Data,
              Message   => "Data mismatch");
      Assert (Condition => Opt1.Get_Name = Interface_MTU,
              Message   => "Name mismatch");
      Assert (Condition => Opt2.Get_Value = 6246,
              Message   => "Uint16 value mismatch");

      begin
         declare
            Opt2 : constant Option_Type'Class := Create
              (Name => Interface_MTU,
               Data => (255, 255, 128));
            pragma Unreferenced (Opt2);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Unsigned 16-bit option INTERFACE_MTU: data size 3, "
                    & "expected 2",
                    Message   => "Exception mismatch ");
      end;
   end Create_Uint16_Option;

   -------------------------------------------------------------------------

   procedure Create_Uint32_Option
   is
      use type Interfaces.Unsigned_32;

      Data : constant Stream_Element_Array := (128, 128, 128, 128);
      Opt1 : constant Option_Type'Class    := Create
        (Name => Time_Offset,
         Data => Data);
   begin
      Assert (Condition => Opt1.Get_Data = Data,
              Message   => "Data mismatch");
      Assert (Condition => Opt1.Get_Name = Time_Offset,
              Message   => "Name mismatch");
      Assert (Condition => U_Int32_Option_Type (Opt1).Get_Value = 2155905152,
              Message   => "Value mismatch");

      begin
         declare
            Opt2 : constant Option_Type'Class := Create
              (Name => Time_Offset,
               Data => (255, 255, 128));
            pragma Unreferenced (Opt2);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Unsigned 32-bit option TIME_OFFSET: data size 3, "
                    & "expected 4",
                    Message   => "Exception mismatch ");
      end;
   end Create_Uint32_Option;

   -------------------------------------------------------------------------

   procedure Deserialize_Option_Lists
   is
      Empty         : constant Stream_Element_Array
        := (16#63#, 16#82#, 16#53#, 16#63#, 16#ff#);
      Data          : constant Stream_Element_Array
        := (16#63#, 16#82#, 16#53#, 16#63#, 16#35#, 16#01#, 16#05#, 16#36#,
            16#04#, 16#c0#, 16#a8#, 16#ef#, 16#01#, 16#33#, 16#04#, 16#00#,
            16#01#, 16#51#, 16#80#, 16#3a#, 16#04#, 16#00#, 16#00#, 16#a8#,
            16#c0#, 16#3b#, 16#04#, 16#00#, 16#01#, 16#27#, 16#50#, 16#01#,
            16#04#, 16#ff#, 16#ff#, 16#ff#, 16#00#, 16#03#, 16#04#, 16#c0#,
            16#a8#, 16#ef#, 16#01#, 16#06#, 16#08#, 16#50#, 16#fe#, 16#a1#,
            16#7e#, 16#50#, 16#fe#, 16#a1#, 16#7d#, 16#0f#, 16#0b#, 16#73#,
            16#77#, 16#69#, 16#73#, 16#73#, 16#2d#, 16#69#, 16#74#, 16#2e#,
            16#63#, 16#68#, 16#ff#);
      No_Options    : constant Stream_Element_Array
        := (16#63#, 16#82#, 16#53#, 16#63#);
      Invalid_Magic : constant Stream_Element_Array
        := (16#63#, 16#82#, 16#51#, 16#63#, 16#ff#);
      No_End_Marker : constant Stream_Element_Array
        := (16#63#, 16#82#, 16#53#, 16#63#, 16#12#);
      Padded_Data   : constant Stream_Element_Array
        := (16#63#, 16#82#, 16#53#, 16#63#, 16#00#, 16#00#, 16#35#, 16#01#,
            16#05#, 16#00#, 16#00#, 16#ff#);

      List : Inst4.Option_List;
   begin
      List := Inst4.Deserialize (Buffer => Empty);
      Assert (Condition => List.Is_Empty,
              Message   => "List not empty");

      List := Inst4.Deserialize (Buffer => Data);

      Assert (Condition => List.Get_Count = 9,
              Message   => "Option count not 9");

      declare
         Count : Natural := 0;

         procedure Inc_Counter
           (Option : Options.Inst.Option_Type'Class);
         --  Increment the count var for each option in the list.

         procedure Inc_Counter
           (Option : Options.Inst.Option_Type'Class)
         is
            pragma Unreferenced (Option);
         begin
            Count := Count + 1;
         end Inc_Counter;
      begin
         List.Iterate (Process => Inc_Counter'Access);
         Assert (Condition => Count = 9,
                 Message   => "Iterate count mismatch");
      end;

      begin
         List := Inst4.Deserialize (Buffer => No_Options);
         Fail (Message => "Expected invalid options error");

      exception
         when Inst.Invalid_Option => null;
      end;

      begin
         List := Inst4.Deserialize (Buffer => Invalid_Magic);
         Fail (Message => "Expected invalid cookie error");

      exception
         when Inst4.Invalid_Cookie => null;
      end;

      begin
         List := Inst4.Deserialize (Buffer => No_End_Marker);
         Fail (Message => "Expected invalid options error");

      exception
         when Inst.Invalid_Option => null;
      end;

      List := Inst4.Deserialize (Buffer => Padded_Data);
      Assert (Condition => List.Get_Count = 1,
              Message   => "Padded: One option expected");
      Assert (Condition => List.Contains (Name => DHCP_Msg_Type),
              Message   => "Padded: Invalid option");
   end Deserialize_Option_Lists;

   -------------------------------------------------------------------------

   procedure Deserialize_Options_From_Stream
   is
      DHCP_Msg     : constant Stream_Element_Array := (16#35#, 16#01#, 16#05#);
      Invalid_Size : constant Stream_Element_Array := (16#35#, 16#00#, 16#05#);
      Custom_Code  : constant Stream_Element_Array := (16#fe#, 16#01#, 16#05#);
      No_Data      : constant Stream_Element_Array := (16#35#, 16#01#);
      Code_Only    : constant Stream_Element_Array := (1 => 16#35#);
      Data_Size    : constant Stream_Element_Array := (16#35#, 16#02#, 16#ff#);
   begin
      declare
         Ref_Data : constant Stream_Element_Array := (1 => 16#05#);
         Opt      : constant Option_Type'Class
           := Deserialize (Buffer => DHCP_Msg);
      begin
         Assert (Condition => Opt.Get_Name = DHCP_Msg_Type,
                 Message   => "Name mismatch");
         Assert (Condition => Opt.Get_Name_Str = "dhcp_msg_type",
                 Message   => "Name string mismatch");
         Assert (Condition => Opt.Get_Size = 1,
                 Message   => "Size mismatch");
         Assert (Condition => Opt.Get_Data = Ref_Data,
                 Message   => "Data mismatch");
         Assert (Condition => Opt.Get_Code = 16#35#,
                 Message   => "Code mismatch");
      end;

      begin
         declare
            Opt : constant Option_Type'Class := Deserialize
              (Buffer => Invalid_Size);
            pragma Unreferenced (Opt);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when Inst.Invalid_Option => null;
      end;

      declare
         Opt : constant Option_Type'Class := Deserialize
           (Buffer => Custom_Code);
      begin
         Assert (Condition => Opt.Get_Name = Site_Specific,
                 Message   => "Site specific name expected");
         Assert (Condition => Opt.Get_Data = (1 => 5),
                 Message   => "Data mismatch");
      end;

      begin
         declare
            Opt : constant Option_Type'Class := Deserialize
              (Buffer => No_Data);
            pragma Unreferenced (Opt);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when Inst.Invalid_Option => null;
      end;

      begin
         declare
            Opt : constant Option_Type'Class := Deserialize
              (Buffer => Code_Only);
            pragma Unreferenced (Opt);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when Inst.Invalid_Option => null;
      end;

      begin
         declare
            Opt : constant Option_Type'Class := Deserialize
              (Buffer => Data_Size);
            pragma Unreferenced (Opt);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when Inst.Invalid_Option => null;
      end;
   end Deserialize_Options_From_Stream;

   -------------------------------------------------------------------------

   procedure Get_Bool_Option_Data_As_String
   is
      Opt1 : constant Option_Type'Class
        := Create (Name => Forward_On_Off,
                   Data => (1 => 0));
      Opt2 : constant Option_Type'Class
        := Create (Name => Forward_On_Off,
                   Data => (1 => 1));
   begin
      Assert (Condition => Opt1.Get_Data_Str = "0",
              Message   => "Expected 0");
      Assert (Condition => Opt2.Get_Data_Str = "1",
              Message   => "Expected 1");
   end Get_Bool_Option_Data_As_String;

   -------------------------------------------------------------------------

   procedure Get_IP_Array_Option_Data_As_String
   is
      Opt1 : constant Option_Type'Class
        := Create (Name => Routers,
                   Data => (192, 168, 10, 1));
      Opt2 : constant Option_Type'Class
        := Create (Name => Domain_Name_Servers,
                   Data => (192, 168, 10, 1, 192, 168, 10, 2));
   begin
      Assert (Condition => Opt1.Get_Data_Str = "192.168.10.1",
              Message   => "IP array string mismatch1");
      Assert (Condition => Opt2.Get_Data_Str = "192.168.10.1 192.168.10.2",
              Message   => "IP array string mismatch2");
   end Get_IP_Array_Option_Data_As_String;

   -------------------------------------------------------------------------

   procedure Get_IP_Option_Data_As_String
   is
      Opt1 : constant Option_Type'Class
        := Create (Name => Subnet_Mask,
                   Data => (255, 255, 255, 0));
   begin
      Assert (Condition => Opt1.Get_Data_Str = "255.255.255.0",
              Message   => "Subnet mask mismatch");
   end Get_IP_Option_Data_As_String;

   -------------------------------------------------------------------------

   procedure Get_IP_Option_Value
   is
      use type Anet.IPv4_Addr_Type;

      IP   : constant Anet.IPv4_Addr_Type := (192, 168, 10, 1);
      Opt1 : constant Option_Type'Class
        := Create (Name => DHCP_Server_Id,
                   Data => (192, 168, 10, 1));
   begin
      Assert (Condition => Inst4.IP_Option_Type (Opt1).Get_Value = IP,
              Message   => "IP value mismatch");
   end Get_IP_Option_Value;

   -------------------------------------------------------------------------

   procedure Get_IP_Pairs_Option_Data_As_String
   is
      Opt1 : constant Option_Type'Class
        := Create (Name => Static_Route,
                   Data => (192, 168, 10, 1, 255, 255, 255, 0));
   begin
      Assert (Condition => Opt1.Get_Data_Str = "192.168.10.1 255.255.255.0",
              Message   => "IP pair mismatch");
   end Get_IP_Pairs_Option_Data_As_String;

   -------------------------------------------------------------------------

   procedure Get_Option_By_Name
   is
      Opts    : Inst4.Option_List;
      Ref_Opt : constant Inst.Option_Type'Class
        := Create (Name => Routers,
                   Data => (192, 168, 10, 1));
   begin
      begin
         declare
            Opt : constant Inst.Option_Type'Class := Opts.Get
              (Name => Routers);
            pragma Unreferenced (Opt);
         begin
            Fail ("Option not found error expected");
         end;

      exception
         when Inst.Option_Not_Found => null;
      end;

      Opts.Append (New_Item => Ref_Opt);
      Assert (Condition => Opts.Get (Name => Routers) = Ref_Opt,
              Message   => "Option mismatch");
   end Get_Option_By_Name;

   -------------------------------------------------------------------------

   procedure Get_Option_Data_As_String
   is
      Opt1   : constant Option_Type'Class
        := Create (Name => Domain_Name,
                   Data => (16#73#, 16#77#, 16#69#, 16#73#, 16#73#, 16#2d#,
                            16#69#, 16#74#, 16#2e#, 16#63#, 16#68#));
      Stream : constant Stream_Element_Array (3 .. 13)
        := (16#73#, 16#77#, 16#69#, 16#73#, 16#73#, 16#2d#,
            16#69#, 16#74#, 16#2e#, 16#63#, 16#68#);
      Opt2   : constant Option_Type'Class
        := Create (Name => Domain_Name,
                   Data => Stream);
   begin
      Assert (Condition => Opt1.Get_Data_Str = "swiss-it.ch",
              Message   => "String mismatch1");
      Assert (Condition => Opt2.Get_Data_Str = "swiss-it.ch",
              Message   => "String mismatch2");
   end Get_Option_Data_As_String;

   -------------------------------------------------------------------------

   procedure Get_Positive_Octet_Option_Data_As_String
   is
      Opt1 : constant Option_Type'Class
        := Create (Name => Default_IP_TTL,
                   Data => (1 => 134));
      Opt2 : constant Option_Type'Class
        := Create (Name => Default_IP_TTL,
                   Data => (1 => 1));
      Opt3 : constant Option_Type'Class
        := Create (Name => Default_IP_TTL,
                   Data => (1 => 255));
   begin
      Assert (Condition => Opt1.Get_Data_Str = "134",
              Message   => "Number mismatch1");
      Assert (Condition => Opt2.Get_Data_Str = "1",
              Message   => "Number mismatch2");
      Assert (Condition => Opt3.Get_Data_Str = "255",
              Message   => "Number mismatch3");
   end Get_Positive_Octet_Option_Data_As_String;

   -------------------------------------------------------------------------

   procedure Get_Raw_Option_Data_As_String
   is
      Opt1 : constant Option_Type'Class
        := Create (Name => PXE_Undefined_2,
                   Data => (16#ab#, 16#09#, 16#12#, 16#09#));
   begin
      Assert (Condition => Opt1.Get_Data_Str = "AB091209",
              Message   => "Hex string mismatch");
   end Get_Raw_Option_Data_As_String;

   -------------------------------------------------------------------------

   procedure Get_Uint16_Array_Option_Data_As_String
   is
      Opt1 : constant Option_Type'Class
        := Create (Name => Plateau_MTU,
                   Data => (16#12#, 16#09#, 16#12#, 16#09#));
      Opt2 : constant Option_Type'Class
        := Create (Name => Plateau_MTU,
                   Data => (0, 0, 0, 0, 0, 0));
      Opt3 : constant Option_Type'Class
        := Create (Name => Plateau_MTU,
                   Data => (255, 255, 255, 255));
   begin
      Assert (Condition => Opt1.Get_Data_Str = "4617 4617",
              Message   => "Numbers mismatch1");
      Assert (Condition => Opt2.Get_Data_Str = "0 0 0",
              Message   => "Numbers mismatch2");
      Assert (Condition => Opt3.Get_Data_Str = "65535 65535",
              Message   => "Number mismatch3");
   end Get_Uint16_Array_Option_Data_As_String;

   -------------------------------------------------------------------------

   procedure Get_Uint16_Option_Data_As_String
   is
      Opt1 : constant Option_Type'Class
        := Create (Name => Boot_File_Size,
                   Data => (16#12#, 16#09#));
      Opt2 : constant Option_Type'Class
        := Create (Name => DHCP_Max_Msg_Size,
                   Data => (0, 0));
      Opt3 : constant Option_Type'Class
        := Create (Name => Max_DG_Assembly,
                   Data => (16#ff#, 16#ff#));
   begin
      Assert (Condition => Opt1.Get_Data_Str = "4617",
              Message   => "Number mismatch1");
      Assert (Condition => Opt2.Get_Data_Str = "0",
              Message   => "Number mismatch2");
      Assert (Condition => Opt3.Get_Data_Str = "65535",
              Message   => "Number mismatch3");
   end Get_Uint16_Option_Data_As_String;

   -------------------------------------------------------------------------

   procedure Get_Uint32_Option_Data_As_String
   is
      Opt1 : constant Option_Type'Class
        := Create (Name => Address_Time,
                   Data => (16#00#, 16#09#, 16#3a#, 16#80#));
      Opt2 : constant Option_Type'Class
        := Create (Name => Address_Time,
                   Data => (0, 0, 0, 0));
      Opt3 : constant Option_Type'Class
        := Create (Name => Address_Time,
                   Data => (16#ff#, 16#ff#, 16#ff#, 16#ff#));
   begin
      Assert (Condition => Opt1.Get_Data_Str = "604800",
              Message   => "Number mismatch1");
      Assert (Condition => Opt2.Get_Data_Str = "0",
              Message   => "Number mismatch2");
      Assert (Condition => Opt3.Get_Data_Str = "4294967295",
              Message   => "Number mismatch3");
   end Get_Uint32_Option_Data_As_String;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Options package");
      T.Add_Test_Routine
        (Routine => Create_Options'Access,
         Name    => "Create options");
      T.Add_Test_Routine
        (Routine => Compare_Options'Access,
         Name    => "Compare options");
      T.Add_Test_Routine
        (Routine => Append_Options_To_List'Access,
         Name    => "Append options to list");
      T.Add_Test_Routine
        (Routine => List_Option_Data_Size'Access,
         Name    => "Get data size from list");
      T.Add_Test_Routine
        (Routine => Deserialize_Options_From_Stream'Access,
         Name    => "Deserialize options");
      T.Add_Test_Routine
        (Routine => Deserialize_Option_Lists'Access,
         Name    => "Deserialize option lists");
      T.Add_Test_Routine
        (Routine => Serialize_Options_To_Stream'Access,
         Name    => "Serialize options");
      T.Add_Test_Routine
        (Routine => Serialize_Option_Lists'Access,
         Name    => "Serialize option lists");
      T.Add_Test_Routine
        (Routine => Get_Option_By_Name'Access,
         Name    => "Get option by name");
      T.Add_Test_Routine
        (Routine => Get_Option_Data_As_String'Access,
         Name    => "Get option data as string");
      T.Add_Test_Routine
        (Routine => Get_Positive_Octet_Option_Data_As_String'Access,
         Name    => "Get positive octet option data as string");
      T.Add_Test_Routine
        (Routine => Get_IP_Option_Data_As_String'Access,
         Name    => "Get IP option data as string");
      T.Add_Test_Routine
        (Routine => Get_IP_Option_Value'Access,
         Name    => "Get IP option value");
      T.Add_Test_Routine
        (Routine => Get_IP_Array_Option_Data_As_String'Access,
         Name    => "Get IP array option data as string");
      T.Add_Test_Routine
        (Routine => Get_IP_Pairs_Option_Data_As_String'Access,
         Name    => "Get IP pairs option data as string");
      T.Add_Test_Routine
        (Routine => Get_Uint16_Array_Option_Data_As_String'Access,
         Name    => "Get uint 16-bit array option data as string");
      T.Add_Test_Routine
        (Routine => Get_Uint16_Option_Data_As_String'Access,
         Name    => "Get uint 16-bit option data as string");
      T.Add_Test_Routine
        (Routine => Get_Uint32_Option_Data_As_String'Access,
         Name    => "Get uint 32-bit option data as string");
      T.Add_Test_Routine
        (Routine => Get_Bool_Option_Data_As_String'Access,
         Name    => "Get boolean option data as string");
      T.Add_Test_Routine
        (Routine => Get_Raw_Option_Data_As_String'Access,
         Name    => "Get raw option data as string");
      T.Add_Test_Routine
        (Routine => Create_Positive_Octet_Option'Access,
         Name    => "Create positive octet option");
      T.Add_Test_Routine
        (Routine => Create_String_Option'Access,
         Name    => "Create string option");
      T.Add_Test_Routine
        (Routine => Create_IP_Option'Access,
         Name    => "Create IP option");
      T.Add_Test_Routine
        (Routine => Create_IP_Array_Option'Access,
         Name    => "Create IP array option");
      T.Add_Test_Routine
        (Routine => Create_IP_Pairs_Option'Access,
         Name    => "Create IP pairs option");
      T.Add_Test_Routine
        (Routine => Create_Uint16_Array_Option'Access,
         Name    => "Create uint16 array option");
      T.Add_Test_Routine
        (Routine => Create_Uint16_Option'Access,
         Name    => "Create 16-bit unsigned option");
      T.Add_Test_Routine
        (Routine => Create_Uint32_Option'Access,
         Name    => "Create 32-bit unsigned option");
      T.Add_Test_Routine
        (Routine => Create_Bool_Option'Access,
         Name    => "Create boolean option");
      T.Add_Test_Routine
        (Routine => Create_Iface_MTU_Option'Access,
         Name    => "Create interface MTU option");
      T.Add_Test_Routine
        (Routine => Create_DHCP_Msg_Kind_Option'Access,
         Name    => "Create DHCP message kind option");
      T.Add_Test_Routine
        (Routine => Create_Raw_Option'Access,
         Name    => "Create raw option");
      T.Add_Test_Routine
        (Routine => List_Option_Contains'Access,
         Name    => "List contains option");
      T.Add_Test_Routine
        (Routine => List_Option_Clear'Access,
         Name    => "List clear");
      T.Add_Test_Routine
        (Routine => List_Option_Remove'Access,
         Name    => "List remove option");
   end Initialize;

   -------------------------------------------------------------------------

   procedure List_Option_Clear
   is
      L : Inst4.Option_List;
   begin
      L.Clear;
      Assert (Condition => L.Get_Count = 0,
              Message   => "Error clearing empty list");

      L.Append (New_Item => Create
                (Name => DHCP_Msg_Type,
                 Data => (1 => 16#05#)));
      L.Clear;
      Assert (Condition => L.Get_Count = 0,
              Message   => "Error clearing list");
   end List_Option_Clear;

   -------------------------------------------------------------------------

   procedure List_Option_Contains
   is
      L : Inst4.Option_List;
   begin
      Assert (Condition => not L.Contains (Name => Subnet_Mask),
              Message   => "Empty list contains subnet mask");

      L.Append (New_Item => Create
                (Name => DHCP_Msg_Type,
                 Data => (1 => 16#05#)));
      L.Append (New_Item => Create
                (Name => Subnet_Mask,
                 Data => (16#ff#, 16#ff#, 16#ff#, 16#00#)));
      Assert (Condition => L.Contains (Name => Subnet_Mask),
              Message   => "Subnet mask not in list");
   end List_Option_Contains;

   -------------------------------------------------------------------------

   procedure List_Option_Data_Size
   is
      L : Inst4.Option_List;
   begin
      Assert (Condition => L.Get_Data_Size = 0,
              Message   => "New list has some data");

      L.Append (New_Item => Create
                (Name => DHCP_Msg_Type,
                 Data => (1 => 16#05#)));
      L.Append (New_Item => Create
                (Name => DHCP_Server_Id,
                 Data => (16#c0#, 16#a8#, 16#ef#, 16#01#)));
      L.Append (New_Item => Create
                (Name => Subnet_Mask,
                 Data => (16#ff#, 16#ff#, 16#ff#, 16#00#)));

      Assert (Condition => L.Get_Data_Size = 9,
              Message   => "Data length not 9");
   end List_Option_Data_Size;

   -------------------------------------------------------------------------

   procedure List_Option_Remove
   is
      L : Inst4.Option_List;
   begin
      L.Append (New_Item => Create
                (Name => DHCP_Msg_Type,
                 Data => (1 => 16#05#)));
      L.Append (New_Item => Create
                (Name => DHCP_Server_Id,
                 Data => (16#c0#, 16#a8#, 16#ef#, 16#01#)));

      L.Remove (Name => DHCP_Server_Id);
      Assert (Condition => L.Get_Count = 1,
              Message   => "Error removing option (1)");
      L.Remove (Name => DHCP_Msg_Type);
      Assert (Condition => L.Get_Count = 0,
              Message   => "Error removing option (2)");

      begin
         L.Remove (Name => DHCP_Msg_Type);
         Fail (Message => "Exception expected");

      exception
         when E : Option_Not_Found =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "No DHCP_MSG_TYPE option found",
                    Message   => "Exception mismatch");
      end;
   end List_Option_Remove;

   -------------------------------------------------------------------------

   procedure Serialize_Option_Lists
   is
      L     : Inst4.Option_List;
      Empty : constant Stream_Element_Array
        := (16#63#, 16#82#, 16#53#, 16#63#, 16#ff#);
      Ref   : constant Stream_Element_Array
        := (16#63#, 16#82#, 16#53#, 16#63#, 16#35#, 16#01#, 16#05#, 16#36#,
            16#04#, 16#c0#, 16#a8#, 16#ef#, 16#01#, 16#01#, 16#04#, 16#ff#,
            16#ff#, 16#ff#, 16#00#, 16#ff#);
   begin
      Assert (Condition => L.Serialize = Empty,
              Message   => "Empty list serialization failed");

      L.Append (New_Item => Create
                (Name => DHCP_Msg_Type,
                 Data => (1 => 16#05#)));
      L.Append (New_Item => Create
                (Name => DHCP_Server_Id,
                 Data => (16#c0#, 16#a8#, 16#ef#, 16#01#)));
      L.Append (New_Item => Create
                (Name => Subnet_Mask,
                 Data => (16#ff#, 16#ff#, 16#ff#, 16#00#)));

      Assert (Condition => L.Serialize = Ref,
              Message   => "List serialization failed");
   end Serialize_Option_Lists;

   -------------------------------------------------------------------------

   procedure Serialize_Options_To_Stream
   is
      R1 : constant Stream_Element_Array := (16#35#, 16#01#, 16#05#);
      O1 : constant Option_Type'Class
        := Create (Name => DHCP_Msg_Type,
                   Data => (1 => 16#05#));
   begin
      Assert (Condition => O1.Serialize = R1,
              Message   => "O1 serialization failed");
   end Serialize_Options_To_Stream;

end DHCP_Options_Tests;
