--
--  Copyright (C) 2018 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2018 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Tags;
with Ada.Streams;
with Ada.Strings.Unbounded;

with Anet.ARP;

with DHCP.Timer.Test;
with DHCP.Timing_Events;

with DHCPv4.ACD.Mock;
with DHCPv4.Timing_Events.Announce.Tests;

package body DHCPv4.ACD.Tests
is

   use Ada.Strings.Unbounded;
   use Ahven;

   -------------------------------------------------------------------------

   procedure Announce
   is
      use type Ada.Streams.Stream_Element_Offset;
      use type Ada.Streams.Stream_Element_Array;
      use type Anet.Hardware_Addr_Type;
      use type Anet.IPv4_Addr_Type;

      Ref_Iface   : constant Unbounded_String := To_Unbounded_String ("eth1");
      Ref_HW_Addr : constant Anet.Hardware_Addr_Type (1 .. 6)
        := (16#ca#, 16#fe#, 16#be#, 16#ef#, 16#ca#, 16#fe#);
      Ref_IP_Addr : constant Anet.IPv4_Addr_Type := (192, 168, 0, 42);
      Ref_Data    : constant Anet.ARP.Header_Type
        := (Operation => Anet.ARP.ARP_Request,
            Src_Ether => Ref_HW_Addr,
            Src_IP    => Ref_IP_Addr,
            Dst_Ether => (others => 0),
            Dst_IP    => Ref_IP_Addr);

      Dummy_Sock  : Mock.Sock_Cleaner;
      Dummy_Timer : DHCP.Timer.Test.Cleaner_Type;
   begin

      --  ACD disabled.

      Announce (IP_Address => Ref_IP_Addr);
      Assert (Condition => Mock.Send_Count = 0,
              Message   => "Announce sent with ACD disabled");
      Assert (Condition => Mock.Init_Count = 0,
              Message   => "ARP socket initialized with ACD disabled");

      --  ACD enabled.

      Iface_Info.Name := Ref_Iface;
      Iface_Info.Mac_Addr := Ref_HW_Addr;
      Initialized := True;

      Announce (IP_Address => Ref_IP_Addr);
      Assert (Condition => Mock.Init_Count = 1,
              Message   => "ARP socket not initialized");
      Assert (Condition => Mock.Bind_Count = 1,
              Message   => "ARP socket not bound to interface");
      Assert (Condition => Mock.Bound_Iface = Ref_Iface,
              Message   => "ARP socket bound to wrong interface");
      Assert (Condition => Mock.Close_Count = 1,
              Message   => "ARP socket not closed");
      Assert (Condition => Mock.Send_Count = 1,
              Message   => "Announce send count mismatch");
      Assert (Condition => Anet.ARP.To_Stream (Header => Ref_Data)
              = Mock.Last_Item (Mock.Last_Item'First .. Mock.Last_Idx),
              Message   => "Announce data mismatch");
      Assert (Condition => Mock.Last_Dst_HW = Anet.Bcast_HW_Addr,
              Message   => "Announce destination hw address mismatch");
      Assert (Condition => Mock.Last_Iface = Ref_Iface,
              Message   => "Announce interface name mismatch");

      Assert (Condition => DHCP.Timer.Event_Count = 1,
              Message   => "Announce event not scheduled");

      declare
         use type Ada.Tags.Tag;
         package TA renames DHCPv4.Timing_Events.Announce;

         Ev : constant DHCP.Timing_Events.Event_Type'Class
           := DHCP.Timer.Test.Get_Next_Event;
      begin
         Assert
           (Condition => Ev'Tag = TA.Announce_Type'Tag,
            Message   => "Non-announce event scheduled");

         Assert (Condition => TA.Tests.Get_IP_Addr
                 (Ev => TA.Announce_Type (Ev)) = Ref_IP_Addr,
                 Message   => "Announce event IP address mismatch");
         Assert (Condition => TA.Tests.Get_Count
                 (Ev => TA.Announce_Type (Ev)) = Constants.ANNOUNCE_NUM - 1,
                 Message   => "Announce event count mismatch");
         DHCP.Timer.Cancel (Event => Ev);
      end;

      Announce (IP_Address         => Ref_IP_Addr,
                Announcement_Count => 1);
      Assert (Condition => DHCP.Timer.Event_Count = 0,
              Message   => "Announce event scheduled");
   end Announce;

   -------------------------------------------------------------------------

   procedure Init
   is
      use type DHCP.Net.Iface_Type;

      Ref_Iface : constant DHCP.Net.Iface_Type
        := (Addrtype => DHCP.Net.Link_Layer,
            Name     => To_Unbounded_String ("eth0"),
            Mac_Addr => (16#ca#, 16#fe#, 16#be#, 16#ef#, 16#ca#, 16#fe#));

      Dummy : Mock.Sock_Cleaner;
   begin
      Assert (Condition => not Initialized,
              Message   => "Uninitialized package already initialized");

      Initialize (Iface => Ref_Iface);
      Assert (Condition => Initialized,
              Message   => "ACD not initialized");
      Assert (Condition => Iface_Info = Ref_Iface,
              Message   => "Interface info mismatch");
   end Init;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for ACD package");
      T.Add_Test_Routine
        (Routine => Init'Access,
         Name    => "Initialize ACD");
      T.Add_Test_Routine
        (Routine => Announce'Access,
         Name    => "Announce IP address");
      T.Add_Test_Routine
        (Routine => Probe'Access,
         Name    => "Probe IP address");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Probe
   is
      use type Ada.Real_Time.Time;
      use type Ada.Streams.Stream_Element_Array;

      Ref_Iface   : constant Unbounded_String := To_Unbounded_String ("eth1");
      Ref_IP_Addr : constant Anet.IPv4_Addr_Type := (192, 168, 0, 42);
      Ref_HW_Addr : constant Anet.Ether_Addr_Type
        := (16#ca#, 16#fe#, 16#be#, 16#ef#, 16#ca#, 16#fe#);
      Ref_Probe   : constant Anet.ARP.Header_Type
        := (Operation => Anet.ARP.ARP_Request,
            Src_Ether => Ref_HW_Addr,
            Src_IP    => Anet.Any_Addr,
            Dst_Ether => (others => 0),
            Dst_IP    => Ref_IP_Addr);

      Collision_Pkt : constant Anet.ARP.Header_Type
        := (Operation => Anet.ARP.ARP_Reply,
            Src_Ether => (16#be#, 16#ef#, 16#be#, 16#ef#, 16#be#, 16#ef#),
            Src_IP    => Ref_IP_Addr,
            Dst_Ether => Ref_HW_Addr,
            Dst_IP    => Anet.Any_Addr);
      Collision_Probe : constant Anet.ARP.Header_Type
        := (Operation => Anet.ARP.ARP_Request,
            Src_Ether => (16#be#, 16#ef#, 16#be#, 16#ef#, 16#be#, 16#ef#),
            Src_IP    => Anet.Any_Addr,
            Dst_Ether => (others => 0),
            Dst_IP    => Ref_IP_Addr);

      Collision_Detected : Boolean;

      Dummy_Sock  : Mock.Sock_Cleaner;
      Dummy_Timer : DHCP.Timer.Test.Cleaner_Type;
   begin

      --  ACD disabled.

      Probe (IP_Address => Ref_IP_Addr,
             Collision  => Collision_Detected);
      Assert (Condition => Mock.Send_Count = 0,
              Message   => "ARP probe sent with ACD disabled");
      Assert (Condition => not Collision_Detected,
              Message   => "Collision detected with ACD disabled");
      Assert (Condition => Mock.Init_Count = 0,
              Message   => "ARP socket initialized with ACD disabled");

      --  ACD enabled.

      Iface_Info.Name := Ref_Iface;
      Iface_Info.Mac_Addr := Ref_HW_Addr;
      Initialized := True;

      Probe (IP_Address => Ref_IP_Addr,
             Collision  => Collision_Detected);
      Assert (Condition => Mock.Init_Count = 1,
              Message   => "ARP socket not initialized");
      Assert (Condition => Mock.Bind_Count = 1,
              Message   => "ARP socket not bound to interface");
      Assert (Condition => Mock.Bound_Iface = Ref_Iface,
              Message   => "ARP socket bound to wrong interface");
      Assert (Condition => Mock.Close_Count = 1,
              Message   => "ARP socket not closed");
      Assert (Condition => Mock.Send_Count = Constants.PROBE_NUM,
              Message   => "ARP probe send count mismatch");
      Assert (Condition => Anet.ARP.To_Stream (Header => Ref_Probe)
              = Mock.Last_Item (1 .. Mock.Last_Idx),
              Message   => "ARP probe packet mismatch");
      Assert (Condition => not Collision_Detected,
              Message   => "Collision detected");
      Assert (Condition => Collision_Count = 0,
              Message   => "Collision count not zero");
      Assert (Condition => Last_Collision = Ada.Real_Time.Time_Last,
              Message   => "Last collision timestamp not Time_Last");
      Assert (Condition => Natural (Mock.Sleep_Calls.Length) = 3,
              Message   => "Sleep calls mismatch (1)");
      declare
         Sleep_Val : Duration := Mock.Sleep_Calls.First_Element;
      begin
         Assert (Condition => Sleep_Val >= 0.0
                 and Sleep_Val <= Duration (Constants.PROBE_WAIT),
                 Message   => "Unexpected initial probe interval");
         Mock.Sleep_Calls.Delete_First;

         Sleep_Val := Mock.Sleep_Calls.First_Element;
         Assert (Condition => Sleep_Val >= Duration (Constants.PROBE_MIN)
                 and Sleep_Val <= Duration (Constants.PROBE_MAX),
                 Message   => "Unexpected second probe interval");
         Mock.Sleep_Calls.Delete_First;

         Sleep_Val := Mock.Sleep_Calls.First_Element;
         Assert (Condition => Sleep_Val >= Duration (Constants.PROBE_MIN)
                 and Sleep_Val <= Duration (Constants.PROBE_MAX),
                 Message   => "Unexpected third probe interval");
         Mock.Sleep_Calls.Clear;
      end;

      --  Trigger collision.

      declare
         use type Ada.Streams.Stream_Element_Offset;

         Packet : Mock.Packet_Type;
         Buf    : constant Ada.Streams.Stream_Element_Array
           := Anet.ARP.To_Stream (Header => Collision_Pkt);
      begin
         Packet.Data (Buf'Range) := Buf;
         Packet.Last_Idx         := Buf'Last;
         Mock.Enqueue (Packet => Packet);

         Probe (IP_Address => Ref_IP_Addr,
                Collision  => Collision_Detected);
         Assert (Condition => Collision_Detected,
                 Message   => "Collision not detected (1)");
         Assert (Condition => Collision_Count = 1,
                 Message   => "Collision not counted (1)");
         Mock.Sleep_Calls.Clear;
      end;

      --  Trigger probing collision.

      declare
         use type Ada.Streams.Stream_Element_Offset;

         Packet : Mock.Packet_Type;
         Buf    : constant Ada.Streams.Stream_Element_Array
           := Anet.ARP.To_Stream (Header => Collision_Probe);
         Buf_2  : constant Ada.Streams.Stream_Element_Array (1 .. 28)
           := (others => 16#fe#);
      begin

         --  Invalid ARP message which must be ignored.

         Packet.Data (Buf_2'Range) := Buf_2;
         Packet.Last_Idx           := Buf_2'Last;
         Mock.Enqueue (Packet => Packet);

         Packet.Data (Buf'Range) := Buf;
         Packet.Last_Idx         := Buf'Last;

         Mock.Enqueue (Packet => Packet);

         Probe (IP_Address => Ref_IP_Addr,
                Collision  => Collision_Detected);
         Assert (Condition => Collision_Detected,
                 Message   => "Collision not detected (2)");
         Assert (Condition => Collision_Count = 2,
                 Message   => "Collision not counted (2)");
         Mock.Sleep_Calls.Clear;
      end;

      --  Rate limiting.

      Mock.Clear;
      declare
         use type Ada.Streams.Stream_Element_Offset;

         Sleep_Val : Duration;
         Packet    : Mock.Packet_Type;
         Buf       : constant Ada.Streams.Stream_Element_Array
           := Anet.ARP.To_Stream (Header => Collision_Probe);
      begin
         Packet.Data (Buf'Range) := Buf;
         Packet.Last_Idx         := Buf'Last;
         Mock.Enqueue (Packet => Packet);

         Collision_Count := Constants.MAX_CONFLICTS;

         Probe (IP_Address => Ref_IP_Addr,
                Collision  => Collision_Detected);
         Assert (Condition => Collision_Detected,
                 Message   => "Collision not detected (3)");
         Assert (Condition => Natural (Mock.Sleep_Calls.Length) = 3,
                 Message   => "Sleep calls mismatch (2)");

         Sleep_Val := Mock.Sleep_Calls.First_Element;

         Assert (Condition => Sleep_Val >= Duration
                 (Constants.RATE_LIMIT_INTERVAL) - 0.2
                 and Sleep_Val <= Duration
                   (Constants.RATE_LIMIT_INTERVAL) + 0.2,
                 Message   => "Unexpected rate limit probe interval"
                 & Sleep_Val'Img);
         Mock.Sleep_Calls.Clear;
      end;

      --  No collision -> reset collision count.

      Probe (IP_Address => Ref_IP_Addr,
             Collision  => Collision_Detected);
      Assert (Condition => not Collision_Detected,
              Message   => "Collision detected (2)");
      Assert (Condition => Collision_Count = 0,
              Message   => "Collision count not zero (2)");
      declare
         Sleep_Val : Duration := Mock.Sleep_Calls.First_Element;
      begin
         Assert (Condition => Sleep_Val >= Duration
                 (Constants.RATE_LIMIT_INTERVAL) - 0.2
                 and Sleep_Val <= Duration
                   (Constants.RATE_LIMIT_INTERVAL) + 0.2,
                 Message   => "Unexpected first probe interval (2)");
         Mock.Sleep_Calls.Delete_First;

         Sleep_Val := Mock.Sleep_Calls.First_Element;
         Assert (Condition => Sleep_Val >= Duration (Constants.PROBE_MIN)
                 and Sleep_Val <= Duration (Constants.PROBE_MAX),
                 Message   => "Unexpected second probe interval (2)");
         Mock.Sleep_Calls.Delete_First;

         Sleep_Val := Mock.Sleep_Calls.First_Element;
         Assert (Condition => Sleep_Val >= Duration (Constants.PROBE_MIN)
                 and Sleep_Val <= Duration (Constants.PROBE_MAX),
                 Message   => "Unexpected third probe interval (2)");
         Mock.Sleep_Calls.Clear;
      end;

      --  No collision with non-probe ARP message.

      Mock.Clear;
      declare
         use type Ada.Streams.Stream_Element_Offset;

         ARP_Reply : constant Anet.ARP.Header_Type
           := (Operation => Anet.ARP.ARP_Request,
               Src_Ether => (16#fe#, 16#fe#, 16#fe#, 16#fe#, 16#fe#, 16#fe#),
               Src_IP    => (192, 168, 0, 57),
               Dst_Ether => (others => 0),
               Dst_IP    => Ref_IP_Addr);

         Packet : Mock.Packet_Type;
         Buf    : constant Ada.Streams.Stream_Element_Array
           := Anet.ARP.To_Stream (Header => ARP_Reply);
      begin
         Packet.Data (Buf'Range) := Buf;
         Packet.Last_Idx         := Buf'Last;
         Mock.Enqueue (Packet => Packet);

         Probe (IP_Address => Ref_IP_Addr,
                Collision  => Collision_Detected);
         Assert (Condition => not Collision_Detected,
                 Message   => "Collision detected (3)");
         Assert (Condition => Collision_Count = 0,
                 Message   => "Collision count not zero (3)");
      end;
   end Probe;

end DHCPv4.ACD.Tests;
