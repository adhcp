--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;
with Ada.Strings.Unbounded;

private with Ada.Finalization;

package DHCPv4.Transmission.Mock
is

   type Sock_Cleaner is private;
   --  Transmission socket test cleanup helper.

   function Get_Last_Iface return String;
   --  Return name of last interface use to transmit a DHCP message using the
   --  mock sockets.

   procedure Clear;
   --  Clear transmission mock data.

   Send_Count    : Natural                           := 0;
   Last_Dst_IP   : Anet.IPv4_Addr_Type               := Anet.Any_Addr;
   Last_Dst_HW   : Anet.Hardware_Addr_Type (1 .. 6)  := (others => 0);
   Last_Dst_Port : Anet.Port_Type                    := 0;
   Last_Item     : Ada.Streams.Stream_Element_Array (1 .. 2048);
   Last_Idx      : Ada.Streams.Stream_Element_Offset := Last_Item'First;
   Last_Iface    : Ada.Strings.Unbounded.Unbounded_String;

private

   type Sock_Cleaner is new Ada.Finalization.Controlled with null record;

   overriding
   procedure Finalize (C : in out Sock_Cleaner);

   overriding
   procedure Initialize (C : in out Sock_Cleaner);

end DHCPv4.Transmission.Mock;
