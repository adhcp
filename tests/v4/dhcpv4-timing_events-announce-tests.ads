--
--  Copyright (C) 2018 secunet Security Networks AG
--  Copyright (C) 2018 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2018 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package DHCPv4.Timing_Events.Announce.Tests
is

   function Get_IP_Addr (Ev : Announce_Type) return Anet.IPv4_Addr_Type;
   --  Return IP address of given announce event.

   function Get_Count (Ev : Announce_Type) return Positive;
   --  Return announcement count of given announce event.

private

   function Get_IP_Addr (Ev : Announce_Type) return Anet.IPv4_Addr_Type
   is (Ev.IP_Address);

   function Get_Count (Ev : Announce_Type) return Positive
   is (Ev.Count);

end DHCPv4.Timing_Events.Announce.Tests;
