--
--  Copyright (C) 2011, 2015 secunet Security Networks AG
--  Copyright (C) 2011, 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package DHCP.Timing_Events.Mock
is

   protected type Counter_Type
   is

      function Value return Natural;
      --  Get current counter value.

      procedure Increment;
      --  Increment counter.

      entry Wait_For_Inc;
      --  Wait for increment of counter.

      procedure Clear;
      --  Reset counter to 0.

   private
      Count       : Natural := 0;
      Incremented : Boolean := False;
   end Counter_Type;

   Counter : Counter_Type;

   type Mock_Event_Type is new Event_Type with private;

   overriding
   procedure Trigger (Event : in out Mock_Event_Type);
   --  Increment trigger counter.

   type Failing_Event_Type (Process : not null access procedure) is
     new Mock_Event_Type with private;

   overriding
   procedure Trigger (Event : in out Failing_Event_Type);
   --  Execute process procedure.

private

   type Mock_Event_Type is new Event_Type with null record;

   type Failing_Event_Type (Process : not null access procedure) is
     new Mock_Event_Type with null record;

end DHCP.Timing_Events.Mock;
