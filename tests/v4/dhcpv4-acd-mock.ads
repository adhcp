--
--  Copyright (C) 2018 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2018 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Containers.Doubly_Linked_Lists;
with Ada.Streams;
with Ada.Strings.Unbounded;

private with Ada.Finalization;

package DHCPv4.ACD.Mock
is

   type Packet_Type is record
      Data     : Ada.Streams.Stream_Element_Array (1 .. 2048);
      Last_Idx : Ada.Streams.Stream_Element_Offset;
   end record;

   type Sock_Cleaner is private;
   --  ARP socket test cleanup helper.

   function Get_Last_Iface return String;
   --  Return name of last interface used to transmit a DHCP message via the
   --  mock socket.

   procedure Clear;
   --  Clear ARP mock data.

   procedure Enqueue (Packet : Packet_Type);
   --  Enqueue given packet on ARP mock socket for reception.

   Send_Count  : Natural := 0;
   Init_Count  : Natural := 0;
   Bind_Count  : Natural := 0;
   Close_Count : Natural := 0;

   Last_Dst_IP : Anet.IPv4_Addr_Type               := Anet.Any_Addr;
   Last_Dst_HW : Anet.Hardware_Addr_Type (1 .. 6)  := (others => 0);
   Last_Item   : Ada.Streams.Stream_Element_Array (1 .. 2048);
   Last_Idx    : Ada.Streams.Stream_Element_Offset := Last_Item'First;
   Last_Iface  : Ada.Strings.Unbounded.Unbounded_String;
   Bound_Iface : Ada.Strings.Unbounded.Unbounded_String;

   Non_Blocking : Boolean := False;

   package Sleep_Calls_Pkg is new Ada.Containers.Doubly_Linked_Lists
     (Element_Type => Duration);

   Sleep_Calls : Sleep_Calls_Pkg.List;

private

   type Sock_Cleaner is new Ada.Finalization.Controlled with null record;

   overriding
   procedure Finalize (C : in out Sock_Cleaner);

   overriding
   procedure Initialize (C : in out Sock_Cleaner);

end DHCPv4.ACD.Mock;
