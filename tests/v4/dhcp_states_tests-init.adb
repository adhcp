--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Tags;
with Ada.Real_Time;

with Anet;

with DHCP.States;
with DHCP.Timer.Test;
with DHCP.Timing_Events;
with DHCP.Notify.Mock;

with DHCPv4.Message;
with DHCPv4.Transactions.Mock;
with DHCPv4.States.Init;
with DHCPv4.States.Selecting;
with DHCPv4.Timing_Events.Bind_Timeout;

package body DHCP_States_Tests.Init is

   use Ahven;
   use DHCPv4;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DHCP client state 'Init'");
      T.Add_Test_Routine
        (Routine => Verify_Name_Getter'Access,
         Name    => "Name getter");
      T.Add_Test_Routine
        (Routine => Verify_Start'Access,
         Name    => "Start procedure");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Verify_Name_Getter
   is
   begin
      Assert (Condition => States.Init.State.Get_Name = "Init",
              Message   => "Name mismatch");
   end Verify_Name_Getter;

   -------------------------------------------------------------------------

   procedure Verify_Start
   is
      use type Anet.Word32;
      use type DHCP.States.State_Handle;
      use type DHCP.Notify.Reason_Type;
      use type DHCPv4.Message.Message_Kind;

      T : Transactions.Mock.Transaction_Type;
      C : DHCP.Notify.Mock.Notify_Cleaner;
      pragma Unreferenced (C);
   begin
      DHCP.Notify.Mock.Install;
      States.State_Type'Class (States.Init.State.all).Start
        (Transaction => T);

      Assert (Condition => T.ID /= 0,
              Message   => "XID is 0");
      Assert (Condition => T.State = States.Selecting.State,
              Message   => "New state not 'Selecting'");
      Assert (Condition => T.Last_Sent_Msg.Get_Kind = Message.Discover,
              Message   => "Message kind not Discover");
      Assert (Condition => T.Send_Count = 1,
              Message   => "Send count not 1");
      Assert (Condition => DHCP.Notify.Mock.Get_Last_Reason
              = DHCP.Notify.Preinit,
              Message   => "Notify reason not Preinit");

      declare
         use Ada.Real_Time;
         use DHCP.Timing_Events;
         use DHCPv4.Timing_Events;
         use type Ada.Tags.Tag;

         Timeout : constant Event_Type'Class := DHCP.Timer.Test.Get_Next_Event;
      begin
         Assert (Condition => Timeout'Tag = Bind_Timeout.Timeout_Type'Tag,
                 Message   => "No bind timeout event");
         Assert (Condition => Timeout.Get_Time =
                   T.Start_Time + Minutes (M => 1),
                 Message   => "Bind timeout incorrect");
      end;
   end Verify_Start;

end DHCP_States_Tests.Init;
