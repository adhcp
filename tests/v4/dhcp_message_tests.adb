--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;
with Ada.Unchecked_Conversion;

with Anet;

with DHCPv4.Options;
with DHCPv4.Message.Thin;

package body DHCP_Message_Tests is

   use Ahven;
   use DHCPv4;
   use DHCPv4.Message;

   generic
      type Enumeration_Type is (<>);
   procedure Validate (Offset : Ada.Streams.Stream_Element_Offset);
   --  This procedure assigns all numeric values of the given enumeration
   --  type to the raw message buffer element at the given offset. It then
   --  calls DHCP.Thin.Validate for the resulting raw message.
   --  It also verifies that an exception is raised for values outside the
   --  enumeration type's range.

   DHCP_Ack : constant Ada.Streams.Stream_Element_Array
     := (16#02#, 16#13#, 16#06#, 16#0c#, 16#9e#, 16#eb#, 16#b7#, 16#66#,
         16#ca#, 16#fe#, 16#80#, 16#00#, 16#0a#, 16#38#, 16#15#, 16#f2#,
         16#c0#, 16#a8#, 16#ef#, 16#77#, 16#c0#, 16#a8#, 16#ef#, 16#01#,
         16#ac#, 16#18#, 16#6b#, 16#0c#, 16#00#, 16#22#, 16#fb#, 16#52#,
         16#8b#, 16#f8#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#74#, 16#65#, 16#73#, 16#74#,
         16#73#, 16#65#, 16#72#, 16#76#, 16#65#, 16#72#, 16#2e#, 16#65#,
         16#78#, 16#61#, 16#6d#, 16#70#, 16#6c#, 16#65#, 16#2e#, 16#63#,
         16#68#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#70#, 16#78#, 16#65#, 16#6c#,
         16#69#, 16#6e#, 16#75#, 16#78#, 16#2e#, 16#30#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#63#, 16#82#, 16#53#, 16#63#,
         16#35#, 16#01#, 16#05#, 16#36#, 16#04#, 16#c0#, 16#a8#, 16#ef#,
         16#01#, 16#33#, 16#04#, 16#00#, 16#01#, 16#51#, 16#80#, 16#3a#,
         16#04#, 16#00#, 16#00#, 16#a8#, 16#c0#, 16#3b#, 16#04#, 16#00#,
         16#01#, 16#27#, 16#50#, 16#01#, 16#04#, 16#ff#, 16#ff#, 16#ff#,
         16#00#, 16#03#, 16#04#, 16#c0#, 16#a8#, 16#ef#, 16#01#, 16#06#,
         16#08#, 16#50#, 16#fe#, 16#a1#, 16#7e#, 16#50#, 16#fe#, 16#a1#,
         16#7d#, 16#0f#, 16#0b#, 16#73#, 16#77#, 16#69#, 16#73#, 16#73#,
         16#2d#, 16#69#, 16#74#, 16#2e#, 16#63#, 16#68#, 16#ff#);
   --  Hex stream of reference DHCP Ack bootp message.

   -------------------------------------------------------------------------

   procedure Add_Option_To_Msg
   is
      use type DHCPv4.Options.Inst.Option_Type'Class;

      Msg     : Message_Type;
      Opts    : Options.Inst4.Option_List;
      Ref_Opt : constant Options.Inst.Option_Type'Class
        := Options.Inst.Create
          (Name => Options.Domain_Name,
           Data => (1 => 32));
   begin
      Opts := Msg.Get_Options;
      Assert (Condition => Opts.Is_Empty,
              Message   => "List not empty");

      Msg.Add_Option (Opt => Ref_Opt);

      Opts := Msg.Get_Options;
      Assert (Condition => Opts.Get_Count = 1,
              Message   => "Opts count not 1");

      Assert (Condition => Opts.Get (Name => Options.Domain_Name) =  Ref_Opt,
              Message   => "Option mismatch");
   end Add_Option_To_Msg;

   -------------------------------------------------------------------------

   procedure Create_Message
   is
   begin
      for I in Message_Kind loop
         declare
            Msg : constant Message_Type := Create (Kind => I);
         begin
            Assert (Condition => Msg.Get_Kind = I,
                    Message   => "Message kind mismatch");

            case I is
               when Discover | Request | Decline | Release | Inform =>
                  Assert (Condition => Msg.Get_Opcode = Boot_Request,
                          Message   => "Opcode is not request");
               when Offer | Acknowledge | Not_Acknowledge =>
                  Assert (Condition => Msg.Get_Opcode = Boot_Reply,
                          Message   => "Opcode is not reply");
            end case;
         end;
      end loop;
   end Create_Message;

   -------------------------------------------------------------------------

   procedure Deserialize_Msg_From_Stream
   is
      use Anet;

      Msg : Message_Type;
   begin
      Msg := Deserialize (Buffer => DHCP_Ack);

      Assert (Condition => Msg.Get_Opcode = Boot_Reply,
              Message   => "Opcode not boot reply");
      Assert (Condition => Msg.Get_Hops = 12,
              Message   => "Hop count not 12");
      Assert (Condition => Msg.Get_Link_Hardware = ATM,
              Message   => "Hardware not ATM");
      Assert (Condition => Msg.Get_HW_Addr_Length = 6,
              Message   => "Hardware addr len not 6");
      Assert (Condition => Msg.Get_Transaction_ID = 16#9eebb766#,
              Message   => "Transaction ID mismatch");
      Assert (Condition => Msg.Get_Seconds = 51966,
              Message   => "Seconds mismatch");
      Assert (Condition => Msg.Has_Broadcast_Set,
              Message   => "Broadcast flag is not set");

      Assert (Condition => Msg.Get_Client_IP = Anet.IPv4_Addr_Type'
                (10, 56, 21, 242),
              Message   => "Client IP is wrong");
      Assert (Condition => Msg.Get_Your_IP = Anet.IPv4_Addr_Type'
                (192, 168, 239, 119),
              Message   => "Your IP is wrong");
      Assert (Condition => Msg.Get_Next_Server_IP = Anet.IPv4_Addr_Type'
                (192, 168, 239, 1),
              Message   => "Server IP is wrong");
      Assert (Condition => Msg.Get_Gateway_IP = Anet.IPv4_Addr_Type'
                (172, 24, 107, 12),
              Message   => "Gateway IP is wrong");
      Assert (Condition => Msg.Get_Server_ID = Anet.IPv4_Addr_Type'
                (192, 168, 239, 1),
              Message   => "Server ID is wrong");

      declare
         Ref_HW_Addr : constant Anet.Hardware_Addr_Type
           := (16#00#, 16#22#, 16#fb#, 16#52#, 16#8b#, 16#f8#);
      begin
         Assert (Condition => Msg.Get_Hardware_Address = Ref_HW_Addr,
                 Message   => "Hardware address wrong");
      end;

      Assert (Condition => Msg.Get_Server_Name = "testserver.example.ch",
              Message   => "Servername is wrong");
      Assert (Condition => Msg.Get_Boot_File_Name = "pxelinux.0",
              Message   => "Boot filename is wrong");

      Assert (Condition => Msg.Get_Options.Get_Count = 9,
              Message   => "Option count not 9");
   end Deserialize_Msg_From_Stream;

   -------------------------------------------------------------------------

   procedure Deserialize_Msg_Invalid_Buffer
   is
      Too_Short1 : constant Ada.Streams.Stream_Element_Array
        := (16#02#, 16#13#);
      Too_Short2 : constant Ada.Streams.Stream_Element_Array (3 .. 237)
        := (others => 16#ff#);
      Message    : Message_Type;
      pragma Unreferenced (Message);
   begin
      begin
         Message := Deserialize (Buffer => Too_Short1);
         Fail (Message => "Invalid msg error expected");

      exception
         when Invalid_Message => null;
      end;

      begin
         Message := Deserialize (Buffer => Too_Short2);
         Fail (Message => "Invalid msg error expected");

      exception
         when Invalid_Message => null;
      end;
   end Deserialize_Msg_Invalid_Buffer;

   -------------------------------------------------------------------------

   procedure Deserialize_Msg_Invalid_Options
   is
      use Ada.Streams;

      Stream : Stream_Element_Array := DHCP_Ack;
      Msg    : Message_Type;
   begin

      --  Invalidate option cookie

      Stream (DHCP_Ack'First + 238) := 0;

      begin
         Msg := Deserialize (Buffer => Stream);
         Fail (Message => "Invalid message expected");

      exception
         when Invalid_Message => null;
      end;

      Stream := DHCP_Ack;

      --  Invalidate an option

      declare
         Opt_Count : constant Natural
           := Deserialize (Buffer => Stream).Get_Options.Get_Count;
      begin
         Stream (DHCP_Ack'First + 243) := 22;
         Msg := Deserialize (Buffer => Stream);
         Assert (Condition => Msg.Get_Options.Get_Count = Opt_Count - 1,
                 Message   => "Invalid message option not skipped");
      end;
   end Deserialize_Msg_Invalid_Options;

   -------------------------------------------------------------------------

   procedure Increment_Hop_Count
   is
      Msg : Message_Type;
   begin
      Assert (Condition => Msg.Get_Hops = 0,
              Message   => "Hop count not 0");

      for I in Hops_Type'First .. Hops_Type'Last - 1 loop
         Msg.Inc_Hops;
         Assert (Condition => Msg.Get_Hops = I + 1,
                 Message   => "Hop count not" & Hops_Type'Image (I + 1));
      end loop;

      begin
         Msg.Inc_Hops;
         Fail (Message => "Invalid message error expected");

      exception
         when Invalid_Message => null;
      end;
   end Increment_Hop_Count;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Message package");
      T.Add_Test_Routine
        (Routine => Raw_Validate_Operation_Code'Access,
         Name    => "Validate raw message: Op");
      T.Add_Test_Routine
        (Routine => Raw_Validate_HType'Access,
         Name    => "Validate raw message: HType");
      T.Add_Test_Routine
        (Routine => Raw_Validate_Hlen'Access,
         Name    => "Validate raw message: Hlen");
      T.Add_Test_Routine
        (Routine => Raw_Validate_Hops_Count'Access,
         Name    => "Validate raw message: Hops");
      T.Add_Test_Routine
        (Routine => Deserialize_Msg_From_Stream'Access,
         Name    => "Deserialize message");
      T.Add_Test_Routine
        (Routine => Deserialize_Msg_Invalid_Buffer'Access,
         Name    => "Deserialize message (invalid buffer)");
      T.Add_Test_Routine
        (Routine => Deserialize_Msg_Invalid_Options'Access,
         Name    => "Deserialize message (invalid options)");
      T.Add_Test_Routine
        (Routine => Serialize_Msg_To_Stream'Access,
         Name    => "Serialize message");
      T.Add_Test_Routine
        (Routine => Set_Get_Gateway_IP'Access,
         Name    => "Set/get gateway IP address");
      T.Add_Test_Routine
        (Routine => Increment_Hop_Count'Access,
         Name    => "Increment hop count");
      T.Add_Test_Routine
        (Routine => Create_Message'Access,
         Name    => "Create message");
      T.Add_Test_Routine
        (Routine => Set_Get_Broadcast_Flag'Access,
         Name    => "Set/get broadcast flag");
      T.Add_Test_Routine
        (Routine => Set_Get_Hardware_Address'Access,
         Name    => "Set/get hardware address");
      T.Add_Test_Routine
        (Routine => Set_Get_XID'Access,
         Name    => "Set/get XID");
      T.Add_Test_Routine
        (Routine => Add_Option_To_Msg'Access,
         Name    => "Add option to message");
      T.Add_Test_Routine
        (Routine => Set_Get_Gateway_IP'Access,
         Name    => "Set/get client IP address");
      T.Add_Test_Routine
        (Routine => Set_Get_Your_IP'Access,
         Name    => "Set/get 'your IP' address");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Raw_Validate_Hlen
   is
      procedure Validate_Hlen is new Validate
        (Enumeration_Type => Anet.HW_Addr_Len_Type);
   begin
      Validate_Hlen (Offset => 3);
   end Raw_Validate_Hlen;

   -------------------------------------------------------------------------

   procedure Raw_Validate_Hops_Count
   is
      procedure Validate_Hops is new Validate
        (Enumeration_Type => Hops_Type);
   begin
      Validate_Hops (Offset => 4);
   end Raw_Validate_Hops_Count;

   -------------------------------------------------------------------------

   procedure Raw_Validate_HType
   is
      procedure Validate_HType is new Validate
        (Enumeration_Type => Hardware_Type);
   begin
      Validate_HType (Offset => 2);
   end Raw_Validate_HType;

   -------------------------------------------------------------------------

   procedure Raw_Validate_Operation_Code
   is
      procedure Validate_Op is new Validate
        (Enumeration_Type => Opcode_Type);
   begin
      Validate_Op (Offset => 1);
   end Raw_Validate_Operation_Code;

   -------------------------------------------------------------------------

   procedure Serialize_Msg_To_Stream
   is
      use type Ada.Streams.Stream_Element_Array;

      Msg : constant Message_Type := Deserialize (Buffer => DHCP_Ack);
   begin
      Assert (Condition => Msg.Serialize = DHCP_Ack,
              Message   => "Stream mismatch");
   end Serialize_Msg_To_Stream;

   -------------------------------------------------------------------------

   procedure Set_Get_Broadcast_Flag
   is
      Msg : Message_Type;
   begin
      Assert (Condition => not Msg.Has_Broadcast_Set,
              Message   => "Broadcast is set by default");

      Msg.Set_Broadcast_Flag (State => True);
      Assert (Condition => Msg.Has_Broadcast_Set,
              Message   => "Broadcast is not set");

      Msg.Set_Broadcast_Flag (State => False);
      Assert (Condition => not Msg.Has_Broadcast_Set,
              Message   => "Broadcast is set");
   end Set_Get_Broadcast_Flag;

   -------------------------------------------------------------------------

   procedure Set_Get_Client_IP
   is
      use type Anet.IPv4_Addr_Type;

      Msg    : Message_Type;
      Ref_IP : constant Anet.IPv4_Addr_Type := (192, 168, 1, 42);
   begin
      Msg.Set_Client_IP (IP => Ref_IP);
      Assert (Condition => Msg.Get_Client_IP = Ref_IP,
              Message   => "Client IP mismatch");
   end Set_Get_Client_IP;

   -------------------------------------------------------------------------

   procedure Set_Get_Gateway_IP
   is
      use type Anet.IPv4_Addr_Type;

      Msg    : Message_Type;
      Ref_IP : constant Anet.IPv4_Addr_Type := (192, 168, 1, 12);
   begin
      Msg.Set_Gateway_IP (IP => Ref_IP);
      Assert (Condition => Msg.Get_Gateway_IP = Ref_IP,
              Message   => "Gateway IP mismatch");
   end Set_Get_Gateway_IP;

   -------------------------------------------------------------------------

   procedure Set_Get_Hardware_Address
   is
      use type Anet.Hardware_Addr_Type;

      Msg          : Message_Type;
      Ref_Def_Addr : constant Anet.Hardware_Addr_Type
        (1 .. Msg.Get_HW_Addr_Length) := (others => 0);
      Ref_HW_Addr  : constant Anet.Hardware_Addr_Type
        := (16#00#, 16#24#, 16#e8#, 16#93#, 16#bd#, 16#75#);
   begin
      Assert (Condition => Msg.Get_Hardware_Address = Ref_Def_Addr,
              Message   => "Default hardware address mismatch");

      Msg.Set_Hardware_Address (Address => Ref_HW_Addr);
      Assert (Condition => Msg.Get_Hardware_Address = Ref_HW_Addr,
              Message   => "Hardware address mismatch");
   end Set_Get_Hardware_Address;

   -------------------------------------------------------------------------

   procedure Set_Get_XID
   is
      use type Anet.Word32;

      Msg : Message_Type;
   begin
      Assert (Condition => Msg.Get_Transaction_ID = 0,
              Message   => "Default XID mismatch");

      Msg.Set_Transaction_ID (XID => 12345);
      Assert (Condition => Msg.Get_Transaction_ID = 12345,
              Message   => "XID not 12345");
   end Set_Get_XID;

   -------------------------------------------------------------------------

   procedure Set_Get_Your_IP
   is
      use type Anet.IPv4_Addr_Type;

      Msg : Message_Type;
   begin
      Assert (Condition => Msg.Get_Your_IP = Anet.Any_Addr,
              Message   => "Default address mismatch");

      Msg.Set_Your_IP (IP => Anet.Bcast_Addr);
      Assert (Condition => Msg.Get_Your_IP = Anet.Bcast_Addr,
              Message   => "Your IP mismatch");
   end Set_Get_Your_IP;

   -------------------------------------------------------------------------

   procedure Validate (Offset : Ada.Streams.Stream_Element_Offset)
   is
      use type Ada.Streams.Stream_Element;

      function Convert is new Ada.Unchecked_Conversion
        (Source => Enumeration_Type,
         Target => Ada.Streams.Stream_Element);

      Message : Thin.Raw_Header_Type;
      Buffer  : Thin.Raw_Header_Buffer_Type;
      for Buffer'Address use Message'Address;
   begin

      --  'Default' initialize test message

      Message.Op    := Boot_Request;
      Message.HType := Ethernet;
      Message.Hlen  := 6;
      Message.Hops  := 0;

      for I in Enumeration_Type'Range loop
         Buffer (Offset) := Convert (S => I);
         Thin.Validate (Item => Message);
      end loop;

      begin
         Buffer (Offset) := Convert (S => Enumeration_Type'First) - 1;
         Thin.Validate (Item => Message);

         Fail (Message => "Exception expected for value"
               & Buffer (Offset)'Img);

      exception
         when Invalid_Message => null;
      end;

      begin
         Buffer (Offset) := Convert (S => Enumeration_Type'Last) + 1;
         Thin.Validate (Item => Message);

         Fail (Message => "Exception expected for value"
               & Buffer (Offset)'Img);

      exception
         when Invalid_Message => null;
      end;
   end Validate;

end DHCP_Message_Tests;
