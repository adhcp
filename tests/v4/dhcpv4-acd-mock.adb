--
--  Copyright (C) 2018 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2018 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet.Types;

package body DHCPv4.ACD.Mock
is

   package ASP renames Anet.Sockets.Packet;

   package Packet_List is new Ada.Containers.Doubly_Linked_Lists
     (Element_Type => Packet_Type);

   type Packet_Socket_Type is new
     ASP.UDP_Socket_Type with null record;

   overriding
   procedure Receive
     (Socket :     Packet_Socket_Type;
      Item   : out Ada.Streams.Stream_Element_Array;
      Last   : out Ada.Streams.Stream_Element_Offset);

   overriding
   procedure Send
     (Socket : Packet_Socket_Type;
      Item   : Ada.Streams.Stream_Element_Array;
      To     : Anet.Hardware_Addr_Type;
      Iface  : Anet.Types.Iface_Name_Type);

   overriding
   procedure Init
     (Socket   : in out Packet_Socket_Type;
      Protocol :        ASP.Protocol_Type := ASP.Proto_Packet_Ip);

   overriding
   procedure Bind
     (Socket : in out Packet_Socket_Type;
      Iface  :        Anet.Types.Iface_Name_Type);

   overriding
   procedure Close (Socket : in out Packet_Socket_Type);

   overriding
   procedure Set_Nonblocking_Mode
     (Socket : Packet_Socket_Type;
      Enable : Boolean := True);

   PS : aliased Packet_Socket_Type;

   Receive_Queue : Packet_List.List;

   -------------------------------------------------------------------------

   procedure Bind
     (Socket : in out Packet_Socket_Type;
      Iface  :        Anet.Types.Iface_Name_Type)
   is
      pragma Unreferenced (Socket);
   begin
      Bind_Count := Bind_Count + 1;
      Bound_Iface := Ada.Strings.Unbounded.To_Unbounded_String
        (String (Iface));
   end Bind;

   -------------------------------------------------------------------------

   procedure Clear
   is
   begin
      Send_Count  := 0;
      Bind_Count  := 0;
      Init_Count  := 0;
      Close_Count := 0;
      Last_Dst_IP := Anet.Any_Addr;
      Last_Item   := (others => 0);
      Last_Idx    := Last_Item'First;
      Receive_Queue.Clear;
   end Clear;

   -------------------------------------------------------------------------

   procedure Close (Socket : in out Packet_Socket_Type)
   is
      pragma Unreferenced (Socket);
   begin
      Close_Count := Close_Count + 1;
   end Close;

   -------------------------------------------------------------------------

   procedure Enqueue (Packet : Packet_Type)
   is
   begin
      Receive_Queue.Append (New_Item => Packet);
   end Enqueue;

   -------------------------------------------------------------------------

   procedure Finalize (C : in out Sock_Cleaner)
   is
      pragma Unreferenced (C);
   begin
      Clear;
      Initialized := False;
      Sock_ARP := Packet_Socket'Access;
      Iface_Info := (Addrtype => DHCP.Net.Link_Layer,
                     others => <>);
      Sleep_Calls.Clear;
   end Finalize;

   -------------------------------------------------------------------------

   function Get_Last_Iface return String
   is (Ada.Strings.Unbounded.To_String (Last_Iface));

   -------------------------------------------------------------------------

   procedure Init
     (Socket   : in out Packet_Socket_Type;
      Protocol :        ASP.Protocol_Type := ASP.Proto_Packet_Ip)
   is
      pragma Unreferenced (Socket, Protocol);
   begin
      Init_Count := Init_Count + 1;
   end Init;

   -------------------------------------------------------------------------

   procedure Initialize (C : in out Sock_Cleaner)
   is
      pragma Unreferenced (C);
   begin
      Sock_ARP := PS'Access;
   end Initialize;

   -------------------------------------------------------------------------

   procedure Receive
     (Socket :     Packet_Socket_Type;
      Item   : out Ada.Streams.Stream_Element_Array;
      Last   : out Ada.Streams.Stream_Element_Offset)
   is
      pragma Unreferenced (Socket);

      use type Ada.Streams.Stream_Element_Offset;
   begin
      if not Receive_Queue.Is_Empty then
         declare
            Cur_Pkt : constant Packet_Type := Receive_Queue.First_Element;
         begin
            if Cur_Pkt.Last_Idx > 0 then
               Last := Item'First + (Cur_Pkt.Last_Idx - Cur_Pkt.Data'First);
               Item (Item'First .. Last)
                 := Cur_Pkt.Data (Cur_Pkt.Data'First .. Cur_Pkt.Last_Idx);
            end if;
            Last := Cur_Pkt.Last_Idx;
            Receive_Queue.Delete_First;
         end;
      else
         Last := 0;
      end if;
   end Receive;

   -------------------------------------------------------------------------

   procedure Send
     (Socket : Packet_Socket_Type;
      Item   : Ada.Streams.Stream_Element_Array;
      To     : Anet.Hardware_Addr_Type;
      Iface  : Anet.Types.Iface_Name_Type)
   is
      use type Ada.Streams.Stream_Element_Offset;

      pragma Unreferenced (Socket);
   begin
      Last_Dst_IP := (1 => Anet.Byte (Item (17)),
                      2 => Anet.Byte (Item (18)),
                      3 => Anet.Byte (Item (19)),
                      4 => Anet.Byte (Item (20)));

      Last_Item (Last_Item'First .. Item'Length) := Item;

      Send_Count  := Send_Count + 1;
      Last_Idx    := Item'Length;
      Last_Dst_HW := To;
      Last_Iface  := Ada.Strings.Unbounded.To_Unbounded_String
        (String (Iface));
   end Send;

   -------------------------------------------------------------------------

   procedure Set_Nonblocking_Mode
     (Socket : Packet_Socket_Type;
      Enable : Boolean := True)
   is
      pragma Unreferenced (Socket);
   begin
      Non_Blocking := Enable;
   end Set_Nonblocking_Mode;

end DHCPv4.ACD.Mock;
