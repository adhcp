--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet;

with DHCPv4;

package body DHCP_Tests is

   use Ahven;
   use DHCPv4;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DHCP types");
      T.Add_Test_Routine
        (Routine => String_To_Server_Name'Access,
         Name    => "String to server name conversion");
      T.Add_Test_Routine
        (Routine => String_To_File_Name'Access,
         Name    => "String to file name conversion");
   end Initialize;

   -------------------------------------------------------------------------

   procedure String_To_File_Name
   is
      use type Anet.Byte_Array;

      F1     : constant File_Name_Type  := (97, 98, 99, 65, others => 0);
      F1_Str : constant String          := "abcA";
      F2_Str : constant String (2 .. 5) := "abcA";
      F3     : constant File_Name_Type  := (others => 0);
      F3_Str : constant String          := "";
      F4_Str : constant String (1 .. File_Name_Type'Length + 1)
        := (others => 'F');
   begin
      Assert (Condition => To_File_Name (Str => F1_Str) = F1,
              Message   => "File name mismatch1");
      Assert (Condition => To_File_Name (Str => F2_Str) = F1,
              Message   => "File name mismatch2");
      Assert (Condition => To_File_Name (Str => F3_Str) = F3,
              Message   => "File name mismatch3");

      begin
         declare
            F : constant File_Name_Type := To_File_Name (Str => F4_Str);
            pragma Unreferenced (F);
         begin
            Fail ("Expected conversion error");
         end;

      exception
         when Conversion_Error => null;
      end;
   end String_To_File_Name;

   -------------------------------------------------------------------------

   procedure String_To_Server_Name
   is
      use type Anet.Byte_Array;

      S1     : constant Server_Name_Type := (97, 98, 99, 65, others => 0);
      S1_Str : constant String           := "abcA";
      S2_Str : constant String (2 .. 5)  := "abcA";
      S3     : constant Server_Name_Type := (others => 0);
      S3_Str : constant String           := "";
      S4_Str : constant String (1 .. Server_Name_Type'Length + 1)
        := (others => 'C');

   begin
      Assert (Condition => To_Server_Name (Str => S1_Str) = S1,
              Message   => "Server name mismatch1");
      Assert (Condition => To_Server_Name (Str => S2_Str) = S1,
              Message   => "Server name mismatch2");
      Assert (Condition => To_Server_Name (Str => S3_Str) = S3,
              Message   => "Server name mismatch3");

      begin
         declare
            S : constant Server_Name_Type := To_Server_Name (Str => S4_Str);
            pragma Unreferenced (S);
         begin
            Fail ("Expected conversion error");
         end;

      exception
         when Conversion_Error => null;
      end;
   end String_To_Server_Name;

end DHCP_Tests;
