--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet;

with DHCP.States;

with DHCPv4.Transactions.Mock;
with DHCPv4.Message;
with DHCPv4.States.Bound;
with DHCPv4.States.Renewing;
with DHCPv4.Database.Mock;

package body DHCP_States_Tests.Bound is

   use Ahven;
   use DHCPv4;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DHCP client state 'Bound'");
      T.Add_Test_Routine
        (Routine => Verify_Name_Getter'Access,
         Name    => "Name getter");
      T.Add_Test_Routine
        (Routine => Verify_Process_T1_Expiry'Access,
         Name    => "T1 expiration handling");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Verify_Name_Getter
   is
   begin
      Assert (Condition => States.Bound.State.Get_Name = "Bound",
              Message   => "Name mismatch");
   end Verify_Name_Getter;

   -------------------------------------------------------------------------

   procedure Verify_Process_T1_Expiry
   is
      use type Anet.IPv4_Addr_Type;
      use type DHCP.States.State_Handle;
      use type DHCPv4.Message.Message_Kind;

      Dummy : DHCPv4.Database.Mock.Cleaner;
      T     : Transactions.Mock.Transaction_Type;
   begin
      Database.Set_Fixed_Address (Addr => (192, 168, 239, 1));
      T.Set_Server_Address (Address => Anet.To_IPv4_Addr ("192.168.1.1"));
      States.State_Type'Class (States.Bound.State.all).Process_T1_Expiry
        (Transaction => T);

      Assert (Condition => T.State = States.Renewing.State,
              Message   => "New state not 'Renewing'");
      Assert (Condition => T.Last_Sent_Msg.Get_Kind = Message.Request,
              Message   => "Message kind not Request");
      Assert (Condition => T.Last_Sent_Msg.Get_Client_IP = (192, 168, 239, 1),
              Message   => "Client IP mismatch");
      Assert (Condition => T.Send_Count = 1,
              Message   => "Send count not 1");
      Assert (Condition => T.Last_Dst = T.Get_Server_Address,
              Message   => "Msg dst mismatch");
   end Verify_Process_T1_Expiry;

end DHCP_States_Tests.Bound;
