--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;
with Ada.Command_Line;

with System.Assertions;

with DHCP.Timer.Test;
with DHCP.Timing_Events.Mock;
with DHCP.Termination;

package body DHCP_Timer_Tests
is

   use Ahven;
   use DHCP;

   -------------------------------------------------------------------------

   procedure Cancel_Events
   is
      use Ada.Real_Time;

      Ev    : Timing_Events.Mock.Mock_Event_Type;
      Dummy : Timer.Test.Cleaner_Type;
   begin
      Timer.Start;
      Ev.Set_Time (At_Time => Clock + Milliseconds (100));

      Timer.Schedule (Event => Ev);
      Timer.Cancel (Event => Ev);

      delay 0.2;

      Assert (Condition => Timing_Events.Mock.Counter.Value /= 1,
              Message   => "Event triggered");
   end Cancel_Events;

   -------------------------------------------------------------------------

   procedure Clear_Timer
   is
      use Ada.Real_Time;

      Ev    : Timing_Events.Mock.Mock_Event_Type;
      Dummy : Timer.Test.Cleaner_Type;
   begin
      Timer.Start;
      Ev.Set_Time (At_Time => Clock + Milliseconds (100));

      Timer.Schedule (Event => Ev);
      Timer.Clear;

      delay 0.2;

      Assert (Condition => Timing_Events.Mock.Counter.Value /= 1,
              Message   => "Event triggered");
   end Clear_Timer;

   -------------------------------------------------------------------------

   procedure Error_Handling
   is
      use type Ada.Command_Line.Exit_Status;

      procedure Raise_Assertion_Error;
      procedure Raise_Constraint_Error;

      ----------------------------------------------------------------------

      procedure Raise_Assertion_Error
      is
      begin
         raise System.Assertions.Assert_Failure with "DO NOT PANIC: Explicit "
           & "raise by failing mock event";
      end Raise_Assertion_Error;

      ----------------------------------------------------------------------

      procedure Raise_Constraint_Error
      is
      begin
         raise Constraint_Error with "DO NOT PANIC: Explicit raise by failing"
           & " mock event";
      end Raise_Constraint_Error;

      ----------------------------------------------------------------------

      Ev_Fail_A : Timing_Events.Mock.Failing_Event_Type
        (Process => Raise_Assertion_Error'Access);
      Ev_Fail_C : Timing_Events.Mock.Failing_Event_Type
        (Process => Raise_Constraint_Error'Access);
      Ev        : Timing_Events.Mock.Mock_Event_Type;

      Dummy : Timer.Test.Cleaner_Type;
   begin
      Timer.Start;
      Ev_Fail_A.Set_Time (At_Time => Ada.Real_Time.Clock);
      Ev_Fail_C.Set_Time (At_Time => Ada.Real_Time.Clock);
      Ev.Set_Time (At_Time => Ada.Real_Time.Clock);

      Timer.Schedule (Event => Ev_Fail_A);
      Timer.Schedule (Event => Ev);

      declare
         Status : Ada.Command_Line.Exit_Status := Ada.Command_Line.Success;
      begin
         select
            delay 0.5;
         then abort
            Status := Termination.Wait;
         end select;
         Assert (Condition => Status = Ada.Command_Line.Success,
                 Message   => "Termination signaled on Assertion error");
      end;

      Timer.Schedule (Event => Ev_Fail_C);

      declare
         Status : Ada.Command_Line.Exit_Status := Ada.Command_Line.Success;
      begin
         select
            delay 0.5;
         then abort
            Status := Termination.Wait;
         end select;
         Assert (Condition => Status = Ada.Command_Line.Failure,
                 Message   => "Termination not signaled on error");
      end;

      select
         Timing_Events.Mock.Counter.Wait_For_Inc;
      or
         delay 1.0;
      end select;

      Assert (Condition => Timing_Events.Mock.Counter.Value = 1,
              Message   => "Event not triggered");
   end Error_Handling;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for timer package");
      T.Add_Test_Routine
        (Routine => Schedule_Events'Access,
         Name    => "Schedule events");
      T.Add_Test_Routine
        (Routine => Cancel_Events'Access,
         Name    => "Cancel events");
      T.Add_Test_Routine
        (Routine => Clear_Timer'Access,
         Name    => "Clear timer");
      T.Add_Test_Routine
        (Routine => Error_Handling'Access,
         Name    => "Handle error");
      T.Add_Test_Routine
        (Routine => Stop_Timer'Access,
         Name    => "Stop timer");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Schedule_Events
   is
      use Ada.Real_Time;

      Ev    : Timing_Events.Mock.Mock_Event_Type;
      Dummy : Timer.Test.Cleaner_Type;
   begin
      Timer.Start;
      Assert (Condition => Timer.Event_Count = 0,
              Message   => "Event count not zero");
      Ev.Set_Time (At_Time => Clock + Milliseconds (MS => 300));

      Timer.Schedule (Event => Ev);
      Assert (Condition => Timer.Event_Count = 1,
              Message   => "Event count not 1");

      select
         Timing_Events.Mock.Counter.Wait_For_Inc;
      or
         delay 1.0;
      end select;

      Assert (Condition => Timing_Events.Mock.Counter.Value = 1,
              Message   => "Event did not trigger");
   end Schedule_Events;

   -------------------------------------------------------------------------

   procedure Stop_Timer
   is
      Ev    : Timing_Events.Mock.Mock_Event_Type;
      Dummy : Timer.Test.Cleaner_Type;
   begin
      Timer.Start;
      Ev.Set_Time (At_Time => Ada.Real_Time.Time_Last);
      Timer.Schedule (Event => Ev);

      Timer.Stop;
      Assert (Condition => Timer.Event_Count = 0,
              Message   => "Stopped Timer has events");
   end Stop_Timer;

end DHCP_Timer_Tests;
