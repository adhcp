--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet;

package body DHCP.Socket_Callbacks.Tests
is

   use Ahven;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Socket_Callbacks package");
      T.Add_Test_Routine
        (Routine => Receive_Error_Handler_Callback'Access,
         Name    => "Receive error handler callback");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Receive_Error_Handler_Callback
   is
      Non_Fatal_Test_Exception : exception;

      Stop : Boolean := False;
   begin
      begin
         raise Non_Fatal_Test_Exception with "Don't panic - test error";

      exception
         when E : others =>
            Handle_Receive_Error (E         => E,
                                  Stop_Flag => Stop);
            Assert (Condition => not Stop,
                    Message   => "Stop signaled on non-fatal exception");
      end;

      begin
         raise Anet.Socket_Error with "Don't panic - test error";

      exception
         when E : others =>
            Handle_Receive_Error (E         => E,
                                  Stop_Flag => Stop);
            Assert (Condition => Stop,
                    Message   => "Termination not signaled");
      end;
   end Receive_Error_Handler_Callback;

end DHCP.Socket_Callbacks.Tests;
