--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCP.Utils.Tests
is

   use Ahven;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Utils package");
      T.Add_Test_Routine
        (Routine => Strip'Access,
         Name    => "Strip strings");
      T.Add_Test_Routine
        (Routine => To_Array'Access,
         Name    => "Value to stream element array conversion");
      T.Add_Test_Routine
        (Routine => To_Value'Access,
         Name    => "Stream element array to value conversion");
      T.Add_Test_Routine
        (Routine => To_String'Access,
         Name    => "Stream element array to string conversion");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Strip
   is
      S1 : constant String := "97CE2153-71C9-E011-B270-35E471056552";
      R1 : constant String := "97CE215371C9E011B27035E471056552";
      S2 : constant String := "foobarfoobazfoobar";
      R2 : constant String := "foofoobazfoo";
   begin
      Assert (Condition => Strip (Str     => "",
                                  Pattern => "") = "",
              Message   => "Stripped string mismatch (1)");
      Assert (Condition => Strip (Str     => "",
                                  Pattern => "foo") = "",
              Message   => "Stripped string mismatch (2)");
      Assert (Condition => Strip (Str     => "foo",
                                  Pattern => "") = "foo",
              Message   => "Stripped string mismatch (3)");
      Assert (Condition => Strip (Str     => S1,
                                  Pattern => "") = S1,
              Message   => "Stripped string mismatch (4)");
      Assert (Condition => Strip (Str     => S1,
                                  Pattern => "-") = R1,
              Message   => "Stripped string mismatch (5)");
      Assert (Condition => Strip (Str     => R1,
                                  Pattern => "-") = R1,
              Message   => "Stripped string mismatch (6)");
      Assert (Condition => Strip (Str     => S2,
                                  Pattern => "bar") = R2,
              Message   => "Stripped string mismatch (7)");
   end Strip;

   -------------------------------------------------------------------------

   procedure To_Array
   is
      use type Ada.Streams.Stream_Element_Array;
   begin
      Assert (Condition => To_Array
              (Value  => 0, Length => 1) = (1 => 0),
              Message   => "Array mismatch (1)");
      Assert (Condition => To_Array
              (Value  => Interfaces.Unsigned_64'Last,
               Length => 8) = (16#ff#, 16#ff#, 16#ff#, 16#ff#,
                16#ff#, 16#ff#, 16#ff#, 16#ff#),
              Message   => "Array mismatch (2)");
      Assert (Condition => To_Array
              (Value  => 8978, Length => 2) = (16#23#, 16#12#),
              Message   => "Array mismatch (3)");
   end To_Array;

   -------------------------------------------------------------------------

   procedure To_String
   is
   begin
      Assert (Condition => To_String (A => (97, 98, 99)) = "abc",
              Message   => "String mismatch");
   end To_String;

   -------------------------------------------------------------------------

   procedure To_Value
   is
   begin
      Assert (Condition => To_Value (A => (1 => 0)) = 0,
              Message   => "Value mismatch (1)");
      Assert (Condition => To_Value
              (A => (16#ff#, 16#ff#, 16#ff#, 16#ff#,
                     16#ff#, 16#ff#, 16#ff#, 16#ff#))
              = Interfaces.Unsigned_64'Last,
              Message   => "Value mismatch (2)");
      Assert (Condition => To_Value (A => (16#23#, 16#12#)) = 8978,
              Message   => "Value mismatch (3)");
   end To_Value;

end DHCP.Utils.Tests;
