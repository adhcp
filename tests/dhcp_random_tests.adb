--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet;

with DHCP.Random;

package body DHCP_Random_Tests is

   use Ahven;
   use DHCP;

   -------------------------------------------------------------------------

   procedure Get_Random_Data
   is
      use type Anet.Word32;

      Data : Anet.Word32 := 0;
   begin
      Data := Random.Get;

      Assert (Condition => Data /= 0,
              Message   => "Random data is 0");
      Assert (Condition => Data /= Random.Get,
              Message   => "Not really random");
   end Get_Random_Data;

   -------------------------------------------------------------------------

   procedure Get_Random_Float
   is
      Data : Float := 0.0;
   begin

      --  Note: This should also not raise an assert failure for the
      --  postcondition meaning that the returned value is within the specified
      --  interval.

      Data := Random.Get (Low  => 1.0,
                          High => 2.0);

      Assert (Condition => Data /= 0.0,
              Message   => "Random float is 0.0");
      Assert (Condition => Data /= Random.Get (Low  => 1.0,
                                               High => 2.0),
              Message   => "Float not really random");
   end Get_Random_Float;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Random package");
      T.Add_Test_Routine
        (Routine => Get_Random_Data'Access,
         Name    => "Get random data");
      T.Add_Test_Routine
        (Routine => Get_Random_Float'Access,
         Name    => "Get random float");
   end Initialize;

end DHCP_Random_Tests;
