--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCPv6.Transmission.Mock
is

   use Ada.Strings.Unbounded;

   -------------------------------------------------------------------------

   procedure Bind
     (Socket : in out UDPv6_Socket_Type;
      Iface  :        Anet.Types.Iface_Name_Type)
   is
      pragma Unreferenced (Socket);
   begin
      Bind_Iface_Count := Bind_Iface_Count + 1;
      Last_Bound_Iface := To_Unbounded_String (String (Iface));
   end Bind;

   -------------------------------------------------------------------------

   procedure Bind
     (Socket     : in out UDPv6_Socket_Type;
      Address    :        Anet.IPv6_Addr_Type := Anet.Any_Addr_V6;
      Port       :        Anet.Port_Type;
      Reuse_Addr :        Boolean             := True)
   is
      pragma Unreferenced (Socket, Address, Port, Reuse_Addr);
   begin
      Bind_Count := Bind_Count + 1;
   end Bind;

   -------------------------------------------------------------------------

   procedure Clear
   is
   begin
      Init_Count       := 0;
      Bind_Count       := 0;
      Bind_Iface_Count := 0;
      Send_Count       := 0;
      Last_Dst_IP      := Anet.Any_Addr_V6;
      Last_Dst_Port    := 0;
      Last_Item        := (others => 0);
      Last_Idx         := Last_Item'First;
      Last_Bound_Iface := Null_Unbounded_String;

      Rcv_Callback      := Null_Receiver_Callback'Access;
      Rcv_Err_Callback  := DHCP.Socket_Callbacks.Handle_Receive_Error'Access;
      Rcv_Listen_Count  := 0;
      Rcv_Stop_Count    := 0;
      Rcv_Reg_Err_Count := 0;
      Rcv_Is_Listening  := False;
   end Clear;

   -------------------------------------------------------------------------

   procedure Finalize (C : in out Sock_Cleaner)
   is
      pragma Unreferenced (C);
   begin
      Clear;
      Iface_Info := (Addrtype => DHCP.Net.IPv6_Address,
                     others => <>);
   end Finalize;

   -------------------------------------------------------------------------

   function Get_Last_Msg return Message.Message_Type
   is
   begin
      return Message.Deserialize
        (Buffer => Last_Item (1 .. Transmission.Mock.Last_Idx));
   end Get_Last_Msg;

   -------------------------------------------------------------------------

   procedure Init (Socket : in out UDPv6_Socket_Type)
   is
      pragma Unreferenced (Socket);
   begin
      Init_Count := Init_Count + 1;
   end Init;

   -------------------------------------------------------------------------

   procedure Listen
     (Receiver : in out Mock_Receiver;
      Callback :        Receivers.UDPv6.Rcv_Item_Callback)
   is
      pragma Unreferenced (Receiver);
   begin
      Rcv_Listen_Count := Rcv_Listen_Count + 1;
      Rcv_Callback     := Callback;
      Rcv_Is_Listening := True;
   end Listen;

   -------------------------------------------------------------------------

   procedure Register_Error_Handler
     (Receiver : in out Mock_Receiver;
      Callback :        Anet.Receivers.Error_Handler_Callback)
   is
      pragma Unreferenced (Receiver);
   begin
      Rcv_Reg_Err_Count := Rcv_Reg_Err_Count + 1;
      Rcv_Err_Callback  := Callback;
   end Register_Error_Handler;

   -------------------------------------------------------------------------

   procedure Send
     (Socket   : UDPv6_Socket_Type;
      Item     : Ada.Streams.Stream_Element_Array;
      Dst_Addr : Anet.IPv6_Addr_Type;
      Dst_Port : Anet.Port_Type)
   is
      pragma Unreferenced (Socket);
   begin
      Last_Item (Last_Item'First .. Item'Length) := Item;

      Send_Count    := Send_Count + 1;
      Last_Dst_IP   := Dst_Addr;
      Last_Dst_Port := Dst_Port;
      Last_Idx      := Item'Length;
   end Send;

   -------------------------------------------------------------------------

   procedure Stop (Receiver : in out Mock_Receiver)
   is
      pragma Unreferenced (Receiver);
   begin
      Rcv_Stop_Count   := Rcv_Stop_Count + 1;
      Rcv_Is_Listening := False;
   end Stop;

end DHCPv6.Transmission.Mock;
