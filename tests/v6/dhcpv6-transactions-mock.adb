--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCPv6.Transactions.Mock
is

   -------------------------------------------------------------------------

   function Get_ID
     (Transaction : Transaction_Type)
      return Types.Transaction_ID_Type
   is (Transaction.ID);

   -------------------------------------------------------------------------

   procedure Increase_Retransmission_Params
     (Transaction : in out Transaction_Type;
      Random      :        Types.Rand_Duration)
   is
      pragma Unreferenced (Random);
   begin
      Transaction.Inc_Retrans_P_Count := Transaction.Inc_Retrans_P_Count + 1;
   end Increase_Retransmission_Params;

   -------------------------------------------------------------------------

   procedure Init (Transaction : out Transaction_Type)
   is
   begin
      Transaction.ID                     := 0;
      Transaction.Reset_Count            := 0;
      Transaction.Start_Count            := 0;
      Transaction.Stop_Count             := 0;
      Transaction.Set_State_Count        := 0;
      Transaction.Set_Start_Time_Count   := 0;
      Transaction.Send_Msg_Count         := 0;
      Transaction.Processs_Msg_Count     := 0;
      Transaction.Process_T_Expiry_Count := 0;
      Transaction.Set_Retrans_P_Count    := 0;
      Transaction.Inc_Retrans_P_Count    := 0;
      Transaction.Reset_Retrans_Count    := 0;
      Transaction.Restart_Srv_Disc_Count := 0;
      Transaction.Max_Retrans_Count_Set  := False;
      Transaction.Max_Retrans_Dur_Set    := False;
      Transaction.Last_Set_State         := Transaction.State;
   end Init;

   -------------------------------------------------------------------------

   procedure Process_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type)
   is
   begin
      Transaction.Last_Proc_Msg      := Msg;
      Transaction.Processs_Msg_Count := Transaction.Processs_Msg_Count + 1;
   end Process_Message;

   -------------------------------------------------------------------------

   procedure Process_Timer_Expiry
     (Transaction : in out Transaction_Type;
      Timer_Kind  :        DHCP.Types.DHCP_Timer_Kind)
   is
   begin
      Transaction.Process_T_Expiry_Count
        := Transaction.Process_T_Expiry_Count + 1;
      Transaction.Last_Timer := Timer_Kind;
   end Process_Timer_Expiry;

   -------------------------------------------------------------------------

   procedure Reset (Transaction : in out Transaction_Type)
   is
   begin
      Transaction.Reset_Count := Transaction.Reset_Count + 1;
   end Reset;

   -------------------------------------------------------------------------

   procedure Reset_Retransmit (Transaction : in out Transaction_Type)
   is
   begin
      Transaction.Reset_Retrans_Count := Transaction.Reset_Retrans_Count + 1;
   end Reset_Retransmit;

   -------------------------------------------------------------------------

   procedure Restart_Server_Discovery (Transaction : in out Transaction_Type)
   is
   begin
      Transaction.Restart_Srv_Disc_Count
        := Transaction.Restart_Srv_Disc_Count + 1;
   end Restart_Server_Discovery;

   -------------------------------------------------------------------------

   procedure Send_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type)
   is

   begin
      Transaction.Last_Sent_Msg  := Msg;
      Transaction.Send_Msg_Count := Transaction.Send_Msg_Count + 1;
   end Send_Message;

   -------------------------------------------------------------------------

   procedure Set_ID
     (Transaction : in out Transaction_Type;
      XID         :        Types.Transaction_ID_Type)
   is
   begin
      Transaction.ID := XID;
   end Set_ID;

   -------------------------------------------------------------------------

   procedure Set_Retransmission_Params
     (Transaction : in out Transaction_Type;
      IRT         :        Duration;
      MRC         :        Natural;
      MRT         :        Duration;
      MRD         :        Duration)
   is
      pragma Unreferenced (IRT, MRC, MRT, MRD);
   begin
      Transaction.Set_Retrans_P_Count := Transaction.Set_Retrans_P_Count + 1;
   end Set_Retransmission_Params;

   -------------------------------------------------------------------------

   procedure Set_Start_Time
     (Transaction : in out Transaction_Type;
      Time        :        Ada.Real_Time.Time)
   is
      pragma Unreferenced (Time);
   begin
      Transaction.Set_Start_Time_Count := Transaction.Set_Start_Time_Count + 1;
   end Set_Start_Time;

   -------------------------------------------------------------------------

   procedure Set_State
     (Transaction : in out Transaction_Type;
      State       :        DHCP.States.State_Handle)
   is
   begin
      Transaction.Last_Set_State  := State;
      Transaction.Set_State_Count := Transaction.Set_State_Count + 1;
      DHCP.States.Root_Context_Type (Transaction).Set_State (State => State);
   end Set_State;

   -------------------------------------------------------------------------

   procedure Start (Transaction : in out Transaction_Type)
   is
   begin
      Transaction.Start_Count := Transaction.Start_Count + 1;
   end Start;

   -------------------------------------------------------------------------

   procedure Stop (Transaction : in out Transaction_Type)
   is
   begin
      Transaction.Stop_Count := Transaction.Stop_Count + 1;
   end Stop;

end DHCPv6.Transactions.Mock;
