--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;
with Ada.Finalization;

with GNAT.OS_Lib;

with DHCP.Cmd_Line;

package body DHCPv6.Cmd_Line.Tests
is

   use Ahven;

   type State_Reset_Type is new Ada.Finalization.Controlled with record
      State   : Cmd_Line_Data_Type;
      Ext_Bin : Ada.Strings.Unbounded.Unbounded_String;
   end record;
   --  Helper object which resets Cmd_Line package state after test run.

   overriding
   procedure Initialize (S : in out State_Reset_Type);
   --  Save current state of Cmd_Line package.

   overriding
   procedure Finalize (S : in out State_Reset_Type);
   --  Restore saved state of Cmd_Line package.

   -------------------------------------------------------------------------

   procedure Finalize (S : in out State_Reset_Type)
   is
   begin
      Instance := S.State;
      DHCP.Cmd_Line.Set_Ext_Notify_Binary (B => To_String (S.Ext_Bin));
   end Finalize;

   -------------------------------------------------------------------------

   procedure Get_DUID_Path
   is
      Ignore_Helper : State_Reset_Type;

      Default_DUID_Path : constant String := "/sys/class/dmi/id/product_uuid";
   begin
      Assert (Condition => Get_DUID_Path = Default_DUID_Path,
              Message   => "Default DUID path mismatch");

      Instance.DUID_Path := To_Unbounded_String (Source => "/tmp/custom_duid");
      Assert (Condition => Get_DUID_Path = "/tmp/custom_duid",
              Message   => "DUID path mismatch");
   end Get_DUID_Path;

   -------------------------------------------------------------------------

   procedure Get_DUID_Type
   is
      use type DUID.DUID_Kind_Type;

      Ignore_Helper : State_Reset_Type;

      Default_DUID_Type : constant DUID.DUID_Kind_Type := 4;
   begin
      Assert (Condition => Get_DUID_Type = Default_DUID_Type,
              Message   => "Default DUID type mismatch");

      Instance.DUID_Type := 2;
      Assert (Condition => Get_DUID_Type = 2,
              Message   => "DUID type mismatch");
   end Get_DUID_Type;

   -------------------------------------------------------------------------

   procedure Get_Interface_Name
   is
      Ignore_Helper : State_Reset_Type;
   begin
      Instance.Iface_Name := To_Unbounded_String (Source => "eth0");

      Assert (Condition => Get_Interface_Name = "eth0",
              Message   => "Interface name mismatch");
   end Get_Interface_Name;

   -------------------------------------------------------------------------

   procedure Get_Stateless
   is
      Dummy : State_Reset_Type;
   begin
      Assert (Condition => not In_Stateless_Mode,
              Message   => "Stateless mode enabled by default");

      Instance.Stateless := True;
      Assert (Condition => In_Stateless_Mode,
              Message   => "Stateless mode not enabled");
   end Get_Stateless;

   -------------------------------------------------------------------------

   procedure Initialize (S : in out State_Reset_Type)
   is
   begin
      S.State   := Instance;
      S.Ext_Bin := To_Unbounded_String (DHCP.Cmd_Line.Get_Ext_Notify_Binary);
   end Initialize;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Cmd_Line package");
      T.Add_Test_Routine
        (Routine => Get_Interface_Name'Access,
         Name    => "Get interface name");
      T.Add_Test_Routine
        (Routine => Get_DUID_Path'Access,
         Name    => "Get DUID path");
      T.Add_Test_Routine
        (Routine => Get_DUID_Type'Access,
         Name    => "Get DUID type");
      T.Add_Test_Routine
        (Routine => Get_Stateless'Access,
         Name    => "Get stateless mode");
      T.Add_Test_Routine
        (Routine => Parse_Parameters'Access,
         Name    => "Parse command line parameters");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Parse_Parameters
   is

      procedure Positive_Test;
      procedure Missing_Interface;
      procedure Interface_Name_Too_Long;
      procedure DUID_Type_Too_Large;
      procedure DUID_Type_Negative;
      procedure DUID_Type_Without_Path;
      procedure No_Stateless_Param;
      procedure Display_Usage;
      procedure Invalid_Param;

      ----------------------------------------------------------------------

      procedure Display_Usage
      is
         Ignore_Helper : State_Reset_Type;

         Parser : GNAT.Command_Line.Opt_Parser;
         Args   : GNAT.OS_Lib.Argument_List_Access
           := GNAT.OS_Lib.Argument_String_To_List
             (Arg_String => "-h");
      begin
         GNAT.Command_Line.Initialize_Option_Scan
           (Parser       => Parser,
            Command_Line => Args);
         Instance.Cmd_Parser := Parser;

         Parse;
         GNAT.OS_Lib.Free (Arg => Args);
         Fail (Message => "Exception expected");

      exception
         when GNAT.Command_Line.Exit_From_Command_Line =>
            GNAT.OS_Lib.Free (Arg => Args);
         when others =>
            GNAT.OS_Lib.Free (Arg => Args);
            raise;
      end Display_Usage;

      ----------------------------------------------------------------------

      procedure DUID_Type_Negative
      is
         Ignore_Helper : State_Reset_Type;

         Parser : GNAT.Command_Line.Opt_Parser;
         Args   : GNAT.OS_Lib.Argument_List_Access
           := GNAT.OS_Lib.Argument_String_To_List
             (Arg_String => "-i eth0 -u ./data/duid -t -1");
      begin
         GNAT.Command_Line.Initialize_Option_Scan
           (Parser       => Parser,
            Command_Line => Args);
         Instance.Cmd_Parser := Parser;

         Parse;
         GNAT.OS_Lib.Free (Arg => Args);
         Fail (Message => "Exception expected");

      exception
         when E : Invalid_Cmd_Line =>
            GNAT.OS_Lib.Free (Arg => Args);
            Assert (Condition => Ada.Exceptions.Exception_Message
                    (X => E) = "DUID type not in allowed range 0 .. 65535",
                    Message   => "Exception message mismatch");
         when others =>
            GNAT.OS_Lib.Free (Arg => Args);
            raise;
      end DUID_Type_Negative;

      ----------------------------------------------------------------------

      procedure DUID_Type_Too_Large
      is
         Ignore_Helper : State_Reset_Type;

         Parser : GNAT.Command_Line.Opt_Parser;
         Args   : GNAT.OS_Lib.Argument_List_Access
           := GNAT.OS_Lib.Argument_String_To_List
             (Arg_String => "-i eth0 -u ./data/duid -t 65536");
      begin
         GNAT.Command_Line.Initialize_Option_Scan
           (Parser       => Parser,
            Command_Line => Args);
         Instance.Cmd_Parser := Parser;

         Parse;
         GNAT.OS_Lib.Free (Arg => Args);
         Fail (Message => "Exception expected");

      exception
         when E : Invalid_Cmd_Line =>
            GNAT.OS_Lib.Free (Arg => Args);
            Assert (Condition => Ada.Exceptions.Exception_Message
                    (X => E) = "DUID type not in allowed range 0 .. 65535",
                    Message   => "Exception message mismatch");
         when others =>
            GNAT.OS_Lib.Free (Arg => Args);
            raise;
      end DUID_Type_Too_Large;

      ----------------------------------------------------------------------

      procedure DUID_Type_Without_Path
      is
         Ignore_Helper : State_Reset_Type;

         Parser : GNAT.Command_Line.Opt_Parser;
         Args   : GNAT.OS_Lib.Argument_List_Access
           := GNAT.OS_Lib.Argument_String_To_List
             (Arg_String => "-i eth0 -t 3");
      begin
         GNAT.Command_Line.Initialize_Option_Scan
           (Parser       => Parser,
            Command_Line => Args);
         Instance.Cmd_Parser := Parser;

         Parse;
         GNAT.OS_Lib.Free (Arg => Args);
         Fail (Message => "Exception expected");

      exception
         when E : Invalid_Cmd_Line =>
            GNAT.OS_Lib.Free (Arg => Args);
            Assert (Condition => Ada.Exceptions.Exception_Message
                    (X => E) = "DUID type specified but no path to DUID file "
                    & "given",
                    Message   => "Exception message mismatch");
         when others =>
            GNAT.OS_Lib.Free (Arg => Args);
            raise;
      end DUID_Type_Without_Path;

      ----------------------------------------------------------------------

      procedure Interface_Name_Too_Long
      is
         Ignore_Helper : State_Reset_Type;

         Parser : GNAT.Command_Line.Opt_Parser;
         Args   : GNAT.OS_Lib.Argument_List_Access
           := GNAT.OS_Lib.Argument_String_To_List
             (Arg_String => "-i invalidinterfacename");
      begin
         GNAT.Command_Line.Initialize_Option_Scan
           (Parser       => Parser,
            Command_Line => Args);
         Instance.Cmd_Parser := Parser;

         Parse;
         GNAT.OS_Lib.Free (Arg => Args);
         Fail (Message => "Exception expected");

      exception
         when E : Invalid_Cmd_Line =>
            GNAT.OS_Lib.Free (Arg => Args);
            Assert (Condition => Ada.Exceptions.Exception_Message
                    (X => E) = "No valid interface specified",
                    Message   => "Exception message mismatch");
         when others =>
            GNAT.OS_Lib.Free (Arg => Args);
            raise;
      end Interface_Name_Too_Long;

      ----------------------------------------------------------------------

      procedure Invalid_Param
      is
         Ignore_Helper : State_Reset_Type;

         Parser : GNAT.Command_Line.Opt_Parser;
         Args   : GNAT.OS_Lib.Argument_List_Access
           := GNAT.OS_Lib.Argument_String_To_List
             (Arg_String => "-i");
      begin
         GNAT.Command_Line.Initialize_Option_Scan
           (Parser       => Parser,
            Command_Line => Args);
         Instance.Cmd_Parser := Parser;

         Parse;
         GNAT.OS_Lib.Free (Arg => Args);
         Fail (Message => "Exception expected");

      exception
         when GNAT.Command_Line.Invalid_Parameter =>
            GNAT.OS_Lib.Free (Arg => Args);
         when others =>
            GNAT.OS_Lib.Free (Arg => Args);
            raise;
      end Invalid_Param;

      ----------------------------------------------------------------------

      procedure Missing_Interface
      is
         Ignore_Helper : State_Reset_Type;

         Parser : GNAT.Command_Line.Opt_Parser;
         Args   : GNAT.OS_Lib.Argument_List_Access
           := GNAT.OS_Lib.Argument_String_To_List (Arg_String => "");
      begin
         GNAT.Command_Line.Initialize_Option_Scan
           (Parser       => Parser,
            Command_Line => Args);
         Instance.Cmd_Parser := Parser;

         Parse;
         GNAT.OS_Lib.Free (Arg => Args);
         Fail (Message => "Exception expected");

      exception
         when E : Invalid_Cmd_Line =>
            GNAT.OS_Lib.Free (Arg => Args);
            Assert (Condition => Ada.Exceptions.Exception_Message
                    (X => E) = "No valid interface specified",
                    Message   => "Exception message mismatch");
         when others =>
            GNAT.OS_Lib.Free (Arg => Args);
            raise;
      end Missing_Interface;

      ----------------------------------------------------------------------

      procedure No_Stateless_Param
      is
         Ignore_Helper : State_Reset_Type;

         Parser : GNAT.Command_Line.Opt_Parser;
         Args   : GNAT.OS_Lib.Argument_List_Access
           := GNAT.OS_Lib.Argument_String_To_List
             (Arg_String => "-i eth1 -e notify -u ./data/duid -t 1");
      begin
         GNAT.Command_Line.Initialize_Option_Scan
           (Parser       => Parser,
            Command_Line => Args);
         Instance.Cmd_Parser := Parser;

         Parse;
         Assert (Condition => not Instance.Stateless,
                 Message   => "Stateless mode mismatch");

      exception
         when others =>
            GNAT.OS_Lib.Free (Arg => Args);
            raise;
      end No_Stateless_Param;

      ----------------------------------------------------------------------

      procedure Positive_Test
      is
         use type DUID.DUID_Kind_Type;

         Ignore_Helper : State_Reset_Type;

         Parser : GNAT.Command_Line.Opt_Parser;
         Args   : GNAT.OS_Lib.Argument_List_Access
           := GNAT.OS_Lib.Argument_String_To_List
             (Arg_String => "-i eth1 -e notify -u ./data/duid -t 1 -s");
      begin
         GNAT.Command_Line.Initialize_Option_Scan
           (Parser       => Parser,
            Command_Line => Args);
         Instance.Cmd_Parser := Parser;

         Parse;
         Assert (Condition => Instance.Iface_Name = "eth1",
                 Message   => "Interface mismatch");
         Assert (Condition => DHCP.Cmd_Line.Get_Ext_Notify_Binary = "notify",
                 Message   => "External notify binary mismatch");
         Assert (Condition => Instance.DUID_Path = "./data/duid",
                 Message   => "DUID path mismatch");
         Assert (Condition => Instance.DUID_Type = 1,
                 Message   => "DUID type mismatch");
         Assert (Condition => Instance.Stateless,
                 Message   => "Stateless mode mismatch");

      exception
         when others =>
            GNAT.OS_Lib.Free (Arg => Args);
            raise;
      end Positive_Test;
   begin
      Positive_Test;
      Missing_Interface;
      Interface_Name_Too_Long;
      DUID_Type_Too_Large;
      DUID_Type_Without_Path;
      DUID_Type_Negative;
      Display_Usage;
      No_Stateless_Param;
      Invalid_Param;
   end Parse_Parameters;

end DHCPv6.Cmd_Line.Tests;
