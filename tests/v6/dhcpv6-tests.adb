--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ahven.Text_Runner;
with Ahven.Framework;

with DHCPv6.IAs.Tests;
with DHCPv6_Options_Tests;
with DHCPv6.Cmd_Line.Tests;
with DHCPv6.DUID.Tests;
with DHCPv6.Message.Tests;
with DHCPv6.Database.Tests;
with DHCPv6.Client.Tests;
with DHCPv6.Transactions.Tests;
with DHCPv6.Transmission.Tests;
with DHCPv6.States.Stateless_Init.Tests;
with DHCPv6.States.Stateless_Config.Tests;
with DHCPv6.States.Init.Tests;
with DHCPv6.States.Soliciting.Tests;
with DHCPv6.States.Requesting.Tests;
with DHCPv6.States.Bound.Tests;
with DHCPv6.States.Renewing.Tests;
with DHCPv6.States.Rebinding.Tests;

package body DHCPv6.Tests
is

   use Ahven.Framework;

   -------------------------------------------------------------------------

   procedure Run
   is
      S : constant Test_Suite_Access
        := Create_Suite (Suite_Name => "DHCPv6 tests");
   begin
      S.Add_Test (T => new DHCPv6.IAs.Tests.Testcase);
      S.Add_Test (T => new DHCPv6_Options_Tests.Testcase);
      S.Add_Test (T => new DHCPv6.Cmd_Line.Tests.Testcase);
      S.Add_Test (T => new DHCPv6.DUID.Tests.Testcase);
      S.Add_Test (T => new DHCPv6.Message.Tests.Testcase);
      S.Add_Test (T => new DHCPv6.Database.Tests.Testcase);
      S.Add_Test (T => new DHCPv6.Transactions.Tests.Testcase);
      S.Add_Test (T => new DHCPv6.Transmission.Tests.Testcase);
      S.Add_Test (T => new DHCPv6.States.Stateless_Init.Tests.Testcase);
      S.Add_Test (T => new DHCPv6.States.Stateless_Config.Tests.Testcase);
      S.Add_Test (T => new DHCPv6.States.Init.Tests.Testcase);
      S.Add_Test (T => new DHCPv6.States.Soliciting.Tests.Testcase);
      S.Add_Test (T => new DHCPv6.States.Requesting.Tests.Testcase);
      S.Add_Test (T => new DHCPv6.States.Bound.Tests.Testcase);
      S.Add_Test (T => new DHCPv6.States.Renewing.Tests.Testcase);
      S.Add_Test (T => new DHCPv6.States.Rebinding.Tests.Testcase);

      --  Run the DHCPv6.Client tests at the end since the logger is stopped
      --  which prevents further debug output.

      S.Add_Test (T => new DHCPv6.Client.Tests.Testcase);

      Ahven.Text_Runner.Run (Suite => S);
      Release_Suite (T => S);
   end Run;

end DHCPv6.Tests;
