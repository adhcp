--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

with DHCP.States;
with DHCP.Types;

with DHCPv6.Message;
with DHCPv6.Types;
with DHCPv6.States.Mock;

package DHCPv6.Transactions.Mock
is

   type Transaction_Type is new States.Transaction_Type
     (Initial_State => States.Mock.State) with record
      ID                     : Types.Transaction_ID_Type  := 0;
      Set_State_Count        : Natural                    := 0;
      Reset_Count            : Natural                    := 0;
      Start_Count            : Natural                    := 0;
      Stop_Count             : Natural                    := 0;
      Set_Start_Time_Count   : Natural                    := 0;
      Send_Msg_Count         : Natural                    := 0;
      Processs_Msg_Count     : Natural                    := 0;
      Process_T_Expiry_Count : Natural                    := 0;
      Set_Retrans_P_Count    : Natural                    := 0;
      Inc_Retrans_P_Count    : Natural                    := 0;
      Reset_Retrans_Count    : Natural                    := 0;
      Restart_Srv_Disc_Count : Natural                    := 0;
      Max_Retrans_Count_Set  : Boolean                    := False;
      Max_Retrans_Dur_Set    : Boolean                    := False;
      Last_Timer             : DHCP.Types.DHCP_Timer_Kind;
      Last_Proc_Msg          : Message.Message_Type;
      Last_Sent_Msg          : Message.Message_Type;
      Last_Set_State         : DHCP.States.State_Handle   := States.Mock.State;
      State_Cleaner          : States.Mock.Cleaner_Type;
   end record;

   overriding
   procedure Restart_Server_Discovery (Transaction : in out Transaction_Type);

   overriding
   procedure Reset_Retransmit (Transaction : in out Transaction_Type);

   overriding
   procedure Set_State
     (Transaction : in out Transaction_Type;
      State       :        DHCP.States.State_Handle);

   overriding
   procedure Set_ID
     (Transaction : in out Transaction_Type;
      XID         :        Types.Transaction_ID_Type);

   overriding
   function Get_ID
     (Transaction : Transaction_Type)
      return Types.Transaction_ID_Type;

   overriding
   procedure Set_Start_Time
     (Transaction : in out Transaction_Type;
      Time        :        Ada.Real_Time.Time);

   overriding
   procedure Reset (Transaction : in out Transaction_Type);

   overriding
   procedure Start (Transaction : in out Transaction_Type);

   overriding
   procedure Stop (Transaction : in out Transaction_Type);

   overriding
   procedure Send_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type);

   use type DHCPv6.Types.Transaction_ID_Type;

   overriding
   procedure Process_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type)
   with
      Pre => Transaction.Get_ID = Msg.Get_Transaction_ID;

   overriding
   procedure Process_Timer_Expiry
     (Transaction : in out Transaction_Type;
      Timer_Kind  :        DHCP.Types.DHCP_Timer_Kind);

   overriding
   procedure Set_Retransmission_Params
     (Transaction : in out Transaction_Type;
      IRT         :        Duration;
      MRC         :        Natural;
      MRT         :        Duration;
      MRD         :        Duration);

   overriding
   procedure Increase_Retransmission_Params
     (Transaction : in out Transaction_Type;
      Random      :        Types.Rand_Duration);

   overriding
   function Max_Retransmit_Count_Reached
     (Transaction : Transaction_Type)
      return Boolean
   is (Transaction.Max_Retrans_Count_Set);

   overriding
   function Max_Retransmit_Duration_Reached
     (Transaction : Transaction_Type)
      return Boolean
     is (Transaction.Max_Retrans_Dur_Set);

   procedure Init (Transaction : out Transaction_Type);

end DHCPv6.Transactions.Mock;
