--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCPv6.States.Mock
is

   Instance : aliased Test_State_Type;

   -------------------------------------------------------------------------

   procedure Finalize (C : in out Cleaner_Type)
   is
      pragma Unreferenced (C);
   begin
      Instance.Start_Count          := 0;
      Instance.Stop_Count           := 0;
      Instance.Process_Reply_Count  := 0;
      Instance.Process_Adv_Count    := 0;
      Instance.Process_T_Fail_Count := 0;
      Instance.Process_T1_Exp_Count := 0;
      Instance.Process_T2_Exp_Count := 0;
      Instance.Process_L_Exp_Count  := 0;
   end Finalize;

   -------------------------------------------------------------------------

   function Get_Process_Advertise_Count return Natural
   is (Instance.Process_Adv_Count);

   -------------------------------------------------------------------------

   function Get_Process_Lease_Expiry_Count return Natural
   is (Instance.Process_L_Exp_Count);

   -------------------------------------------------------------------------

   function Get_Process_Reply_Count return Natural
   is (Instance.Process_Reply_Count);

   -------------------------------------------------------------------------

   function Get_Process_T1_Expiry_Count return Natural
   is (Instance.Process_T1_Exp_Count);

   -------------------------------------------------------------------------

   function Get_Process_T2_Expiry_Count return Natural
   is (Instance.Process_T2_Exp_Count);

   -------------------------------------------------------------------------

   function Get_Process_Trans_Failure_Count return Natural
   is (Instance.Process_T_Fail_Count);

   -------------------------------------------------------------------------

   function Get_Start_Count return Natural is (Instance.Start_Count);

   -------------------------------------------------------------------------

   function Get_Stop_Count return Natural is (Instance.Stop_Count);

   -------------------------------------------------------------------------

   procedure Process_Advertise_Msg
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      pragma Unreferenced (State, Transaction, Msg);
   begin
      Instance.Process_Adv_Count := Instance.Process_Adv_Count + 1;
   end Process_Advertise_Msg;

   -------------------------------------------------------------------------

   procedure Process_Lease_Expiry
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class)
   is
      pragma Unreferenced (State, Transaction);
   begin
      Instance.Process_L_Exp_Count := Instance.Process_L_Exp_Count + 1;
   end Process_Lease_Expiry;

   -------------------------------------------------------------------------

   procedure Process_Reply_Msg
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      pragma Unreferenced (State, Transaction, Msg);
   begin
      Instance.Process_Reply_Count := Instance.Process_Reply_Count + 1;
   end Process_Reply_Msg;

   -------------------------------------------------------------------------

   procedure Process_T1_Expiry
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class)
   is
      pragma Unreferenced (State, Transaction);
   begin
      Instance.Process_T1_Exp_Count := Instance.Process_T1_Exp_Count + 1;
   end Process_T1_Expiry;

   -------------------------------------------------------------------------

   procedure Process_T2_Expiry
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class)
   is
      pragma Unreferenced (State, Transaction);
   begin
      Instance.Process_T2_Exp_Count := Instance.Process_T2_Exp_Count + 1;
   end Process_T2_Expiry;

   -------------------------------------------------------------------------

   procedure Process_Transmission_Failure
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      pragma Unreferenced (State, Transaction, Msg);
   begin
      Instance.Process_T_Fail_Count := Instance.Process_T_Fail_Count + 1;
   end Process_Transmission_Failure;

   -------------------------------------------------------------------------

   procedure Start
     (State       : access Test_State_Type;
      Transaction : in out DHCP.States.Root_Context_Type'Class)
   is
      pragma Unreferenced (State, Transaction);
   begin
      Instance.Start_Count := Instance.Start_Count + 1;
   end Start;

   -------------------------------------------------------------------------

   procedure Stop
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class)
   is
      pragma Unreferenced (State, Transaction);
   begin
      Instance.Stop_Count := Instance.Stop_Count + 1;
   end Stop;

   -------------------------------------------------------------------------

   function State return DHCP.States.State_Handle is (Instance'Access);

end DHCPv6.States.Mock;
