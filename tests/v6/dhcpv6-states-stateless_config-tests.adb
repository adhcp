--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

with DHCP.Notify.Mock;
with DHCP.Timer.Test;
with DHCP.Timing_Events.Start;

with DHCPv6.Constants;
with DHCPv6.Options;
with DHCPv6.Database.Mock;
with DHCPv6.Transactions.Mock;
with DHCPv6.States.Stateless_Init;

package body DHCPv6.States.Stateless_Config.Tests
is

   use Ahven;

   -------------------------------------------------------------------------

   procedure Get_Name
   is
   begin
      Assert (Condition => State.Get_Name = "Stateless_Config",
              Message   => "Name mismatch");
   end Get_Name;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DHCPv6 client state 'Stateless_Config'");
      T.Add_Test_Routine
        (Routine => Get_Name'Access,
         Name    => "Name getter");
      T.Add_Test_Routine
        (Routine => Process_Reply_Msg'Access,
         Name    => "Process reply message");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Process_Reply_Msg
   is
      DNS_Opt : constant Options.Inst.Option_Type'Class
        := Options.Inst.Create
          (Name => Options.Name_Servers,
           Data => (16#20#, 16#01#, 16#0d#, 16#b8#, 16#00#, 16#00#, 16#00#,
                    16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
                    16#00#, 16#35#));

      procedure Process_Reply;
      procedure Process_Reply_Infinity_IRT;
      procedure Process_Reply_Minimum_IRT;
      procedure Process_Reply_No_IRT_Option;

      ----------------------------------------------------------------------

      procedure Process_Reply
      is
         use type DHCP.States.State_Handle;
         use type DHCP.Notify.Reason_Type;
         use type Options.Inst.Option_Type'Class;

         IRT_Opt : constant Options.Inst.Option_Type'Class
           := Options.Inst.Create
             (Name => Options.Information_Refresh_Time,
              Data => (16#00#, 16#01#, 16#ef#, 16#0a#));
         T   : Transactions.Mock.Transaction_Type;
         Msg : Message.Message_Type := Message.Create (Kind => Message.Reply);

         Dummy_1 : Database.Mock.Cleaner;
         Dummy_2 : DHCP.Notify.Mock.Notify_Cleaner;
         Dummy_3 : DHCP.Timer.Test.Cleaner_Type;
      begin
         DHCP.Notify.Mock.Install;

         Msg.Add_Option (Opt => Options.Inst6.Default_Request_Option);
         Msg.Add_Option (Opt => DNS_Opt);
         Msg.Add_Option (Opt => IRT_Opt);

         States.State_Type'Class (State.all).Process_Reply_Msg
           (Transaction => T,
            Msg         => Msg);

         Assert (Condition => Database.Get_Global_Options.Get
                 (Name => Options.Name_Servers) = DNS_Opt,
                 Message   => "DNS server option mismatch");
         Assert (Condition => not Database.Get_Global_Options.Contains
                 (Name => Options.Option_Request),
                 Message   => "Non-config option in database");
         Assert (Condition => T.State = Stateless_Init.State,
                 Message   => "New state not 'Stateless_Init'");
         Assert (Condition => T.Reset_Retrans_Count = 1,
                 Message   => "Transaction retransmission not reset");
         Assert (Condition => T.Reset_Count = 1,
                 Message   => "Transaction data not reset");
         Assert (Condition => DHCP.Notify.Mock.Get_Last_Reason
                 = DHCP.Notify.Bound,
                 Message   => "Bound notification not triggered");

         declare
            Ev_Count : constant Natural := DHCP.Timer.Event_Count;
         begin
            Assert (Condition => DHCP.Timer.Event_Count = 1,
                    Message   => "Start event not scheduled (Event count"
                    & Ev_Count'Img & ")");
         end;

         declare
            use Ada.Real_Time;
            Ev  : constant DHCP.Timing_Events.Start.Start_Type
              := DHCP.Timing_Events.Start.Start_Type
                (DHCP.Timer.Test.Get_Next_Event);
            Dur : constant Duration
              := To_Duration (Ev.Get_Time - Ada.Real_Time.Clock);
         begin
            Assert (Condition => Dur - 126730.0 < 1.0,
                    Message   => "Information refresh time mismatch");
            DHCP.Timer.Cancel (Event => Ev);
         end;
      end Process_Reply;

      ----------------------------------------------------------------------

      procedure Process_Reply_Infinity_IRT
      is
         IRT_Opt : constant Options.Inst.Option_Type'Class
           := Options.Inst.Create
             (Name => Options.Information_Refresh_Time,
              Data => (16#ff#, 16#ff#, 16#ff#, 16#ff#));

         T   : Transactions.Mock.Transaction_Type;
         Msg : Message.Message_Type := Message.Create (Kind => Message.Reply);

         Dummy_1 : Database.Mock.Cleaner;
         Dummy_2 : DHCP.Notify.Mock.Notify_Cleaner;
         Dummy_3 : DHCP.Timer.Test.Cleaner_Type;
      begin
         DHCP.Notify.Mock.Install;

         Msg.Add_Option (Opt => Options.Inst6.Default_Request_Option);
         Msg.Add_Option (Opt => DNS_Opt);
         Msg.Add_Option (Opt => IRT_Opt);

         States.State_Type'Class (State.all).Process_Reply_Msg
           (Transaction => T,
            Msg         => Msg);

         Assert (Condition => DHCP.Timer.Event_Count = 0,
                 Message   => "Information refresh scheduled for infinity");
      end Process_Reply_Infinity_IRT;

      ----------------------------------------------------------------------

      procedure Process_Reply_Minimum_IRT
      is
         IRT_Opt : constant Options.Inst.Option_Type'Class
           := Options.Inst.Create
             (Name => Options.Information_Refresh_Time,
              Data => (16#00#, 16#00#, 16#00#, 16#ff#));

         T   : Transactions.Mock.Transaction_Type;
         Msg : Message.Message_Type := Message.Create (Kind => Message.Reply);

         Dummy_1 : Database.Mock.Cleaner;
         Dummy_2 : DHCP.Notify.Mock.Notify_Cleaner;
         Dummy_3 : DHCP.Timer.Test.Cleaner_Type;
      begin
         DHCP.Notify.Mock.Install;

         Msg.Add_Option (Opt => Options.Inst6.Default_Request_Option);
         Msg.Add_Option (Opt => DNS_Opt);
         Msg.Add_Option (Opt => IRT_Opt);

         States.State_Type'Class (State.all).Process_Reply_Msg
           (Transaction => T,
            Msg         => Msg);

         declare
            use Ada.Real_Time;
            Ev  : constant DHCP.Timing_Events.Start.Start_Type
              := DHCP.Timing_Events.Start.Start_Type
                (DHCP.Timer.Test.Get_Next_Event);
            Dur : constant Duration
              := To_Duration (Ev.Get_Time - Ada.Real_Time.Clock);
         begin
            Assert (Condition => abs (Dur - Constants.IRT_MINIMUM) < 1.0,
                    Message   => "Minimum Information refresh time mismatch");
            DHCP.Timer.Cancel (Event => Ev);
         end;
      end Process_Reply_Minimum_IRT;

      ----------------------------------------------------------------------

      procedure Process_Reply_No_IRT_Option
      is
         T   : Transactions.Mock.Transaction_Type;
         Msg : Message.Message_Type := Message.Create (Kind => Message.Reply);

         Dummy_1 : Database.Mock.Cleaner;
         Dummy_2 : DHCP.Notify.Mock.Notify_Cleaner;
         Dummy_3 : DHCP.Timer.Test.Cleaner_Type;
      begin
         DHCP.Notify.Mock.Install;

         Msg.Add_Option (Opt => Options.Inst6.Default_Request_Option);
         Msg.Add_Option (Opt => DNS_Opt);

         States.State_Type'Class (State.all).Process_Reply_Msg
           (Transaction => T,
            Msg         => Msg);

         declare
            use Ada.Real_Time;
            Ev  : constant DHCP.Timing_Events.Start.Start_Type
              := DHCP.Timing_Events.Start.Start_Type
                (DHCP.Timer.Test.Get_Next_Event);
            Dur : constant Duration
              := To_Duration (Ev.Get_Time - Ada.Real_Time.Clock);
         begin
            Assert (Condition => abs (Dur - Constants.IRT_DEFAULT) < 1.0,
                    Message   => "Default Information refresh time mismatch");
            DHCP.Timer.Cancel (Event => Ev);
         end;
      end Process_Reply_No_IRT_Option;
   begin
      Process_Reply;
      Process_Reply_Infinity_IRT;
      Process_Reply_Minimum_IRT;
      Process_Reply_No_IRT_Option;
   end Process_Reply_Msg;

end DHCPv6.States.Stateless_Config.Tests;
