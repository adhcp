--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;
with Ada.Strings.Unbounded;

with Anet.Receivers;
with Anet.Sockets.Inet;

with DHCP.Socket_Callbacks;

private with Ada.Finalization;

package DHCPv6.Transmission.Mock
is

   type Sock_Cleaner is private;
   --  Transmission socket test cleanup helper.

   procedure Clear;
   --  Clear transmission mock data.

   function Get_Last_Msg return Message.Message_Type;
   --  Convenience helper, that returns the last sent DHCPv6 message.

   Init_Count       : Natural                           := 0;
   Bind_Count       : Natural                           := 0;
   Bind_Iface_Count : Natural                           := 0;
   Send_Count       : Natural                           := 0;
   Last_Dst_IP      : Anet.IPv6_Addr_Type               := Anet.Any_Addr_V6;
   Last_Dst_Port    : Anet.Port_Type                    := 0;
   Last_Item        : Ada.Streams.Stream_Element_Array (1 .. 2048);
   Last_Idx         : Ada.Streams.Stream_Element_Offset := Last_Item'First;
   Last_Bound_Iface : Ada.Strings.Unbounded.Unbounded_String;

   procedure Null_Receiver_Callback
     (Item : Ada.Streams.Stream_Element_Array;
      Src  : Anet.Sockets.Inet.IPv6_Sockaddr_Type) is null;

   Rcv_Callback      : Receivers.UDPv6.Rcv_Item_Callback
     := Null_Receiver_Callback'Access;
   Rcv_Err_Callback  : Anet.Receivers.Error_Handler_Callback
     := DHCP.Socket_Callbacks.Handle_Receive_Error'Access;
   Rcv_Listen_Count  : Natural := 0;
   Rcv_Stop_Count    : Natural := 0;
   Rcv_Reg_Err_Count : Natural := 0;
   Rcv_Is_Listening  : Boolean := False;

   type UDPv6_Socket_Type is new
     Anet.Sockets.Inet.UDPv6_Socket_Type with null record;

   overriding
   procedure Init (Socket : in out UDPv6_Socket_Type);

   overriding
   procedure Send
     (Socket   : UDPv6_Socket_Type;
      Item     : Ada.Streams.Stream_Element_Array;
      Dst_Addr : Anet.IPv6_Addr_Type;
      Dst_Port : Anet.Port_Type);

   overriding
   procedure Bind
     (Socket : in out UDPv6_Socket_Type;
      Iface  :        Anet.Types.Iface_Name_Type);

   overriding
   procedure Bind
     (Socket     : in out UDPv6_Socket_Type;
      Address    :        Anet.IPv6_Addr_Type := Anet.Any_Addr_V6;
      Port       :        Anet.Port_Type;
      Reuse_Addr :        Boolean             := True);

   type Mock_Receiver is new Receivers.UDPv6.Receiver_Type with null record;

   overriding
   procedure Listen
     (Receiver : in out Mock_Receiver;
      Callback :        Receivers.UDPv6.Rcv_Item_Callback);

   overriding
   procedure Register_Error_Handler
     (Receiver : in out Mock_Receiver;
      Callback :        Anet.Receivers.Error_Handler_Callback);

   overriding
   procedure Stop (Receiver : in out Mock_Receiver);

   overriding
   function Is_Listening (Receiver : Mock_Receiver) return Boolean
   is (Rcv_Is_Listening);

private

   type Sock_Cleaner is new Ada.Finalization.Controlled with null record;

   overriding
   procedure Finalize (C : in out Sock_Cleaner);

end DHCPv6.Transmission.Mock;
