--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;
with Ada.Strings.Unbounded;

with Anet.Sockets.Inet;

with DHCPv6.Constants;
with DHCPv6.Transmission.Mock;

package body DHCPv6.Transmission.Tests
is

   use Ada.Strings.Unbounded;
   use Ahven;

   procedure Null_Test_Callback
     (Item : Ada.Streams.Stream_Element_Array;
      Src  : Anet.Sockets.Inet.IPv6_Sockaddr_Type) is null;

   -------------------------------------------------------------------------

   procedure Get_Iface_Name
   is
      use type Anet.Types.Iface_Name_Type;
   begin
      Assert (Condition => Get_Iface_Name = "",
              Message   => "Default interface name mismatch");

      Iface_Info.Name := To_Unbounded_String ("wlan0");
      Assert (Condition => Get_Iface_Name = "wlan0",
              Message   => "Interface name mismatch");
   end Get_Iface_Name;

   -------------------------------------------------------------------------

   procedure Initialize
   is
      use type DHCP.Net.Iface_Type;
      use type Receivers.UDPv6.Rcv_Item_Callback;

      Iface_Name : constant Unbounded_String := To_Unbounded_String ("lo");
      Ref_Iface  : constant DHCP.Net.Iface_Type
        := (Addrtype => DHCP.Net.IPv6_Address,
            Name     => Iface_Name,
            Address6 => Anet.Loopback_Addr_V6,
            Netmask6 => Anet.Any_Addr_V6);

      Dummy : Mock.Sock_Cleaner;
   begin
      Initialize (Client_Iface => Ref_Iface,
                  Listen_Cb    => Null_Test_Callback'Access);

      Assert (Condition => Iface_Info = Ref_Iface,
              Message   => "Interface info mismatch");
      Assert (Condition => Mock.Init_Count = 1,
              Message   => "Socket not initialized");
      Assert (Condition => Mock.Bind_Iface_Count = 1,
              Message   => "Socket not bound to interface");
      Assert (Condition => Mock.Last_Bound_Iface = Iface_Name,
              Message   => "Bound interface mismatch");
      Assert (Condition => Mock.Bind_Count = 1,
              Message   => "Socket not bound");
      Assert (Condition => Mock.Rcv_Is_Listening,
              Message   => "Receiver not listening");
      Assert (Condition => Mock.Rcv_Reg_Err_Count = 1,
              Message   => "Receiver error handler not registered");
      Assert (Condition => Mock.Rcv_Callback = Null_Test_Callback'Access,
              Message   => "Receiver callback mismatch");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Transmission package");
      T.Add_Test_Routine
        (Routine => Get_Iface_Name'Access,
         Name    => "Get interface name");
      T.Add_Test_Routine
        (Routine => Initialize'Access,
         Name    => "Initialize transmission layer");
      T.Add_Test_Routine
        (Routine => Send'Access,
         Name    => "Send message");
      T.Add_Test_Routine
        (Routine => Stop_Transmission'Access,
         Name    => "Stop transmission layer");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Send
   is
      use type Ada.Streams.Stream_Element_Array;
      use type Anet.IPv6_Addr_Type;
      use type Anet.Double_Byte;

      Msg      : constant DHCPv6.Message.Message_Type
        := DHCPv6.Message.Create (Kind => DHCPv6.Message.Information_Request);
      Ref_Data : constant Ada.Streams.Stream_Element_Array
        := Msg.Serialize;

      Dummy : Mock.Sock_Cleaner;
   begin
      Send (Message => Msg);
      Assert (Condition => Mock.Send_Count = 1,
              Message   => "Send count mismatch");
      Assert (Condition => Mock.Last_Dst_IP
              = Constants.All_DHCP_Relay_Agents_and_Servers,
              Message   => "Send IP mismatch");
      Assert (Condition => Mock.Last_Dst_Port = Constants.DHCPv6_Server_Port,
              Message   => "Send port mismatch");
      Assert (Condition => Mock.Last_Item
              (Mock.Last_Item'First .. Mock.Last_Idx) = Ref_Data,
              Message   => "Send data mismatch");
   end Send;

   -------------------------------------------------------------------------

   procedure Stop_Transmission
   is
      Dummy : Mock.Sock_Cleaner;
   begin

      --  Stopping a non-listening receiver should be handled gracefully.

      Stop;
      Assert (Condition => Mock.Rcv_Stop_Count = 0,
              Message   => "Non-listening receiver stopped");

      Mock.Rcv_Is_Listening := True;
      Stop;
      Assert (Condition => Mock.Rcv_Stop_Count = 1,
              Message   => "Receiver not stopped");
      Assert (Condition => not Mock.Rcv_Is_Listening,
              Message   => "Receiver still listening");
   end Stop_Transmission;

end DHCPv6.Transmission.Tests;
