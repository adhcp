--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Timer.Test;
with DHCP.Notify.Mock;
with DHCP.Types;

with DHCPv6.Database.Mock;
with DHCPv6.IAs;
with DHCPv6.Options;
with DHCPv6.States.Rebinding;
with DHCPv6.Transactions.Mock;

package body DHCPv6.States.Renewing.Tests
is

   use Ahven;

   -------------------------------------------------------------------------

   procedure Alloc_Notify
   is
      use type DHCP.Notify.Reason_Type;

      Dummy : DHCP.Notify.Mock.Notify_Cleaner;
   begin
      DHCP.Notify.Mock.Install;

      Allocation_State_Type'Class (State.all).Notify_Allocation;
      Assert
        (Condition => DHCP.Notify.Mock.Get_Last_Reason = DHCP.Notify.Renew,
         Message   => "No renew notification");
   end Alloc_Notify;

   -------------------------------------------------------------------------

   procedure Get_Name
   is
   begin
      Assert (Condition => State.Get_Name = "Renewing",
              Message   => "Name mismatch");
   end Get_Name;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DHCPv6 client state 'Renewing'");
      T.Add_Test_Routine
        (Routine => Get_Name'Access,
         Name    => "Name getter");
      T.Add_Test_Routine
        (Routine => Alloc_Notify'Access,
         Name    => "Allocation notification");
      T.Add_Test_Routine
        (Routine => T2_Expiry'Access,
         Name    => "T2 expiry");
   end Initialize;

   -------------------------------------------------------------------------

   procedure T2_Expiry
   is
      use type DHCP.States.State_Handle;

      T : Transactions.Mock.Transaction_Type;

      Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
      Dummy_2 : Database.Mock.Cleaner;
   begin
      Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
      Database.Add_Global_Option (Opt => Database.Mock.Ref_Server_ID);
      Database.Add_IA (IA => IAs.Create (IAID => 42));

      States.State_Type'Class (State.all).Process_T2_Expiry (Transaction => T);

      Assert (Condition => T.ID /= 0,
              Message   => "XID is 0");
      Assert (Condition => T.Set_Retrans_P_Count = 1,
              Message   => "Retransmission parameters not set");
      Assert (Condition => T.Set_Start_Time_Count = 1,
              Message   => "Start time not set");
      Assert (Condition => T.Send_Msg_Count = 1,
              Message   => "No message sent");

      declare
         Msg  : constant Message.Message_Type      := T.Last_Sent_Msg;
         Opts : constant Options.Inst6.Option_List := Msg.Get_Options;
      begin
         Assert (Condition => Msg.Get_Kind = Message.Rebind,
                 Message   => "Message kind not rebind");
         Assert (Condition => Msg.Get_Transaction_ID = T.ID,
                 Message   => "Message transaction ID mismatch");
         Assert (Condition => Opts.Contains
                 (Name => Options.Client_Identifier),
                 Message   => "Client ID option missing");
         Assert (Condition => not Opts.Contains
                 (Name => Options.Server_Identifier),
                 Message   => "Server ID option present");
         Assert (Condition => Opts.Contains
                 (Name => Options.Inst6.Default_Request_Option.Get_Name),
                 Message   => "Default request option missing");
         Assert (Condition => Opts.Contains (Name => Options.IA_NA),
                 Message   => "IA_NA option missing");

         declare
            use type DHCP.Types.IAID_Type;

            IA : constant Options.Inst6.IA_NA_Option_Type
              := Options.Inst6.IA_NA_Option_Type
                (Opts.Get (Name => Options.IA_NA));
         begin
            Assert (Condition => IA.Get_IAID = 42,
                    Message   => "IAID mismatch");
         end;
      end;

      Assert (Condition => T.State = Rebinding.State,
              Message   => "Not transitioned to state "
              & Rebinding.State.Get_Name);
   end T2_Expiry;

end DHCPv6.States.Renewing.Tests;
