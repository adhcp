--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ahven.Framework;

package DHCPv6.IAs.Tests
is

   type Testcase is new Ahven.Framework.Test_Case with null record;

   procedure Initialize (T : in out Testcase);
   --  Initialize testcase.

   procedure Create_IA;
   --  Verify identity association creation function.

   procedure Get_IA_ID;
   --  Verify IA ID getter.

   procedure Set_Timestamp;
   --  Verify IA timestamp setter.

   procedure Set_Timeouts;
   --  Verify IA timeout setter.

   procedure Get_T1;
   --  Verify IA T1 timeout getter.

   procedure Get_T2;
   --  Verify IA T2 timeout getter.

   procedure Get_Expiry;
   --  Verify IA expiry timeout getter.

   procedure Get_Timestamp;
   --  Verify IA timestamp getter.

   procedure Add_Address;
   --  Verify addition of addresses to IA.

   procedure Remove_Address;
   --  Verify removal of addresses to IA.

   procedure Iterate;
   --  Verify iteration of IA addresses;

   procedure Discard_Expired_Addresses;
   --  Verify removal of expired IA addresses.

   procedure Get_Addresses;
   --  Verify IA addresses getter.

   procedure To_Option;
   --  Verify IA to IA_NA option conversion.

end DHCPv6.IAs.Tests;
