--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Timer.Test;
with DHCP.Notify.Mock;

with DHCPv6.Message;
with DHCPv6.Options;
with DHCPv6.Database.Mock;
with DHCPv6.Transactions.Mock;
with DHCPv6.States.Stateless_Config;

package body DHCPv6.States.Stateless_Init.Tests
is

   use Ahven;

   -------------------------------------------------------------------------

   procedure Get_Name
   is
   begin
      Assert (Condition => State.Get_Name = "Stateless_Init",
              Message   => "Name mismatch");
   end Get_Name;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DHCPv6 client state 'Stateless_Init'");
      T.Add_Test_Routine
        (Routine => Get_Name'Access,
         Name    => "Name getter");
      T.Add_Test_Routine
        (Routine => Start'Access,
         Name    => "Start procedure");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Start
   is
      use type DHCP.States.State_Handle;
      use type DHCP.Notify.Reason_Type;

      T : Transactions.Mock.Transaction_Type;

      Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
      Dummy_2 : DHCP.Notify.Mock.Notify_Cleaner;
      Dummy_3 : Database.Mock.Cleaner;
   begin
      DHCP.Notify.Mock.Install;
      Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);

      States.State_Type'Class (State.all).Start (Transaction => T);

      Assert (Condition => T.ID /= 0,
              Message   => "XID is 0");
      Assert (Condition => T.Set_Retrans_P_Count = 1,
              Message   => "Retransmission parameters not set");
      Assert (Condition => T.Set_Start_Time_Count = 1,
              Message   => "Start time not set");
      Assert (Condition => T.Send_Msg_Count = 1,
              Message   => "No message sent");
      Assert (Condition => DHCP.Notify.Mock.Get_Last_Reason
              = DHCP.Notify.Preinit,
              Message   => "Preinit notification not triggered");

      declare
         use type Options.Inst.Raw_Option_Type;

         Msg  : constant Message.Message_Type      := T.Last_Sent_Msg;
         Opts : constant Options.Inst6.Option_List := Msg.Get_Options;
      begin
         Assert (Condition => Msg.Get_Kind = Message.Information_Request,
                 Message   => "Message kind not information-request");
         Assert (Condition => Msg.Get_Transaction_ID = T.ID,
                 Message   => "Message transaction ID mismatch");
         Assert (Condition => Opts.Contains
                 (Name => Options.Client_Identifier),
                 Message   => "Client ID option missing");
         Assert (Condition => Opts.Contains
                 (Name => Options.Inst6.Default_Request_Option.Get_Name),
                 Message   => "Default request option missing");
         Assert (Condition => Options.Inst.Raw_Option_Type
                 (Opts.Get
                    (Name => Options.Inst6.Default_Request_Option.Get_Name))
                 = Options.Inst6.Inf_Default_Request_Option,
                 Message   => "Default request option mismatch");
      end;

      Assert (Condition => T.State = Stateless_Config.State,
              Message   => "Not transitioned to state "
              & Stateless_Config.State.Get_Name);
   end Start;

end DHCPv6.States.Stateless_Init.Tests;
