--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package DHCPv6.Cmd_Line.Mock
is

   procedure Set_Cmd_Args
     (Iface_Name : String;
      DUID_Path  : String;
      DUID_Type  : DUID.DUID_Kind_Type;
      Stateless  : Boolean);
   --  Set command line arguments to given values.

end DHCPv6.Cmd_Line.Mock;
