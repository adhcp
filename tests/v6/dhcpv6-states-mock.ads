--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

private with Ada.Finalization;

package DHCPv6.States.Mock
is

   type Test_State_Type is new State_Type with private;

   overriding
   function Get_Name (State : not null access Test_State_Type) return String
   is ("Test_State");

   overriding
   procedure Start
     (State       : access Test_State_Type;
      Transaction : in out DHCP.States.Root_Context_Type'Class);

   function Get_Start_Count return Natural;

   overriding
   procedure Stop
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class);

   function Get_Stop_Count return Natural;

   overriding
   procedure Process_Reply_Msg
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type);

   function Get_Process_Reply_Count return Natural;

   overriding
   procedure Process_Advertise_Msg
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type);

   function Get_Process_Advertise_Count return Natural;

   overriding
   procedure Process_Transmission_Failure
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type);

   function Get_Process_Trans_Failure_Count return Natural;

   overriding
   procedure Process_T1_Expiry
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class);

   function Get_Process_T1_Expiry_Count return Natural;

   overriding
   procedure Process_T2_Expiry
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class);

   function Get_Process_T2_Expiry_Count return Natural;

   overriding
   procedure Process_Lease_Expiry
     (State       : access Test_State_Type;
      Transaction : in out Transaction_Type'Class);

   function Get_Process_Lease_Expiry_Count return Natural;

   function State return DHCP.States.State_Handle;
   --  Return handle to singleton mock state instance.

   type Cleaner_Type is private;

private

   type Test_State_Type is new State_Type with record
      Start_Count          : Natural := 0;
      Stop_Count           : Natural := 0;
      Process_Reply_Count  : Natural := 0;
      Process_Adv_Count    : Natural := 0;
      Process_T_Fail_Count : Natural := 0;
      Process_T1_Exp_Count : Natural := 0;
      Process_T2_Exp_Count : Natural := 0;
      Process_L_Exp_Count  : Natural := 0;
   end record;

   type Cleaner_Type is new Ada.Finalization.Controlled with null record;

   overriding
   procedure Finalize (C : in out Cleaner_Type);

end DHCPv6.States.Mock;
