--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ahven.Framework;

package DHCPv6.Transactions.Tests
is

   type Testcase is new Ahven.Framework.Test_Case with null record;

   procedure Initialize (T : in out Testcase);
   --  Initialize testcase.

   procedure Set_ID;
   --  Verify transaction ID setter.

   procedure Get_ID;
   --  Verify transaction ID getter.

   procedure Start;
   --  Verify starting a transaction.

   procedure Stop;
   --  Verify stopping a transaction.

   procedure Reset;
   --  Verify resetting a transaction.

   procedure Process_Message;
   --  Verify transaction message processing.

   procedure Set_Start_Time;
   --  Verify start time setter.

   procedure Set_Retransmission_Params;
   --  Verify retransmission parameters setter.

   procedure Increase_Retransmission_Params;
   --  Verify incrementation of retransmission parameters.

   procedure Max_Retransmit_Count_Reached;
   --  Verify max retransmission count check function.

   procedure Max_Retransmit_Duration_Reached;
   --  Verify max retransmission duration check function.

   procedure Schedule_Retransmit;
   --  Verify scheduling of retransmission.

   procedure Reset_Retransmit;
   --  Verify reset of retransmission.

   procedure Restart_Server_Discovery;
   --  Verify restart of server discovery.

   procedure Send_Message;
   --  Verify transaction message sending.

   procedure Process_Timer_Expiry;
   --  Verify timer expiration handling.

   procedure Get_Elapsed_Time;
   --  Verify elapsed time getter.

   procedure Validate_Message_Options;
   --  Verify message option validator.

end DHCPv6.Transactions.Tests;
