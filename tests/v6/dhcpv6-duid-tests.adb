--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with System.Assertions;

with Ada.Exceptions;

package body DHCPv6.DUID.Tests
is

   use Ahven;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DUID package");
      T.Add_Test_Routine
        (Routine => Parse_UUID'Access,
         Name    => "Parse UUID string");
      T.Add_Test_Routine
        (Routine => To_Array'Access,
         Name    => "Convert DUID type to stream element array");
      T.Add_Test_Routine
        (Routine => Read_DUID_From_File'Access,
         Name    => "Read DUID from file");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Parse_UUID
   is
      use Ada.Streams;

      procedure Positive_Test;
      procedure Invalid_Hex_String;

      ----------------------------------------------------------------------

      procedure Invalid_Hex_String
      is
         UUID : constant String := "1234beef-caa1-0000-1098-35c471foobar";
      begin
         declare
            Dummy : constant Stream_Element_Array := Parse_UUID (Str => UUID);
         begin
            Fail (Message => "Exception expected");
         end;

      exception
         when System.Assertions.Assert_Failure => null;
      end Invalid_Hex_String;

      ----------------------------------------------------------------------

      procedure Positive_Test
      is
         UUID : constant String := "97CE2153-71C9-E011-B270-35E471056552";
         Ref  : constant Stream_Element_Array :=
           (16#97#, 16#CE#, 16#21#, 16#53#, 16#71#, 16#C9#, 16#E0#, 16#11#,
            16#B2#, 16#70#, 16#35#, 16#E4#, 16#71#, 16#05#, 16#65#, 16#52#);
      begin
         Assert (Condition => Parse_UUID (Str => UUID) = Ref,
                 Message   => "Parsed UUID mismatch");
      end Positive_Test;
   begin
      Positive_Test;
      Invalid_Hex_String;
   end Parse_UUID;

   -------------------------------------------------------------------------

   procedure Read_DUID_From_File
   is
      use DHCPv6.Options.Inst;

      procedure Read_DUID_UUID;
      procedure Read_DUID_LLT;
      procedure DUID_File_Too_Large;

      ----------------------------------------------------------------------

      procedure DUID_File_Too_Large
      is
      begin
         declare
            Dummy : constant Raw_Option_Type := Read_DUID_From_File
              (Path      => "data/dhcp-ack1.dat",
               DUID_Type => 1);
         begin
            Fail (Message => "Exception expected");
         end;

      exception
         when E : Invalid_DUID =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Invalid data for DUID type 1 in file "
                    & "'data/dhcp-ack1.dat'",
                    Message   => "Exception message mismatch");
      end DUID_File_Too_Large;

      ----------------------------------------------------------------------

      procedure Read_DUID_LLT
      is
         Ref_Opt : constant Raw_Option_Type := Raw_Option_Type
           (Create (Name => Options.Client_Identifier,
                    Data => (16#00#, 16#01#, 16#00#, 16#01#, 16#1d#, 16#9f#,
                             16#92#, 16#9d#, 16#08#, 16#00#, 16#27#, 16#ae#,
                             16#b5#, 16#ed#)));
      begin
         Assert (Condition => Read_DUID_From_File
                 (Path      => "data/duid_llt",
                  DUID_Type => 1) = Ref_Opt,
                 Message   => "DUID-LLT mismatch");
      end Read_DUID_LLT;

      ----------------------------------------------------------------------

      procedure Read_DUID_UUID
      is
         Ref_Opt : constant Raw_Option_Type := Raw_Option_Type
           (Create (Name => Options.Client_Identifier,
                    Data => (16#00#, 16#04#, 16#97#, 16#CE#, 16#21#, 16#53#,
                             16#71#, 16#C9#, 16#E0#, 16#11#, 16#B2#, 16#70#,
                             16#35#, 16#E4#, 16#71#, 16#05#, 16#65#, 16#52#)));
      begin
         Assert (Condition => Read_DUID_From_File
                 (Path      => "data/duid_uuid",
                  DUID_Type => DUID_UUID) = Ref_Opt,
                 Message   => "DUID-UUID mismatch");
      end Read_DUID_UUID;
   begin
      Read_DUID_UUID;
      Read_DUID_LLT;
      DUID_File_Too_Large;
   end Read_DUID_From_File;

   -------------------------------------------------------------------------

   procedure To_Array
   is
      use Ada.Streams;
   begin
      for I in DUID_Kind_Type'Range loop
         declare
            Ref_Arr : constant Stream_Element_Array :=
              (1 => Stream_Element (I / 2 ** 8),
               2 => Stream_Element (I mod 2 ** 8));
         begin
            Assert (Condition => To_Array (DUID_Type => I) = Ref_Arr,
                    Message   => "Array mismatch for DUID type" & I'Img);
         end;
      end loop;
   end To_Array;

end DHCPv6.DUID.Tests;
