--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Timer.Test;

with DHCPv6.Database.Mock;
with DHCPv6.IAs;
with DHCPv6.Message;
with DHCPv6.Options;
with DHCPv6.States.Requesting;
with DHCPv6.Transactions.Mock;

package body DHCPv6.States.Soliciting.Tests
is

   use Ahven;

   -------------------------------------------------------------------------

   procedure Get_Name
   is
   begin
      Assert (Condition => State.Get_Name = "Soliciting",
              Message   => "Name mismatch");
   end Get_Name;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DHCPv6 client state 'Soliciting'");
      T.Add_Test_Routine
        (Routine => Get_Name'Access,
         Name    => "Name getter");
      T.Add_Test_Routine
        (Routine => Process_Advertise_Message'Access,
         Name    => "Process advertise message");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Process_Advertise_Message
   is
      use type DHCP.States.State_Handle;

      Ref_IA : constant Options.Inst.Option_Type'Class
        := Options.Create (IAID => 25,
                           T1   => 3200.0,
                           T2   => 6400.0);

      T   : Transactions.Mock.Transaction_Type;
      Msg : Message.Message_Type := Message.Create (Kind => Message.Advertise);

      Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
      Dummy_3 : Database.Mock.Cleaner;
   begin
      Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
      Database.Add_IA (IA => IAs.Create (IAID => 25));

      Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);
      Msg.Add_Option (Opt => Ref_IA);
      Msg.Add_Option (Opt => Options.Create (IAID => 26,
                                             T1   => 3200.0,
                                             T2   => 6400.0));

      States.State_Type'Class (State.all).Process_Advertise_Msg
        (Transaction => T,
         Msg         => Msg);

      Assert (Condition => T.ID /= 0,
              Message   => "XID is 0");
      Assert (Condition => T.Set_Retrans_P_Count = 1,
              Message   => "Retransmission parameters not set");
      Assert (Condition => T.Set_Start_Time_Count = 1,
              Message   => "Start time not set");
      Assert (Condition => T.Send_Msg_Count = 1,
              Message   => "No message sent");

      Assert (Condition => Database.Get_Global_Options.Contains
              (Name => Options.Server_Identifier),
              Message   => "SID not stored in database");

      declare
         use type Options.Inst.Option_Type'Class;

         Msg  : constant Message.Message_Type      := T.Last_Sent_Msg;
         Opts : constant Options.Inst6.Option_List := Msg.Get_Options;
      begin
         Assert (Condition => Msg.Get_Kind = Message.Request,
                 Message   => "Message kind not request");
         Assert (Condition => Msg.Get_Transaction_ID = T.ID,
                 Message   => "Message transaction ID mismatch");
         Assert (Condition => Opts.Contains
                 (Name => Options.Client_Identifier),
                 Message   => "Client ID option missing");
         Assert (Condition => Opts.Contains
                 (Name => Options.Server_Identifier),
                 Message   => "Server ID option missing");
         Assert (Condition => Opts.Contains
                 (Name => Options.Inst6.Default_Request_Option.Get_Name),
                 Message   => "Default request option missing");
         Assert (Condition => Opts.Contains (Name => Options.IA_NA),
                 Message   => "IA_NA option missing");
         Assert (Condition => Opts.Get (Name => Options.IA_NA) = Ref_IA,
                 Message   => "IA_NA option mismatch");
      end;

      Assert (Condition => T.State = Requesting.State,
              Message   => "Not transitioned to state "
              & Requesting.State.Get_Name);
   end Process_Advertise_Message;

end DHCPv6.States.Soliciting.Tests;
