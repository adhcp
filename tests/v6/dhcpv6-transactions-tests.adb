--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

with Anet;

with DHCP.Timer.Test;

with DHCPv6.Constants;
with DHCPv6.Database.Mock;
with DHCPv6.Options;
with DHCPv6.Types;
with DHCPv6.States.Mock;
with DHCPv6.Transmission.Mock;

package body DHCPv6.Transactions.Tests
is

   use Ahven;

   -------------------------------------------------------------------------

   procedure Get_Elapsed_Time
   is
      use Ada.Real_Time;

      Time_Delta : constant Duration := 100.0;
      Timestamp  : constant Time     := Clock - To_Time_Span (D => 60.0);

      T : Transaction_Type;
   begin
      T.RT_Count := 1;
      T.Set_Start_Time (Time => Timestamp);
      Assert (Condition => T.Get_Elapsed_Time - 60.0 * 100 < Time_Delta,
              Message   => "Elapsed time mismatch");

      T.Set_Start_Time (Time => Timestamp - To_Time_Span
                        (D => Duration (2 ** 16 / 100)));
      Assert (Condition => T.Get_Elapsed_Time = Duration (2 ** 16 - 1),
              Message   => "Elapsed time not restricted to 2 ** 16 - 1");

      T.RT_Count := 0;
      Assert (Condition => T.Get_Elapsed_Time = 0.0,
              Message   => "Elapsed time for non-started transaction not 0");
   end Get_Elapsed_Time;

   -------------------------------------------------------------------------

   procedure Get_ID
   is
      Ref_ID : constant Types.Transaction_ID_Type := 421298;
      T      : Transaction_Type;
   begin
      T.ID := Ref_ID;
      Assert (Condition => T.Get_ID = Ref_ID,
              Message   => "Transaction ID mismatch");
   end Get_ID;

   -------------------------------------------------------------------------

   procedure Increase_Retransmission_Params
   is
      T : Transaction_Type;
   begin

      --  Client retransmit params for request, RFC 3315, section 18.1.1

      T.IRT := Constants.REQ_TIMEOUT;
      T.MRT := Constants.REQ_MAX_RT;
      T.MRC := Constants.REQ_MAX_RC;
      T.MRD := 0.0;

      T.Increase_Retransmission_Params (Random => 0.0);
      Assert (Condition => T.RT_Count = 1,
              Message   => "RT count mismatch");
      Assert (Condition => T.RT = T.IRT,
              Message   => "RT mismatch (1)");
      Assert (Condition => T.IRT = Constants.REQ_TIMEOUT,
              Message   => "IRT changed");
      Assert (Condition => T.MRT = Constants.REQ_MAX_RT,
              Message   => "MRT changed");
      Assert (Condition => T.MRC = Constants.REQ_MAX_RC,
              Message   => "MRC changed");
      Assert (Condition => T.MRD = 0.0,
              Message   => "MRD changed");

      declare
         Prev_RT : constant Duration := T.RT;
      begin
         T.Increase_Retransmission_Params (Random => 0.1);
         Assert (Condition => T.RT = 2 * Prev_RT + 0.1 * Prev_RT,
                 Message   => "RT mismatch (2)");
      end;

      T.RT := T.MRT + 1.0;
      T.Increase_Retransmission_Params (Random => 0.02);
      Assert (Condition => T.RT = T.MRT + T.MRT * 0.02,
              Message   => "RT not bound to MRT");

      T.RT  := 4.0;
      T.MRT := 0.0;
      T.Increase_Retransmission_Params (Random => 0.0);
      Assert (Condition => T.RT = 8.0,
              Message   => "RT bound by MRT=0");
   end Increase_Retransmission_Params;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Transaction package");
      T.Add_Test_Routine
        (Routine => Set_ID'Access,
         Name    => "Set ID");
      T.Add_Test_Routine
        (Routine => Get_ID'Access,
         Name    => "Get ID");
      T.Add_Test_Routine
        (Routine => Start'Access,
         Name    => "Start transaction");
      T.Add_Test_Routine
        (Routine => Stop'Access,
         Name    => "Stop transaction");
      T.Add_Test_Routine
        (Routine => Reset'Access,
         Name    => "Reset transaction");
      T.Add_Test_Routine
        (Routine => Process_Message'Access,
         Name    => "Process message");
      T.Add_Test_Routine
        (Routine => Set_Start_Time'Access,
         Name    => "Set start time");
      T.Add_Test_Routine
        (Routine => Set_Retransmission_Params'Access,
         Name    => "Set retransmission parameters");
      T.Add_Test_Routine
        (Routine => Increase_Retransmission_Params'Access,
         Name    => "Increase retransmission parameters");
      T.Add_Test_Routine
        (Routine => Max_Retransmit_Count_Reached'Access,
         Name    => "Check max retransmission count");
      T.Add_Test_Routine
        (Routine => Max_Retransmit_Duration_Reached'Access,
         Name    => "Check max retransmission duration");
      T.Add_Test_Routine
        (Routine => Schedule_Retransmit'Access,
         Name    => "Schedule retransmission");
      T.Add_Test_Routine
        (Routine => Reset_Retransmit'Access,
         Name    => "Reset retransmission");
      T.Add_Test_Routine
        (Routine => Restart_Server_Discovery'Access,
         Name    => "Restart server discovery");
      T.Add_Test_Routine
        (Routine => Send_Message'Access,
         Name    => "Send message");
      T.Add_Test_Routine
        (Routine => Process_Timer_Expiry'Access,
         Name    => "Process timer expiry");
      T.Add_Test_Routine
        (Routine => Get_Elapsed_Time'Access,
         Name    => "Get elapsed time");
      T.Add_Test_Routine
        (Routine => Validate_Message_Options'Access,
         Name    => "Validate message options");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Max_Retransmit_Count_Reached
   is
      T : Transaction_Type;
   begin
      T.RT_Count := 1;
      T.MRC      := 10;
      Assert (Condition => not T.Max_Retransmit_Count_Reached,
              Message   => "RT count max reached (1)");

      T.RT_Count := T.MRC - 1;
      Assert (Condition => not T.Max_Retransmit_Count_Reached,
              Message   => "RT count max reached (2)");

      T.MRC := T.RT_Count;
      Assert (Condition => T.Max_Retransmit_Count_Reached,
              Message   => "RT count max not reached (1)");

      T.RT_Count := 1823476;
      Assert (Condition => T.Max_Retransmit_Count_Reached,
              Message   => "RT count max not reached (2)");

      T.MRC := 0;
      Assert (Condition => not T.Max_Retransmit_Count_Reached,
              Message   => "RT count max reached (3)");
   end Max_Retransmit_Count_Reached;

   -------------------------------------------------------------------------

   procedure Max_Retransmit_Duration_Reached
   is
      use type Ada.Real_Time.Time;

      Now : constant Ada.Real_Time.Time := Ada.Real_Time.Clock;
      T   : Transaction_Type;
   begin
      T.Set_Start_Time (Time => Ada.Real_Time.Time_Last);

      T.MRD        := 10.0;
      Assert (Condition => not T.Max_Retransmit_Duration_Reached,
              Message   => "RT duration max reached (1)");

      T.Set_Start_Time (Time => Now);
      T.MRD        := 10.0;
      Assert (Condition => not T.Max_Retransmit_Duration_Reached,
              Message   => "RT duration max reached (2)");

      T.Set_Start_Time (Time => Now - Ada.Real_Time.To_Time_Span (D => T.MRD));
      Assert (Condition => T.Max_Retransmit_Duration_Reached,
              Message   => "RT duration max not reached (1)");

      T.Set_Start_Time (Time => Ada.Real_Time.Time_First);
      Assert (Condition => T.Max_Retransmit_Duration_Reached,
              Message   => "RT duration max not reached (2)");

      T.MRD := 0.0;
      Assert (Condition => not T.Max_Retransmit_Duration_Reached,
              Message   => "RT duration max reached (3)");
   end Max_Retransmit_Duration_Reached;

   -------------------------------------------------------------------------

   procedure Process_Message
   is
      Other_CID : constant Options.Inst.Raw_Option_Type
        := Options.Inst.Raw_Option_Type
          (Options.Inst.Create
             (Name => Options.Client_Identifier,
              Data => (16#00#, 16#04#, 16#97#, 16#CE#, 16#21#, 16#53#,
                       16#71#, 16#C9#, 16#E0#, 16#11#, 16#B2#, 16#70#)));

      procedure Process_Reply;
      procedure Process_Reply_Missing_SID;
      procedure Process_Reply_Missing_CID;
      procedure Process_Reply_CID_Mismatch;
      procedure Process_Reply_Illegal_Option;
      procedure Process_Unsupported_Msg;
      procedure Process_Advertise;
      procedure Process_Advertise_CID_Mismatch;
      procedure Process_Advertise_Missing_CID;
      procedure Process_Advertise_NoAddrsAvail;

      ----------------------------------------------------------------------

      procedure Process_Advertise
      is
         Msg : Message.Message_Type := Message.Create
           (Kind => Message.Advertise);
         T   : Transaction_Type;

         Dummy_S  : States.Mock.Cleaner_Type;
         Dummy_Db : Database.Mock.Cleaner;
      begin
         Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
         Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);
         Msg.Add_Option (Opt => Database.Mock.Ref_Client_ID);

         T.Set_State (State => States.Mock.State);
         T.Process_Message (Msg => Msg);
         Assert (Condition => States.Mock.Get_Process_Advertise_Count = 1,
                 Message   => "Advertise message not processed");
      end Process_Advertise;

      ----------------------------------------------------------------------

      procedure Process_Advertise_CID_Mismatch
      is
         Msg : Message.Message_Type := Message.Create
           (Kind => Message.Advertise);
         T   : Transaction_Type;

         Dummy_S  : States.Mock.Cleaner_Type;
         Dummy_Db : Database.Mock.Cleaner;
      begin
         Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
         Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);
         Msg.Add_Option (Opt => Other_CID);

         T.Set_State (State => States.Mock.State);
         T.Process_Message (Msg => Msg);
         Assert (Condition => States.Mock.Get_Process_Advertise_Count = 0,
                 Message   => "Advertise message processed");
      end Process_Advertise_CID_Mismatch;

      ----------------------------------------------------------------------

      procedure Process_Advertise_Missing_CID
      is
         Msg : Message.Message_Type := Message.Create
           (Kind => Message.Advertise);
         T   : Transaction_Type;

         Dummy_S  : States.Mock.Cleaner_Type;
         Dummy_Db : Database.Mock.Cleaner;
      begin
         Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
         Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);

         T.Set_State (State => States.Mock.State);
         T.Process_Message (Msg => Msg);
         Assert (Condition => States.Mock.Get_Process_Advertise_Count = 0,
                 Message   => "Advertise message processed");
      end Process_Advertise_Missing_CID;

      ----------------------------------------------------------------------

      procedure Process_Advertise_NoAddrsAvail
      is
         Msg : Message.Message_Type := Message.Create
           (Kind => Message.Advertise);
         T   : Transaction_Type;

         Dummy_S  : States.Mock.Cleaner_Type;
         Dummy_Db : Database.Mock.Cleaner;
      begin
         Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
         Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);
         Msg.Add_Option (Opt => Database.Mock.Ref_Client_ID);

         declare
            Addr_Opts, IA_Opts : Options.Inst6.Option_List;
         begin
            Addr_Opts.Append (New_Item => Options.Inst.Create
                              (Name => Options.Status_Code,
                               Data => (16#00#, 16#02#)));
            IA_Opts.Append (New_Item => Options.Create
                            (Address   => Anet.Loopback_Addr_V6,
                             Preferred => 3200.0,
                             Valid     => 6400.0,
                             Opts      => Addr_Opts));
            Msg.Add_Option (Opt => Options.Create (IAID => 25,
                                                   T1   => 3200.0,
                                                   T2   => 6400.0,
                                                   Opts => IA_Opts));
         end;

         T.Set_State (State => States.Mock.State);
         T.Process_Message (Msg => Msg);
         Assert (Condition => States.Mock.Get_Process_Advertise_Count = 0,
                 Message   => "Advertise message processed");
      end Process_Advertise_NoAddrsAvail;

      ----------------------------------------------------------------------

      procedure Process_Reply
      is
         Msg : Message.Message_Type := Message.Create (Kind => Message.Reply);
         T   : Transaction_Type;

         Dummy_S  : States.Mock.Cleaner_Type;
         Dummy_Db : Database.Mock.Cleaner;
      begin
         Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
         Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);
         Msg.Add_Option (Opt => Database.Mock.Ref_Client_ID);

         T.Set_State (State => States.Mock.State);
         T.RT_Count := 5;
         T.Process_Message (Msg => Msg);
         Assert (Condition => States.Mock.Get_Process_Reply_Count = 1,
                 Message   => "Reply message not processed");
         Assert (Condition => T.RT_Count = 5,
                 Message   => "Retransmission counter reset");
      end Process_Reply;

      ----------------------------------------------------------------------

      procedure Process_Reply_CID_Mismatch
      is
         Msg : Message.Message_Type := Message.Create (Kind => Message.Reply);
         T   : Transaction_Type;

         Dummy_S  : States.Mock.Cleaner_Type;
         Dummy_Db : Database.Mock.Cleaner;
      begin
         Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
         Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);
         Msg.Add_Option (Opt => Other_CID);

         T.Set_State (State => States.Mock.State);
         T.Process_Message (Msg => Msg);
         Assert (Condition => States.Mock.Get_Process_Reply_Count = 0,
                 Message   => "Reply message processed");
      end Process_Reply_CID_Mismatch;

      ----------------------------------------------------------------------

      procedure Process_Reply_Illegal_Option
      is
         Msg : Message.Message_Type := Message.Create (Kind => Message.Reply);
         T   : Transaction_Type;

         Dummy_S  : States.Mock.Cleaner_Type;
         Dummy_Db : Database.Mock.Cleaner;
      begin
         Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
         Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);
         Msg.Add_Option (Opt => Database.Mock.Ref_Client_ID);

         Msg.Add_Option (Opt => Options.Inst.Create
                         (Name => Options.Reconfigure_Message,
                          Data => (1 => 12)));

         T.Set_State (State => States.Mock.State);
         T.Process_Message (Msg => Msg);
         Assert (Condition => States.Mock.Get_Process_Reply_Count = 0,
                 Message   => "Reply message processed");
      end Process_Reply_Illegal_Option;

      ----------------------------------------------------------------------

      procedure Process_Reply_Missing_CID
      is
         Msg : Message.Message_Type := Message.Create (Kind => Message.Reply);
         T   : Transaction_Type;

         Dummy : States.Mock.Cleaner_Type;
      begin
         Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);

         T.Set_State (State => States.Mock.State);
         T.Process_Message (Msg => Msg);
         Assert (Condition => States.Mock.Get_Process_Reply_Count = 0,
                 Message   => "Reply message processed");
      end Process_Reply_Missing_CID;

      ----------------------------------------------------------------------

      procedure Process_Reply_Missing_SID
      is
         T : Transaction_Type;

         Dummy : States.Mock.Cleaner_Type;
      begin
         T.Set_State (State => States.Mock.State);
         T.Process_Message (Msg => Message.Create (Kind => Message.Reply));
         Assert (Condition => States.Mock.Get_Process_Reply_Count = 0,
                 Message   => "Reply message processed");
      end Process_Reply_Missing_SID;

      ----------------------------------------------------------------------

      procedure Process_Unsupported_Msg
      is
         T : Transaction_Type;

         Dummy : States.Mock.Cleaner_Type;
      begin
         T.Set_State (State => States.Mock.State);
         T.Process_Message (Msg => Message.Create
                            (Kind => Message.Reconfigure));
         Assert (Condition => States.Mock.Get_Process_Reply_Count = 0,
                 Message   => "Unsupported message processed");
      end Process_Unsupported_Msg;
   begin
      Process_Reply;
      Process_Reply_Missing_SID;
      Process_Reply_Missing_CID;
      Process_Reply_CID_Mismatch;
      Process_Reply_Illegal_Option;
      Process_Advertise;
      Process_Advertise_CID_Mismatch;
      Process_Advertise_Missing_CID;
      Process_Advertise_NoAddrsAvail;
      Process_Unsupported_Msg;
   end Process_Message;

   -------------------------------------------------------------------------

   procedure Process_Timer_Expiry
   is

      procedure Process_Retransmit;
      procedure Process_T1;
      procedure Process_T2;
      procedure Process_Lease_Expiry;

      ----------------------------------------------------------------------

      procedure Process_Lease_Expiry
      is
         T : Transaction_Type;

         Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
         Dummy_2 : States.Mock.Cleaner_Type;
      begin
         T.Set_State (State => States.Mock.State);

         T.Process_Timer_Expiry (Timer_Kind => DHCP.Types.Lease_Expiry);
         Assert (Condition => States.Mock.Get_Process_Lease_Expiry_Count = 1,
                 Message   => "Lease expiration not processed");
      end Process_Lease_Expiry;

      ----------------------------------------------------------------------

      procedure Process_Retransmit
      is
         T : Transaction_Type;

         Dummy_1 : DHCPv6.Transmission.Mock.Sock_Cleaner;
         Dummy_2 : DHCP.Timer.Test.Cleaner_Type;
         Dummy_3 : States.Mock.Cleaner_Type;
      begin
         T.Set_State (State => States.Mock.State);

         --  Regular retransmission

         T.Process_Timer_Expiry (Timer_Kind => DHCP.Types.Retransmit);
         Assert (Condition => Transmission.Mock.Send_Count = 1,
                 Message   => "No retransmission of message");

         --  Failed retransmission due to retry count.

         T.RT_Count := 10;
         T.MRC      := 9;
         T.Process_Timer_Expiry (Timer_Kind => DHCP.Types.Retransmit);
         Assert
           (Condition => States.Mock.Get_Process_Trans_Failure_Count = 1,
            Message   => "No transmission failure due to max retry count");

         --  Failed retransmission due to retry duration.

         T.RT_Count   := 1;
         T.Set_Start_Time (Time => Ada.Real_Time.Time_First);
         T.MRD        := 1.0;
         T.Process_Timer_Expiry (Timer_Kind => DHCP.Types.Retransmit);
         Assert
           (Condition => States.Mock.Get_Process_Trans_Failure_Count = 2,
            Message   => "No transmission failure due to max retry delay");
      end Process_Retransmit;

      ----------------------------------------------------------------------

      procedure Process_T1
      is
         T : Transaction_Type;

         Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
         Dummy_2 : States.Mock.Cleaner_Type;
      begin
         T.Set_State (State => States.Mock.State);

         T.Process_Timer_Expiry (Timer_Kind => DHCP.Types.T1);
         Assert (Condition => States.Mock.Get_Process_T1_Expiry_Count = 1,
                 Message   => "T1 expiration not processed");
      end Process_T1;

      ----------------------------------------------------------------------

      procedure Process_T2
      is
         T : Transaction_Type;

         Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
         Dummy_2 : States.Mock.Cleaner_Type;
      begin
         T.Set_State (State => States.Mock.State);

         T.Process_Timer_Expiry (Timer_Kind => DHCP.Types.T2);
         Assert (Condition => States.Mock.Get_Process_T2_Expiry_Count = 1,
                 Message   => "T2 expiration not processed");
      end Process_T2;
   begin
      Process_Retransmit;
      Process_T1;
      Process_T2;
      Process_Lease_Expiry;
   end Process_Timer_Expiry;

   -------------------------------------------------------------------------

   procedure Reset
   is
      use type Ada.Real_Time.Time;

      T : Transaction_Type;
   begin
      T.Set_Start_Time (Time => Ada.Real_Time.Clock);

      T.ID       := 12387468;
      T.RT_Count := 423;
      T.RT       := 0.1;
      T.IRT      := 2.3;
      T.MRC      := 57;
      T.MRT      := 60.0;
      T.MRD      := 120.0;
      T.Reset;

      Assert (Condition => T.ID = Types.Transaction_ID_Type'First,
              Message   => "Transaction ID not reset");
      Assert (Condition => T.Get_Start_Time = Ada.Real_Time.Time_First,
              Message   => "Transaction start time not reset");
      Assert (Condition => T.RT_Count = 0,
              Message   => "Transaction RT count not reset");
      Assert (Condition => T.RT = 0.0,
              Message   => "Transaction RT not reset");
      Assert (Condition => T.IRT = 0.0,
              Message   => "Transaction IRT not reset");
      Assert (Condition => T.MRC = 0,
              Message   => "Transaction MRC not reset");
      Assert (Condition => T.MRT = 0.0,
              Message   => "Transaction MRT not reset");
      Assert (Condition => T.MRD = 0.0,
              Message   => "Transaction MRD not reset");
   end Reset;

   -------------------------------------------------------------------------

   procedure Reset_Retransmit
   is
      T : Transaction_Type;

      Dummy : DHCP.Timer.Test.Cleaner_Type;
   begin
      T.RT_Count := 2;
      T.RT       := 60.0;
      T.IRT      := 0.1;
      T.Retransmit_Ev.Set_Time (At_Time => Ada.Real_Time.Time_Last);
      DHCP.Timer.Schedule (Event => T.Retransmit_Ev);

      T.Reset_Retransmit;

      Assert (Condition => T.RT_Count = 0,
              Message   => "Transaction RT count not reset");
      Assert (Condition => T.RT = T.IRT,
              Message   => "Transaction RT not reset to IRT");
      Assert (Condition => DHCP.Timer.Event_Count = 0,
              Message   => "Retransmission event still scheduled");
   end Reset_Retransmit;

   -------------------------------------------------------------------------

   procedure Restart_Server_Discovery
   is
      T : Transaction_Type;
   begin
      T.Set_State (State => States.Mock.State);

      T.Restart_Server_Discovery;

      Assert (Condition => States.Mock.Get_Stop_Count = 1,
              Message   => "Transaction not stopped");
      Assert (Condition => States.Mock.Get_Stop_Count = 1,
              Message   => "Transaction not started");
   end Restart_Server_Discovery;

   -------------------------------------------------------------------------

   procedure Schedule_Retransmit
   is
      use Ada.Real_Time;

      Time_Delta : constant Time_Span := To_Time_Span (1.0);
      T          : Transaction_Type;

      Dummy : DHCP.Timer.Test.Cleaner_Type;
   begin
      T.Schedule_Retransmit (In_Time => 60.0);

      Assert (Condition => T.Retransmit_Ev.Get_Time
              - (Clock + To_Time_Span (D => 60.0)) < Time_Delta,
              Message   => "Retransmission time mismatch");

      declare
         use type DHCP.Timing_Events.Timer_Expiry.Expiry_Type;

         Ev : constant DHCP.Timing_Events.Timer_Expiry.Expiry_Type
           := DHCP.Timing_Events.Timer_Expiry.Expiry_Type
             (DHCP.Timer.Test.Get_Next_Event);
      begin
         Assert (Condition => Ev = T.Retransmit_Ev,
                 Message   => "Retransmission event not scheduled");
      end;
   end Schedule_Retransmit;

   -------------------------------------------------------------------------

   procedure Send_Message
   is
      use Ada.Real_Time;
      use type Message.Message_Type;

      Ref_Msg    : constant Message.Message_Type
        := Message.Create (Kind => Message.Solicit);
      Time_Delta : constant Time_Span := To_Time_Span (D => 1.0);
      T          : Transaction_Type;

      Dummy_1 : Transmission.Mock.Sock_Cleaner;
      Dummy_2 : DHCP.Timer.Test.Cleaner_Type;
   begin
      T.ID  := 12387468;
      T.RT  := 0.0;
      T.IRT := Constants.SOL_TIMEOUT;
      T.MRT := Constants.SOL_MAX_RT;
      T.MRD := Constants.SOL_MAX_DELAY;

      T.Send_Message (Msg => Ref_Msg);

      Assert (Condition => Transmission.Mock.Send_Count = 1,
              Message   => "Message not transmitted");
      Assert (Condition => T.Last_Msg = Ref_Msg,
              Message   => "Sent message kind mismatch");
      Assert (Condition => Transmission.Mock.Get_Last_Msg.Get_Options.Contains
              (Name => Options.Elapsed_Time),
              Message   => "Message does not contain elapsed-time option");
      Assert (Condition => T.Retransmit_Ev.Get_Time
              - (Clock + To_Time_Span (D => 60.0)) < Time_Delta,
              Message   => "Retransmission event time mismatch");
      Assert (Condition => DHCP.Timer.Event_Count = 1,
              Message   => "Retransmission event not scheduled");
      Assert (Condition => T.RT_Count /= 0,
              Message   => "Retransmission counter not increased");
      Assert (Condition => T.RT /= T.IRT,
              Message   => "Retransmission timeout not increased");
      Assert (Condition => T.RT > T.IRT,
              Message   => "Retransmission timeout not in expected range");
   end Send_Message;

   -------------------------------------------------------------------------

   procedure Set_ID
   is
      Ref_ID : constant Types.Transaction_ID_Type := 421298;
      T      : Transaction_Type;
   begin
      T.Set_ID (XID => Ref_ID);
      Assert (Condition => T.ID = Ref_ID,
              Message   => "Transaction ID mismatch");
   end Set_ID;

   -------------------------------------------------------------------------

   procedure Set_Retransmission_Params
   is
      T : Transaction_Type;
   begin
      Assert (Condition => T.RT_Count = 0,
              Message   => "Default RT count mismatch");
      Assert (Condition => T.RT = 0.0,
              Message   => "Default RT mismatch");
      Assert (Condition => T.IRT = 0.0,
              Message   => "Default IRT mismatch");
      Assert (Condition => T.MRC = 0,
              Message   => "Default MRC mismatch");
      Assert (Condition => T.MRT = 0.0,
              Message   => "Default MRT mismatch");
      Assert (Condition => T.MRD = 0.0,
              Message   => "Default MRD mismatch");

      T.RT_Count := 10;
      T.RT       := 32.0;

      T.Set_Retransmission_Params
        (IRT => 1.0,
         MRC => 10,
         MRT => 64.0,
         MRD => 120.0);
      Assert (Condition => T.RT_Count = 0,
              Message   => "RT count mismatch");
      Assert (Condition => T.RT = 1.0,
              Message   => "RT mismatch");
      Assert (Condition => T.IRT = 1.0,
              Message   => "IRT mismatch");
      Assert (Condition => T.MRC = 10,
              Message   => "MRC mismatch");
      Assert (Condition => T.MRT = 64.0,
              Message   => "MRT mismatch");
      Assert (Condition => T.MRD = 120.0,
              Message   => "MRD mismatch");
   end Set_Retransmission_Params;

   -------------------------------------------------------------------------

   procedure Set_Start_Time
   is
      use type Ada.Real_Time.Time;

      Now : constant Ada.Real_Time.Time := Ada.Real_Time.Clock;
      T   : Transaction_Type;
   begin
      Assert (Condition => T.Get_Start_Time = Ada.Real_Time.Time_First,
              Message   => "Default start time mismatch");

      T.Set_Start_Time (Time => Now);
      Assert (Condition => T.Get_Start_Time = Now,
              Message   => "Start time mismatch");
   end Set_Start_Time;

   -------------------------------------------------------------------------

   procedure Start
   is
      T : Transaction_Type;

      Dummy : States.Mock.Cleaner_Type;
   begin
      T.Set_State (State => States.Mock.State);

      T.Start;

      Assert (Condition => States.Mock.Get_Start_Count = 1,
              Message   => "State start procedure not invoked");
   end Start;

   -------------------------------------------------------------------------

   procedure Stop
   is
      T : Transaction_Type;

      Dummy : States.Mock.Cleaner_Type;
   begin
      T.Set_State (State => States.Mock.State);

      T.Stop;

      Assert (Condition => States.Mock.Get_Stop_Count = 1,
              Message   => "State stop procedure not invoked");
   end Stop;

   -------------------------------------------------------------------------

   procedure Validate_Message_Options
   is
      Msg : Message.Message_Type := Message.Create (Kind => Message.Reply);
   begin
      Msg.Add_Option (Opt => Database.Mock.Ref_Client_ID);
      Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);
      Msg.Add_Option (Opt => Options.Inst6.Default_Request_Option);

      Assert (Condition => Valid_Message_Options
              (Msg           => Msg,
               Required_Opts => No_Opts,
               Invalid_Opts  => No_Opts),
              Message   => "Message options invalid (1)");
      Assert (Condition => Valid_Message_Options
              (Msg           => Msg,
               Required_Opts => (Options.Client_Identifier,
                                 Options.Server_Identifier),
               Invalid_Opts  => No_Opts),
              Message   => "Message options invalid (2)");
      Assert (Condition => Valid_Message_Options
              (Msg           => Msg,
               Required_Opts => No_Opts,
               Invalid_Opts  => (Options.Name_Servers,
                                 Options.Status_Code)),
              Message   => "Message options invalid (3)");
      Assert (Condition => Valid_Message_Options
              (Msg           => Msg,
               Required_Opts => (Options.Client_Identifier,
                                 Options.Server_Identifier),
               Invalid_Opts  => (Options.Name_Servers,
                                 Options.Status_Code)),
              Message   => "Message options invalid (4)");
      Assert (Condition => not Valid_Message_Options
              (Msg           => Msg,
               Required_Opts => (Options.Client_Identifier,
                                 Options.IA_NA),
               Invalid_Opts  => (Options.Name_Servers,
                                 Options.Status_Code)),
              Message   => "Message options invalid (5)");
      Assert (Condition => not Valid_Message_Options
              (Msg           => Msg,
               Required_Opts => (1 => Options.Server_Identifier),
               Invalid_Opts  => (1 => Options.Option_Request)),
              Message   => "Message options invalid (6)");
   end Validate_Message_Options;

end DHCPv6.Transactions.Tests;
