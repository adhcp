--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ahven.Framework;

package DHCPv6.Database.Tests
is

   type Testcase is new Ahven.Framework.Test_Case with null record;

   procedure Initialize (T : in out Testcase);
   --  Initialize testcase.

   procedure Init_DB;
   --  Verify database initialization.

   procedure Is_Initialized;
   --  Verify database initialization status getter.

   procedure Get_Client_ID;
   --  Verify client ID getter.

   procedure Reset_DB;
   --  Verify database reset operation.

   procedure Add_Global_Option;
   --  Verify addition of global config option.

   procedure Remove_Global_Option;
   --  Verify removal of global config option.

   procedure Get_Global_Options;
   --  Verify global options getter.

   procedure Contains_Option;
   --  Verify existence check of global config option.

   procedure Add_IA;
   --  Verify addition of identity association.

   procedure Contains_IA;
   --  Verify existence check of identity association.

   procedure Get_IA;
   --  Verify identity association getter.

   procedure Remove_IA;
   --  Verify identity association removal.

   procedure Iterate;
   --  Verify identity association iteration.

   procedure Get_IA_Count;
   --  Verify identity association count getter.

   procedure Update;
   --  Verify identity association update.

   procedure Write;
   --  Verify database to file serialization.

   procedure Load;
   --  Verify database from file deserialization.

end DHCPv6.Database.Tests;
