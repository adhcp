--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ahven.Framework;

package DHCPv6_Options_Tests
is

   type Testcase is new Ahven.Framework.Test_Case with null record;

   procedure Initialize (T : in out Testcase);
   --  Initialize testcase.

   procedure Create_Raw_Options;
   --  Create raw options.

   procedure Deserialize_Options;
   --  Deserialize options from stream.

   procedure Serialize_Option;
   --  Serialize option to stream.

   procedure Deserialize_Option_Lists;
   --  Check option list deserialization.

   procedure Serialize_Option_Lists;
   --  Check option list serialization.

   procedure Create_DNS_Name_Server_Option;
   --  Verify DNS recursive name server option creation.

   procedure Create_Status_Code_Option;
   --  Verify status code option creation.

   procedure Create_IA_Addr_Option;
   --  Verify IA addr option creation.

   procedure Create_IA_NA_Option;
   --  Verify IA NA option creation.

   procedure List_Contains;
   --  Verify list contains function.

end DHCPv6_Options_Tests;
