--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Finalization;
with Ada.Real_Time;

with Anet;

with DHCP.Notify.Mock;
with DHCP.States;
with DHCP.Timer.Test;

with DHCPv6.Constants;
with DHCPv6.Cmd_Line.Mock;
with DHCPv6.Database.Mock;
with DHCPv6.DUID;
with DHCPv6.Options;
with DHCPv6.Database;
with DHCPv6.Message;
with DHCPv6.States.Stateless_Init;
with DHCPv6.Transmission.Mock;
with DHCPv6.Transactions.Mock;
with DHCPv6.Types;
with DHCPv6.Timing_Events.Handle_Msg;

package body DHCPv6.Client.Tests
is

   use Ahven;

   package OI renames DHCPv6.Options.Inst;

   type Cleaner_Type is new Ada.Finalization.Controlled with null record;

   overriding
   procedure Finalize (C : in out Cleaner_Type);

   overriding
   procedure Initialize (C : in out Cleaner_Type);

   Mock_T : aliased Transactions.Mock.Transaction_Type;

   -------------------------------------------------------------------------

   procedure Finalize (C : in out Cleaner_Type)
   is
      pragma Unreferenced (C);
   begin
      Transaction := Instance'Access;
   end Finalize;

   -------------------------------------------------------------------------

   procedure Initialize (C : in out Cleaner_Type)
   is
      pragma Unreferenced (C);
   begin
      Mock_T.Init;
      Transaction := Mock_T'Access;
   end Initialize;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Client package");
      T.Add_Test_Routine
        (Routine => Run_Client'Access,
         Name    => "Run client");
      T.Add_Test_Routine
        (Routine => Process_Reply'Access,
         Name    => "Process server reply");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Process_Reply
   is
      Src : constant Anet.Sockets.Inet.IPv6_Sockaddr_Type
        := (Addr => Anet.Loopback_Addr_V6,
            Port => Constants.DHCPv6_Server_Port);

      procedure Process_Message;
      procedure Process_Invalid_Data;
      procedure Process_Mismatching_XID;

      ----------------------------------------------------------------------

      procedure Process_Invalid_Data
      is
         Dummy : Cleaner_Type;
      begin
         Process_Server_Reply (Buffer => (16#ff#, 16#00#, 16#ff#, 16#00#),
                               Src    => Src);
      end Process_Invalid_Data;

      ----------------------------------------------------------------------

      procedure Process_Message
      is
         XID : constant Types.Transaction_ID_Type := 12381137;
         Msg : Message.Message_Type
           := Message.Create (Kind => Message.Reply);

         Dummy_1 : Cleaner_Type;
         Dummy_2 : DHCP.Timer.Test.Cleaner_Type;
      begin
         Mock_T.ID := XID;
         Msg.Set_Transaction_ID (XID => XID);
         Process_Server_Reply (Buffer => Msg.Serialize,
                               Src    => Src);

         Assert (Condition => DHCP.Timer.Event_Count = 1,
                 Message   => "Event not scheduled");
         declare
            use Ada.Real_Time;
            use type Message.Message_Type;

            Ev : Timing_Events.Handle_Msg.Handle_Msg_Type
              := Timing_Events.Handle_Msg.Handle_Msg_Type
                (DHCP.Timer.Test.Get_Next_Event);
         begin
            Assert (Condition => Ev.Get_Time = Ada.Real_Time.Time_First,
                    Message   => "Message not handled immediately");

            Ev.Trigger;
            Assert (Condition => Mock_T.Processs_Msg_Count = 1,
                    Message   => "Message not processed");
            Assert (Condition => Mock_T.Last_Proc_Msg = Msg,
                    Message   => "Processed message mismatch");

            DHCP.Timer.Cancel (Event => Ev);
         end;
      end Process_Message;

      ----------------------------------------------------------------------

      procedure Process_Mismatching_XID
      is
         Msg : Message.Message_Type
           := Message.Create (Kind => Message.Reply);

         Dummy : Cleaner_Type;
      begin
         Mock_T.ID := 42;
         Msg.Set_Transaction_ID (XID => 10234987);
         Process_Server_Reply (Buffer => Msg.Serialize,
                               Src    => Src);
         Assert (Condition => Mock_T.Processs_Msg_Count = 0,
                 Message   => "Message with mismatching XID processed");
      end Process_Mismatching_XID;
   begin
      Process_Message;
      Process_Invalid_Data;
      Process_Mismatching_XID;
   end Process_Reply;

   -------------------------------------------------------------------------

   procedure Run_Client
   is
      use type DHCP.States.State_Handle;
      use type OI.Raw_Option_Type;

      Ref_Opt : constant OI.Raw_Option_Type := OI.Raw_Option_Type
        (OI.Create (Name => Options.Client_Identifier,
                    Data => (16#00#, 16#04#, 16#97#, 16#CE#, 16#21#, 16#53#,
                             16#71#, 16#C9#, 16#E0#, 16#11#, 16#B2#, 16#70#,
                             16#35#, 16#E4#, 16#71#, 16#05#, 16#65#, 16#52#)));

      procedure Run_Statefull;
      procedure Run_Stateless;

      ----------------------------------------------------------------------

      procedure Run_Statefull
      is
         Dummy_Timer  : DHCP.Timer.Test.Cleaner_Type;
         Dummy_TX     : Transmission.Mock.Sock_Cleaner;
         Dummy_Notify : DHCP.Notify.Mock.Notify_Cleaner;
         Dummy_Db     : Database.Mock.Cleaner;
         Dummy_T      : Cleaner_Type;
      begin
         Cmd_Line.Mock.Set_Cmd_Args
           (Iface_Name => "lo",
            DUID_Path  => "data/duid_uuid",
            DUID_Type  => DUID.DUID_UUID,
            Stateless  => False);
         Run;

         Assert (Condition => Transmission.Mock.Rcv_Is_Listening,
                 Message   => "Receiver not listening");
         Assert (Condition => Database.Is_Initialized,
                 Message   => "Database not initialized");
         Assert (Condition => OI.Raw_Option_Type
                 (Database.Get_Client_ID) = Ref_Opt,
                 Message   => "Client ID mismatch");
         Assert (Condition => DHCP.Notify.Handler_Count (DHCP.Notify.Bound)
                 = 1,
                 Message   => "Registered observer count mismatch");
         Assert (Condition => Mock_T.Set_State_Count = 0,
                 Message   => "State changed");

         Stop;
         Assert (Condition => Mock_T.Stop_Count = 1,
                 Message   => "Transaction not stopped");
         Assert (Condition => DHCP.Timer.Event_Count = 0,
                 Message   => "Timer not stopped");
         Assert (Condition => not Transmission.Mock.Rcv_Is_Listening,
                 Message   => "Receiver still listening");
      end Run_Statefull;

      ----------------------------------------------------------------------

      procedure Run_Stateless
      is
         Dummy_Timer  : DHCP.Timer.Test.Cleaner_Type;
         Dummy_TX     : Transmission.Mock.Sock_Cleaner;
         Dummy_Notify : DHCP.Notify.Mock.Notify_Cleaner;
         Dummy_Db     : Database.Mock.Cleaner;
         Dummy_T      : Cleaner_Type;
      begin
         Cmd_Line.Mock.Set_Cmd_Args
           (Iface_Name => "lo",
            DUID_Path  => "data/duid_uuid",
            DUID_Type  => DUID.DUID_UUID,
            Stateless  => True);
         Run;

         Assert (Condition => Transmission.Mock.Rcv_Is_Listening,
                 Message   => "Receiver not listening");
         Assert (Condition => Database.Is_Initialized,
                 Message   => "Database not initialized");
         Assert (Condition => OI.Raw_Option_Type
                 (Database.Get_Client_ID) = Ref_Opt,
                 Message   => "Client ID mismatch");
         Assert (Condition => DHCP.Notify.Handler_Count (DHCP.Notify.Bound)
                 = 1,
                 Message   => "Registered observer count mismatch");
         Assert (Condition => Mock_T.Set_State_Count = 1,
                 Message   => "State not set");
         Assert (Condition => Mock_T.Last_Set_State
                 = States.Stateless_Init.State,
                 Message   => "State mismatch "
                 & Mock_T.Last_Set_State.Get_Name);

         Stop;
         Assert (Condition => DHCP.Timer.Event_Count = 0,
                 Message   => "Timer not stopped");
         Assert (Condition => not Transmission.Mock.Rcv_Is_Listening,
                 Message   => "Receiver still listening");
      end Run_Stateless;
   begin
      Run_Stateless;
      Run_Statefull;
   end Run_Client;

end DHCPv6.Client.Tests;
