--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

with DHCP.Notify.Mock;
with DHCP.Timer.Test;
with DHCP.Types;
with DHCP.Timing_Events.Timer_Expiry;

with DHCPv6.Database.Mock;
with DHCPv6.IAs;
with DHCPv6.Message;
with DHCPv6.Options;
with DHCPv6.States.Mock;
with DHCPv6.Transactions.Mock;

package body DHCPv6.States.Requesting.Tests
is

   use Ahven;

   package OI  renames DHCPv6.Options.Inst;
   package OI6 renames DHCPv6.Options.Inst6;

   -------------------------------------------------------------------------

   procedure Get_Name
   is
   begin
      Assert (Condition => State.Get_Name = "Requesting",
              Message   => "Name mismatch");
   end Get_Name;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DHCPv6 client state 'Requesting'");
      T.Add_Test_Routine
        (Routine => Get_Name'Access,
         Name    => "Name getter");
      T.Add_Test_Routine
        (Routine => Process_Reply_Message'Access,
         Name    => "Process reply message");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Process_Reply_Message
   is

      procedure Process_Reply;
      procedure SID_Mismatch;
      procedure Status_Error;
      procedure Status_Not_On_Link;
      procedure IA_Status_Not_On_Link;
      procedure Zero_Timers;

      ----------------------------------------------------------------------

      procedure IA_Status_Not_On_Link
      is
         T   : Transactions.Mock.Transaction_Type;
         Msg : Message.Message_Type := Message.Create (Kind => Message.Reply);

         Opts : OI6.Option_List;

         Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
         Dummy_2 : Database.Mock.Cleaner;
      begin
         Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
         Database.Add_Global_Option (Opt => Database.Mock.Ref_Server_ID);

         Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);

         Opts.Append (New_Item => Options.Inst.Create
                      (Name => Options.Status_Code,
                       Data => (16#00#, 16#04#)));
         declare
            IA : constant OI6.IA_NA_Option_Type
              := Options.Create (IAID => 52,
                                 T1   => 16779216.0,
                                 T2   => 251661240.0,
                                 Opts => Opts);
         begin
            Database.Add_IA
              (IA => IAs.Create (IAID => DHCP.Types.IAID_Type (52)));

            Msg.Add_Option (Opt => IA);
         end;

         States.State_Type'Class (State.all).Process_Reply_Msg
           (Transaction => T,
            Msg         => Msg);

         Assert (Condition => DHCP.Timer.Event_Count = 0,
                 Message   => "Timer no cleared");
         Assert (Condition => T.Restart_Srv_Disc_Count = 1,
                 Message   => "Server discovery not restarted");
      end IA_Status_Not_On_Link;

      ----------------------------------------------------------------------

      procedure Process_Reply
      is
         use type DHCP.Notify.Reason_Type;
         use type OI.Option_Type'Class;

         Now     : constant Ada.Real_Time.Time   := Ada.Real_Time.Clock;
         Ref_DNS : constant OI.Option_Type'Class := OI.Create
           (Name => Options.Name_Servers,
            Data => (16#20#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
                     16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
                     16#00#, 16#ff#, 16#20#, 16#00#, 16#00#, 16#00#, 16#00#,
                     16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
                     16#00#, 16#00#, 16#00#, 16#fe#));
         IA_Addr : constant OI6.IA_Address_Option_Type
           := OI6.IA_Address_Option_Type
             (Options.Inst.Create
                (Name => Options.IA_Address,
                 Data => (16#20#, 16#01#, 16#0d#, 16#00#, 16#00#, 16#01#,
                          16#00#, 16#02#, 16#00#, 16#00#, 16#00#, 16#00#,
                          16#00#, 16#00#, 16#12#, 16#01#, 16#00#, 16#00#,
                          16#1c#, 16#1f#, 16#00#, 16#00#, 16#2d#, 16#4c#)));

         T   : Transactions.Mock.Transaction_Type;
         Msg : Message.Message_Type := Message.Create
           (Kind => Message.Reply);

         Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
         Dummy_2 : Database.Mock.Cleaner;
         Dummy_3 : DHCP.Notify.Mock.Notify_Cleaner;
      begin
         DHCP.Notify.Mock.Install;
         Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
         Database.Add_Global_Option (Opt => Database.Mock.Ref_Server_ID);

         Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);
         Msg.Add_Option (Opt => Ref_DNS);

         for I in Natural range 1 .. 3 loop
            declare
               IA : IAs.Identity_Association_Type := IAs.Create
                 (IAID => DHCP.Types.IAID_Type (I));
            begin
               Database.Add_IA (IA => IA);
               IAs.Set_Timestamp (IA        => IA,
                                  Timestamp => Now);
               IAs.Set_Timeouts (IA => IA,
                                 T1 => Duration (I * 1000),
                                 T2 => Duration (I * 2000));
               IAs.Add_Address (IA      => IA,
                                Address => IA_Addr);
               Msg.Add_Option (Opt => IAs.To_Option (IA => IA));
            end;
         end loop;

         States.State_Type'Class (State.all).Process_Reply_Msg
           (Transaction => T,
            Msg         => Msg);

         Assert (Condition => T.Reset_Count = 1,
                 Message   => "Transaction not reset");
         Assert (Condition => T.Reset_Retrans_Count = 1,
                 Message   => "Transaction retransmission not canceled");
         Assert (Condition => Database.Contains_Option
                 (Name => Options.Name_Servers),
                 Message   => "Name server option not stored in DB");
         Assert (Condition => Database.Get_Global_Options.Get
                 (Name => Options.Name_Servers) = Ref_DNS,
                 Message   => "Name server option mismatch");

         declare
            procedure Check_IA (IA : IAs.Identity_Association_Type);
            procedure Check_IA (IA : IAs.Identity_Association_Type)
            is
               use Ada.Real_Time;

               ID : constant DHCP.Types.IAID_Type := IAs.Get_ID (IA => IA);
            begin
               Assert (Condition => To_Duration
                       (IAs.Get_Timestamp (IA => IA) - Now) < 1.0,
                       Message   => "IA" & ID'Img & " Timestamp mismatch");
               Assert (Condition => IAs.Get_T1
                       (IA => IA) = Duration (ID) * 1000.0,
                       Message   => "IA" & ID'Img & " T1 mismatch");
               Assert (Condition => IAs.Get_T2
                       (IA => IA) = Duration (ID) * 2000.0,
                       Message   => "IA" & ID'Img & " T2 mismatch");
               Assert (Condition => IAs.Get_Addresses (IA => IA).Get
                       (Name => Options.IA_Address)
                       = OI.Option_Type'Class (IA_Addr),
                       Message   => "IA" & ID'Img & " address mismatch");
            end Check_IA;
         begin
            Database.Iterate (Process => Check_IA'Access);
         end;

         Assert (Condition => DHCP.Timer.Event_Count = 3,
                 Message   => "T1, T2 and Lease expiry timers not scheduled");

         declare
            use Ada.Real_Time;
            use type DHCP.Types.DHCP_Timer_Kind;

            Ev  : constant DHCP.Timing_Events.Timer_Expiry.Expiry_Type
              := DHCP.Timing_Events.Timer_Expiry.Expiry_Type
                (DHCP.Timer.Test.Get_Next_Event);
            Dur : constant Duration := To_Duration (Ev.Get_Time - Now);
         begin
            Assert (Condition => Ev.Timer_Kind = DHCP.Types.T1,
                    Message   => "T1 not correct timer kind");
            Assert (Condition =>  Dur > 999.0 and then Dur < 1001.0,
                    Message   => "T1 not in 1000 seconds");
            DHCP.Timer.Cancel (Event => Ev);
         end;

         declare
            use Ada.Real_Time;
            use type DHCP.Types.DHCP_Timer_Kind;

            Ev  : constant DHCP.Timing_Events.Timer_Expiry.Expiry_Type
              := DHCP.Timing_Events.Timer_Expiry.Expiry_Type
                (DHCP.Timer.Test.Get_Next_Event);
            Dur : constant Duration := To_Duration (Ev.Get_Time - Now);
         begin
            Assert (Condition => Ev.Timer_Kind = DHCP.Types.T2,
                    Message   => "T2 not correct timer kind");
            Assert (Condition =>  Dur > 1999.0 and then Dur < 2001.0,
                    Message   => "T2 not in 2000 seconds");
            DHCP.Timer.Cancel (Event => Ev);
         end;

         declare
            use Ada.Real_Time;
            use type DHCP.Types.DHCP_Timer_Kind;

            Ev  : constant DHCP.Timing_Events.Timer_Expiry.Expiry_Type
              := DHCP.Timing_Events.Timer_Expiry.Expiry_Type
                (DHCP.Timer.Test.Get_Next_Event);
            Dur : constant Duration := To_Duration (Ev.Get_Time - Now);
         begin
            Assert (Condition => Ev.Timer_Kind = DHCP.Types.Lease_Expiry,
                    Message   => "Lease expiry not correct timer kind");
            Assert (Condition =>  Dur > 11595.0 and then Dur < 11597.0,
                    Message   => "Lease expiry not in 11596 seconds");
            DHCP.Timer.Cancel (Event => Ev);
         end;

         Assert
           (Condition => DHCP.Notify.Mock.Get_Last_Reason = DHCP.Notify.Bound,
            Message   => "No bound notification");
      end Process_Reply;

      ----------------------------------------------------------------------

      procedure SID_Mismatch
      is
         use type DHCP.States.State_Handle;

         T   : Transactions.Mock.Transaction_Type;
         Msg : Message.Message_Type := Message.Create (Kind => Message.Reply);

         Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
         Dummy_3 : Database.Mock.Cleaner;
      begin
         Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
         Database.Add_Global_Option (Opt => Database.Mock.Ref_Server_ID);

         Msg.Add_Option (Opt => OI.Create
           (Name => Options.Server_Identifier,
            Data => (16#00#, 16#01#, 16#00#, 16#01#, 16#ff#, 16#ff#, 16#ff#)));

         States.State_Type'Class (State.all).Process_Reply_Msg
           (Transaction => T,
            Msg         => Msg);

         Assert (Condition => T.Reset_Count = 0,
                 Message   => "Transaction reset");
         Assert (Condition => T.Reset_Retrans_Count = 0,
                 Message   => "Transaction retransmission canceled");
         Assert (Condition => T.State = Mock.State,
                 Message   => "Transitioned to state " & T.State.Get_Name);
      end SID_Mismatch;

      ----------------------------------------------------------------------

      procedure Status_Error
      is
         T   : Transactions.Mock.Transaction_Type;
         Msg : Message.Message_Type := Message.Create (Kind => Message.Reply);

         Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
         Dummy_3 : Database.Mock.Cleaner;
      begin
         Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
         Database.Add_Global_Option (Opt => Database.Mock.Ref_Server_ID);

         Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);
         Msg.Add_Option (Opt => Options.Inst.Create
                         (Name => Options.Status_Code,
                          Data => (16#00#, 16#01#)));

         States.State_Type'Class (State.all).Process_Reply_Msg
           (Transaction => T,
            Msg         => Msg);

         Assert (Condition => T.Reset_Count = 0,
                 Message   => "Transaction reset");
         Assert (Condition => T.Reset_Retrans_Count = 0,
                 Message   => "Transaction retransmission canceled");
         Assert (Condition => T.Set_State_Count = 0,
                 Message   => "Transitioned to state "
                 & T.Last_Set_State.Get_Name);
      end Status_Error;

      ----------------------------------------------------------------------

      procedure Status_Not_On_Link
      is
         T   : Transactions.Mock.Transaction_Type;
         Msg : Message.Message_Type := Message.Create (Kind => Message.Reply);

         Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
         Dummy_2 : Database.Mock.Cleaner;
      begin
         Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
         Database.Add_Global_Option (Opt => Database.Mock.Ref_Server_ID);

         Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);
         Msg.Add_Option (Opt => Options.Inst.Create
                         (Name => Options.Status_Code,
                          Data => (16#00#, 16#04#)));

         States.State_Type'Class (State.all).Process_Reply_Msg
           (Transaction => T,
            Msg         => Msg);

         Assert (Condition => DHCP.Timer.Event_Count = 0,
                 Message   => "Timer no cleared");
         Assert (Condition => T.Restart_Srv_Disc_Count = 1,
                 Message   => "Server discovery not restarted");
      end Status_Not_On_Link;

      ----------------------------------------------------------------------

      procedure Zero_Timers
      is
         use type OI.Option_Type'Class;

         Now     : constant Ada.Real_Time.Time   := Ada.Real_Time.Clock;
         Ref_DNS : constant OI.Option_Type'Class := OI.Create
           (Name => Options.Name_Servers,
            Data => (16#20#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
                     16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
                     16#00#, 16#ff#, 16#20#, 16#00#, 16#00#, 16#00#, 16#00#,
                     16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
                     16#00#, 16#00#, 16#00#, 16#fe#));
         IA_Addr : constant OI6.IA_Address_Option_Type
           := OI6.IA_Address_Option_Type
             (Options.Inst.Create
                (Name => Options.IA_Address,
                 Data => (16#20#, 16#01#, 16#0d#, 16#00#, 16#00#, 16#01#,
                          16#00#, 16#02#, 16#00#, 16#00#, 16#00#, 16#00#,
                          16#00#, 16#00#, 16#12#, 16#01#, 16#00#, 16#00#,
                          16#1c#, 16#20#, 16#00#, 16#00#, 16#2d#, 16#4c#)));
         Pref_Lifetime : constant Duration := 7200.0;

         T   : Transactions.Mock.Transaction_Type;
         Msg : Message.Message_Type := Message.Create
           (Kind => Message.Reply);

         Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
         Dummy_2 : Database.Mock.Cleaner;
         Dummy_3 : DHCP.Notify.Mock.Notify_Cleaner;
      begin
         DHCP.Notify.Mock.Install;
         Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
         Database.Add_Global_Option (Opt => Database.Mock.Ref_Server_ID);

         Msg.Add_Option (Opt => Database.Mock.Ref_Server_ID);
         Msg.Add_Option (Opt => Ref_DNS);

         declare
            Pref, Valid : Duration;
         begin
            OI6.Get_Lifetimes (Option    => IA_Addr,
                               Preferred => Pref,
                               Valid     => Valid);
         end;
         declare
            IA : IAs.Identity_Association_Type := IAs.Create (IAID => 1);
         begin
            Database.Add_IA (IA => IA);
            IAs.Set_Timestamp (IA        => IA,
                               Timestamp => Now);
            IAs.Set_Timeouts (IA => IA,
                              T1 => 0.0,
                              T2 => 0.0);
            IAs.Add_Address (IA      => IA,
                             Address => IA_Addr);
            Msg.Add_Option (Opt => IAs.To_Option (IA => IA));
         end;

         States.State_Type'Class (State.all).Process_Reply_Msg
           (Transaction => T,
            Msg         => Msg);

         Assert (Condition => T.Reset_Count = 1,
                 Message   => "Transaction not reset");
         Assert (Condition => T.Reset_Retrans_Count = 1,
                 Message   => "Transaction retransmission not canceled");
         Assert (Condition => Database.Contains_Option
                 (Name => Options.Name_Servers),
                 Message   => "Name server option not stored in DB");
         Assert (Condition => Database.Get_Global_Options.Get
                 (Name => Options.Name_Servers) = Ref_DNS,
                 Message   => "Name server option mismatch");

         declare
            use Ada.Real_Time;

            IA : constant IAs.Identity_Association_Type
              := Database.Get_IA (ID => 1);
         begin
            Assert (Condition => To_Duration
                    (IAs.Get_Timestamp (IA => IA) - Now) < 1.0,
                    Message   => "IA Timestamp mismatch");
            Assert (Condition => IAs.Get_T1
                    (IA => IA) = Pref_Lifetime * 0.5,
                    Message   => "IA T1 mismatch");
            Assert (Condition => IAs.Get_T2
                    (IA => IA) = Pref_Lifetime * 0.8,
                    Message   => "IA T2 mismatch");
            Assert (Condition => IAs.Get_Addresses (IA => IA).Get
                    (Name => Options.IA_Address)
                    = OI.Option_Type'Class (IA_Addr),
                    Message   => "IA address mismatch");
         end;

         Assert (Condition => DHCP.Timer.Event_Count = 3,
                 Message   => "T1, T2 and Lease expiry timers not scheduled");
      end Zero_Timers;
   begin
      Process_Reply;
      Zero_Timers;
      SID_Mismatch;
      Status_Error;
      Status_Not_On_Link;
      IA_Status_Not_On_Link;
   end Process_Reply_Message;

end DHCPv6.States.Requesting.Tests;
