--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.States;
with DHCP.Timer.Test;
with DHCP.Types;

with DHCPv6.Database.Mock;
with DHCPv6.IAs;
with DHCPv6.Options;
with DHCPv6.Transactions.Mock;
with DHCPv6.States.Init;
with DHCPv6.States.Renewing;

package body DHCPv6.States.Bound.Tests
is

   use Ahven;

   package OI6 renames DHCPv6.Options.Inst6;

   -------------------------------------------------------------------------

   procedure Get_Name
   is
   begin
      Assert (Condition => State.Get_Name = "Bound",
              Message   => "Name mismatch");
   end Get_Name;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for DHCPv6 client state 'Bound'");
      T.Add_Test_Routine
        (Routine => Get_Name'Access,
         Name    => "Name getter");
      T.Add_Test_Routine
        (Routine => Stop'Access,
         Name    => "Stop operation");
      T.Add_Test_Routine
        (Routine => T1_Expiry'Access,
         Name    => "T1 expiry");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Stop
   is
      use type DHCP.States.State_Handle;

      IA_Addr : constant OI6.IA_Address_Option_Type
        := OI6.IA_Address_Option_Type
          (Options.Inst.Create
             (Name => Options.IA_Address,
              Data => (16#20#, 16#01#, 16#0d#, 16#00#, 16#00#, 16#01#,
                       16#00#, 16#02#, 16#00#, 16#00#, 16#00#, 16#00#,
                       16#00#, 16#00#, 16#12#, 16#01#, 16#00#, 16#00#,
                       16#1c#, 16#1f#, 16#00#, 16#00#, 16#2d#, 16#4c#)));

      T      : Transactions.Mock.Transaction_Type;
      Ref_IA : IAs.Identity_Association_Type := IAs.Create (IAID => 42);

      Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
      Dummy_2 : Database.Mock.Cleaner;
   begin
      Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
      Database.Add_Global_Option (Opt => Database.Mock.Ref_Server_ID);

      IAs.Add_Address (IA      => Ref_IA,
                       Address => IA_Addr);
      Database.Add_IA (IA => Ref_IA);
      Database.Add_IA (IA => IAs.Create (IAID => 23));

      States.State_Type'Class (State.all).Stop (Transaction => T);

      Assert (Condition => T.Reset_Count = 1,
              Message   => "Transaction not reset");
      Assert (Condition => T.Set_Retrans_P_Count = 1,
              Message   => "Retransmission parameters not set");
      Assert (Condition => T.Set_Start_Time_Count = 1,
              Message   => "Start time not set");
      Assert (Condition => T.Send_Msg_Count = 1,
              Message   => "No message sent");

      declare
         use type Options.Inst.Option_Type'Class;

         Msg  : constant Message.Message_Type      := T.Last_Sent_Msg;
         Opts : constant Options.Inst6.Option_List := Msg.Get_Options;
      begin
         Assert (Condition => Msg.Get_Kind = Message.Release,
                 Message   => "Message kind not release");
         Assert (Condition => Opts.Contains
                 (Name => Options.Client_Identifier),
                 Message   => "Client ID option missing");
         Assert (Condition => Opts.Contains
                 (Name => Options.Server_Identifier),
                 Message   => "Server ID option missing");
         Assert (Condition => Opts.Contains (Name => Options.IA_NA),
                 Message   => "IA_NA option missing");
         Assert (Condition => Opts.Get
                 (Name => Options.IA_NA) = Options.Inst.Option_Type'Class
                 (IAs.To_Option (IA => Ref_IA)),
                 Message   => "IA_NA option mismatch");
      end;

      Assert (Condition => Database.Get_IA_Count = 0,
              Message   => "Not all IAs removed from database");
      Assert (Condition => Database.Get_Global_Options.Get_Count = 1,
              Message   => "Not all config options removed from database");
      Assert (Condition => Database.Contains_Option
              (Name => Options.Client_Identifier),
              Message   => "Client ID removed from database");

      Assert (Condition => T.State = Init.State,
              Message   => "Not transitioned to state "
              & Init.State.Get_Name);
   end Stop;

   -------------------------------------------------------------------------

   procedure T1_Expiry
   is
      use type DHCP.States.State_Handle;

      T : Transactions.Mock.Transaction_Type;

      Dummy_1 : DHCP.Timer.Test.Cleaner_Type;
      Dummy_2 : Database.Mock.Cleaner;
   begin
      Database.Initialize (Client_ID => Database.Mock.Ref_Client_ID);
      Database.Add_Global_Option (Opt => Database.Mock.Ref_Server_ID);
      Database.Add_IA (IA => IAs.Create (IAID => 23));

      States.State_Type'Class (State.all).Process_T1_Expiry (Transaction => T);

      Assert (Condition => T.ID /= 0,
              Message   => "XID is 0");
      Assert (Condition => T.Set_Retrans_P_Count = 1,
              Message   => "Retransmission parameters not set");
      Assert (Condition => T.Set_Start_Time_Count = 1,
              Message   => "Start time not set");
      Assert (Condition => T.Send_Msg_Count = 1,
              Message   => "No message sent");

      declare
         Msg  : constant Message.Message_Type      := T.Last_Sent_Msg;
         Opts : constant Options.Inst6.Option_List := Msg.Get_Options;
      begin
         Assert (Condition => Msg.Get_Kind = Message.Renew,
                 Message   => "Message kind not renew");
         Assert (Condition => Msg.Get_Transaction_ID = T.ID,
                 Message   => "Message transaction ID mismatch");
         Assert (Condition => Opts.Contains
                 (Name => Options.Client_Identifier),
                 Message   => "Client ID option missing");
         Assert (Condition => Opts.Contains
                 (Name => Options.Server_Identifier),
                 Message   => "Server ID option missing");
         Assert (Condition => Opts.Contains
                 (Name => Options.Inst6.Default_Request_Option.Get_Name),
                 Message   => "Default request option missing");
         Assert (Condition => Opts.Contains (Name => Options.IA_NA),
                 Message   => "IA_NA option missing");

         declare
            use type DHCP.Types.IAID_Type;

            IA : constant Options.Inst6.IA_NA_Option_Type
              := Options.Inst6.IA_NA_Option_Type
                (Opts.Get (Name => Options.IA_NA));
         begin
            Assert (Condition => IA.Get_IAID = 23,
                    Message   => "IAID mismatch");
         end;
      end;

      Assert (Condition => T.State = Renewing.State,
              Message   => "Not transitioned to state "
              & Renewing.State.Get_Name);
   end T1_Expiry;

end DHCPv6.States.Bound.Tests;
