--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;
with Ada.Exceptions;

with Interfaces;

with Anet;

with DHCP.Types;

with DHCPv6.Options;

package body DHCPv6_Options_Tests
is

   use Ahven;
   use Ada.Streams;
   use DHCPv6.Options;

   -------------------------------------------------------------------------

   procedure Create_DNS_Name_Server_Option
   is
   begin
      declare
         D : constant Stream_Element_Array
           := (16#20#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
               16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#ff#,
               16#20#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
               16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#fe#);
         O : constant Inst6.IP_Array_Option_Type
           := Inst6.IP_Array_Option_Type
             (Inst.Create (Name => Name_Servers,
                           Data => D));
      begin
         Assert (Condition => O.Get_Name = Name_Servers,
                 Message   => "Name mismatch");
         Assert (Condition => O.Get_Addrlen = 16,
                 Message   => "Addrlen mismatch");
         Assert (Condition => O.Get_Data_Str = "2000::ff 2000::fe",
                 Message   => "String mismatch " & O.Get_Data_Str);
      end;

      begin
         declare
            Dummy : constant Inst6.IP_Array_Option_Type
              := Inst6.IP_Array_Option_Type
                (Inst.Create (Name => Name_Servers,
                              Data => (1 => 0)));
         begin
            Fail (Message => "Exception expected");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "IP array option NAME_SERVERS: data size 1, expected"
                    & " multiple of 16",
                    Message   => "Exception mismatch");
      end;
   end Create_DNS_Name_Server_Option;

   -------------------------------------------------------------------------

   procedure Create_IA_Addr_Option
   is
   begin
      declare
         use type Inst6.IA_Address_Option_Type;

         D : constant Stream_Element_Array
           := (16#20#, 16#01#, 16#0d#, 16#b8#, 16#11#, 16#11#, 16#00#, 16#00#,
               16#35#, 16#56#, 16#a4#, 16#d2#, 16#6e#, 16#b2#, 16#c8#, 16#6e#,
               16#00#, 16#00#, 16#0e#, 16#10#, 16#ff#, 16#ff#, 16#ff#, 16#ff#,
               16#00#, 16#0d#, 16#00#, 16#04#, 16#00#, 16#05#, 16#4f#, 16#4b#);
         O : constant Inst6.IA_Address_Option_Type
           := Inst6.IA_Address_Option_Type
             (Inst.Create (Name => IA_Address,
                           Data => D));

         Opts : constant Inst6.Option_List'Class := O.Get_Options;

         A : constant Inst6.IA_Address_Option_Type
           := Create (Address   => Anet.To_IPv6_Addr
                      (Str => "2001:0DB8:1111:0000:3556:A4D2:6EB2:C86E"),
                      Preferred => 3600.0,
                      Valid     => Duration (Interfaces.Unsigned_32'Last),
                      Opts      => Opts);

         Pref, Valid : Duration;
      begin
         Inst6.Get_Lifetimes (Option    => O,
                              Preferred => Pref,
                              Valid     => Valid);

         Assert (Condition => Anet.To_String (Address => O.Get_Address)
                 = "2001:db8:1111:0:3556:a4d2:6eb2:c86e",
                 Message   => "Address mismatch");

         Assert (Condition => Pref = 3600.0,
                 Message   => "Preferred lifetime incorrect" & Pref'Img);
         Assert (Condition => Valid = Duration (Interfaces.Unsigned_32'Last),
                 Message   => "Valid lifetime incorrect" & Valid'Img);

         Assert (Condition => Opts.Get_Count = 1,
                 Message   => "Option count mismatch");

         Assert (Condition => A = O,
                 Message   => "IA address options mismatch");
      end;

      begin
         declare
            D : constant Stream_Element_Array
              := (16#20#, 16#01#, 16#0d#, 16#b8#, 16#11#, 16#11#, 16#00#,
                  16#00#, 16#35#, 16#56#, 16#a4#, 16#d2#, 16#6e#, 16#b2#,
                  16#c8#, 16#6e#, 16#00#, 16#00#, 16#0e#, 16#10#, 16#00#,
                  16#00#, 16#1c#, 16#20#,

                  16#00#, 16#07#, 16#00#, 16#01#, 16#00#);
            Dummy : constant Inst6.IA_Address_Option_Type
              := Inst6.IA_Address_Option_Type
                (Inst.Create (Name => IA_Address,
                              Data => D));
         begin
            Fail (Message => "Exception expected (1)");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "IA_ADDRESS option: contains invalid option 7",
                    Message   => "Exception mismatch (1)");
      end;

      begin
         declare
            D : constant Stream_Element_Array
              := (16#20#, 16#01#, 16#0d#, 16#b8#, 16#11#, 16#11#, 16#00#,
                  16#00#, 16#35#, 16#56#, 16#a4#, 16#d2#, 16#6e#, 16#b2#,
                  16#c8#, 16#6e#, 16#00#, 16#00#, 16#0e#, 16#10#, 16#00#,
                  16#00#, 16#1c#);
            Dummy : constant Inst6.IA_Address_Option_Type
              := Inst6.IA_Address_Option_Type
                (Inst.Create (Name => IA_Address,
                              Data => D));
         begin
            Fail (Message => "Exception expected (2)");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "IA_ADDRESS option: Data payload too short",
                    Message   => "Exception mismatch (2)");
      end;

      begin
         declare
            D : constant Stream_Element_Array
              := (16#20#, 16#01#, 16#0d#, 16#b8#, 16#11#, 16#11#, 16#00#,
                  16#00#, 16#35#, 16#56#, 16#a4#, 16#d2#, 16#6e#, 16#b2#,
                  16#c8#, 16#6e#, 16#00#, 16#00#, 16#2c#, 16#21#, 16#00#,
                  16#00#, 16#1c#, 16#20#);
            Dummy : constant Inst6.IA_Address_Option_Type
              := Inst6.IA_Address_Option_Type
                (Inst.Create (Name => IA_Address,
                              Data => D));
         begin
            Fail (Message => "Exception expected (3)");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "IA_ADDRESS option: Preferred lifetime of "
                    & "11297.000000000 exceeds valid lifetime of "
                    & "7200.000000000",
                    Message   => "Exception mismatch (3)");
      end;
   end Create_IA_Addr_Option;

   -------------------------------------------------------------------------

   procedure Create_IA_NA_Option
   is
   begin
      declare
         use type DHCP.Types.IAID_Type;
         use type Inst6.IA_NA_Option_Type;

         D : constant Stream_Element_Array
           := (16#01#, 16#00#, 16#00#, 16#01#, 16#01#, 16#00#, 16#07#, 16#d0#,
               16#0f#, 16#00#, 16#0b#, 16#b8#, 16#00#, 16#05#, 16#00#, 16#18#,
               16#20#, 16#01#, 16#0d#, 16#b8#, 16#11#, 16#11#, 16#00#, 16#00#,
               16#35#, 16#56#, 16#a4#, 16#d2#, 16#6e#, 16#b2#, 16#c8#, 16#6e#,
               16#00#, 16#00#, 16#0e#, 16#10#, 16#00#, 16#00#, 16#1c#, 16#20#,
               16#00#, 16#0d#, 16#00#, 16#1e#, 16#00#, 16#00#, 16#41#, 16#6c#,
               16#6c#, 16#20#, 16#61#, 16#64#, 16#64#, 16#72#, 16#65#, 16#73#,
               16#73#, 16#65#, 16#73#, 16#20#, 16#77#, 16#65#, 16#72#, 16#65#,
               16#20#, 16#61#, 16#73#, 16#73#, 16#69#, 16#67#, 16#6e#, 16#65#,
               16#64#, 16#2e#);
         O : constant Inst6.IA_NA_Option_Type
           := Inst6.IA_NA_Option_Type
             (Inst.Create (Name => IA_NA,
                           Data => D));

         Opts : constant Inst6.Option_List'Class := O.Get_Options;

         A : constant Inst6.IA_NA_Option_Type
           := Create (IAID => 16777217,
                      T1   => 16779216.0,
                      T2   => 251661240.0,
                      Opts => Opts);

         T1, T2 : Duration;
      begin
         Assert (Condition => O.Get_IAID = 16777217,
                 Message   => "IAID mismatch");

         O.Get_Ts (T1 => T1,
                   T2 => T2);
         Assert (Condition => T1 = 16779216.0,
                 Message   => "T1 mismatch" & T1'Img);
         Assert (Condition => T2 = 251661240.0,
                 Message   => "T2 mismatch" & T2'Img);

         Assert (Condition => Opts.Get_Count = 2,
                 Message   => "Option count mismatch");

         Assert (Condition => O = A,
                 Message   => "IA_NA options mismatch");
      end;

      begin
         declare
            Dummy : constant Inst6.IA_NA_Option_Type
              := Inst6.IA_NA_Option_Type
                (Inst.Create (Name => IA_NA,
                              Data => (16#00#, 16#02#)));
         begin
            Fail (Message => "Exception expected (1)");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "IA_NA option: Data payload too short",
                    Message   => "Exception mismatch (1)");
      end;

      begin
         declare
            D : constant Stream_Element_Array
              := (16#01#, 16#00#, 16#00#, 16#01#, 16#01#, 16#00#, 16#07#,
                  16#d0#, 16#0f#, 16#00#, 16#0b#, 16#b8#, 16#00#, 16#07#,
                  16#00#, 16#01#, 16#00#);
            Dummy : constant Inst6.IA_NA_Option_Type
              := Inst6.IA_NA_Option_Type
                (Inst.Create (Name => IA_NA,
                              Data => D));
         begin
            Fail (Message => "Exception expected (2)");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "IA_NA option: contains invalid option 7",
                    Message   => "Exception mismatch (2)");
      end;

      begin
         declare
            D : constant Stream_Element_Array
              := (16#01#, 16#00#, 16#00#, 16#01#, 16#00#, 16#00#, 16#0b#,
                  16#b8#, 16#00#, 16#00#, 16#07#, 16#d0#);
            Dummy : constant Inst6.IA_NA_Option_Type
              := Inst6.IA_NA_Option_Type
                (Inst.Create (Name => IA_NA,
                              Data => D));
         begin
            Fail (Message => "Exception expected (3)");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "IA_NA option: T1 3000.000000000 > T2 2000.000000000",
                    Message   => "Exception mismatch (3)");
      end;
   end Create_IA_NA_Option;

   -------------------------------------------------------------------------

   procedure Create_Raw_Options
   is
      D   : constant Stream_Element_Array := (192, 168, 10, 1);
      Opt : constant Inst.Option_Type'Class
        := Inst.Create (Name => Unknown,
                        Data => D);
   begin
      Assert (Condition => Opt.Get_Data = D,
              Message   => "Data mismatch");
      Assert (Condition => Opt.Get_Name = Unknown,
              Message   => "Name mismatch");

      begin
         declare
            L     : constant Stream_Element_Array
              (0 .. Inst.Size_Type'Last + 1)
              := (others => 12);
            Dummy : constant Inst.Option_Type'Class
              := Inst.Create (Name => Unknown,
                              Data => L);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Could not create option: data size not in range",
                    Message   => "Exception mismatch");
      end;
   end Create_Raw_Options;

   -------------------------------------------------------------------------

   procedure Create_Status_Code_Option
   is
   begin
      declare
         use type Inst6.Status_Code_Type;

         D : constant Stream_Element_Array
           := (16#00#, 16#05#, 16#4f#, 16#4b#);
         O : constant Inst6.Status_Code_Option_Type
           := Inst6.Status_Code_Option_Type
             (Inst.Create (Name => Status_Code,
                           Data => D));
      begin
         Assert (Condition => O.Get_Status = Inst6.Status_Use_Multicast,
                 Message   => "Status mismatch");
         Assert (Condition => O.Get_Status_Message = "OK",
                 Message   => "Message mismatch");
      end;

      declare
         D : constant Stream_Element_Array
           := (16#00#, 16#00#);
         O : constant Inst6.Status_Code_Option_Type
           := Inst6.Status_Code_Option_Type
             (Inst.Create (Name => Status_Code,
                           Data => D));
      begin
         Assert (Condition => O.Get_Status_Message'Length = 0,
                 Message   => "Empty string expected");
      end;

      begin
         declare
            D : constant Stream_Element_Array
              := (16#12#, 16#12#, 16#4f#, 16#4b#);
            Dummy : constant Inst6.Status_Code_Option_Type
              := Inst6.Status_Code_Option_Type
                (Inst.Create (Name => Status_Code,
                              Data => D));
         begin
            Fail (Message => "Exception expected (1)");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "STATUS_CODE option: Invalid status code 4626",
                    Message   => "Exception mismatch (1)");
      end;

      begin
         declare
            D : constant Stream_Element_Array
              := (1 => 16#00#);
            Dummy : constant Inst6.Status_Code_Option_Type
              := Inst6.Status_Code_Option_Type
                (Inst.Create (Name => Status_Code,
                              Data => D));
         begin
            Fail (Message => "Exception expected (2)");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "STATUS_CODE option: Invalid format",
                    Message   => "Exception mismatch (2)");
      end;
   end Create_Status_Code_Option;

   -------------------------------------------------------------------------

   procedure Deserialize_Option_Lists
   is
      use type Inst6.Option_Code_Array;
   begin
      declare
         C     : constant Inst6.Option_Code_Array (1 .. 0)
           := (others => Code_Type'Last);
         Empty : constant Stream_Element_Array (1 .. 0) := (others => 0);
         L     : Inst6.Option_List;
      begin
         L := Inst6.Deserialize (Buffer => Empty);

         Assert (Condition => L.Get_Count = 0,
                 Message   => "Option count not 0");
         Assert (Condition => L.Get_Codes = C,
                 Message   => "Empty array expected");
      end;

      declare
         Invalid : constant Stream_Element_Array (1 .. 3) := (others => 12);
         Dummy   : Inst6.Option_List;
      begin
         Dummy := Inst6.Deserialize (Buffer => Invalid);
         Fail (Message => "Exception expected");

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Could not parse malformed options",
                    Message   => "Exception message mismatch");
      end;

      declare
         C : constant Inst6.Option_Code_Array (1 .. 3) := (1, 6, 8);
         D : constant Stream_Element_Array
           := (
               --  Client ID

               16#00#, 16#01#, 16#00#, 16#0e#, 16#00#, 16#01#, 16#00#, 16#01#,
               16#1d#, 16#9a#, 16#91#, 16#9e#, 16#08#, 16#00#, 16#27#, 16#ae#,
               16#b5#, 16#ed#,

               --  Option request

               16#00#, 16#06#, 16#00#, 16#04#, 16#00#, 16#17#, 16#00#, 16#18#,

               --  Elapsed time

               16#00#, 16#08#, 16#00#, 16#02#, 16#00#, 16#00#
              );
         L : Inst6.Option_List;
      begin
         L := Inst6.Deserialize (Buffer => D);

         Assert (Condition => L.Get_Count = 3,
                 Message   => "Option count not 3");
         Assert (Condition => L.Get_Data_Size = 20,
                 Message   => "Size mismatch");
         Assert (Condition => L.Contains (Name => Client_Identifier),
                 Message   => "Client ID missing");
         Assert (Condition => L.Contains (Name => Option_Request),
                 Message   => "Option request missing");
         Assert (Condition => L.Contains (Name => Elapsed_Time),
                 Message   => "Elapsed time missing");
         Assert (Condition => L.Get_Codes = C,
                 Message   => "Option codes mismatch");
      end;

      Skip_Invalid :
      declare
         D : constant Stream_Element_Array
           := (
               --  Client ID

               16#00#, 16#01#, 16#00#, 16#0e#, 16#00#, 16#01#, 16#00#, 16#01#,
               16#1d#, 16#9a#, 16#91#, 16#9e#, 16#08#, 16#00#, 16#27#, 16#ae#,
               16#b5#, 16#ed#,

               --  Option request
               16#00#, 16#06#, 16#00#, 16#04#, 16#00#, 16#17#, 16#00#, 16#18#,

               --  Invalid Elapsed time option

               16#00#, 16#08#, 16#00#, 16#01#, 16#00#, 16#00#
              );
         L : Inst6.Option_List;
      begin
         L := Inst6.Deserialize (Buffer => D);

         Assert (Condition => L.Get_Count = 2,
                 Message   => "Option count not 2");
         Assert (Condition => not L.Contains (Name => Elapsed_Time),
                 Message   => "Elapsed time option present");
      end Skip_Invalid;
   end Deserialize_Option_Lists;

   -------------------------------------------------------------------------

   procedure Deserialize_Options
   is

   begin
      declare
         OK : constant Stream_Element_Array
           := (16#cb#, 16#ab#, 16#00#, 16#02#, 16#05#, 16#01#);

         Ref_Data : constant Stream_Element_Array := (16#05#, 16#01#);
         Opt      : constant Inst.Option_Type'Class
           := Inst.Deserialize (Buffer => OK);
      begin
         Assert (Condition => Opt.Get_Name = Unknown,
                 Message   => "Name mismatch");
         Assert (Condition => Opt.Get_Name_Str = "unknown",
                 Message   => "Name string mismatch");
         Assert (Condition => Opt.Get_Size = 2,
                 Message   => "Size mismatch");
         Assert (Condition => Opt.Get_Data = Ref_Data,
                 Message   => "Data mismatch");
         Assert (Condition => Opt.Get_Code = 52139,
                 Message   => "Code mismatch");
      end;

      begin
         declare
            Invalid_Size : constant Stream_Element_Array
              := (16#22#, 16#12#, 16#00#, 16#00#, 16#05#);

            Dummy : constant Inst.Option_Type'Class := Inst.Deserialize
              (Buffer => Invalid_Size);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Option UNKNOWN has invalid data size: 1",
                    Message   => "Exception mismatch (1)");
      end;

      begin
         declare
            No_Data : constant Stream_Element_Array
              := (16#22#, 16#12#, 16#00#, 16#01#);

            Dummy : constant Inst.Option_Type'Class := Inst.Deserialize
              (Buffer => No_Data);
         begin
            Fail (Message => "Expected invalid option error");
         end;

      exception
         when E : Inst.Invalid_Option =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Option UNKNOWN has no data",
                    Message   => "Exception mismatch (2)");
      end;
   end Deserialize_Options;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Options package");
      T.Add_Test_Routine
        (Routine => Create_Raw_Options'Access,
         Name    => "Create raw options");
      T.Add_Test_Routine
        (Routine => Deserialize_Options'Access,
         Name    => "Deserialize options");
      T.Add_Test_Routine
        (Routine => Serialize_Option'Access,
         Name    => "Serialize option");
      T.Add_Test_Routine
        (Routine => Deserialize_Option_Lists'Access,
         Name    => "Deserialize option lists");
      T.Add_Test_Routine
        (Routine => Serialize_Option_Lists'Access,
         Name    => "Serialize option lists");
      T.Add_Test_Routine
        (Routine => Create_DNS_Name_Server_Option'Access,
         Name    => "Create DNS recursive name server option");
      T.Add_Test_Routine
        (Routine => Create_Status_Code_Option'Access,
         Name    => "Create status code option");
      T.Add_Test_Routine
        (Routine => Create_IA_Addr_Option'Access,
         Name    => "Create IA address option");
      T.Add_Test_Routine
        (Routine => Create_IA_NA_Option'Access,
         Name    => "Create IA NA option");
      T.Add_Test_Routine
        (Routine => List_Contains'Access,
         Name    => "List contains function");
   end Initialize;

   -------------------------------------------------------------------------

   procedure List_Contains
   is
      L : Inst6.Option_List := Inst6.Empty_List;
   begin
      Assert (Condition => not L.Contains (Name => Reconfigure_Message),
              Message   => "Empty list contains option");

      L.Append (New_Item => Inst.Create
                (Name => Rapid_Commit,
                 Data => (16#00#, 16#17#, 16#00#, 16#18#)));
      Assert (Condition => L.Contains (Name => Rapid_Commit),
              Message   => "List does not contain option");

      Assert (Condition => not L.Contains (Name => Reconfigure_Message),
              Message   => "List contains non-existent option");
   end List_Contains;

   -------------------------------------------------------------------------

   procedure Serialize_Option
   is
      R1 : constant Stream_Element_Array
        := (16#00#, 16#08#, 16#00#, 16#02#, 16#05#, 16#12#);
      O1 : constant Inst.Option_Type'Class
        := Inst.Create (Name => Elapsed_Time,
                        Data => (16#05#, 16#12#));

      R2 : constant Stream_Element_Array
        := (16#cb#, 16#ab#, 16#00#, 16#02#, 16#05#, 16#01#);
      O2 : constant Inst.Option_Type'Class
        := Inst.Deserialize (Buffer => R2);
   begin
      Assert (Condition => O1.Serialize = R1,
              Message   => "Serialization failed (1)");
      Assert (Condition => O2.Serialize = R2,
              Message   => "Serialization failed (2)");
   end Serialize_Option;

   -------------------------------------------------------------------------

   procedure Serialize_Option_Lists
   is
      Empty : constant Stream_Element_Array (1 .. 0) := (others => 0);

      D : constant Stream_Element_Array
        := (
            --  Client ID

            16#00#, 16#01#, 16#00#, 16#0e#, 16#00#, 16#01#, 16#00#, 16#01#,
            16#1d#, 16#9a#, 16#91#, 16#9e#, 16#08#, 16#00#, 16#27#, 16#ae#,
            16#b5#, 16#ed#,

            --  Option request

            16#00#, 16#06#, 16#00#, 16#04#, 16#00#, 16#17#, 16#00#, 16#18#,

            --  Elapsed time

            16#00#, 16#08#, 16#00#, 16#02#, 16#00#, 16#12#
           );

      L : Inst6.Option_List;
   begin
      Assert (Condition => L.Serialize = Empty,
              Message   => "Empty list expected");

      L.Append (New_Item => Inst.Create
                (Name => Client_Identifier,
                 Data => (16#00#, 16#01#, 16#00#, 16#01#, 16#1d#, 16#9a#,
                          16#91#, 16#9e#, 16#08#, 16#00#, 16#27#, 16#ae#,
                          16#b5#, 16#ed#)));
      L.Append (New_Item => Inst.Create
                (Name => Option_Request,
                 Data => (16#00#, 16#17#, 16#00#, 16#18#)));
      L.Append (New_Item => Inst.Create
                (Name => Elapsed_Time,
                 Data => (16#00#, 16#12#)));

      Assert (Condition => L.Serialize = D,
              Message   => "Serialization failed");
   end Serialize_Option_Lists;

end DHCPv6_Options_Tests;
