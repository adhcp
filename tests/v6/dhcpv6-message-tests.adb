--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;

with DHCPv6.Options;

package body DHCPv6.Message.Tests
is

   use Ahven;

   -------------------------------------------------------------------------

   procedure Add_Option_To_Msg
   is
      use type DHCPv6.Options.Inst.Option_Type'Class;

      Msg     : Message_Type;
      Opts    : Options.Inst6.Option_List;
      Ref_Opt : constant Options.Inst.Option_Type'Class
        := Options.Inst.Create
          (Name => Options.Preference,
           Data => (1 => 7));
   begin
      Opts := Msg.Get_Options;
      Assert (Condition => Opts.Is_Empty,
              Message   => "List not empty");

      Msg.Add_Option (Opt => Ref_Opt);

      Opts := Msg.Get_Options;
      Assert (Condition => Opts.Get_Count = 1,
              Message   => "Opts count not 1");

      Assert (Condition => Opts.Get (Name => Options.Preference) =  Ref_Opt,
              Message   => "Option mismatch");
   end Add_Option_To_Msg;

   -------------------------------------------------------------------------

   procedure Create_Message
   is
      Msg : constant Message_Type := Create (Kind => Reconfigure);
   begin
      Assert (Condition => Msg.Kind = Reconfigure,
              Message   => "Kind mismatch");
   end Create_Message;

   -------------------------------------------------------------------------

   procedure Deserialize_Msg
   is
      use type DHCPv6.Types.Transaction_ID_Type;

      B : constant Ada.Streams.Stream_Element_Array
        := (16#07#, 16#1d#, 16#8f#, 16#76#, 16#00#, 16#02#, 16#00#, 16#0e#,
            16#00#, 16#01#, 16#00#, 16#01#, 16#1d#, 16#9f#, 16#9a#, 16#3f#,
            16#08#, 16#00#, 16#27#, 16#5d#, 16#d7#, 16#9b#);
      M : constant Message_Type := Deserialize (Buffer => B);
   begin
      Assert (Condition => M.Get_Kind = Reply,
              Message   => "Message kind mismatch");
      Assert (Condition => M.Get_Transaction_ID = 1937270,
              Message   => "Transaction ID mismatch");
      Assert (Condition => M.Get_Options.Get_Count = 1,
              Message   => "Option count mismatch");
   end Deserialize_Msg;

   -------------------------------------------------------------------------

   procedure Deserialize_Msg_Invalid_Kind
   is
      B     : constant Ada.Streams.Stream_Element_Array
        := (16#99#, 16#1d#, 16#8f#, 16#76#, 16#00#);
      Dummy : Message_Type;
   begin
      Dummy := Deserialize (Buffer => B);
      Fail (Message => "Exception expected");

   exception
      when E : Invalid_Message =>
         Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                 = "Could not deserialize message - invalid kind value 153",
                 Message   => "Exception mismatch");
   end Deserialize_Msg_Invalid_Kind;

   -------------------------------------------------------------------------

   procedure Deserialize_Msg_Invalid_Options
   is
   begin
      Malformed :
      declare
         B     : constant Ada.Streams.Stream_Element_Array
           := (16#07#, 16#1d#, 16#8f#, 16#76#, 16#00#, 16#00#, 16#07#);
         Dummy : Message_Type;
      begin
         Dummy := Deserialize (Buffer => B);
         Fail (Message => "Exception expected (1)");

      exception
         when E : Invalid_Message =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Could not deserialize message - Could not parse "
                    & "malformed options",
                    Message   => "Exception mismatch (2)");
      end Malformed;

      Invalid_Option_Present :
      declare
         B     : constant Ada.Streams.Stream_Element_Array
           := (16#07#, 16#1d#, 16#8f#, 16#76#, 16#00#, 16#05#, 16#00#, 16#18#,
               16#20#, 16#01#, 16#0d#, 16#b8#, 16#00#, 16#01#, 16#00#, 16#02#,
               16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#10#, 16#03#,
               16#00#, 16#00#, 16#0e#, 16#10#, 16#00#, 16#00#, 16#0e#, 16#10#);
         Dummy : Message_Type;
      begin
         Dummy := Deserialize (Buffer => B);
         Fail (Message => "Exception expected (2)");

      exception
         when E : Invalid_Message =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Could not deserialize message - Illegal option "
                    & "IA_ADDRESS present",
                    Message   => "Exception mismatch (2)");
      end Invalid_Option_Present;
   end Deserialize_Msg_Invalid_Options;

   -------------------------------------------------------------------------

   procedure Deserialize_Msg_Invalid_Size
   is
      B     : constant Ada.Streams.Stream_Element_Array
        := (16#99#, 16#1d#, 16#8f#);
      Dummy : Message_Type;
   begin
      Dummy := Deserialize (Buffer => B);
      Fail (Message => "Exception expected");

   exception
      when E : Invalid_Message =>
         Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                 = "Could not deserialize message - invalid buffer size: 3 "
                 & "byte(s)",
                 Message   => "Exception mismatch");
   end Deserialize_Msg_Invalid_Size;

   -------------------------------------------------------------------------

   procedure Get_Kind
   is
      Msg : constant Message_Type
        := (Kind   => Rebind,
            others => <>);
   begin
      Assert (Condition => Msg.Get_Kind = Rebind,
              Message   => "Kind mismatch");
   end Get_Kind;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Message package");
      T.Add_Test_Routine
        (Routine => Create_Message'Access,
         Name    => "Create message");
      T.Add_Test_Routine
        (Routine => Get_Kind'Access,
         Name    => "Get message kind");
      T.Add_Test_Routine
        (Routine => Set_Transaction_ID'Access,
         Name    => "Set transaction ID");
      T.Add_Test_Routine
        (Routine => Add_Option_To_Msg'Access,
         Name    => "Add option to message");
      T.Add_Test_Routine
        (Routine => Deserialize_Msg'Access,
         Name    => "Deserialize message");
      T.Add_Test_Routine
        (Routine => Deserialize_Msg_Invalid_Size'Access,
         Name    => "Deserialize message (invalid size)");
      T.Add_Test_Routine
        (Routine => Deserialize_Msg_Invalid_Kind'Access,
         Name    => "Deserialize message (invalid kind)");
      T.Add_Test_Routine
        (Routine => Deserialize_Msg_Invalid_Options'Access,
         Name    => "Deserialize message (invalid options)");
      T.Add_Test_Routine
        (Routine => Serialize_Msg'Access,
         Name    => "Serialize message");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Serialize_Msg
   is
      use type Ada.Streams.Stream_Element_Array;

      B : constant Ada.Streams.Stream_Element_Array
        := (16#07#, 16#1d#, 16#8f#, 16#76#, 16#00#, 16#0d#, 16#00#, 16#04#,
            16#00#, 16#05#, 16#4f#, 16#4b#);
      M : Message_Type := Create (Kind => Reply);
   begin
      M.Set_Transaction_ID (XID => 1937270);
      M.Add_Option
        (Opt => Options.Inst.Create
           (Name => Options.Status_Code,
            Data => (16#00#, 16#05#, 16#4f#, 16#4b#)));

      Assert (Condition => M.Serialize = B,
              Message   => "Buffer mismatch");
   end Serialize_Msg;

   -------------------------------------------------------------------------

   procedure Set_Transaction_ID
   is
      use type DHCPv6.Types.Transaction_ID_Type;

      Msg : Message_Type
        := (Kind   => Rebind,
            others => <>);
   begin
      Msg.Set_Transaction_ID (XID => 12);
      Assert (Condition => Msg.X_ID = 12,
              Message   => "XID mismatch (1)");
      Assert (Condition => Msg.Get_Transaction_ID = 12,
              Message   => "XID mismatch (2)");
   end Set_Transaction_ID;

end DHCPv6.Message.Tests;
