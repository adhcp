--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

private with Ada.Finalization;

with DHCPv6.Options;

package DHCPv6.Database.Mock
is

   type Cleaner is private;
   --  Database test cleanup helper.

   package OI renames DHCPv6.Options.Inst;

   Ref_Client_ID : constant OI.Raw_Option_Type := OI.Raw_Option_Type
     (OI.Create
        (Name => Options.Client_Identifier,
         Data => (16#00#, 16#04#, 16#97#, 16#CE#, 16#21#, 16#53#,
                  16#71#, 16#C9#, 16#E0#, 16#11#, 16#B2#, 16#70#,
                  16#35#, 16#E4#, 16#71#, 16#05#, 16#65#, 16#52#)));

   Ref_Server_ID : constant OI.Raw_Option_Type := OI.Raw_Option_Type
     (OI.Create
        (Name => Options.Server_Identifier,
         Data => (16#00#, 16#01#, 16#00#, 16#01#, 16#1d#, 16#95#, 16#2b#,
                  16#30#, 16#08#, 16#00#, 16#27#, 16#5d#, 16#d7#, 16#9b#)));

private

   type Cleaner is new Ada.Finalization.Controlled with null record;

   overriding
   procedure Finalize (C : in out Cleaner);

end DHCPv6.Database.Mock;
