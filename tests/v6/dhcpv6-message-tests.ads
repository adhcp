--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ahven.Framework;

package DHCPv6.Message.Tests
is

   type Testcase is new Ahven.Framework.Test_Case with null record;

   procedure Initialize (T : in out Testcase);
   --  Initialize testcase.

   procedure Create_Message;
   --  Verify message creation.

   procedure Get_Kind;
   --  Verify message kind getter.

   procedure Set_Transaction_ID;
   --  Verify transaction ID setter/getter.

   procedure Add_Option_To_Msg;
   --  Verify adding of options.

   procedure Deserialize_Msg;
   --  Check DHCPv6 message deserialization.

   procedure Deserialize_Msg_Invalid_Size;
   --  Check DHCPv6 message deserialization (invalid size).

   procedure Deserialize_Msg_Invalid_Kind;
   --  Check DHCPv6 message deserialization (invalid kind).

   procedure Deserialize_Msg_Invalid_Options;
   --  Check DHCPv6 message deserialization (invalid options).

   procedure Serialize_Msg;
   --  Serialize DHCPv6 message to stream element array.

end DHCPv6.Message.Tests;
