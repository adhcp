--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCPv6.Constants;

package body DHCPv6.IAs.Tests
is

   use Ahven;

   package OI  renames DHCPv6.Options.Inst;
   package OI6 renames DHCPv6.Options.Inst6;

   Ref_IA_Addr : constant OI6.IA_Address_Option_Type
     := OI6.IA_Address_Option_Type
       (Options.Inst.Create
          (Name => Options.IA_Address,
           Data => (16#20#, 16#01#, 16#0d#, 16#b8#, 16#00#, 16#01#, 16#00#,
                    16#02#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
                    16#10#, 16#01#, 16#00#, 16#00#, 16#1c#, 16#20#, 16#00#,
                    16#00#, 16#1d#, 16#4c#)));

   -------------------------------------------------------------------------

   procedure Add_Address
   is
      use type Ada.Containers.Count_Type;

      IA_Addr : constant OI6.IA_Address_Option_Type
        := OI6.IA_Address_Option_Type
          (Options.Inst.Create
             (Name => Options.IA_Address,
              Data => (16#20#, 16#01#, 16#0d#, 16#00#, 16#00#, 16#01#, 16#00#,
                       16#02#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
                       16#12#, 16#01#, 16#00#, 16#00#, 16#1c#, 16#1f#, 16#00#,
                       16#00#, 16#2d#, 16#4c#)));

      IA : Identity_Association_Type;
   begin
      Add_Address (IA      => IA,
                   Address => Ref_IA_Addr);
      Assert (Condition => IA.Addresses.Contains
              (Key => Ref_IA_Addr.Get_Address),
              Message   => "Addition of address failed");

      Add_Address (IA      => IA,
                   Address => Ref_IA_Addr);
      Assert (Condition => IA.Addresses.Length = 1,
              Message   => "Update of address failed (1)");
      Assert (Condition => IA.Addresses.Contains
              (Key => Ref_IA_Addr.Get_Address),
              Message   => "Update of address failed (2)");

      Add_Address (IA      => IA,
                   Address => IA_Addr);
      Assert (Condition => IA.Addresses.Length = 2,
              Message   => "Addition of second address failed");
   end Add_Address;

   -------------------------------------------------------------------------

   procedure Create_IA
   is
      use type Ada.Real_Time.Time;
      use type DHCP.Types.IAID_Type;

      IA : constant Identity_Association_Type := Create (IAID => 42);
   begin
      Assert (Condition => IA.IAID = 42,
              Message   => "IAID mismatch");
      Assert (Condition => IA.Timestamp = Ada.Real_Time.Time_First,
              Message   => "Timestamp mismatch");
      Assert (Condition => IA.T1 = 0.0,
              Message   => "T1 mismatch");
      Assert (Condition => IA.T2 = 0.0,
              Message   => "T2 mismatch");
      Assert (Condition => IA.Addresses.Is_Empty,
              Message   => "Addresses not empty");
   end Create_IA;

   -------------------------------------------------------------------------

   procedure Discard_Expired_Addresses
   is
      use type Options.Inst.Option_Type'Class;

      IA_Addr_Infinity : constant OI6.IA_Address_Option_Type
        := OI6.IA_Address_Option_Type
          (Options.Inst.Create
             (Name => Options.IA_Address,
              Data => (16#20#, 16#01#, 16#0d#, 16#66#, 16#00#, 16#01#, 16#00#,
                       16#02#, 16#52#, 16#00#, 16#00#, 16#25#, 16#00#, 16#00#,
                       16#12#, 16#01#, 16#00#, 16#00#, 16#1c#, 16#1f#, 16#ff#,
                       16#ff#, 16#ff#, 16#ff#)));
      IA_Addr_Zero     : constant OI6.IA_Address_Option_Type
        := OI6.IA_Address_Option_Type
          (Options.Inst.Create
             (Name => Options.IA_Address,
              Data => (16#20#, 16#01#, 16#0d#, 16#66#, 16#00#, 16#01#, 16#00#,
                       16#02#, 16#52#, 16#00#, 16#00#, 16#25#, 16#00#, 16#00#,
                       16#12#, 16#01#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
                       16#00#, 16#00#, 16#00#)));

      IA    : Identity_Association_Type := Create (IAID => 23);
      Addrs : OI6.Option_List;
   begin
      IA.Timestamp := Ada.Real_Time.Time_First;
      IA.Addresses.Insert (Key      => IA_Addr_Zero.Get_Address,
                           New_Item => IA_Addr_Zero);
      Discard_Expired_Addresses (IA            => IA,
                                 Removed_Addrs => Addrs);
      Assert (Condition => IA.Addresses.Is_Empty,
              Message   => "Address with valid set to 0 not discarded");
      Assert (Condition => Addrs.Get_Count = 1,
              Message   => "Address missing in removed addresses list (1)");
      Assert (Condition => Addrs.Get
              (Name => Options.IA_Address) = OI.Option_Type'Class
              (IA_Addr_Zero),
              Message   => "Removed address mismatch (1)");

      IA.Addresses.Insert (Key      => Ref_IA_Addr.Get_Address,
                           New_Item => Ref_IA_Addr);
      Discard_Expired_Addresses (IA            => IA,
                                 Removed_Addrs => Addrs);
      Assert (Condition => IA.Addresses.Is_Empty,
              Message   => "Expired address not discarded (1)");
      Assert (Condition => Addrs.Get_Count = 1,
              Message   => "Address missing in removed addresses list (2)");
      Assert (Condition => Addrs.Get
              (Name => Options.IA_Address) = OI.Option_Type'Class
              (Ref_IA_Addr),
              Message   => "Removed address mismatch (2)");

      IA.Timestamp := Ada.Real_Time.Clock;
      IA.Addresses.Insert (Key      => Ref_IA_Addr.Get_Address,
                           New_Item => Ref_IA_Addr);
      Discard_Expired_Addresses (IA            => IA,
                                 Removed_Addrs => Addrs);
      Assert (Condition => not IA.Addresses.Is_Empty,
              Message   => "Non-expired address discarded");
      Assert (Condition => Addrs.Is_Empty,
              Message   => "Removed addresses list not empty");

      IA.Timestamp := Ada.Real_Time.Time_First;
      IA.Addresses.Insert (Key      => IA_Addr_Infinity.Get_Address,
                           New_Item => IA_Addr_Infinity);
      Discard_Expired_Addresses (IA            => IA,
                                 Removed_Addrs => Addrs);
      Assert (Condition => IA.Addresses.Contains
              (Key => IA_Addr_Infinity.Get_Address),
              Message   => "Infinitely valid address discarded");
      Assert (Condition => not IA.Addresses.Contains
              (Key => Ref_IA_Addr.Get_Address),
              Message   => "Expired address not discarded (2)");
      Assert (Condition => Addrs.Get_Count = 1,
              Message   => "Address missing in removed addresses list (3)");
      Assert (Condition => Addrs.Get
              (Name => Options.IA_Address) = OI.Option_Type'Class
              (Ref_IA_Addr),
              Message   => "Removed address mismatch (3)");

      IA.Timestamp := Ada.Real_Time.Time_First;
      IA.Addresses.Insert (Key      => Anet.Loopback_Addr_V6,
                           New_Item => Ref_IA_Addr);
      IA.Addresses.Insert (Key      => Ref_IA_Addr.Get_Address,
                           New_Item => Ref_IA_Addr);
      Discard_Expired_Addresses (IA            => IA,
                                 Removed_Addrs => Addrs);
      Assert (Condition => Addrs.Get_Count = 2,
              Message   => "Address missing in removed addresses list"
              & Addrs.Get_Count'Img);
   end Discard_Expired_Addresses;

   -------------------------------------------------------------------------

   procedure Get_Addresses
   is
      procedure Check_Addr (Option : Options.Inst.Option_Type'Class);
      procedure Check_Addr (Option : Options.Inst.Option_Type'Class)
      is
         use type OI6.IA_Address_Option_Type;
      begin
         Assert (Condition => OI6.IA_Address_Option_Type
                 (Option) = Ref_IA_Addr,
                 Message   => "Address mismatch");
      end Check_Addr;

      IA : Identity_Association_Type := Create (IAID => 24);
   begin
      Assert (Condition => Get_Addresses (IA => IA).Is_Empty,
              Message   => "Got addresses for empty IA");

      IA.Addresses.Insert (Key      => Ref_IA_Addr.Get_Address,
                           New_Item => Ref_IA_Addr);
      IA.Addresses.Insert (Key      => Anet.Loopback_Addr_V6,
                           New_Item => Ref_IA_Addr);

      declare
         Opts : constant OI6.Option_List'Class := Get_Addresses (IA => IA);
      begin
         Assert (Condition => Opts.Get_Count = 2,
                 Message   => "Address count mismatch");
         Opts.Iterate (Process => Check_Addr'Access);
      end;
   end Get_Addresses;

   -------------------------------------------------------------------------

   procedure Get_Expiry
   is
      IA_Addr : constant OI6.IA_Address_Option_Type
        := OI6.IA_Address_Option_Type
          (Options.Inst.Create
             (Name => Options.IA_Address,
              Data => (16#20#, 16#01#, 16#0d#, 16#00#, 16#00#, 16#01#, 16#00#,
                       16#02#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
                       16#12#, 16#01#, 16#00#, 16#00#, 16#1c#, 16#1f#, 16#00#,
                       16#00#, 16#2d#, 16#4c#)));
      IA_Addr_Infinity : constant OI6.IA_Address_Option_Type
        := OI6.IA_Address_Option_Type
          (Options.Inst.Create
             (Name => Options.IA_Address,
              Data => (16#20#, 16#01#, 16#0d#, 16#00#, 16#00#, 16#01#, 16#00#,
                       16#02#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
                       16#12#, 16#01#, 16#00#, 16#00#, 16#1c#, 16#1f#, 16#ff#,
                       16#ff#, 16#ff#, 16#ff#)));

      IA : Identity_Association_Type := Create (IAID => 24);
   begin
      Assert (Condition => Get_Expiry (IA => IA) = 0.0,
              Message   => "Expiration time mismatch (1)");

      Add_Address (IA      => IA,
                   Address => Ref_IA_Addr);
      Assert (Condition => Get_Expiry (IA => IA) = 7500.0,
              Message   => "Expiration time mismatch (2)");

      Add_Address (IA      => IA,
                   Address => IA_Addr);
      Assert (Condition => Get_Expiry (IA => IA) = 11596.0,
              Message   => "Expiration time mismatch (3)");

      Add_Address (IA      => IA,
                   Address => IA_Addr_Infinity);
      Assert (Condition => Get_Expiry (IA => IA) = Constants.Infinity,
              Message   => "Expiration time mismatch (4)");
   end Get_Expiry;

   -------------------------------------------------------------------------

   procedure Get_IA_ID
   is
      use type DHCP.Types.IAID_Type;

      IA : constant Identity_Association_Type
        := (IAID   => 241214,
            others => <>);
   begin
      Assert (Condition => Get_ID (IA => IA) = 241214,
              Message   => "IA ID mismatch");
   end Get_IA_ID;

   -------------------------------------------------------------------------

   procedure Get_T1
   is
      IA : constant Identity_Association_Type
        := (T1     => 42.0,
            others => <>);
   begin
      Assert (Condition => Get_T1 (IA => IA) = 42.0,
              Message   => "T1 mismatch");
   end Get_T1;

   -------------------------------------------------------------------------

   procedure Get_T2
   is
      IA : constant Identity_Association_Type
        := (T2     => 4123.0,
            others => <>);
   begin
      Assert (Condition => Get_T2 (IA => IA) = 4123.0,
              Message   => "T2 mismatch");
   end Get_T2;

   -------------------------------------------------------------------------

   procedure Get_Timestamp
   is
      use type Ada.Real_Time.Time;

      Now : constant Ada.Real_Time.Time := Ada.Real_Time.Clock;
      IA  : constant Identity_Association_Type
        := (Timestamp => Now,
            others    => <>);
   begin
      Assert (Condition => Get_Timestamp (IA => IA) = Now,
              Message   => "Timestamp mismatch");
   end Get_Timestamp;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for identity association package");
      T.Add_Test_Routine
        (Routine => Create_IA'Access,
         Name    => "Create identity association");
      T.Add_Test_Routine
        (Routine => Get_IA_ID'Access,
         Name    => "Get IA ID");
      T.Add_Test_Routine
        (Routine => Set_Timestamp'Access,
         Name    => "Set IA timestamp");
      T.Add_Test_Routine
        (Routine => Set_Timeouts'Access,
         Name    => "Set IA timeouts");
      T.Add_Test_Routine
        (Routine => Get_T1'Access,
         Name    => "Get IA T1");
      T.Add_Test_Routine
        (Routine => Get_T2'Access,
         Name    => "Get IA T2");
      T.Add_Test_Routine
        (Routine => Get_Expiry'Access,
         Name    => "Get IA expiration time");
      T.Add_Test_Routine
        (Routine => Get_Timestamp'Access,
         Name    => "Get IA Timestamp");
      T.Add_Test_Routine
        (Routine => Add_Address'Access,
         Name    => "Add address to IA");
      T.Add_Test_Routine
        (Routine => Remove_Address'Access,
         Name    => "Remove address from IA");
      T.Add_Test_Routine
        (Routine => Iterate'Access,
         Name    => "Iterate IA addresses");
      T.Add_Test_Routine
        (Routine => Discard_Expired_Addresses'Access,
         Name    => "Discard expired addresses");
      T.Add_Test_Routine
        (Routine => Get_Addresses'Access,
         Name    => "Get addresses");
      T.Add_Test_Routine
        (Routine => To_Option'Access,
         Name    => "IA to option conversion");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Iterate
   is
      IA    : Identity_Association_Type;
      Count : Natural := 0;

      procedure Check_Addr (Addr : OI6.IA_Address_Option_Type);
      procedure Check_Addr (Addr : OI6.IA_Address_Option_Type)
      is
         use type OI6.IA_Address_Option_Type;
      begin
         Count := Count + 1;
         Assert (Condition => Addr = Ref_IA_Addr,
                 Message   => "Address" & Count'Img & " mismatch");
      end Check_Addr;

      Addr : Anet.IPv6_Addr_Type := Ref_IA_Addr.Get_Address;
   begin
      for I in Natural range 1 .. 5 loop
         Addr (Addr'Last) := Anet.Byte (I);
         IA.Addresses.Insert (Key      => Addr,
                              New_Item => Ref_IA_Addr);
      end loop;

      Iterate (IA      => IA,
               Process => Check_Addr'Access);
      Assert (Condition => Count = 5,
              Message   => "Iteration count mismatch");
   end Iterate;

   -------------------------------------------------------------------------

   procedure Remove_Address
   is
      IA : Identity_Association_Type;
   begin
      IA.Addresses.Insert (Key      => Ref_IA_Addr.Get_Address,
                           New_Item => Ref_IA_Addr);

      Remove_Address (IA      => IA,
                      Address => Anet.To_IPv6_Addr
                        (Str => "2001:0db8:0001:0002:0000:0000:0000:1001"));
      Assert (Condition => IA.Addresses.Is_Empty,
              Message   => "Removal of address from IA failed");

      --  Removal of non-existent address must not raise an exception

      Remove_Address (IA      => IA,
                      Address => Anet.Loopback_Addr_V6);
   end Remove_Address;

   -------------------------------------------------------------------------

   procedure Set_Timeouts
   is
      IA : Identity_Association_Type;
   begin
      Set_Timeouts (IA => IA,
                    T1 => 2048.0,
                    T2 => 9001.0);

      Assert (Condition => IA.T1 = 2048.0,
              Message   => "T1 mismatch");
      Assert (Condition => IA.T2 = 9001.0,
              Message   => "T2 mismatch");
   end Set_Timeouts;

   -------------------------------------------------------------------------

   procedure Set_Timestamp
   is
      use type Ada.Real_Time.Time;

      Now : constant Ada.Real_Time.Time := Ada.Real_Time.Clock;
      IA  : Identity_Association_Type;
   begin
      Set_Timestamp (IA        => IA,
                     Timestamp => Now);

      Assert (Condition => IA.Timestamp = Now,
              Message   => "Timestamp mismatch");
   end Set_Timestamp;

   -------------------------------------------------------------------------

   procedure To_Option
   is
      use type OI6.IA_NA_Option_Type;

      Ref_Opt : constant Options.Inst6.IA_NA_Option_Type
        := Options.Create (IAID => 16777217,
                           T1   => 16779216.0,
                           T2   => 251661240.0,
                           Opts => Options.Inst6.Empty_List);

      IA : Identity_Association_Type
        := (IAID   => 16777217,
            T1     => 16779216.0,
            T2     => 251661240.0,
            others => <>);
   begin
      Assert (Condition => To_Option (IA => IA) = Ref_Opt,
              Message   => "IA options mismatch");

      IA.IAID := 23;
      Assert (Condition => To_Option (IA => IA) /= Ref_Opt,
              Message   => "IA options match");
   end To_Option;

end DHCPv6.IAs.Tests;
