--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;
with Ada.Real_Time;

with Anet.Util;

with DHCP.OS;

with DHCPv6.Database.IO;
with DHCPv6.Database.Mock;
with DHCPv6.Options;

with Test_Utils;

package body DHCPv6.Database.Tests
is

   use Ahven;

   Ref_DB : constant String := "data/database-v6-"
     & Test_Utils.Get_Word_Size & ".ref";

   -------------------------------------------------------------------------

   procedure Add_Global_Option
   is
      use type DHCPv6.Options.Inst.Option_Type'Class;
      use type DHCPv6.Options.Inst.Raw_Option_Type;

      Ref_Time : constant Options.Inst.Option_Type'Class := Options.Inst.Create
        (Name => Options.Elapsed_Time,
         Data => (16#00#, 16#42#));

      Dummy : Mock.Cleaner;
   begin
      Add_Global_Option (Opt => Options.Inst6.Default_Request_Option);
      Assert (Condition => Instance.Global_Opts.Get_Count = 1,
              Message   => "Option not added");
      Assert (Condition => DHCPv6.Options.Inst.Raw_Option_Type
              (Instance.Global_Opts.Get
                 (Name => Options.Inst6.Default_Request_Option.Get_Name))
              = Options.Inst6.Default_Request_Option,
              Message   => "Added option mismatch");

      Instance.Global_Opts.Append (New_Item => Options.Inst.Create
                                   (Name => Options.Elapsed_Time,
                                    Data => (16#00#, 16#23#)));
      Assert (Condition => Instance.Global_Opts.Get
              (Name => Options.Elapsed_Time) /= Ref_Time,
              Message   => "Elapsed time option matches");
      Add_Global_Option (Opt => Ref_Time);
      Assert (Condition => Instance.Global_Opts.Get
              (Name => Options.Elapsed_Time) = Ref_Time,
              Message   => "Elapsed time option mismatch");
   end Add_Global_Option;

   -------------------------------------------------------------------------

   procedure Add_IA
   is
      IA : constant IAs.Identity_Association_Type := IAs.Create (IAID => 75);

      Dummy : Mock.Cleaner;
   begin
      Assert (Condition => Instance.IAs.Is_Empty,
              Message   => "Default database contains IAs");

      Add_IA (IA => IA);
      Assert (Condition => Instance.IAs.Contains (Key => 75),
              Message   => "Error adding IA");

      begin
         Add_IA (IA => IA);
         Fail (Message => "Exception expected");

      exception
         when E : IA_Present =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Existing identity association with ID 75",
                    Message   => "Exception message mismatch");
      end;
   end Add_IA;

   -------------------------------------------------------------------------

   procedure Contains_IA
   is
      IA : constant IAs.Identity_Association_Type
        := IAs.Create (IAID => 16589);

      Dummy : Mock.Cleaner;
   begin
      Assert (Condition => not Contains (IA => 12),
              Message   => "Non-existent IA in empty database");

      Instance.IAs.Insert (Key      => 16589,
                           New_Item => IA);
      Assert (Condition => Contains (IA => 16589),
              Message   => "IA not in database");
      Assert (Condition => not Contains (IA => 98778),
              Message   => "Non-existent IA in database");
   end Contains_IA;

   -------------------------------------------------------------------------

   procedure Contains_Option
   is
      Dummy : Mock.Cleaner;
   begin
      Assert (Condition => not Contains_Option
              (Name => Options.Client_Identifier),
              Message   => "Contains non-existent option");

      Instance.Global_Opts.Append (New_Item => Mock.Ref_Client_ID);
      Assert (Condition => Contains_Option (Name => Options.Client_Identifier),
              Message   => "Does not contain CID option");
   end Contains_Option;

   -------------------------------------------------------------------------

   procedure Get_Client_ID
   is
      use type Options.Inst.Raw_Option_Type;

      Dummy : Mock.Cleaner;
   begin
      Instance.Global_Opts.Append (New_Item => Mock.Ref_Client_ID);
      Assert (Condition => Options.Inst.Raw_Option_Type (Get_Client_ID)
              = Mock.Ref_Client_ID,
              Message   => "Client ID mismatch");
   end Get_Client_ID;

   -------------------------------------------------------------------------

   procedure Get_Global_Options
   is
      Dummy : Mock.Cleaner;
   begin
      Instance.Global_Opts.Append (New_Item => Mock.Ref_Client_ID);

      declare
         Opts : constant Options.Inst6.Option_List := Get_Global_Options;
      begin
         Assert (Condition => Opts.Get_Count = 1,
                 Message   => "Options count mismatch");
         Assert (Condition => Opts.Contains
                 (Name => Options.Client_Identifier),
                 Message   => "Options element mismatch");
      end;
   end Get_Global_Options;

   -------------------------------------------------------------------------

   procedure Get_IA
   is
      use type DHCPv6.IAs.Identity_Association_Type;

      IA : constant IAs.Identity_Association_Type := IAs.Create (IAID => 2134);

      Dummy : Mock.Cleaner;
   begin
      Instance.IAs.Insert (Key      => 2134,
                           New_Item => IA);

      Assert (Condition => Get_IA (ID => 2134) = IA,
              Message   => "IA mismatch");

      begin
         declare
            Unused : constant IAs.Identity_Association_Type
              := Get_IA (ID => 55);
         begin
            Fail (Message => "Exception expected");
         end;

      exception
         when E : IA_Missing =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "No identity association with ID 55",
                    Message   => "Exception message mismatch");
      end;
   end Get_IA;

   -------------------------------------------------------------------------

   procedure Get_IA_Count
   is
      Dummy : Mock.Cleaner;
   begin
      Assert (Condition => Get_IA_Count = 0,
              Message   => "Empty database IA count not 0");

      for I in Natural range 1 .. 10 loop
         Instance.IAs.Insert
           (Key      => DHCP.Types.IAID_Type (I),
            New_Item => IAs.Create (IAID => DHCP.Types.IAID_Type (I)));
         Assert (Condition => Get_IA_Count = I,
                 Message   => "Database IA count not" & I'Img);
      end loop;
   end Get_IA_Count;

   -------------------------------------------------------------------------

   procedure Init_DB
   is
      use type Options.Inst.Raw_Option_Type;

      Dummy : Mock.Cleaner;
   begin
      Initialize (Client_ID => Mock.Ref_Client_ID);
      Assert (Condition => Options.Inst.Raw_Option_Type
              (Instance.Global_Opts.Get
                 (Name => Options.Client_Identifier)) = Mock.Ref_Client_ID,
              Message   => "Client ID mismatch");
   end Init_DB;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for Database package");
      T.Add_Test_Routine
        (Routine => Init_DB'Access,
         Name    => "Initialize database");
      T.Add_Test_Routine
        (Routine => Is_Initialized'Access,
         Name    => "Initialize database");
      T.Add_Test_Routine
        (Routine => Get_Client_ID'Access,
         Name    => "Get client ID");
      T.Add_Test_Routine
        (Routine => Reset_DB'Access,
         Name    => "Reset database");
      T.Add_Test_Routine
        (Routine => Add_Global_Option'Access,
         Name    => "Add global option");
      T.Add_Test_Routine
        (Routine => Remove_Global_Option'Access,
         Name    => "Remove global option");
      T.Add_Test_Routine
        (Routine => Get_Global_Options'Access,
         Name    => "Get global options");
      T.Add_Test_Routine
        (Routine => Contains_Option'Access,
         Name    => "Contains global option");
      T.Add_Test_Routine
        (Routine => Add_IA'Access,
         Name    => "Add identity association");
      T.Add_Test_Routine
        (Routine => Contains_IA'Access,
         Name    => "Contains identity association");
      T.Add_Test_Routine
        (Routine => Get_IA'Access,
         Name    => "Get identity association");
      T.Add_Test_Routine
        (Routine => Remove_IA'Access,
         Name    => "Remove identity association");
      T.Add_Test_Routine
        (Routine => Update'Access,
         Name    => "Update identity association");
      T.Add_Test_Routine
        (Routine => Iterate'Access,
         Name    => "Iterate identity association(s)");
      T.Add_Test_Routine
        (Routine => Iterate'Access,
         Name    => "Get identity association count");
      T.Add_Test_Routine
        (Routine => Write'Access,
         Name    => "Write database to file");
      T.Add_Test_Routine
        (Routine => Load'Access,
         Name    => "Load database from file");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Is_Initialized
   is
      Dummy : Mock.Cleaner;
   begin
      Assert (Condition => not Is_Initialized,
              Message   => "Database is initialized");

      Instance.Global_Opts.Append (New_Item => Mock.Ref_Client_ID);
      Assert (Condition => Is_Initialized,
              Message   => "Database not initialized");
   end Is_Initialized;

   -------------------------------------------------------------------------

   procedure Iterate
   is
      Dummy : Mock.Cleaner;
      Count : Natural := 0;

      procedure Check_IA (IA : IAs.Identity_Association_Type);
      procedure Check_IA (IA : IAs.Identity_Association_Type)
      is
      begin
         Count := Count + 1;
         Assert (Condition => IAs.Get_ID (IA => IA)
                 = DHCP.Types.IAID_Type (Count),
                 Message   => "IAID" & Count'Img & " mismatch");
      end Check_IA;
   begin
      for I in DHCP.Types.IAID_Type range 1 .. 5 loop
         Instance.IAs.Insert (Key      => I,
                              New_Item => IAs.Create (IAID => I));
      end loop;

      Iterate (Process => Check_IA'Access);
      Assert (Condition => Count = 5,
              Message   => "Iteration count mismatch");
   end Iterate;

   -------------------------------------------------------------------------

   procedure Load
   is
      procedure Load_File;
      procedure Load_Nonexistent_File;
      procedure Load_Read_Error;

      ----------------------------------------------------------------------

      procedure Load_File
      is
         Dummy : Mock.Cleaner;
      begin
         IO.Load (Filename => Ref_DB);

         Assert (Condition => Instance.Global_Opts.Contains
                 (Name => Options.Client_Identifier),
                 Message   => "Loaded database mismatch");
      end Load_File;

      ----------------------------------------------------------------------

      procedure Load_Nonexistent_File
      is
      begin
         IO.Load (Filename => "nonexistent");
         Fail (Message => "Exception expected");

      exception
         when E : DHCP.OS.IO_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Error opening file - nonexistent: No such file or "
                    & "directory",
                    Message   => "Exception message mismatch");
      end Load_Nonexistent_File;

      ----------------------------------------------------------------------

      procedure Load_Read_Error
      is
      begin
         IO.Load (Filename => "data");
         Fail (Message => "Exception expected");

      exception
         when E : DHCP.OS.IO_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Unable to read data from file 'data'",
                    Message   => "Exception message mismatch");
      end Load_Read_Error;
   begin
      Load_File;
      Load_Nonexistent_File;
      Load_Read_Error;
   end Load;

   -------------------------------------------------------------------------

   procedure Remove_Global_Option
   is
      Opt1  : constant Options.Inst.Option_Type'Class := Options.Inst.Create
        (Name => Options.Elapsed_Time,
         Data => (16#00#, 16#23#));
      Dummy : Mock.Cleaner;
   begin
      Instance.Global_Opts.Append (New_Item => Opt1);
      Instance.Global_Opts.Append
        (New_Item => Options.Inst6.Default_Request_Option);

      Remove_Global_Option (Name => Options.Elapsed_Time);
      Assert (Condition => Instance.Global_Opts.Get_Count = 1,
              Message   => "Error removing option");
      Assert (Condition => not Instance.Global_Opts.Contains
              (Name => Options.Elapsed_Time),
              Message   => "Wrong option removed");
      Assert (Condition => Instance.Global_Opts.Contains
              (Name => Options.Option_Request),
              Message   => "Wrong option removed");

      Remove_Global_Option (Name => Options.Elapsed_Time);
      Assert (Condition => Instance.Global_Opts.Get_Count = 1,
              Message   => "Error removing non-existent option");
   end Remove_Global_Option;

   -------------------------------------------------------------------------

   procedure Remove_IA
   is
      IA : constant IAs.Identity_Association_Type
        := IAs.Create (IAID => 23554);

      Dummy : Mock.Cleaner;
   begin
      Instance.IAs.Insert (Key      => 23554,
                           New_Item => IA);
      Remove_IA (ID => 23554);

      Assert (Condition => Instance.IAs.Is_Empty,
              Message   => "IA not removed");

      begin
         Remove_IA (ID => 23554);
         Fail (Message => "Exception expected");

      exception
         when E : IA_Missing =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "No identity association with ID 23554",
                    Message   => "Exception message mismatch");
      end;
   end Remove_IA;

   -------------------------------------------------------------------------

   procedure Reset_DB
   is
      use type Options.Inst.Option_Type'Class;

      Dummy : Mock.Cleaner;
   begin
      Instance.Global_Opts.Append
        (New_Item => Mock.Ref_Client_ID);
      Instance.Global_Opts.Append
        (New_Item => Options.Inst6.Default_Request_Option);
      Instance.IAs.Insert (Key      => 23554,
                           New_Item => IAs.Create (IAID => 23554));
      Reset;

      Assert (Condition => Instance.IAs.Is_Empty,
              Message   => "IAs not reset");
      Assert (Condition => Instance.Global_Opts.Get_Count = 1,
              Message   => "Config options not reset");
      Assert (Condition => Instance.Global_Opts.Get
              (Name => Options.Client_Identifier)
              = Options.Inst.Option_Type'Class (Mock.Ref_Client_ID),
              Message   => "Client ID not retained");
   end Reset_DB;

   -------------------------------------------------------------------------

   procedure Update
   is
      use type DHCPv6.IAs.Identity_Association_Type;

      Dummy  : Mock.Cleaner;
      Now    : constant Ada.Real_Time.Time := Ada.Real_Time.Clock;
      Ref_T1 : constant Duration           := 4.0;
      Ref_T2 : constant Duration           := 8.0;
      Ref_IA : constant IAs.Identity_Association_Type
        := IAs.Create (IAID => 42);

      procedure Set_Timeouts
        (ID :        DHCP.Types.IAID_Type;
         IA : in out IAs.Identity_Association_Type);
      procedure Set_Timeouts
        (ID :        DHCP.Types.IAID_Type;
         IA : in out IAs.Identity_Association_Type)
      is
      begin
         Assert (Condition => ID = 42,
                 Message   => "IAID mismatch");
         IAs.Set_Timestamp (IA        => IA,
                            Timestamp => Now);
         IAs.Set_Timeouts (IA => IA,
                           T1 => Ref_T1,
                           T2 => Ref_T2);
      end Set_Timeouts;
   begin
      Instance.IAs.Insert (Key      => 42,
                           New_Item => Ref_IA);
      Update_IA (ID     => 42,
                 Update => Set_Timeouts'Access);

      declare
         use type Ada.Real_Time.Time;

         New_IA : constant IAs.Identity_Association_Type
           := Instance.IAs.Element (Key => 42);
      begin
         Assert (Condition => Ref_IA /= New_IA,
                 Message   => "IA not changed");
         Assert (Condition => IAs.Get_Timestamp (IA => New_IA) = Now,
                 Message   => "Timestamp mismatch");
         Assert (Condition => IAs.Get_T1 (IA => New_IA) = Ref_T1,
                 Message   => "T1 mismatch");
         Assert (Condition => IAs.Get_T2 (IA => New_IA) = Ref_T2,
                 Message   => "T2 mismatch");
      end;

      begin
         Update_IA (ID     => 23,
                    Update => Set_Timeouts'Access);

      exception
         when E : IA_Missing =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "No identity association with ID 23",
                    Message   => "Exception message mismatch");
      end;
   end Update;

   -------------------------------------------------------------------------

   procedure Write
   is
      procedure Write_File;
      procedure Write_Error;

      ----------------------------------------------------------------------

      procedure Write_Error
      is
      begin
         IO.Write (Filename => "data");
         Fail (Message => "Exception expected");

      exception
         when E : DHCP.OS.IO_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Error writing object to file - data: Is a directory",
                    Message   => "Exception message mismatch");
      end Write_Error;

      ----------------------------------------------------------------------

      procedure Write_File
      is
         F_Name : constant String := "/tmp/database-v6-"
           & Anet.Util.Random_String (Len => 8);

         Dummy : Mock.Cleaner;
      begin
         Instance.Global_Opts.Append (New_Item => Mock.Ref_Client_ID);
         IO.Write (Filename => F_Name);

         Assert (Condition => Test_Utils.Equal_Files
                 (Filename1 => F_Name,
                  Filename2 => Ref_DB),
                 Message   => "Written database mismatch");
         DHCP.OS.Delete_File (Filename => F_Name);
      end Write_File;
   begin
      Write_File;
      Write_Error;
   end Write;

end DHCPv6.Database.Tests;
