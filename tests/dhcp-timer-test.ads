--
--  Copyright (C) 2011-2015 secunet Security Networks AG
--  Copyright (C) 2011-2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011-2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Finalization;

with DHCP.Timing_Events;

package DHCP.Timer.Test
is

   function Get_Next_Event return Timing_Events.Event_Type'Class;

   type Cleaner_Type is new Ada.Finalization.Controlled with null record;

   overriding
   procedure Initialize (C : in out Cleaner_Type);
   --  Clear timer queue.

   overriding
   procedure Finalize (C : in out Cleaner_Type);
   --  Clear timer queue and stop timer.

end DHCP.Timer.Test;
