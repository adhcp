--
--  Copyright (C) 2014 secunet Security Networks AG
--  Copyright (C) 2014 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2014 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Cmd_Line;

package body DHCP_Cmdline_Tests is

   use Ahven;
   use DHCP;

   -------------------------------------------------------------------------

   procedure Ext_Notify_Binary_Getter
   is
      Cmd : constant String := "/usr/sbin/notifier";
   begin
      Cmd_Line.Set_Ext_Notify_Binary (B => Cmd);
      Assert (Condition => Cmd_Line.Get_Ext_Notify_Binary = Cmd,
              Message   => "Command mismatch");
   end Ext_Notify_Binary_Getter;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for command line package");
      T.Add_Test_Routine
        (Routine => Ext_Notify_Binary_Getter'Access,
         Name    => "Get external notification binary name");
   end Initialize;

end DHCP_Cmdline_Tests;
