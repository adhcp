--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ahven.Framework;

package DHCP_Timing_Events_Tests is

   type Testcase is new Ahven.Framework.Test_Case with null record;

   procedure Initialize (T : in out Testcase);
   --  Initialize testcase.

   procedure Event_Setters_Getters;
   --  Verify event setters/getters.

   procedure Queue_Event_Insertion;
   --  Verify event insertion into queue.

   procedure Queue_Event_Removal;
   --  Verify event removal from queue.

   procedure  Queue_Next_Time_Getter;
   --  Verify queue next event time getter.

   procedure Queue_Next_Event_Getter;
   --  Verify queue next event getter.

   procedure Queue_Event_Time_Changed;
   --  Verify queue event time changed entry.

   procedure Queue_Reset;
   --  Verify resetting of event queue.

   procedure Event_Start;
   --  Verify start event.

   procedure Event_Handle_Message;
   --  Verify handle message event.

   procedure Event_Timer_Expiry;
   --  Verify timer expiry event.

   procedure Event_Bind_Timeout;
   --  Verify bind timeout event.

   procedure Event_Announce;
   --  Verify ACD announcement event.

end DHCP_Timing_Events_Tests;
