--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;

with Test_Utils;

package body DHCP.OS.Tests
is

   use Ahven;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "Tests for OS package");
      T.Add_Test_Routine
        (Routine => Read_File_String'Access,
         Name    => "Read file with string content");
   end Initialize;

   -------------------------------------------------------------------------

   procedure Read_File_String
   is
      procedure Positive_Test;
      procedure Nonexistent_File;
      procedure Unreadable_File;
      procedure Nonaccessible_File;

      ----------------------------------------------------------------------

      procedure Nonaccessible_File
      is
      begin
         if Test_Utils.Has_Root_Perms then
            Skip (Message => "Must not be run as root");
         end if;

         declare
            Dummy : constant String := Read_File (Filename => "/root");
         begin
            Fail (Message => "Expected exception");
         end;

      exception
         when E : IO_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Error opening file - /root: Permission denied",
                      Message   => "Exception message mismatch (3)");
      end Nonaccessible_File;

      ----------------------------------------------------------------------

      procedure Nonexistent_File
      is
      begin
         declare
            Dummy : constant String := Read_File (Filename => "nonexistent");
         begin
            Fail (Message => "Expected exception");
         end;

      exception
         when E : IO_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Error opening file - nonexistent: No such file or "
                    & "directory",
                    Message   => "Exception message mismatch (1)");
      end Nonexistent_File;

      ----------------------------------------------------------------------

      procedure Positive_Test
      is
         Ref_Str : constant String := "interface=lo"
           & ASCII.LF & "ip=192.168.231.199"
           & ASCII.LF & "wins=192.168.231.10"
           & ASCII.LF & "router=192.168.231.1 192.168.231.254";
      begin
         Assert (Condition => Read_File
                 (Filename => "data/simple.lease") = Ref_Str,
                 Message   => "File content mismatch");
      end Positive_Test;

      ----------------------------------------------------------------------

      procedure Unreadable_File
      is
      begin
         declare
            Dummy : constant String := Read_File (Filename => "data");
         begin
            Fail (Message => "Expected exception");
         end;

      exception
         when E : IO_Error =>
            Assert (Condition => Ada.Exceptions.Exception_Message (X => E)
                    = "Unable to read data from file 'data'",
                    Message   => "Exception message mismatch (2)");
      end Unreadable_File;
   begin
      Positive_Test;
      Nonexistent_File;
      Unreadable_File;
      Nonaccessible_File;
   end Read_File_String;

end DHCP.OS.Tests;
