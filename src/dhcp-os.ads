--
--  Copyright (C) 2011-2015 secunet Security Networks AG
--  Copyright (C) 2011-2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011-2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;

with Anet.OS;

package DHCP.OS is

   procedure Execute (Command : String) renames Anet.OS.Execute;
   --  Execute given command with /bin/sh.

   function Read_File
     (Filename : String)
      return Ada.Streams.Stream_Element_Array
      renames Anet.OS.Read_File;
   --  Read file given by filename and return binary file content.

   function Read_File (Filename : String) return String;
   --  Return content of file given by filename as string.

   procedure Delete_File
     (Filename       : String;
      Ignore_Missing : Boolean := True)
      renames Anet.OS.Delete_File;
   --  Delete a file given by filename string. Ignore missing specifies if
   --  trying to delete a nonexistent file should raise an IO error exception.

   function Get_Pid return Positive;
   --  Return the process ID of the calling process.

   function Get_Pgid return Positive;
   --  Return the process group ID of the calling process.

   IO_Error : exception;

end DHCP.OS;
