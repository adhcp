--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet;

with DHCP.Logger;
with DHCP.Termination;

package body DHCP.Socket_Callbacks
is

   package L renames DHCP.Logger;

   -------------------------------------------------------------------------

   procedure Handle_Receive_Error
     (E         :        Ada.Exceptions.Exception_Occurrence;
      Stop_Flag : in out Boolean)
   is
      use Ada.Exceptions;
   begin
      if Exception_Identity (X => E) = Anet.Socket_Error'Identity then
         L.Log (Level   => L.Error,
                Message => "Receiver: terminating due to socket error");
         L.Log (Level   => L.Error,
                Message => Ada.Exceptions.Exception_Information (X => E));
         DHCP.Termination.Signal (Exit_Status => DHCP.Termination.Failure);
         Stop_Flag := True;
      else

         --  Print out all other errors and resume normal operation.

         L.Log (Level   => L.Error,
                Message => "Receiver: resuming normal operation "
                & "after exception");
         L.Log (Level   => L.Error,
                Message => Ada.Exceptions.Exception_Information (X => E));
      end if;
   end Handle_Receive_Error;

end DHCP.Socket_Callbacks;
