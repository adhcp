--
--  Copyright (C) 2011, 2015 secunet Security Networks AG
--  Copyright (C) 2011, 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Containers.Doubly_Linked_Lists;

package body DHCP.Notify
is

   type Handler_Entry_Type is record
      Reason  : Reason_Type;
      Handler : Notfication_Handler;
   end record;

   package LOH is new Ada.Containers.Doubly_Linked_Lists
     (Element_Type => Handler_Entry_Type);

   Observers : LOH.List;
   --  List of observers.

   -------------------------------------------------------------------------

   procedure Clear
   is
   begin
      Observers.Clear;
   end Clear;

   -------------------------------------------------------------------------

   function Handler_Count (Reason : Reason_Type) return Natural
   is
      Count : Natural := 0;

      procedure Inc_Count (Position : LOH.Cursor);
      --  Increase counter if handler is registered for given reason.

      procedure Inc_Count (Position : LOH.Cursor)
      is
      begin
         if LOH.Element (Position).Reason = Reason then
            Count := Count + 1;
         end if;
      end Inc_Count;
   begin
      Observers.Iterate (Process => Inc_Count'Access);
      return Count;
   end Handler_Count;

   -------------------------------------------------------------------------

   procedure Register
     (Reasons : Reason_Array;
      Handler : Notfication_Handler)
   is
   begin
      for I in Reasons'Range loop
         Observers.Append
           (New_Item => Handler_Entry_Type'(Reason  => Reasons (I),
                                            Handler => Handler));
      end loop;
   end Register;

   -------------------------------------------------------------------------

   procedure Update (Reason : Reason_Type)
   is
      procedure Handle_Notification (Position : LOH.Cursor);
      --  Call handler procedure for matching observer.

      procedure Handle_Notification (Position : LOH.Cursor)
      is
         E : constant Handler_Entry_Type := LOH.Element (Position);
      begin
         if E.Reason = Reason then
            E.Handler (R => Reason);
         end if;
      end Handle_Notification;
   begin
      Observers.Iterate (Process => Handle_Notification'Access);
   end Update;

end DHCP.Notify;
