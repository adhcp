--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCP.Timing_Events is

   -------------------------------------------------------------------------

   function Get_Time (Event : Event_Type'Class) return Ada.Real_Time.Time
   is
   begin
      return Event.Time;
   end Get_Time;

   -------------------------------------------------------------------------
   procedure Set_Time
     (Event   : in out Event_Type'Class;
      At_Time :        Ada.Real_Time.Time)
   is
   begin
      Event.Time := At_Time;
   end Set_Time;

end DHCP.Timing_Events;
