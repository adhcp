--
--  Copyright (C) 2011-2015 secunet Security Networks AG
--  Copyright (C) 2011-2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011-2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

generic

   type Message_Type is private;

   type Transaction_Type (<>) is abstract tagged limited private;

   with procedure Process_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message_Type) is abstract;

package DHCP.Timing_Events.Handle_Msg
is

   type Handle_Msg_Type
     (T : not null access Transaction_Type'Class)
   is new Event_Type with private;

   overriding
   procedure Trigger (Event : in out Handle_Msg_Type);
   --  Let the transaction process the given DHCP message.

   procedure Set_Message
     (Event : in out Handle_Msg_Type;
      Msg   :        Message_Type);
   --  Set message to handle when the event is triggered.

private

   type Handle_Msg_Type
     (T : not null access Transaction_Type'Class)
   is new DHCP.Timing_Events.Event_Type with record
      Msg : Message_Type;
   end record;

end DHCP.Timing_Events.Handle_Msg;
