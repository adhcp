--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;

with Interfaces;

package DHCP.Utils
is

   function Strip
     (Str     : String;
      Pattern : String)
      return String;
   --  Returns the given string with all occurrences of the specified pattern
   --  stripped.

   function To_Value
     (A : Ada.Streams.Stream_Element_Array)
      return Interfaces.Unsigned_64
   with
      Pre => A'Length <= Interfaces.Unsigned_64'Size / 8;
   --  Convert value stored in array to number (big-endian representation).

   use type Interfaces.Unsigned_64;

   function To_Array
     (Value  : Interfaces.Unsigned_64;
      Length : Positive)
      return Ada.Streams.Stream_Element_Array
   with
      Pre => Value <= (2 ** (Length * 8)) - 1;
   --  Convert given value to stream array of given length
   --  (big-endian representation).

   function To_String (A : Ada.Streams.Stream_Element_Array) return String;
   --  Convert given stream array to string representation.

end DHCP.Utils;
