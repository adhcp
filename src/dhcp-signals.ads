--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Interrupts;

package DHCP.Signals is

   protected type Handler_Type (Signal : Ada.Interrupts.Interrupt_ID) is
      entry Wait;
   private
      procedure Handle_Signal;
      pragma Attach_Handler (Handle_Signal, Signal);

      Occurred : Boolean := False;
   end Handler_Type;
   --  Signal handler used for signal processing. Calling the wait entry will
   --  suspend the calling task until the specified signal has occurred.

end DHCP.Signals;
