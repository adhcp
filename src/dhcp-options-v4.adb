--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;
with Ada.Strings.Fixed;

with Interfaces;

with DHCP.Logger;
with DHCP.Utils;

package body DHCP.Options.V4
is

   pragma Assert (Code_Type'Size = 8);
   pragma Assert (Len_Byte_Count = 1);
   --  DHCPv4 code and option-len fields are 1 byte.

   use Ada.Streams;

   Magic_Cookie : constant Stream_Element_Array (1 .. 4)
     := (16#63#, 16#82#, 16#53#, 16#63#);
   --  DHCP option magic cookie.

   -------------------------------------------------------------------------

   function Create
     (Name    : Option_Name_Type;
      Address : Anet.IPv4_Addr_Type)
      return IP_Option_Type
   is
   begin
      return IP_Option_Type
        (Create
           (Name => Name,
            Data => (1 => Stream_Element (Address (1)),
                     2 => Stream_Element (Address (2)),
                     3 => Stream_Element (Address (3)),
                     4 => Stream_Element (Address (4)))));
   end Create;

   -------------------------------------------------------------------------

   function Deserialize
     (Buffer : Ada.Streams.Stream_Element_Array)
      return Option_List
   is
      Current_Idx : Stream_Element_Offset := Buffer'First;
      List        : Option_List;
   begin

      --  Check minimal length of buffer: magic cookie + end marker

      if Buffer'Length < Magic_Cookie'Length + 1 then
         raise Invalid_Option with
           "Malformed option list contains no options";
      end if;

      if Buffer (Current_Idx .. Current_Idx + 3) /= Magic_Cookie then
         raise Invalid_Cookie with "Buffer has no magic cookie";
      end if;
      Current_Idx := Current_Idx + 4;

      loop
         --  Skip PADding options

         while Buffer (Current_Idx) = 0 loop
            Current_Idx := Current_Idx + 1;
         end loop;

         exit when Buffer (Current_Idx) = 255; -- End marker

         declare
            Len : constant Stream_Element_Offset
              := Stream_Element_Offset (Buffer (Current_Idx + 1));
         begin
            begin
               List.Data.Append
                 (New_Item => Deserialize
                    (Buffer => Buffer
                         (Current_Idx .. Current_Idx + 1 + Len)));

            exception
               when E : Invalid_Option =>
                  Logger.Log (Level   => Logger.Warning,
                              Message => "Skipping invalid option - "
                              & Ada.Exceptions.Exception_Message (X => E));
            end;

            --  Move current index to the beginning of the next option:
            --  Current index + code byte + length byte + data length

            Current_Idx := Current_Idx + 2 + Len;
         end;
      end loop;

      return List;

   exception
      when Constraint_Error =>
         raise Invalid_Option with "Could not parse malformed options";
   end Deserialize;

   -------------------------------------------------------------------------

   function Get_Data_Str (Option : String_Option_Type) return String
   is
   begin
      return Utils.To_String (A => Option.Data);
   end Get_Data_Str;

   -------------------------------------------------------------------------

   function Get_Value (Option : IP_Option_Type) return Anet.IPv4_Addr_Type
   is
   begin
      return Anet.IPv4_Addr_Type'(1 => Anet.Byte (Option.Data (1)),
                                  2 => Anet.Byte (Option.Data (2)),
                                  3 => Anet.Byte (Option.Data (3)),
                                  4 => Anet.Byte (Option.Data (4)));
   end Get_Value;

   -------------------------------------------------------------------------

   function Serialize
     (List : Option_List)
      return Ada.Streams.Stream_Element_Array
   is

      --  The length of the buffer (Upper) is calculated as follows:
      --    Magic cookie length      +
      --    End marker byte          +
      --    Data size of all options +
      --    Option count * 2 bytes for type value (TV) fields.

      Upper  : constant Stream_Element_Offset
        := 5 + Stream_Element_Offset (List.Get_Data_Size + 2 * List.Get_Count);
      Buffer : Stream_Element_Array (1 .. Upper);
      Idx    : Stream_Element_Offset;

      procedure Serialize (Option : DHCP.Options.Option_Type'Class);
      --  Serialize an option to the buffer.

      procedure Serialize (Option : DHCP.Options.Option_Type'Class)
      is
         Size : constant Size_Type := Option.Get_Size;
      begin
         Buffer (Idx .. Idx + Size + 1) := Option.Serialize;
         Idx := Idx + Size + 2;
      end Serialize;
   begin
      Buffer (1 .. 4) := Magic_Cookie;
      Idx := 5;

      List.Iterate (Process => Serialize'Access);

      --  Append End_Marker (code 255).

      Buffer (Buffer'Last) := 255;

      return Buffer;
   end Serialize;

   -------------------------------------------------------------------------

   procedure Validate (Option : IP_Option_Type)
   is
      Length : constant Natural := Option.Data'Length;
   begin
      if Length /= 4 then
         raise Invalid_Option with "IP option "
           & To_Name (Code => Option.Code)'Img & ": data size" & Length'Img
           & ", expected 4";
      end if;
   end Validate;

   -------------------------------------------------------------------------

   procedure Validate (Option : IP_Pairs_Option_Type)
   is
      Length : constant Natural := Option.Data'Length;
   begin
      if Option.Data'Length mod 8 /= 0 then
         raise Invalid_Option with "IP pairs option "
           & To_Name (Code => Option.Code)'Img
           & ": data size" & Length'Img & ", expected multiple of 8";
      end if;
   end Validate;

   -------------------------------------------------------------------------

   procedure Validate (Option : Bool_Option_Type)
   is
      subtype Boolean_Range is Natural range 0 .. 1;

      Value : Natural;
   begin
      Byte_Option_Type (Option).Validate;

      Value := Natural (Option.Data (Option.Data'First));
      if Value not in Boolean_Range then
         raise Invalid_Option with "Boolean option "
           & To_Name (Code => Option.Code)'Img & ": data" & Value'Img
           & ", expected 0 or 1";
      end if;
   end Validate;

   -------------------------------------------------------------------------

   procedure Validate (Option : Positive_Octet_Option_Type)
   is
      subtype Positive_Range is Positive range 1 .. 255;

      Value : Natural;
   begin
      Byte_Option_Type (Option).Validate;

      Value := Natural (Option.Data (Option.Data'First));
      if Value not in Positive_Range then
         raise Invalid_Option with "Positive octet option "
           & To_Name (Code => Option.Code)'Img
           & ": data" & Value'Img & " not in range 1 .. 255";
      end if;
   end Validate;

   -------------------------------------------------------------------------

   procedure Validate (Option : Iface_MTU_Option_Type)
   is
      use type Interfaces.Unsigned_16;
   begin
      U_Int16_Option_Type (Option).Validate;

      if To_U_Int16 (Octets => Option.Data) < 68 then
         raise Invalid_Option with To_Name (Code => Option.Code)'Img
           & " option: data value illegal (" & Ada.Strings.Fixed.Trim
           (Source => To_U_Int16 (Octets => Option.Data)'Img,
            Side   => Ada.Strings.Left) & ")";
      end if;
   end Validate;

   -------------------------------------------------------------------------

   procedure Validate (Option : Msg_Kind_Option_Type)
   is
      subtype Valid_Msg_Kind is Stream_Element range 1 .. 8;
   begin
      Positive_Octet_Option_Type (Option).Validate;

      if Option.Data (1) not in Valid_Msg_Kind'Range then
         raise Invalid_Option with To_Name (Code => Option.Code)'Img
           & " option: data value illegal (" & Ada.Strings.Fixed.Trim
           (Source => Option.Data (1)'Img,
            Side   => Ada.Strings.Left) & ")";
      end if;
   end Validate;

end DHCP.Options.V4;
