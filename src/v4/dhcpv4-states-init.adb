--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

with DHCP.Logger;
with DHCP.Random;
with DHCP.Timer;
with DHCP.Notify;

with DHCPv4.States.Selecting;
with DHCPv4.Timing_Events.Bind_Timeout;

package body DHCPv4.States.Init is

   package L renames DHCP.Logger;

   Instance : aliased Init_State_Type;

   -------------------------------------------------------------------------

   function Get_Name (State : not null access Init_State_Type) return String
   is
      pragma Unreferenced (State);
   begin
      return "Init";
   end Get_Name;

   -------------------------------------------------------------------------

   procedure Start
     (State       : access Init_State_Type;
      Transaction : in out DHCP.States.Root_Context_Type'Class)
   is
      pragma Unreferenced (State);
      use type Ada.Real_Time.Time;

      Now : constant Ada.Real_Time.Time  := Ada.Real_Time.Clock;
      ID  : constant Transaction_ID_Type := DHCP.Random.Get;
      Req : Message.Message_Type         := Message.Create
        (Kind => Message.Discover);

      Bind_Timeout : Timing_Events.Bind_Timeout.Timeout_Type
        (T => Transaction_Type'Class (Transaction)'Access);
   begin
      L.Log (Message => "Starting new transaction with ID "
             & To_String (XID => ID));
      Transaction_Type'Class (Transaction).Set_ID (XID => ID);
      Transaction_Type'Class (Transaction).Set_Start_Time (Time => Now);

      DHCP.Notify.Update (Reason => DHCP.Notify.Preinit);

      --  Get rid of any pending events since they belong to a previous
      --  transaction now.

      DHCP.Timer.Clear;

      Transaction_Type'Class (Transaction).Prepare_Message (Msg => Req);
      Transaction_Type'Class (Transaction).Send_Message
        (Msg => Req,
         Dst => Anet.Bcast_Addr);

      Bind_Timeout.Set_Time (At_Time => Now + Ada.Real_Time.Minutes (M => 1));
      DHCP.Timer.Schedule (Event => Bind_Timeout);

      Transaction.Set_State (State => Selecting.State);
   end Start;

   -------------------------------------------------------------------------

   function State return DHCP.States.State_Handle
   is
   begin
      return Instance'Access;
   end State;

end DHCPv4.States.Init;
