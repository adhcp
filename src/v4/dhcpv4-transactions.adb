--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

with DHCP.Timer;
with DHCP.Logger;

with DHCPv4.Options;
with DHCPv4.Transmission;

package body DHCPv4.Transactions is

   package L renames DHCP.Logger;

   procedure Schedule_Retransmit
     (Transaction : in out Transaction_Type;
      In_Time     :        Duration);
   --  Schedule retransmission of last sent message in given amount of time
   --  from now.

   -------------------------------------------------------------------------

   function Get_ID (Transaction : Transaction_Type) return Transaction_ID_Type
   is
   begin
      return Transaction.ID;
   end Get_ID;

   -------------------------------------------------------------------------

   function Get_Server_Address
     (Transaction : Transaction_Type)
      return Anet.IPv4_Addr_Type
   is
   begin
      return Transaction.Cur_Server_Addr;
   end Get_Server_Address;

   -------------------------------------------------------------------------

   procedure Increase_Retry_Delay (Transaction : in out Transaction_Type)
   is
   begin

      --  Cap delay at maximum of 64 seconds; RFC 2131, section 4.1.

      if Transaction.Retry_Delay = 64.0 then
         return;
      end if;

      if Transaction.Exp_Backoff then
         Transaction.Retry_Delay := Transaction.Retry_Delay * 2;
      end if;
   end Increase_Retry_Delay;

   -------------------------------------------------------------------------

   procedure Prepare_Message
     (Transaction :        Transaction_Type;
      Msg         : in out Message.Message_Type)
   is
   begin
      Msg.Set_Hardware_Address (Address => Transmission.Get_Iface_Mac);
      Msg.Set_Transaction_ID (XID => Transaction.ID);
      Msg.Add_Option (Opt => Options.Inst4.Default_Param_Req_List);
   end Prepare_Message;

   -------------------------------------------------------------------------

   procedure Process_Bind_Timeout (Transaction : in out Transaction_Type)
   is
   begin
      States.State_Type'Class (Transaction.State.all).Process_Bind_Timeout
        (Transaction => Transaction);
   end Process_Bind_Timeout;

   -------------------------------------------------------------------------

   procedure Process_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type)
   is
      Kind : constant Message.Message_Kind := Msg.Get_Kind;
   begin
      DHCP.Timer.Cancel (Event => Transaction.Retransmit_Ev);

      case Kind is
         when Message.Offer           =>
            States.State_Type'Class
              (Transaction.State.all).Process_Offer
              (Transaction => Transaction,
               Msg         => Msg);
         when Message.Acknowledge     =>
            States.State_Type'Class
              (Transaction.State.all).Process_Ack
              (Transaction => Transaction,
               Msg         => Msg);
         when Message.Not_Acknowledge =>
            States.State_Type'Class
              (Transaction.State.all).Process_Nack
              (Transaction => Transaction,
               Msg         => Msg);
         when others                  =>
            L.Log (Message => "Ignoring message");
      end case;
   end Process_Message;

   -------------------------------------------------------------------------

   procedure Process_Timer_Expiry
     (Transaction : in out Transaction_Type;
      Timer_Kind  :        DHCP.Types.DHCP_Timer_Kind)
   is
      use DHCP.Types;
   begin
      L.Log (Message => "Timer " & Timer_Kind'Img & " expired while in state "
             & Transaction.State.Get_Name);

      case Timer_Kind is
         when Retransmit =>
            Transaction.Send_Message
              (Msg => Transaction.Last_Msg,
               Dst => Transaction.Last_Dst);
         when T1         =>
            States.State_Type'Class
              (Transaction.State.all).Process_T1_Expiry
              (Transaction => Transaction);
         when T2         =>
            States.State_Type'Class
              (Transaction.State.all).Process_T2_Expiry
              (Transaction => Transaction);
         when Lease_Expiry =>
            States.State_Type'Class
              (Transaction.State.all).Process_Lease_Expiry
              (Transaction => Transaction);
      end case;
   end Process_Timer_Expiry;

   -------------------------------------------------------------------------

   procedure Reset (Transaction : in out Transaction_Type)
   is
   begin
      Transaction.Set_Start_Time (Time => Ada.Real_Time.Time_First);

      Transaction.ID              := 0;
      Transaction.Cur_Server_Addr := Anet.Any_Addr;
      Transaction.Reset_Retry_Delay;
   end Reset;

   -------------------------------------------------------------------------

   procedure Reset_Retry_Delay (Transaction : in out Transaction_Type)
   is
   begin
      Transaction.Retry_Delay := Initial_Retry_Delay;
   end Reset_Retry_Delay;

   -------------------------------------------------------------------------

   procedure Schedule_Retransmit
     (Transaction : in out Transaction_Type;
      In_Time     :        Duration)
   is
      use Ada.Real_Time;

      New_Time : constant Time := Clock + To_Time_Span (D => In_Time);
   begin

      --  Cancel any potentially pending retransmission.

      DHCP.Timer.Cancel (Event => Transaction.Retransmit_Ev);

      Transaction.Retransmit_Ev.Set_Time (At_Time => New_Time);

      L.Log (Level   => L.Info,
             Message => "Retransmission timer in" & Long_Integer (In_Time)'Img
             & " seconds");

      DHCP.Timer.Schedule (Event => Transaction.Retransmit_Ev);
   end Schedule_Retransmit;

   -------------------------------------------------------------------------

   procedure Send_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type;
      Dst         :        Anet.IPv4_Addr_Type)
   is
      use type DHCPv4.Message.Message_Kind;
   begin
      Transmission.Send (Message     => Msg,
                         Destination => Dst);

      Transaction.Last_Msg := Msg;
      Transaction.Last_Dst := Dst;
      if Msg.Get_Kind = Message.Request then
         Transaction.Set_Start_Time;
      end if;

      Schedule_Retransmit (Transaction => Transaction,
                           In_Time     => Transaction.Retry_Delay);
      Transaction.Increase_Retry_Delay;
   end Send_Message;

   -------------------------------------------------------------------------

   procedure Set_Exp_Backoff
     (Transaction : in out Transaction_Type;
      New_State   :        Boolean)
   is
   begin
      Transaction.Exp_Backoff := New_State;
   end Set_Exp_Backoff;

   -------------------------------------------------------------------------

   procedure Set_ID
     (Transaction : in out Transaction_Type;
      XID         :        Transaction_ID_Type)
   is
   begin
      Transaction.ID := XID;
   end Set_ID;

   -------------------------------------------------------------------------

   procedure Set_Server_Address
     (Transaction : in out Transaction_Type;
      Address     :        Anet.IPv4_Addr_Type)
   is
   begin
      Transaction.Cur_Server_Addr := Address;
   end Set_Server_Address;

   -------------------------------------------------------------------------

   procedure Set_State
     (Transaction : in out Transaction_Type;
      State       :        DHCP.States.State_Handle)
   is
   begin
      L.Log (Message => "State transition: " & Transaction.State.Get_Name
             & " -> " & State.Get_Name);
      DHCP.States.Root_Context_Type (Transaction).Set_State (State => State);
   end Set_State;

   -------------------------------------------------------------------------

   procedure Start (Transaction : in out Transaction_Type)
   is
   begin
      States.State_Type'Class
        (Transaction.State.all).Start (Transaction => Transaction);
   end Start;

end DHCPv4.Transactions;
