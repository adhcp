--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package DHCPv4.States.Requesting is

   type Requesting_State_Type is new Allocation_State_Type with private;
   --  The client is waiting to hear back from the server to which it sent it's
   --  request.

   overriding
   function Get_Name
     (State : not null access Requesting_State_Type)
      return String;
   --  Return name of state as string.

   overriding
   procedure Notify_Allocation (State : access Requesting_State_Type);
   --  Send lease allocation notification with reason 'Bound'.

   overriding
   procedure Process_Ack
     (State       : access Requesting_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type);
   --  Perform ACD and decline if address collision is detected. Otherwise
   --  process offer and announce IP address use.

   overriding
   procedure Process_Nack
     (State       : access Requesting_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type);
   --  Restart lease allocation process in 10 seconds.

   function State return DHCP.States.State_Handle;
   --  Return access to singleton.

private

   type Requesting_State_Type is new Allocation_State_Type with null record;

end DHCPv4.States.Requesting;
