--
--  Copyright (C) 2014, 2015 secunet Security Networks AG
--  Copyright (C) 2014, 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2014, 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Text_IO;
with Ada.Exceptions;
with Ada.Strings.Unbounded;

with DHCPv4.Options;
with DHCPv4.Database;

package body DHCPv4.Notify_Simple
is

   use Ada.Strings.Unbounded;

   function U
     (Source : String)
      return Unbounded_String
      renames To_Unbounded_String;

   Lease_Optnames : constant array
     (Options.Option_Name) of Unbounded_String
     := (Options.Subnet_Mask            => U ("subnet"),
         Options.Time_Offset            => U ("timezone"),
         Options.Routers                => U ("router"),
         Options.Domain_Name_Servers    => U ("dns"),
         Options.LPR_Servers            => U ("lprsrv"),
         Options.Host_Name              => U ("hostname"),
         Options.Boot_File_Size         => U ("bootsize"),
         Options.Domain_Name            => U ("domain"),
         Options.Swap_Server            => U ("swapsrv"),
         Options.Root_Path              => U ("rootpath"),
         Options.Default_IP_TTL         => U ("ipttl"),
         Options.Interface_MTU          => U ("mtu"),
         Options.Broadcast_Address      => U ("broadcast"),
         Options.Static_Route           => U ("routes"),
         Options.NIS_Domain             => U ("nisdomain"),
         Options.NIS_Servers            => U ("nissrv"),
         Options.NTP_Servers            => U ("ntpsrv"),
         Options.NETBIOS_Name_Servers   => U ("wins"),
         Options.Address_Time           => U ("lease"),
         Options.DHCP_Msg_Type          => U ("dhcptype"),
         Options.DHCP_Server_Id         => U ("serverid"),
         Options.DHCP_Message           => U ("message"),
         Options.TFTP_Server_Name       => U ("tftp"),
         Options.Bootfile_Name          => U ("bootfile"),
         Options.Domain_Search          => U ("search"),
         Options.SIP_Servers            => U ("sipsrv"),
         Options.Classless_Static_Route => U ("staticroutes"),
         Options.PXE_Undefined_5        => U ("vlanid"),
         Options.PXE_Undefined_6        => U ("vlanpriority"),
         others                         => Null_Unbounded_String);
   --  Option name mangling table. These are the same option names used by
   --  busybox/udhcpc. Option names msstaticroutes (249) and wpad (252) are not
   --  supported/mangled.

   -------------------------------------------------------------------------

   procedure Write_Lease_File
     (State    : DHCP.Notify.Reason_Type;
      Iface    : String;
      Filename : String)
   is
      use Ada.Text_IO;
      use type DHCP.Notify.Reason_Type;

      File : File_Type;

      procedure Write_Option (Opt : Options.Inst.Option_Type'Class);
      --  Write option to simple lease file.

      procedure Write_Option (Opt : Options.Inst.Option_Type'Class)
      is
         Opt_Key  : Unbounded_String;
         Opt_Name : constant Unbounded_String := Lease_Optnames (Opt.Get_Name);
      begin
         if Opt_Name /= Null_Unbounded_String then
            Opt_Key := Opt_Name;
         else
            Opt_Key := U (Opt.Get_Name_Str);
         end if;

         Put_Line (File => File,
                   Item => To_String (Opt_Key) & "=" & Opt.Get_Data_Str);
      end Write_Option;
   begin
      Create (File => File,
              Mode => Ada.Text_IO.Out_File,
              Name => Filename);

      Put_Line (File => File,
                Item => "interface=" & Iface);

      if State /= DHCP.Notify.Bound then

         --  Not (yet) in Bound state, return.

         Close (File => File);
         return;
      end if;

      Put_Line (File => File,
                Item => "ip=" & Anet.To_String
                  (Address => Database.Get_Fixed_Address));

      Database.Get_Options.Iterate (Process => Write_Option'Access);

      Close (File => File);

   exception
      when E : others =>
         if Is_Open (File => File) then
            Close (File => File);
         end if;

         raise Lease_Error with "Unable to write lease to file '"
           & Filename & "': " & Ada.Exceptions.Exception_Name (E);
   end Write_Lease_File;

end DHCPv4.Notify_Simple;
