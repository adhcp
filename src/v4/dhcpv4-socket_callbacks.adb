--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Text_IO;
with Ada.Strings.Fixed;
with Ada.Calendar.Formatting;

with Anet.IPv4;

with DHCPv4.Message;
with DHCPv4.Options;

package body DHCPv4.Socket_Callbacks is

   procedure Print
     (Data    : Ada.Streams.Stream_Element_Array;
      Src_Str : String);
   --  Print DHCP message contained in packet.

   -------------------------------------------------------------------------

   procedure Print
     (Data    : Ada.Streams.Stream_Element_Array;
      Src_Str : String)
   is
      Timestamp : constant Ada.Calendar.Time := Ada.Calendar.Clock;
      Msg       : constant Message.Message_Type
        := Message.Deserialize (Buffer => Data);
   begin
      Ada.Text_IO.New_Line;
      Ada.Text_IO.Put_Line ("---[" & Ada.Calendar.Formatting.Image (Timestamp)
                            & "]---");

      Ada.Text_IO.Put_Line ("From  : " & Src_Str);
      Ada.Text_IO.Put_Line ("OP    : " & Msg.Get_Opcode'Img);
      Ada.Text_IO.Put_Line ("HType : " & Msg.Get_Link_Hardware'Img);
      Ada.Text_IO.Put_Line ("Hlen  :"  & Msg.Get_HW_Addr_Length'Img);
      Ada.Text_IO.Put_Line ("Hops  :"  & Msg.Get_Hops'Img);
      Ada.Text_IO.Put_Line ("XID   :"  & Msg.Get_Transaction_ID'Img);
      Ada.Text_IO.Put_Line ("Secs  :"  & Msg.Get_Seconds'Img);
      Ada.Text_IO.Put_Line ("BFlag : " & Msg.Has_Broadcast_Set'Img);

      Ada.Text_IO.Put_Line
        ("CIAddr: " & Anet.To_String (Address => Msg.Get_Client_IP));
      Ada.Text_IO.Put_Line
        ("YIAddr: " & Anet.To_String (Address => Msg.Get_Your_IP));
      Ada.Text_IO.Put_Line
        ("SIAddr: " & Anet.To_String (Address => Msg.Get_Next_Server_IP));
      Ada.Text_IO.Put_Line
        ("GIAddr: " & Anet.To_String (Address => Msg.Get_Gateway_IP));
      Ada.Text_IO.Put_Line
        ("CHAddr: " & Anet.To_String (Address => Msg.Get_Hardware_Address));

      Ada.Text_IO.Put_Line ("SName : " & Msg.Get_Server_Name);
      Ada.Text_IO.Put_Line ("File  : " & Msg.Get_Boot_File_Name);
      Ada.Text_IO.Put_Line ("Opts  :");
      declare
         procedure Print (Option : Options.Inst.Option_Type'Class);
         --  Print out an option.

         procedure Print (Option : Options.Inst.Option_Type'Class)
         is
            Size : constant String := Ada.Strings.Fixed.Trim
              (Source => Option.Get_Size'Img,
               Side   => Ada.Strings.Left);
         begin
            Ada.Text_IO.Put (Option.Get_Name'Img & " (" & Size & "): ");
            Ada.Text_IO.Put_Line (Option.Get_Data_Str);
         end Print;
      begin
         Msg.Get_Options.Iterate (Process => Print'Access);
      end;
   end Print;

   -------------------------------------------------------------------------

   procedure Print
     (Data : Ada.Streams.Stream_Element_Array;
      Src  : Anet.Sockets.Inet.IPv4_Sockaddr_Type)
   is
   begin
      Print (Data    => Data,
             Src_Str => Anet.To_String (Address => Src.Addr)
             & " (" & Src.Port'Img & " )");
   end Print;

   -------------------------------------------------------------------------

   procedure Print_Raw
     (Data : Ada.Streams.Stream_Element_Array;
      Src  : Ether_Addr_Type)
   is
   begin
      Print (Data    => Anet.IPv4.Validate_And_Strip (Packet => Data),
             Src_Str => Anet.To_String (Address => Src));
   end Print_Raw;

end DHCPv4.Socket_Callbacks;
