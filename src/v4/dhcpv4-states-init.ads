--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package DHCPv4.States.Init is

   type Init_State_Type is new State_Type with private;
   --  Initialization state where a client begins the process of acquiring a
   --  lease. This state is revisited when a lease ends or when a lease
   --  negotiation fails.

   overriding
   function Get_Name (State : not null access Init_State_Type) return String;
   --  Return name of state as string.

   overriding
   procedure Start
     (State       : access Init_State_Type;
      Transaction : in out DHCP.States.Root_Context_Type'Class);
   --  Start DHCP transaction.

   function State return DHCP.States.State_Handle;
   --  Return access to singleton.

private

   type Init_State_Type is new State_Type with null record;

end DHCPv4.States.Init;
