--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCPv4.Database
is

   -------------------------------------------------------------------------

   function Get_Fixed_Address return Anet.IPv4_Addr_Type
   is (Instance.Fixed_Address);

   -------------------------------------------------------------------------

   function Get_Options return Options.Inst4.Option_List is (Instance.Opts);

   -------------------------------------------------------------------------

   procedure Reset
   is
   begin
      Instance.Fixed_Address := Anet.Any_Addr;
      Instance.Opts.Clear;
   end Reset;

   -------------------------------------------------------------------------

   procedure Set_Fixed_Address (Addr : Anet.IPv4_Addr_Type)
   is
   begin
      Instance.Fixed_Address := Addr;
   end Set_Fixed_Address;

   -------------------------------------------------------------------------

   procedure Set_Options (Opts : Options.Inst4.Option_List)
   is
   begin
      Instance.Opts := Opts;
   end Set_Options;

end DHCPv4.Database;
