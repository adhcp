--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;

private with Ada.Strings.Unbounded;

with DHCP.Marshaling;

with DHCPv4.Options;

package DHCPv4.Message is

   type Message_Kind is
     (Discover,
      Offer,
      Request,
      Decline,
      Acknowledge,
      Not_Acknowledge,
      Release,
      Inform);
   --  DHCP message kind.

   type Message_Type is new DHCP.Marshaling.Serializable_Object with private;
   --  DHCP message.

   overriding
   function Serialize
     (Message : Message_Type)
      return Ada.Streams.Stream_Element_Array;
   --  Serialize given DHCP message to stream element array.

   overriding
   function Deserialize
     (Buffer : Ada.Streams.Stream_Element_Array)
      return Message_Type;
   --  Create a DHCP message from given stream element array.

   function Create (Kind : Message_Kind) return Message_Type;
   --  Create a DHCP message of given kind.

   function Get_Opcode (Msg : Message_Type) return Opcode_Type;
   --  Return operation code (request or reply).

   function Get_Kind (Msg : Message_Type) return Message_Kind;
   --  Return the kind of the DHCP message (discover, offer, ...).

   function Get_Link_Hardware (Msg : Message_Type) return Hardware_Type;
   --  Return the link-layer hardware type.

   function Get_HW_Addr_Length
     (Msg : Message_Type)
      return Anet.HW_Addr_Len_Type;
   --  Return the length of the hardware address.

   function Get_Hops (Msg : Message_Type) return Hops_Type;
   --  Return the hop count.

   procedure Inc_Hops (Msg : in out Message_Type);
   --  Increment the hop count.

   procedure Set_Transaction_ID
     (Msg : in out Message_Type;
      XID :        Transaction_ID_Type);
   --  Set transaction identifier (XID).

   function Get_Transaction_ID (Msg : Message_Type) return Transaction_ID_Type;
   --  Return transaction identifier.

   function Get_Seconds (Msg : Message_Type) return Seconds_Type;
   --  Return number of seconds elapsed since a client began an attempt to
   --  acquire or renew a lease.

   procedure Set_Broadcast_Flag
     (Msg   : in out Message_Type;
      State :        Boolean);
   --  Set broadcast flag to specified state.

   function Has_Broadcast_Set (Msg : Message_Type) return Boolean;
   --  Returns True if the broadcast flag is set.

   function Get_Client_IP (Msg : Message_Type) return Anet.IPv4_Addr_Type;
   --  Return the (currently assigned) client IP address.

   procedure Set_Client_IP
     (Msg : in out Message_Type;
      IP  :        Anet.IPv4_Addr_Type);
   --  Set client IP address.

   function Get_Your_IP (Msg : Message_Type) return Anet.IPv4_Addr_Type;
   --  Return the IP address that the server is assigning to the client.

   procedure Set_Your_IP
     (Msg : in out Message_Type;
      IP  :        Anet.IPv4_Addr_Type);
   --  Set 'your IP' address.

   function Get_Next_Server_IP (Msg : Message_Type) return Anet.IPv4_Addr_Type;
   --  Return the IP address of the next DHCP server (siaddr).

   function Get_Server_ID (Msg : Message_Type) return Anet.IPv4_Addr_Type;
   --  Return the server ID (the IP address of the server).

   procedure Set_Gateway_IP
     (Msg : in out Message_Type;
      IP  :        Anet.IPv4_Addr_Type);
   --  Set the gateway/relay agent IP address.

   function Get_Gateway_IP (Msg : Message_Type) return Anet.IPv4_Addr_Type;
   --  Return the gateway/relay agent IP address.

   procedure Set_Hardware_Address
     (Msg     : in out Message_Type;
      Address :        Anet.Hardware_Addr_Type);
   --  Set the hardware address.

   function Get_Hardware_Address
     (Msg : Message_Type)
      return Anet.Hardware_Addr_Type;
   --  Return hardware address.

   function Get_Server_Name (Msg : Message_Type) return String;
   --  Return name of DHCP server.

   function Get_Boot_File_Name (Msg : Message_Type) return String;
   --  Return name of boot file.

   function Get_Options (Msg : Message_Type) return Options.Inst4.Option_List;
   --  Return DHCP options.

   procedure Add_Option
     (Msg : in out Message_Type;
      Opt :        Options.Inst.Option_Type'Class);
   --  Add given option to DHCP message.

   Invalid_Message : exception;

private

   for Message_Kind use
     (Discover         => 1,
      Offer            => 2,
      Request          => 3,
      Decline          => 4,
      Acknowledge      => 5,
      Not_Acknowledge  => 6,
      Release          => 7,
      Inform           => 8);

   type Message_Type is new DHCP.Marshaling.Serializable_Object with record
      Op_Code        : Opcode_Type           := Boot_Request;
      Link_Hardware  : Hardware_Type         := Ethernet;
      HW_Addr_Len    : Anet.HW_Addr_Len_Type := 6;
      Hop_Count      : Hops_Type             := 0;
      X_ID           : Transaction_ID_Type   := 0;
      Seconds        : Seconds_Type          := 0;
      Broadcast_Flag : Boolean               := False;
      Client_IP      : Anet.IPv4_Addr_Type   := Anet.Any_Addr;
      Your_IP        : Anet.IPv4_Addr_Type   := Anet.Any_Addr;
      Server_IP      : Anet.IPv4_Addr_Type   := Anet.Any_Addr;
      Gateway_IP     : Anet.IPv4_Addr_Type   := Anet.Any_Addr;
      Hardware_Addr  : Anet.Hardware_Addr_Type
        (Anet.HW_Addr_Len_Type'Range) := (others => 0);
      Server_Name    : Ada.Strings.Unbounded.Unbounded_String;
      Boot_File_Name : Ada.Strings.Unbounded.Unbounded_String;
      Opts           : Options.Inst4.Option_List;
   end record;

end DHCPv4.Message;
