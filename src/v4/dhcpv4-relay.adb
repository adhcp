--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;
with Ada.Strings.Fixed;
with Ada.Exceptions;

with Anet.Sockets.Filters;
with Anet.Sockets.Inet;
with Anet.Sockets.Packet;
with Anet.UDP;
with Anet.IPv4;

with DHCP.Net;
with DHCP.Logger;
with DHCP.Socket_Callbacks;

with DHCPv4.Message;
with DHCPv4.Receivers;

package body DHCPv4.Relay is

   package L renames DHCP.Logger;

   Sock_Server : Anet.Sockets.Inet.UDPv4_Socket_Type;
   --  Socket used to forward messages to the DHCP server.

   Sock_Cli_Packet : aliased Anet.Sockets.Packet.UDP_Socket_Type;
   --  Packet socket used to communicate with clients on the served interface.

   Rcvr_Cli_Packet : Receivers.Packet.Receiver_Type
     (S => Sock_Cli_Packet'Access);
   --  L2 packet receiver.

   Sock_Cli_Iface_IP : aliased Anet.Sockets.Inet.UDPv4_Socket_Type;
   --  Socket bound to the client interface IP. Used to receive either replies
   --  from the server or requests forwarded by other relay agents.

   Rcvr_Cli_IP : Receivers.UDPv4.Receiver_Type
     (S => Sock_Cli_Iface_IP'Access);
   --  IP packet receiver.

   Srv_IP : Anet.IPv4_Addr_Type;
   --  IP address of DHCP server.

   Cli_Iface : DHCP.Net.Iface_Type (Addrtype => DHCP.Net.IPv4_Address);
   --  Served client interface.

   Cli_IP : Anet.IPv4_Addr_Type;
   --  IP address of client interface.

   procedure Relay_Client_Message
     (Buffer : Ada.Streams.Stream_Element_Array;
      Src    : Ether_Addr_Type);
   --  Relay DHCP client message contained in buffer to server.

   procedure Relay_Server_Message
     (Buffer : Ada.Streams.Stream_Element_Array;
      Src    : Anet.Sockets.Inet.IPv4_Sockaddr_Type);
   --  Relay messages received from either a DHCP server or a DHCP relay
   --  agent. If the message is a BOOTP reply from a server, relay it to the
   --  client network. If it is a relayed BOOTP request from an other relay
   --  agent, forward it to the DHCP server.

   procedure Check_Configuration;
   --  Check runtime configuration.

   -------------------------------------------------------------------------

   procedure Check_Configuration
   is
   begin
      if DHCP.Net.Is_Local_Iface (Address => Srv_IP) then
         raise Config_Error with "Specified server IP is a local address ("
           & Anet.To_String (Address => Srv_IP) & ")";
      end if;
   end Check_Configuration;

   -------------------------------------------------------------------------

   procedure Relay_Client_Message
     (Buffer : Ada.Streams.Stream_Element_Array;
      Src    : Ether_Addr_Type)
   is
      pragma Unreferenced (Src);

      use type Anet.IPv4_Addr_Type;

      Msg : Message.Message_Type;
   begin
      Msg := Message.Deserialize
        (Buffer => Anet.IPv4.Validate_And_Strip (Packet => Buffer));

      if Msg.Get_Opcode = Boot_Request then
         if Msg.Get_Hops = Hops_Type'Last then

            --  Silently discard request because it has a hop count of 16. See
            --  RFC 1542, section 4.1.1.

            return;
         end if;

         L.Log (Message => "Relaying message with XID"
                & Msg.Get_Transaction_ID'Img & " from "
                & Anet.To_String (Address => Msg.Get_Hardware_Address)
                & " to server " & Anet.To_String (Address => Srv_IP));

         if Msg.Get_Gateway_IP = Anet.Any_Addr then
            Msg.Set_Gateway_IP (IP => Cli_IP);
         end if;

         --  Relay client request to DHCP server.

         Msg.Inc_Hops;
         Sock_Server.Send (Item     => Msg.Serialize,
                           Dst_Addr => Srv_IP,
                           Dst_Port => Bootp_Server_Port);
      end if;

   exception
      when E : Anet.IPv4.Invalid_IP_Packet | Anet.UDP.Invalid_UDP_Packet =>
         L.Log (Level   => L.Info,
                Message => "Ignoring packet ("
                & Ada.Exceptions.Exception_Message (X => E) & ")");

      when E : Message.Invalid_Message =>

         --  Discard invalid messages, see RFC 1542; section 2.1.

         L.Log
           (Level   => L.Info,
            Message => "Discarding message (XID" & Msg.Get_Transaction_ID'Img
            & "): " & Ada.Exceptions.Exception_Message (X => E));

      when E : Anet.Socket_Error =>

         --  Unable to send, continue operation (network may recover).

         L.Log
           (Level   => L.Warning,
            Message => "Unable to send message (XID"
            & Msg.Get_Transaction_ID'Img & "): "
            & Ada.Exceptions.Exception_Message (X => E));
   end Relay_Client_Message;

   -------------------------------------------------------------------------

   procedure Relay_Server_Message
     (Buffer : Ada.Streams.Stream_Element_Array;
      Src    : Anet.Sockets.Inet.IPv4_Sockaddr_Type)
   is
      use type Anet.IPv4_Addr_Type;

      Msg      : Message.Message_Type;
      Port_Str : constant String := Ada.Strings.Fixed.Trim
        (Source => Src.Port'Img,
         Side   => Ada.Strings.Both);
   begin
      Msg := Message.Deserialize (Buffer => Buffer);

      case Msg.Get_Opcode is

         when Boot_Request =>
            if Msg.Get_Hops = Hops_Type'Last then

               --  Silently discard request because it has a hop count of 16.
               --  See RFC 1542, section 4.1.1.

               return;
            end if;

            --  This is a request forwarded from another relay agent via
            --  unicast directly to our client interface address.

            L.Log (Message => "Relaying message with XID"
                   & Msg.Get_Transaction_ID'Img & " from relay agent "
                   & Anet.To_String (Address => Src.Addr) & ":"
                   & Port_Str & " to server " & Anet.To_String
                     (Address => Srv_IP));

            --  Relay request to DHCP server.

            Msg.Inc_Hops;
            Sock_Server.Send (Item     => Msg.Serialize,
                              Dst_Addr => Srv_IP,
                              Dst_Port => Bootp_Server_Port);

         when Boot_Reply =>

            --  It is the responsibility of BOOTP servers to send reply
            --  messages directly to the relay agent identified in the 'giaddr'
            --  field. Therefore, a relay agent may assume that all reply
            --  messages it receives are intended for BOOTP clients on its
            --  directly-connected networks. RFC 1542, section 4.1.2

            if Msg.Get_Gateway_IP /= Cli_IP then

               --  Silently discard this boot reply because the giaddr does not
               --  match our client interface. See RFC 1542 section 4.1.2.

               return;
            end if;

            L.Log (Message => "Relaying message with XID"
                   & Msg.Get_Transaction_ID'Img & " from server "
                   & Anet.To_String (Address => Src.Addr) & ":"
                   & Port_Str & " to client " & Anet.To_String
                     (Address => Msg.Get_Hardware_Address));

            --  Relay server response to DHCP client.

            declare
               Dst_IP : Anet.IPv4_Addr_Type     := Msg.Get_Your_IP;
               Dst_HW : Anet.Hardware_Addr_Type := Msg.Get_Hardware_Address;
            begin
               if Msg.Has_Broadcast_Set then
                  Dst_IP := Anet.Bcast_Addr;
                  Dst_HW := (others => 16#ff#);
               end if;

               Msg.Inc_Hops;
               Sock_Cli_Packet.Send
                 (Item  => Anet.IPv4.Create_Packet
                    (Payload  => Msg.Serialize,
                     Src_IP   => Cli_IP,
                     Src_Port => Bootp_Server_Port,
                     Dst_IP   => Dst_IP,
                     Dst_Port => Bootp_Client_Port),
                  To    => Dst_HW,
                  Iface => DHCP.Net.Get_Name (Iface => Cli_Iface));
            end;
      end case;

   exception
      when E : Message.Invalid_Message =>

         --  Discard invalid messages, see RFC 1542; section 2.1.

         L.Log
           (Level   => L.Info,
            Message => "Discarding message (XID" & Msg.Get_Transaction_ID'Img
            & "): " & Ada.Exceptions.Exception_Message (X => E));

      when E : Anet.Socket_Error =>

         --  Unable to send, continue operation (network may recover).

         L.Log
           (Level   => L.Warning,
            Message => "Unable to send message (XID"
            & Msg.Get_Transaction_ID'Img & "): "
            & Ada.Exceptions.Exception_Message (X => E));
   end Relay_Server_Message;

   -------------------------------------------------------------------------

   procedure Run
     (Server_IP    : Anet.IPv4_Addr_Type;
      Client_Iface : Anet.Types.Iface_Name_Type)
   is
   begin
      DHCP.Net.Discover_Ifaces;

      Cli_Iface := DHCP.Net.Get_Iface
        (Name     => Client_Iface,
         Addrtype => DHCP.Net.IPv4_Address);
      Cli_IP := Cli_Iface.Address;
      Srv_IP := Server_IP;

      Check_Configuration;

      L.Log (Message => "Listening for BOOTP requests on interface "
             & String (Client_Iface) & " with IP "
             & Anet.To_String (Address => Cli_Iface.Address));
      L.Log (Message => "Forwarding BOOTP messages to server "
             & Anet.To_String (Address => Server_IP));

      Sock_Cli_Packet.Init;
      Anet.Sockets.Filters.Set_Filter
        (Socket => Anet.Sockets.Socket_Type (Sock_Cli_Packet),
         Filter => Anet.Sockets.Filters.Filter_UDP_Port_Bootps);
      Sock_Cli_Packet.Bind (Iface => Client_Iface);
      Sock_Cli_Packet.Set_Socket_Option
        (Option => Anet.Sockets.Broadcast,
         Value  => True);

      Sock_Cli_Iface_IP.Init;
      Sock_Cli_Iface_IP.Bind (Address => Cli_Iface.Address,
                              Port    => Bootp_Server_Port);

      Sock_Server.Init;
      Sock_Server.Bind (Port => Bootp_Server_Port);

      Rcvr_Cli_Packet.Register_Error_Handler
        (Callback => DHCP.Socket_Callbacks.Handle_Receive_Error'Access);
      Rcvr_Cli_IP.Register_Error_Handler
        (Callback => DHCP.Socket_Callbacks.Handle_Receive_Error'Access);

      Rcvr_Cli_Packet.Listen (Callback => Relay_Client_Message'Access);
      Rcvr_Cli_IP.Listen (Callback => Relay_Server_Message'Access);
   end Run;

   -------------------------------------------------------------------------

   procedure Stop
   is
   begin
      Rcvr_Cli_Packet.Stop;
      Rcvr_Cli_IP.Stop;
   end Stop;

end DHCPv4.Relay;
