--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCPv4.Message;
with DHCPv4.States.Init;

private with DHCP.States;
private with DHCP.Types;
private with DHCP.Timing_Events.Timer_Expiry;

package DHCPv4.Transactions is

   type Transaction_Type is new States.Transaction_Type
     (Initial_State => States.Init.State) with private;
   --  The transaction type implements a DHCP transaction encompassing it's
   --  associated data and current state.

   function Get_ID (Transaction : Transaction_Type) return Transaction_ID_Type;
   --  Return XID of given transaction.

   procedure Set_Exp_Backoff
     (Transaction : in out Transaction_Type;
      New_State   :        Boolean);
   --  Enable/Disable the exponential backoff retry strategy. Disabling leads
   --  to a constant retry delay.

   Initial_Retry_Delay : constant := 4.0;
   --  Value of initial retry delay (4 seconds).

private

   type Transaction_Type is new
     States.Transaction_Type (Initial_State => States.Init.State) with record
      ID              : Transaction_ID_Type := 0;
      Last_Msg        : Message.Message_Type;
      Last_Dst        : Anet.IPv4_Addr_Type := Anet.Any_Addr;
      Retry_Delay     : Duration            := Initial_Retry_Delay;
      Exp_Backoff     : Boolean             := True;
      Retransmit_Ev   : DHCP.Timing_Events.Timer_Expiry.Expiry_Type
        (T          => Transaction_Type'Access,
         Timer_Kind => DHCP.Types.Retransmit);
      Cur_Server_Addr : Anet.IPv4_Addr_Type := Anet.Any_Addr;
   end record;

   overriding
   procedure Start (Transaction : in out Transaction_Type);

   overriding
   procedure Process_Timer_Expiry
     (Transaction : in out Transaction_Type;
      Timer_Kind  :        DHCP.Types.DHCP_Timer_Kind);

   overriding
   procedure Process_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type);

   overriding
   procedure Set_State
     (Transaction : in out Transaction_Type;
      State       :        DHCP.States.State_Handle);

   overriding
   procedure Set_ID
     (Transaction : in out Transaction_Type;
      XID         :        Transaction_ID_Type);

   overriding
   procedure Prepare_Message
     (Transaction :        Transaction_Type;
      Msg         : in out Message.Message_Type);

   overriding
   procedure Increase_Retry_Delay (Transaction : in out Transaction_Type);

   overriding
   procedure Reset_Retry_Delay (Transaction : in out Transaction_Type);

   overriding
   procedure Set_Server_Address
     (Transaction : in out Transaction_Type;
      Address     :        Anet.IPv4_Addr_Type);

   overriding
   function Get_Server_Address
     (Transaction : Transaction_Type)
      return Anet.IPv4_Addr_Type;

   overriding
   procedure Send_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type;
      Dst         :        Anet.IPv4_Addr_Type);

   overriding
   procedure Process_Bind_Timeout (Transaction : in out Transaction_Type);

   overriding
   procedure Reset (Transaction : in out Transaction_Type);

end DHCPv4.Transactions;
