--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Strings.Unbounded;

with Anet.IPv4;
with Anet.Sockets.Filters;

with DHCP.Logger;

with DHCP.Socket_Callbacks;

package body DHCPv4.Transmission
is

   use Ada.Strings.Unbounded;

   package L renames DHCP.Logger;

   -------------------------------------------------------------------------

   function Get_Iface_Mac return Anet.Hardware_Addr_Type
   is
   begin
      return Iface_Info.Mac_Addr;
   end Get_Iface_Mac;

   -------------------------------------------------------------------------

   function Get_Iface_Name return Anet.Types.Iface_Name_Type
   is
   begin
      return Anet.Types.Iface_Name_Type (To_String (Iface_Info.Name));
   end Get_Iface_Name;

   -------------------------------------------------------------------------

   procedure Initialize
     (Client_Iface : DHCP.Net.Iface_Type;
      Listen_Cb    : Receivers.Packet.Rcv_Item_Callback)
   is
      Iface_Name : constant Anet.Types.Iface_Name_Type
        := Anet.Types.Iface_Name_Type (To_String (Client_Iface.Name));
   begin
      Iface_Info := Client_Iface;

      Sock_Ucast.Init;
      Sock_Ucast.Bind (Address => Anet.Any_Addr,
                       Port    => Bootp_Client_Port);
      Sock_Ucast.Bind (Iface => Iface_Name);
      Anet.Sockets.Filters.Set_Filter
        (Socket => Anet.Sockets.Socket_Type (Sock_Ucast.all),
         Filter => Anet.Sockets.Filters.Filter_Discard_All);

      Sock_Bcast.Init;
      Sock_Bcast.Bind (Iface => Iface_Name);
      Anet.Sockets.Filters.Set_Filter
        (Socket => Anet.Sockets.Socket_Type (Sock_Bcast.all),
         Filter => Anet.Sockets.Filters.Filter_UDP_Port_Bootpc);

      Receiver.Register_Error_Handler
        (Callback => DHCP.Socket_Callbacks.Handle_Receive_Error'Access);
      Receiver.Listen (Callback => Listen_Cb);
   end Initialize;

   -------------------------------------------------------------------------

   procedure Send
     (Message     : DHCPv4.Message.Message_Type;
      Destination : Anet.IPv4_Addr_Type)
   is
      use type Anet.IPv4_Addr_Type;
   begin
      L.Log (Message => "Sending " & Message.Get_Kind'Img & " message to "
             & Anet.To_String (Address => Destination));

      if Destination = Anet.Bcast_Addr then
         Sock_Bcast.Send
           (Item  => Anet.IPv4.Create_Packet
              (Payload  => Message.Serialize,
               Src_IP   => Anet.Any_Addr,
               Src_Port => Bootp_Client_Port,
               Dst_IP   => Anet.Bcast_Addr,
               Dst_Port => Bootp_Server_Port),
            To    => Anet.Bcast_HW_Addr,
            Iface => Anet.Types.Iface_Name_Type (To_String (Iface_Info.Name)));
      else
         Sock_Ucast.Send
           (Item     => Message.Serialize,
            Dst_Addr => Destination,
            Dst_Port => Bootp_Server_Port);
      end if;
   end Send;

   -------------------------------------------------------------------------

   procedure Stop
   is
   begin
      Receiver.Stop;
   end Stop;

end DHCPv4.Transmission;
