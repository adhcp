--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;

with System;

package DHCPv4.Message.Thin is

   use type System.Bit_Order;

   BFlag_Idx : constant := 8 ** Boolean'Pos
     (System.Default_Bit_Order = System.Low_Order_First);

   type Flags_Type is array (1 .. 16) of Boolean;
   for Flags_Type'Size use 16;
   pragma Pack (Flags_Type);

   type Raw_Header_Type is record
      Op     : Opcode_Type;
      HType  : Hardware_Type;
      Hlen   : Anet.HW_Addr_Len_Type;
      Hops   : Hops_Type;
      XID    : Transaction_ID_Type;
      Secs   : Seconds_Type;
      Flags  : Flags_Type;
      CIAddr : Anet.IPv4_Addr_Type;
      YIAddr : Anet.IPv4_Addr_Type;
      SIAddr : Anet.IPv4_Addr_Type;
      GIAddr : Anet.IPv4_Addr_Type;
      CHAddr : Anet.Hardware_Addr_Type (Anet.HW_Addr_Len_Type'Range);
      SName  : Server_Name_Type;
      File   : File_Name_Type;
   end record;
   --  DHCP message type.

   for Raw_Header_Type use record
      Op      at   0 range 0 .. 7;
      HType   at   1 range 0 .. 7;
      Hlen    at   2 range 0 .. 7;
      Hops    at   3 range 0 .. 7;
      XID     at   4 range 0 .. 31;
      Secs    at   8 range 0 .. 15;
      Flags   at  10 range 0 .. 15;
      CIAddr  at  12 range 0 .. 31;
      YIAddr  at  16 range 0 .. 31;
      SIAddr  at  20 range 0 .. 31;
      GIAddr  at  24 range 0 .. 31;
      CHAddr  at  28 range 0 .. 127;
      SName   at  44 range 0 .. 511;
      File    at 108 range 0 .. 1023;
   end record;

   for Raw_Header_Type'Size use 1888;
   for Raw_Header_Type'Alignment use 1;

   use type Ada.Streams.Stream_Element_Offset;

   Upper_Index : constant Ada.Streams.Stream_Element_Offset
     := Raw_Header_Type'Object_Size / Ada.Streams.Stream_Element'Object_Size;

   subtype Raw_Header_Buffer_Type is
     Ada.Streams.Stream_Element_Array (1 .. Upper_Index);

   procedure Validate (Item : Raw_Header_Type);
   --  Validate given raw message. Throws an invalid message exception if any
   --  message field contains an illegal value.

end DHCPv4.Message.Thin;
