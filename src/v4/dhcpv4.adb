--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCPv4 is

   -------------------------------------------------------------------------

   function To_File_Name (Str : String) return File_Name_Type
   is
   begin
      if Str'Length > File_Name_Type'Length then
         raise Conversion_Error with "String too long for file name "
           & "conversion";
      end if;

      return Name : File_Name_Type := (others => 0) do
         Name (Name'First .. Name'First + Str'Length - 1)
           := Anet.To_Bytes (Str => Str);
      end return;
   end To_File_Name;

   -------------------------------------------------------------------------

   function To_Server_Name (Str : String) return Server_Name_Type
   is
   begin
      if Str'Length > Server_Name_Type'Length then
         raise Conversion_Error with "String too long for server name "
           & "conversion";
      end if;

      return Name : Server_Name_Type := (others => 0) do
         Name (Name'First .. Name'First + Str'Length - 1)
           := Anet.To_Bytes (Str => Str);
      end return;
   end To_Server_Name;

   -------------------------------------------------------------------------

   function To_String (XID : Transaction_ID_Type) return String
   is
      ID_Str : constant String := XID'Img;
   begin
      return ID_Str (ID_Str'First + 1 .. ID_Str'Last);
   end To_String;

end DHCPv4;
