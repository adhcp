--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;

with Anet.Byte_Swapping;

with DHCPv4.Message.Thin;

package body DHCPv4.Message is

   function To_Raw_Header
     (Data : Ada.Streams.Stream_Element_Array)
      return Thin.Raw_Header_Buffer_Type;
   --  Convert stream array data to raw header buffer.

   -------------------------------------------------------------------------

   procedure Add_Option
     (Msg : in out Message_Type;
      Opt :        Options.Inst.Option_Type'Class)
   is
   begin
      Msg.Opts.Append (New_Item => Opt);
   end Add_Option;

   -------------------------------------------------------------------------

   function Create (Kind : Message_Kind) return Message_Type
   is
      Msg_Type_Opt : constant Options.Inst.Option_Type'Class
        := Options.Inst.Create
          (Name => Options.DHCP_Msg_Type,
           Data => (1 => Kind'Enum_Rep));
   begin
      return Msg : Message_Type do
         Options.Inst4.Append (List     => Msg.Opts,
                               New_Item => Msg_Type_Opt);
         case Kind is
            when Discover | Request | Decline | Release | Inform =>
               Msg.Op_Code := Boot_Request;
            when Offer | Acknowledge | Not_Acknowledge =>
               Msg.Op_Code := Boot_Reply;
         end case;
      end return;
   end Create;

   -------------------------------------------------------------------------

   function Deserialize
     (Buffer : Ada.Streams.Stream_Element_Array)
      return Message_Type
   is
      use Ada.Streams;
      use Ada.Strings.Unbounded;

      package BS renames Anet.Byte_Swapping;

      Raw_Buffer : Thin.Raw_Header_Buffer_Type;
      Message    : Message_Type;
      Raw_Hdr    : Thin.Raw_Header_Type;

      for Raw_Hdr'Address use Raw_Buffer'Address;
   begin
      if Buffer'Length <= 235 then
         raise Invalid_Message with "Could not deserialize message - "
           & "invalid buffer size:" & Buffer'Length'Img & " byte(s)";
      end if;

      Raw_Buffer := To_Raw_Header
        (Data => Buffer (Buffer'First .. Buffer'First + 235));

      Thin.Validate (Item => Raw_Hdr);

      Message.Op_Code        := Raw_Hdr.Op;
      Message.Link_Hardware  := Raw_Hdr.HType;
      Message.HW_Addr_Len    := Raw_Hdr.Hlen;
      Message.Hop_Count      := Raw_Hdr.Hops;
      Message.X_ID           := BS.Network_To_Host (Input => Raw_Hdr.XID);
      Message.Seconds        := BS.Network_To_Host (Input => Raw_Hdr.Secs);
      Message.Broadcast_Flag := Raw_Hdr.Flags (Thin.BFlag_Idx);
      Message.Client_IP      := Raw_Hdr.CIAddr;
      Message.Your_IP        := Raw_Hdr.YIAddr;
      Message.Server_IP      := Raw_Hdr.SIAddr;
      Message.Gateway_IP     := Raw_Hdr.GIAddr;
      Message.Hardware_Addr  := Raw_Hdr.CHAddr;
      Message.Server_Name    := To_Unbounded_String
        (Source => Anet.To_String (Bytes => Raw_Hdr.SName));
      Message.Boot_File_Name := To_Unbounded_String
        (Source => Anet.To_String (Bytes => Raw_Hdr.File));

      --  Options

      begin
         Message.Opts := Options.Inst4.Deserialize
           (Buffer => Buffer (Buffer'First + 236 .. Buffer'Last));

      exception
         when E : Options.Inst.Invalid_Option
            | Options.Inst4.Invalid_Cookie =>
            raise Invalid_Message with "Options invalid - "
              & Ada.Exceptions.Exception_Message (X => E);
      end;

      return Message;
   end Deserialize;

   -------------------------------------------------------------------------

   function Get_Boot_File_Name (Msg : Message_Type) return String
   is
   begin
      return Ada.Strings.Unbounded.To_String
        (Source => Msg.Boot_File_Name);
   end Get_Boot_File_Name;

   -------------------------------------------------------------------------

   function Get_Client_IP (Msg : Message_Type) return Anet.IPv4_Addr_Type
   is
   begin
      return Msg.Client_IP;
   end Get_Client_IP;

   -------------------------------------------------------------------------

   function Get_Gateway_IP (Msg : Message_Type) return Anet.IPv4_Addr_Type
   is
   begin
      return Msg.Gateway_IP;
   end Get_Gateway_IP;

   -------------------------------------------------------------------------

   function Get_Hardware_Address
     (Msg : Message_Type)
      return Anet.Hardware_Addr_Type
   is
   begin
      return Msg.Hardware_Addr
        (Anet.HW_Addr_Len_Type'First .. Msg.HW_Addr_Len);
   end Get_Hardware_Address;

   -------------------------------------------------------------------------

   function Get_Hops (Msg : Message_Type) return Hops_Type
   is
   begin
      return Msg.Hop_Count;
   end Get_Hops;

   -------------------------------------------------------------------------

   function Get_HW_Addr_Length
     (Msg : Message_Type)
      return Anet.HW_Addr_Len_Type
   is
   begin
      return Msg.HW_Addr_Len;
   end Get_HW_Addr_Length;

   -------------------------------------------------------------------------

   function Get_Kind (Msg : Message_Type) return Message_Kind
   is
      Opt : constant Options.Inst.Option_Type'Class
        := Msg.Get_Options.Get (Name => Options.DHCP_Msg_Type);
   begin
      return Message_Kind'Enum_Val (Opt.Get_Data (1));
   end Get_Kind;

   -------------------------------------------------------------------------

   function Get_Link_Hardware (Msg : Message_Type) return Hardware_Type
   is
   begin
      return Msg.Link_Hardware;
   end Get_Link_Hardware;

   -------------------------------------------------------------------------

   function Get_Next_Server_IP (Msg : Message_Type) return Anet.IPv4_Addr_Type
   is
   begin
      return Msg.Server_IP;
   end Get_Next_Server_IP;

   -------------------------------------------------------------------------

   function Get_Opcode (Msg : Message_Type) return Opcode_Type
   is
   begin
      return Msg.Op_Code;
   end Get_Opcode;

   -------------------------------------------------------------------------

   function Get_Options (Msg : Message_Type) return Options.Inst4.Option_List
   is
   begin
      return Msg.Opts;
   end Get_Options;

   -------------------------------------------------------------------------

   function Get_Seconds (Msg : Message_Type) return Seconds_Type
   is
   begin
      return Msg.Seconds;
   end Get_Seconds;

   -------------------------------------------------------------------------

   function Get_Server_ID (Msg : Message_Type) return Anet.IPv4_Addr_Type
   is
      Opt : constant Options.Inst.Option_Type'Class
        := Msg.Get_Options.Get (Name => Options.DHCP_Server_Id);
   begin
      return Options.Inst4.IP_Option_Type (Opt).Get_Value;
   end Get_Server_ID;

   -------------------------------------------------------------------------

   function Get_Server_Name (Msg : Message_Type) return String
   is
   begin
      return Ada.Strings.Unbounded.To_String
        (Source => Msg.Server_Name);
   end Get_Server_Name;

   -------------------------------------------------------------------------

   function Get_Transaction_ID (Msg : Message_Type) return Transaction_ID_Type
   is
   begin
      return Msg.X_ID;
   end Get_Transaction_ID;

   -------------------------------------------------------------------------

   function Get_Your_IP (Msg : Message_Type) return Anet.IPv4_Addr_Type
   is
   begin
      return Msg.Your_IP;
   end Get_Your_IP;

   -------------------------------------------------------------------------

   function Has_Broadcast_Set (Msg : Message_Type) return Boolean
   is
   begin
      return Msg.Broadcast_Flag;
   end Has_Broadcast_Set;

   -------------------------------------------------------------------------

   procedure Inc_Hops (Msg : in out Message_Type)
   is
   begin
      Msg.Hop_Count := Msg.Hop_Count + 1;

   exception
      when Constraint_Error =>
         raise Invalid_Message with "Cannot increase hop count, already"
           & Msg.Hop_Count'Img;
   end Inc_Hops;

   -------------------------------------------------------------------------

   function Serialize
     (Message : Message_Type)
      return Ada.Streams.Stream_Element_Array
   is
      use Ada.Streams;
      use Ada.Strings.Unbounded;

      package BS renames Anet.Byte_Swapping;

      Hdr : Thin.Raw_Header_Buffer_Type;
      Msg : Thin.Raw_Header_Type;

      for Msg'Address use Hdr'Address;
   begin
      Msg.Op    := Message.Op_Code;
      Msg.HType := Message.Link_Hardware;
      Msg.Hlen  := Message.HW_Addr_Len;
      Msg.Hops  := Message.Hop_Count;
      Msg.XID   := BS.Host_To_Network (Input => Message.X_ID);
      Msg.Secs  := BS.Host_To_Network (Input => Message.Seconds);

      Msg.Flags                  := (others => False);
      Msg.Flags (Thin.BFlag_Idx) := Message.Broadcast_Flag;

      Msg.CIAddr := Message.Client_IP;
      Msg.YIAddr := Message.Your_IP;
      Msg.SIAddr := Message.Server_IP;
      Msg.GIAddr := Message.Gateway_IP;
      Msg.CHAddr := Message.Hardware_Addr;

      Msg.SName := To_Server_Name (Str => To_String (Message.Server_Name));
      Msg.File  := To_File_Name (Str => To_String (Message.Boot_File_Name));

      declare
         Opts : constant Stream_Element_Array := Message.Opts.Serialize;
      begin
         return Stream : Stream_Element_Array (1 .. Hdr'Length + Opts'Length)
         do
            Stream (Stream'First .. Stream'First + Hdr'Length - 1) := Hdr;
            Stream (Stream'First + Hdr'Length .. Stream'Last)      := Opts;
         end return;
      end;
   end Serialize;

   -------------------------------------------------------------------------

   procedure Set_Broadcast_Flag
     (Msg   : in out Message_Type;
      State :        Boolean)
   is
   begin
      Msg.Broadcast_Flag := State;
   end Set_Broadcast_Flag;

   -------------------------------------------------------------------------

   procedure Set_Client_IP
     (Msg : in out Message_Type;
      IP  :        Anet.IPv4_Addr_Type)
   is
   begin
      Msg.Client_IP := IP;
   end Set_Client_IP;

   -------------------------------------------------------------------------

   procedure Set_Gateway_IP
     (Msg : in out Message_Type;
      IP  :        Anet.IPv4_Addr_Type)
   is
   begin
      Msg.Gateway_IP := IP;
   end Set_Gateway_IP;

   -------------------------------------------------------------------------

   procedure Set_Hardware_Address
     (Msg     : in out Message_Type;
      Address :        Anet.Hardware_Addr_Type)
   is
      use type Anet.HW_Addr_Len_Type;

      Last_Idx : constant Anet.HW_Addr_Len_Type
        := Msg.Hardware_Addr'First + Address'Length - 1;
   begin
      Msg.Hardware_Addr (Msg.Hardware_Addr'First .. Last_Idx) := Address;
      if Last_Idx < Msg.Hardware_Addr'Last then
         Msg.Hardware_Addr (Last_Idx + 1 .. Msg.Hardware_Addr'Last)
           := (others => 0);
      end if;

      Msg.HW_Addr_Len := Address'Length;
   end Set_Hardware_Address;

   -------------------------------------------------------------------------

   procedure Set_Transaction_ID
     (Msg : in out Message_Type;
      XID :        Transaction_ID_Type)
   is
   begin
      Msg.X_ID := XID;
   end Set_Transaction_ID;

   -------------------------------------------------------------------------

   procedure Set_Your_IP
     (Msg : in out Message_Type;
      IP  :        Anet.IPv4_Addr_Type)
   is
   begin
      Msg.Your_IP := IP;
   end Set_Your_IP;

   -------------------------------------------------------------------------

   function To_Raw_Header
     (Data : Ada.Streams.Stream_Element_Array)
      return Thin.Raw_Header_Buffer_Type
   is
      Buffer : Thin.Raw_Header_Buffer_Type := (others => 0);
   begin
      if Data'Length /= Thin.Raw_Header_Buffer_Type'Length then
         raise Constraint_Error with "Could not convert stream data to raw"
           & " hdr buffer";
      end if;

      Buffer (Buffer'First .. Data'Length) := Data;
      return Buffer;
   end To_Raw_Header;

end DHCPv4.Message;
