--
--  Copyright (C) 2014, 2015 secunet Security Networks AG
--  Copyright (C) 2014, 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2014, 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Notify;

package DHCPv4.Notify_Simple
is

   procedure Write_Lease_File
     (State    : DHCP.Notify.Reason_Type;
      Iface    : String;
      Filename : String);
   --  Create a lease file with given filename containing the DHCP
   --  configuration read from the database as key, value pairs. If the lease
   --  cannot be written, an exception is raised.

   Lease_Error : exception;

end DHCPv4.Notify_Simple;
