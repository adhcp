--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

with DHCP.Timer;
with DHCP.Logger;
with DHCP.Types;
with DHCP.Notify;
with DHCP.Timing_Events.Start;
with DHCP.Timing_Events.Timer_Expiry;

with DHCPv4.Options;
with DHCPv4.States.Init;
with DHCPv4.States.Bound;
with DHCPv4.Database;

package body DHCPv4.States is

   package L renames DHCP.Logger;

   procedure Schedule_Timer
     (Transaction : in out Transaction_Type'Class;
      Timer_Kind  :        DHCP.Types.DHCP_Timer_Kind;
      In_Time     :        Duration);
   --  Schedule timer of given kind to trigger in specified amount of time from
   --  transaction start time.

   -------------------------------------------------------------------------

   procedure Process_Ack
     (State       : access Allocation_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      Y_IP : constant Anet.IPv4_Addr_Type := Msg.Get_Your_IP;
   begin
      Transaction.Reset_Retry_Delay;

      --  Since we have successfully allocated a lease all pending events are
      --  obsolete.

      DHCP.Timer.Clear;

      L.Log (Message => "Allocated lease for IP address " & Anet.To_String
             (Address => Y_IP));

      Database.Set_Fixed_Address (Addr => Y_IP);
      Database.Set_Options (Opts => Msg.Get_Options);

      declare
         Srv_IP_Str : constant String := Anet.To_String
           (Address => Msg.Get_Server_ID);
      begin
         L.Log (Message => "Setting current DHCP server address to "
                & Srv_IP_Str);
         Transaction.Set_Server_Address
           (Address => Anet.To_IPv4_Addr (Str => Srv_IP_Str));
      end;

      declare
         Lease_Time : constant Options.Inst.Option_Type'Class
           := Msg.Get_Options.Get (Name => Options.Address_Time);
         Lease_Dur  : constant Duration
           := Duration'Value (Lease_Time.Get_Data_Str);
         T1_Dur     : Duration := Lease_Dur / 2;
         T2_Dur     : Duration := (Lease_Dur / 8) * 7;
      begin
         if Msg.Get_Options.Contains (Name => Options.Renewal_Time) then
            T1_Dur := Duration'Value
              (Msg.Get_Options.Get
                 (Name => Options.Renewal_Time).Get_Data_Str);
         end if;

         if Msg.Get_Options.Contains (Name => Options.Rebinding_Time) then
            T2_Dur := Duration'Value
              (Msg.Get_Options.Get
                 (Name => Options.Rebinding_Time).Get_Data_Str);
         end if;

         if Lease_Dur >= T2_Dur and then T2_Dur >= T1_Dur then
            Schedule_Timer (Transaction => Transaction,
                            Timer_Kind  => DHCP.Types.T1,
                            In_Time     => T1_Dur);
            Schedule_Timer (Transaction => Transaction,
                            Timer_Kind  => DHCP.Types.T2,
                            In_Time     => T2_Dur);
            Schedule_Timer (Transaction => Transaction,
                            Timer_Kind  => DHCP.Types.Lease_Expiry,
                            In_Time     => Lease_Dur);
         else
            raise Invalid_Lease_Time with "Timer durations invalid: T1"
              & Natural (T1_Dur)'Img & "< T2" & Natural (T2_Dur)'Img
              & "< Lease" & Natural (Lease_Dur)'Img;
         end if;
      end;

      Transaction.Set_State (State => Bound.State);
      Allocation_State_Type'Class (State.all).Notify_Allocation;
   end Process_Ack;

   -------------------------------------------------------------------------

   procedure Process_Bind_Timeout
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class)
   is
      pragma Unreferenced (State);
      use Ada.Real_Time;

      Ev : DHCP.Timing_Events.Start.Start_Type (T => Transaction'Access);
   begin
      L.Log (Message => "Unable to acquire IP address (timeout)");

      DHCP.Timer.Clear;
      Transaction.Reset_Retry_Delay;
      Transaction.Set_State (State => States.Init.State);
      DHCP.Notify.Update (Reason => DHCP.Notify.Timeout);

      --  Restart in 1 minute.

      Ev.Set_Time (At_Time => Clock + Minutes (M => 1));
      DHCP.Timer.Schedule (Event => Ev);
   end Process_Bind_Timeout;

   -------------------------------------------------------------------------

   procedure Process_Nack
     (State       : access Allocation_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      pragma Unreferenced (State, Msg);
   begin
      L.Log (Message => "Server rejected request, restarting");

      --  Since we are about to start a fresh transaction all pending events
      --  are obsolete.

      DHCP.Timer.Clear;
      Database.Reset;
      Transaction.Reset;
      Transaction.Set_State (State => States.Init.State);
      Transaction.Start;
   end Process_Nack;

   -------------------------------------------------------------------------

   procedure Schedule_Timer
     (Transaction : in out Transaction_Type'Class;
      Timer_Kind  :        DHCP.Types.DHCP_Timer_Kind;
      In_Time     :        Duration)
   is
      use Ada.Real_Time;

      Ev : DHCP.Timing_Events.Timer_Expiry.Expiry_Type
        (T          => Transaction'Access,
         Timer_Kind => Timer_Kind);
      New_Time : Time;
   begin
      New_Time := Transaction.Get_Start_Time + To_Time_Span (D => In_Time);
      L.Log
        (Level   => L.Info,
         Message => "Timer " & Timer_Kind'Img & " in"
         & Long_Long_Integer (To_Duration (New_Time - Clock))'Img
         & " seconds");

      Ev.Set_Time (At_Time => New_Time);
      DHCP.Timer.Schedule (Event => Ev);
   end Schedule_Timer;

end DHCPv4.States;
