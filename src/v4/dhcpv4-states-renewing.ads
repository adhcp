--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package DHCPv4.States.Renewing is

   type Renewing_State_Type is new Allocation_State_Type with private;
   --  A client is trying to renew it's DHCP lease. It regularly sends DHCP
   --  request messages to the server that granted it's current lease, until it
   --  gets a reply or the rebinding timer (T2) expires.

   overriding
   function Get_Name
     (State : not null access Renewing_State_Type)
      return String;
   --  Return name of state as string.

   overriding
   procedure Process_T2_Expiry
     (State       : access Renewing_State_Type;
      Transaction : in out Transaction_Type'Class);
   --  Transition to 'Rebinding'.

   overriding
   procedure Notify_Allocation (State : access Renewing_State_Type);
   --  Send lease allocation notification with reason 'Renew'.

   function State return DHCP.States.State_Handle;
   --  Return access to singleton.

private

   type Renewing_State_Type is new Allocation_State_Type with null record;

end DHCPv4.States.Renewing;
