--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCPv4.Options;
with DHCPv4.States.Requesting;

package body DHCPv4.States.Selecting is

   Instance : aliased Selecting_State_Type;

   -------------------------------------------------------------------------

   function Get_Name
     (State : not null access Selecting_State_Type)
      return String
   is
      pragma Unreferenced (State);
   begin
      return "Selecting";
   end Get_Name;

   -------------------------------------------------------------------------

   procedure Process_Offer
     (State       : access Selecting_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      pragma Unreferenced (State);

      Req : Message.Message_Type := Message.Create (Kind => Message.Request);
   begin
      Transaction.Reset_Retry_Delay;

      Transaction.Prepare_Message (Msg => Req);
      Req.Add_Option (Opt => Msg.Get_Options.Get
                      (Name => Options.DHCP_Server_Id));
      Req.Add_Option (Opt => Options.Inst4.Create
                      (Name    => Options.Address_Request,
                       Address => Msg.Get_Your_IP));

      Transaction.Send_Message (Msg => Req,
                                Dst => Anet.Bcast_Addr);
      Transaction.Set_State (State => Requesting.State);
   end Process_Offer;

   -------------------------------------------------------------------------

   function State return DHCP.States.State_Handle
   is
   begin
      return Instance'Access;
   end State;

end DHCPv4.States.Selecting;
