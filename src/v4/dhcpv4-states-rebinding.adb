--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Timer;
with DHCP.Logger;
with DHCP.Notify;

with DHCPv4.States.Init;
with DHCPv4.Database;

package body DHCPv4.States.Rebinding is

   package L renames DHCP.Logger;

   Instance : aliased Rebinding_State_Type;

   -------------------------------------------------------------------------

   function Get_Name
     (State : not null access Rebinding_State_Type)
      return String
   is
      pragma Unreferenced (State);
   begin
      return "Rebinding";
   end Get_Name;

   -------------------------------------------------------------------------

   procedure Notify_Allocation (State : access Rebinding_State_Type)
   is
      pragma Unreferenced (State);
   begin
      DHCP.Notify.Update (Reason => DHCP.Notify.Rebind);
   end Notify_Allocation;

   -------------------------------------------------------------------------

   procedure Process_Lease_Expiry
     (State       : access Rebinding_State_Type;
      Transaction : in out Transaction_Type'Class)
   is
      pragma Unreferenced (State);
   begin
      L.Log (Message => "Lease expired, restarting");

      --  Since we are about to start a fresh transaction all pending events
      --  are obsolete.

      DHCP.Timer.Clear;
      Database.Reset;
      Transaction.Reset;
      Transaction.Set_State (State => States.Init.State);
      Transaction.Start;
   end Process_Lease_Expiry;

   -------------------------------------------------------------------------

   function State return DHCP.States.State_Handle
   is
   begin
      return Instance'Access;
   end State;

end DHCPv4.States.Rebinding;
