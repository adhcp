--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Logger;

with DHCPv4.Database;
with DHCPv4.States.Renewing;

package body DHCPv4.States.Bound is

   package L renames DHCP.Logger;

   Instance : aliased Bound_State_Type;

   -------------------------------------------------------------------------

   function Get_Name (State : not null access Bound_State_Type) return String
   is
      pragma Unreferenced (State);
   begin
      return "Bound";
   end Get_Name;

   -------------------------------------------------------------------------

   procedure Process_T1_Expiry
     (State       : access Bound_State_Type;
      Transaction : in out Transaction_Type'Class)
   is
      pragma Unreferenced (State);

      Req : Message.Message_Type := Message.Create (Kind => Message.Request);
   begin
      L.Log
        (Message => "Trying to renew DHCP lease with " & Anet.To_String
           (Address => Transaction.Get_Server_Address));

      Transaction.Prepare_Message (Msg => Req);
      Message.Set_Client_IP (Msg => Req,
                             IP  => Database.Get_Fixed_Address);
      Transaction.Send_Message (Msg => Req,
                                Dst => Transaction.Get_Server_Address);
      Transaction.Set_State (State => Renewing.State);
   end Process_T1_Expiry;

   -------------------------------------------------------------------------

   function State return DHCP.States.State_Handle
   is
   begin
      return Instance'Access;
   end State;

end DHCPv4.States.Bound;
