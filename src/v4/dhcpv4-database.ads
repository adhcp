--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCPv4.Options;

package DHCPv4.Database
is

   procedure Set_Fixed_Address (Addr : Anet.IPv4_Addr_Type);
   --  Set fixed address.

   use type Anet.IPv4_Addr_Type;

   function Get_Fixed_Address return Anet.IPv4_Addr_Type
   with
      Pre => not Is_Clear;

   procedure Set_Options (Opts : Options.Inst4.Option_List);
   --  Set configuration options.

   function Get_Options return Options.Inst4.Option_List
   with
      Pre => not Is_Clear;
   --  Return configuration options currently stored in database.

   function Is_Clear return Boolean;
   --  Returns True if no configuration is set.

   procedure Reset;
   --  Reset database to initial state.

private

   type Database_Type is record
      Fixed_Address : Anet.IPv4_Addr_Type := Anet.Any_Addr;
      Opts          : Options.Inst4.Option_List;
   end record;

   Instance : Database_Type;

   function Is_Clear return Boolean is
     (Instance.Fixed_Address = Anet.Any_Addr and then Instance.Opts.Is_Empty);

end DHCPv4.Database;
