--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCPv4.Message.Thin is

   -------------------------------------------------------------------------

   procedure Validate (Item : Raw_Header_Type)
   is
   begin
      if not Item.Op'Valid then
         raise Invalid_Message with "Invalid operation code";
      end if;

      if not Item.HType'Valid then
         raise Invalid_Message with "Invalid hardware type";
      end if;

      if not Item.Hlen'Valid then
         raise Invalid_Message with "Invalid hardware address length";
      end if;

      if not Item.Hops'Valid then
         raise Invalid_Message with "Invalid hops count";
      end if;
   end Validate;

end DHCPv4.Message.Thin;
