--
--  Copyright (C) 2018 secunet Security Networks AG
--  Copyright (C) 2018 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2018 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

private with Ada.Real_Time;

with Anet;

private with Anet.Sockets.Packet;

with DHCP.Net;
with DHCPv4.Constants;

package DHCPv4.ACD
is

   use type DHCP.Net.Iface_Addr_Type;

   procedure Initialize (Iface : DHCP.Net.Iface_Type)
   with
      Pre => Iface.Addrtype = DHCP.Net.Link_Layer;
   --  Initialize DHCPv4 ACD layer. Use the specified interface for sending ARP
   --  messages.

   procedure Announce
     (IP_Address         : Anet.IPv4_Addr_Type;
      Announcement_Count : Natural := Constants.ANNOUNCE_NUM);
   --  Announce that the specified IPv4 address is used.

   procedure Probe
     (IP_Address :     Anet.IPv4_Addr_Type;
      Collision  : out Boolean);
   --  Probe if the specified IPv4 address is already in use on the local link.

private

   Initialized : Boolean := False;
   --  Set to True if ACD is initialized and enabled.

   Iface_Info : DHCP.Net.Iface_Type (Addrtype => DHCP.Net.Link_Layer);
   --  Information about currently used network interface.

   Packet_Socket : aliased Anet.Sockets.Packet.UDP_Socket_Type;
   --  Packet socket used to broadcast and receivce ARP packets.

   Sock_ARP : not null access Anet.Sockets.Packet.UDP_Socket_Type'Class
     := Packet_Socket'Access;
   --  APR socket handle required for improved testability to make socket
   --  implementation replaceable.

   Collision_Count : Natural := 0;
   --  Number of consecutively detected address collisions.

   Last_Collision  : Ada.Real_Time.Time := Ada.Real_Time.Time_Last;
   --  Timestamp of last detected address collision.

end DHCPv4.ACD;
