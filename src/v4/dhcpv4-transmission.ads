--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet.Types;

with DHCP.Net;

with DHCPv4.Message;
with DHCPv4.Receivers;

private with Anet.Sockets.Inet;
private with Anet.Sockets.Packet;

package DHCPv4.Transmission
is

   use type DHCP.Net.Iface_Addr_Type;

   procedure Initialize
     (Client_Iface : DHCP.Net.Iface_Type;
      Listen_Cb    : Receivers.Packet.Rcv_Item_Callback)
     with
       Pre => Client_Iface.Addrtype = DHCP.Net.Link_Layer;
   --  Initialize DHCPv4 transmission layer. Use the specified client
   --  interface for sending messages and registers the specified callback with
   --  the receiver.

   procedure Stop;
   --  Stop listening for DHCPv4 messages.

   procedure Send
     (Message     : DHCPv4.Message.Message_Type;
      Destination : Anet.IPv4_Addr_Type);
   --  Send given DHCP message to specified destination.

   function Get_Iface_Name return Anet.Types.Iface_Name_Type;
   --  Return name of currently used interface.

   function Get_Iface_Mac return Anet.Hardware_Addr_Type;
   --  Return hardware address of currently used interface.

private

   Iface_Info : DHCP.Net.Iface_Type (Addrtype => DHCP.Net.Link_Layer);
   --  Information about currently used network interface.

   Packet_Socket : aliased Anet.Sockets.Packet.UDP_Socket_Type;
   --  Packet socket used to receive responses from server and to send layer 2
   --  broadcast messages.

   Sock_Bcast : not null access Anet.Sockets.Packet.UDP_Socket_Type'Class
     := Packet_Socket'Access;
   --  Broadcast socket handle required for improved testability to make socket
   --  implementation replaceable.

   UDP_Socket : aliased Anet.Sockets.Inet.UDPv4_Socket_Type;
   --  Inet/UDP socket used in renewing state to send unicast requests to
   --  server.

   Sock_Ucast : not null access Anet.Sockets.Inet.UDPv4_Socket_Type'Class
     := UDP_Socket'Access;
   --  Unicast socket handle required for improved testability to make socket
   --  implementation replaceable.

   Receiver : Receivers.Packet.Receiver_Type (S => Packet_Socket'Access);
   --  L2 packet receiver.

end DHCPv4.Transmission;
