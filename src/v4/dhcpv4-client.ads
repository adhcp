--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet.Types;

package DHCPv4.Client is

   procedure Run
     (Client_Iface  : Anet.Types.Iface_Name_Type;
      One_Shot_Mode : Boolean;
      ACD_Enable    : Boolean);
   --  Start the DHCP client service on given client interface. If the one shot
   --  mode flag is set, the client will exit after acquiring a lease or signal
   --  an error if it fails. If ACD is set then Address Collision Detection
   --  according to RFC 5227 is performed.

   procedure Stop;
   --  Stop the DHCP client service.

   Config_Error : exception;
   --  Raised if invalid configuration is provided.

end DHCPv4.Client;
