--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

with DHCP.Timer;
with DHCP.Logger;
with DHCP.Notify;
with DHCP.Timing_Events.Start;

with DHCPv4.ACD;
with DHCPv4.Constants;
with DHCPv4.Options;
with DHCPv4.States.Init;
with DHCPv4.Database;
with DHCPv4.Transmission;

package body DHCPv4.States.Requesting is

   package L renames DHCP.Logger;

   Instance : aliased Requesting_State_Type;

   procedure Restart
     (Transaction : in out Transaction_Type'Class;
      Message     :        String);
   --  Restart given transaction and log specified message.

   -------------------------------------------------------------------------

   function Get_Name
     (State : not null access Requesting_State_Type)
      return String
   is
      pragma Unreferenced (State);
   begin
      return "Requesting";
   end Get_Name;

   -------------------------------------------------------------------------

   procedure Notify_Allocation (State : access Requesting_State_Type)
   is
      pragma Unreferenced (State);
   begin
      DHCP.Notify.Update (Reason => DHCP.Notify.Bound);
   end Notify_Allocation;

   -------------------------------------------------------------------------

   procedure Process_Ack
     (State       : access Requesting_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      Has_Collision : Boolean;
   begin
      ACD.Probe (IP_Address => Msg.Get_Your_IP,
                 Collision  => Has_Collision);
      if Has_Collision then
         declare
            Decline_Msg : Message.Message_Type
              := Message.Create (Kind => Message.Decline);
         begin
            Decline_Msg.Set_Hardware_Address
              (Address => Transmission.Get_Iface_Mac);
            Decline_Msg.Set_Transaction_ID (XID => Transaction.Get_ID);
            Decline_Msg.Add_Option (Opt => Options.Inst4.Create
                                    (Name    => Options.Address_Request,
                                     Address => Msg.Get_Your_IP));
            Decline_Msg.Add_Option (Opt => Msg.Get_Options.Get
                                    (Name => Options.DHCP_Server_Id));
            Transaction.Send_Message (Msg => Decline_Msg,
                                      Dst => Anet.Bcast_Addr);

            Restart (Transaction => Transaction,
                     Message     => "Declined offer for "
                     & Anet.To_String (Address => Msg.Get_Your_IP) & " from "
                     & Anet.To_String (Address => Msg.Get_Server_ID)
                     & " due to collision, restarting in"
                     & Constants.RESTART_DELAY'Img & " seconds");
         end;
      else

         --  Redispatch to parent implementation.

         declare
            type Alloc_State_Access is access all Allocation_State_Type;
         begin
            States.Process_Ack
              (State       => Alloc_State_Access (State),
               Transaction => Transaction,
               Msg         => Msg);
            ACD.Announce (IP_Address => Msg.Get_Your_IP);
         end;
      end if;
   end Process_Ack;

   -------------------------------------------------------------------------

   procedure Process_Nack
     (State       : access Requesting_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      pragma Unreferenced (State, Msg);
   begin
      Restart
        (Transaction => Transaction,
         Message     => "Server rejected request, restarting in"
         & Constants.RESTART_DELAY'Img & " seconds");
   end Process_Nack;

   -------------------------------------------------------------------------

   procedure Restart
     (Transaction : in out Transaction_Type'Class;
      Message     :        String)
   is
      use Ada.Real_Time;

      Ev : DHCP.Timing_Events.Start.Start_Type (T => Transaction'Access);
   begin
      L.Log (Message => Message);

      --  Since we are about to start a fresh transaction all pending events
      --  are obsolete.

      DHCP.Timer.Clear;
      Database.Reset;
      Transaction.Reset;
      Transaction.Set_State (State => States.Init.State);

      Ev.Set_Time (At_Time => Clock + Seconds (S => Constants.RESTART_DELAY));
      DHCP.Timer.Schedule (Event => Ev);
   end Restart;

   -------------------------------------------------------------------------

   function State return DHCP.States.State_Handle
   is
   begin
      return Instance'Access;
   end State;

end DHCPv4.States.Requesting;
