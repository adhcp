--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Tags;

with DHCP.Options.V4;

package DHCPv4.Options
is

   type Option_Name is
     (

      --  RFC 1497 Vendor Extensions

      Pad,
      Subnet_Mask,
      Time_Offset,
      Routers,
      Time_Servers,
      Name_Servers,
      Domain_Name_Servers,
      Log_Servers,
      Quotes_Servers,
      LPR_Servers,
      Impress_Servers,
      RLP_Servers,
      Host_Name,
      Boot_File_Size,
      Merit_Dump_File,
      Domain_Name,
      Swap_Server,
      Root_Path,
      Extension_File,

      --  IP Layer Parameters per Host

      Forward_On_Off,
      Src_Rte_On_Off,
      Policy_Filter,
      Max_DG_Assembly,
      Default_IP_TTL,
      Timeout_MTU,
      Plateau_MTU,

      --  IP Layer Parameters per Interface

      Interface_MTU,
      Subnet_MTU,
      Broadcast_Address,
      Mask_Discovery,
      Mask_Supplier,
      Router_Discovery,
      Router_Request,
      Static_Route,

      --  Link Layer Parameters per Interface

      Trailers,
      ARP_Timeout,
      Ethernet,

      --  TCP Parameters

      Default_TCP_TTL,
      Keepalive_Time,
      Keepalive_Garbage,

      --  Application and Service Parameters

      NIS_Domain,
      NIS_Servers,
      NTP_Servers,
      Vendor_Specific,
      NETBIOS_Name_Servers,
      NETBIOS_Dist_Servers,
      NETBIOS_Node_Type,
      NETBIOS_Scope,
      X_Window_Font_Servers,
      X_Window_Managers,
      Address_Request,
      Address_Time,
      Overload,
      DHCP_Msg_Type,
      DHCP_Server_Id,
      Parameter_List,
      DHCP_Message,
      DHCP_Max_Msg_Size,
      Renewal_Time,
      Rebinding_Time,
      Class_Id,
      Client_Id,
      NetWare_IP_Domain,
      NetWare_IP_Option,
      NIS_Plus_Domain,
      NIS_Plus_Servers,
      TFTP_Server_Name,
      Bootfile_Name,
      Home_Agent_Addrs,
      SMTP_Servers,
      POP3_Servers,
      NNTP_Servers,
      WWW_Servers,
      Finger_Servers,
      IRC_Servers,
      Street_Talk_Servers,
      STDA_Servers,
      User_Class,
      Directory_Agent,
      Service_Scope,
      Rapid_Commit,
      Client_FQDN,
      Relay_Agent_Information,
      ISNS,
      NDS_Servers,
      NDS_Tree_Name,
      NDS_Context,
      BCMCS_Controller_Names,
      BCMCS_Controller_Address,
      Authentication,
      Client_Last_Transaction_Time,
      Associated_IP,
      Client_System,
      Client_NDI,
      LDAP,
      UUID_GUID,
      User_Auth,
      Geo_Conf_Civic,
      P_Code,
      T_Code,
      Netinfo_Address,
      Netinfo_Tag,
      URL,
      Auto_Config,
      Name_Service_Search,
      Subnet_Selection,
      Domain_Search,
      SIP_Servers,
      Classless_Static_Route,
      CCC,
      Geo_Conf,
      VI_VCO,
      VI_VSO,
      PXE_Undefined_1,
      PXE_Undefined_2,
      PXE_Undefined_3,
      PXE_Undefined_4,
      PXE_Undefined_5,
      PXE_Undefined_6,
      PXE_Undefined_7,
      PXE_Undefined_8,
      Pana_Agent,
      V4_Lost,
      Capwap_AC_V4,
      IPv4_Address_MoS,
      IPv4_FQDN_MoS,
      SIP_UA_Conf_Domains,
      IPv4_Address_ANDSF,
      IPv6_Address_ANDSF,
      TFTP_Server,
      Etherboot_1,
      IP_Telephone,
      Etherboot_2,
      PXE_Linux_Magic,
      Configuration_File,
      Path_Prefix,
      Reboot_Time,
      IPv6_RD,
      V4_Access_Domain,
      Subnet_Allocation,
      Virtual_Subnet_Select,

      End_Marker,

      --  Site specific vendor extensions (non-IANA)

      Site_Specific);
   --  DHCP option names.

   type Code_Type is range 0 .. 255
     with
       Size => 8;

   function Code_To_Name (Code : Code_Type) return Option_Name;
   --  Convert given code to option name.

   function Name_To_Code (Name : Option_Name) return Code_Type;
   --  Convert given name to option code.

   function Code_To_Tag (Code : Code_Type) return Ada.Tags.Tag;
   --  Return object tag for given option code.

   --  Option instances.

   package Inst is new DHCP.Options
     (Code_Type        => Code_Type,
      Option_Name_Type => Option_Name,
      To_Name          => Code_To_Name,
      To_Tag           => Code_To_Tag,
      To_Code          => Name_To_Code);

   package Inst4 is new Inst.V4;

end DHCPv4.Options;
