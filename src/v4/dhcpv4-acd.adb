--
--  Copyright (C) 2018 secunet Security Networks AG
--  Copyright (C) 2018 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2018 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;
with Ada.Streams;
with Ada.Strings.Unbounded;

with GNAT.OS_Lib;

with Anet.ARP;
with Anet.Types;
with Anet.Constants;

with DHCP.Logger;
with DHCP.Random;
with DHCP.Timer;

with DHCPv4.Timing_Events.Announce;

package body DHCPv4.ACD
is

   use Ada.Strings.Unbounded;
   package L renames DHCP.Logger;

   procedure Sleep (Interval : Duration);
   --  Suspend execution for the specified interval.

   Null_HW_Addr : constant Anet.Ether_Addr_Type := (others => 0);

   procedure Setup_Socket (Iface : Anet.Types.Iface_Name_Type)
   with
      Pre => Initialized;
   --  Initialize and open ARP socket on given interface.

   -------------------------------------------------------------------------

   procedure Announce
     (IP_Address         : Anet.IPv4_Addr_Type;
      Announcement_Count : Natural := Constants.ANNOUNCE_NUM)
   is
      Iface_Name  : constant Anet.Types.Iface_Name_Type
        := Anet.Types.Iface_Name_Type (To_String (Iface_Info.Name));
      ARP_Message : constant Anet.ARP.Header_Type
        := Anet.ARP.Header_Type'
          (Operation => Anet.ARP.ARP_Request,
           Src_Ether => Iface_Info.Mac_Addr,
           Src_IP    => IP_Address,
           Dst_Ether => Null_HW_Addr,
           Dst_IP    => IP_Address);
   begin
      if not Initialized then
         return;
      end if;

      L.Log (Message => "Announcing IP address "
             & Anet.To_String (Address => IP_Address) & " on interface '"
             & String (Iface_Name) & "'");

      Setup_Socket (Iface => Iface_Name);
      Sock_ARP.Send
        (Item  => Anet.ARP.To_Stream (Header => ARP_Message),
         To    => Anet.Bcast_HW_Addr,
         Iface => Anet.Types.Iface_Name_Type (Iface_Name));
      Sock_ARP.Close;

      if Announcement_Count > 1 then
         declare
            use Ada.Real_Time;

            Ev : Timing_Events.Announce.Announce_Type
              := Timing_Events.Announce.Create_Event
                (IP_Address => IP_Address,
                 Count      => Announcement_Count - 1);
            Delay_Time : constant Ada.Real_Time.Time_Span
              := Ada.Real_Time.Seconds (S => Constants.ANNOUNCE_INTERVAL);
         begin
            Ev.Set_Time (At_Time => Ada.Real_Time.Clock + Delay_Time);
            DHCP.Timer.Schedule (Event => Ev);
         end;
      end if;

   exception
      when others =>
         Sock_ARP.Close;
         raise;
   end Announce;

   -------------------------------------------------------------------------

   procedure Initialize (Iface : DHCP.Net.Iface_Type)
   is
   begin
      Iface_Info  := Iface;
      Initialized := True;
   end Initialize;

   -------------------------------------------------------------------------

   procedure Probe
     (IP_Address :     Anet.IPv4_Addr_Type;
      Collision  : out Boolean)
   is
      use type Ada.Streams.Stream_Element_Offset;
      use Ada.Real_Time;

      Iface_Name : constant Anet.Types.Iface_Name_Type
        := Anet.Types.Iface_Name_Type (To_String (Iface_Info.Name));
      Probe_Msg  : constant Anet.ARP.Header_Type
        := Anet.ARP.Header_Type'
          (Operation => Anet.ARP.ARP_Request,
           Src_Ether => Iface_Info.Mac_Addr,
           Src_IP    => Anet.Any_Addr,
           Dst_Ether => Null_HW_Addr,
           Dst_IP    => IP_Address);

      Buffer   : Ada.Streams.Stream_Element_Array (1 .. 512);
      Last_Idx : Ada.Streams.Stream_Element_Offset;
      Interval : Duration;
   begin
      Collision := False;

      if not Initialized then
         return;
      end if;

      Setup_Socket (Iface => Iface_Name);

      L.Log (Message => "Probing IP address "
             & Anet.To_String (Address => IP_Address) & " on interface '"
             & String (Iface_Name) & "'");

      --  Delay random interval 0 .. PROBE_WAIT.

      Interval := Duration (DHCP.Random.Get
                            (Low  => 0.0,
                             High => Float (Constants.PROBE_WAIT)));

      if Collision_Count >= Constants.MAX_CONFLICTS then

         --  Limit rate of probes to no more than one attempt per
         --  RATE_LIMIT_INTERVAL.

         Interval := To_Duration
           (TS => Last_Collision + Seconds (S => Constants.RATE_LIMIT_INTERVAL)
            - Ada.Real_Time.Clock);

         L.Log (Message => "Rate limiting probing due to"
                & Collision_Count'Img & " consecutive collisions");
      end if;

      --  Send PROBE_NUM probe packets.

      for I in Natural range 1 .. Constants.PROBE_NUM loop
         L.Log (Message => "Next ARP probe for address "
                & Anet.To_String (Address => IP_Address)
                & " in" & Interval'Img & " seconds");
         Sleep (Interval => Interval);
         Sock_ARP.Send
           (Item  => Anet.ARP.To_Stream (Header => Probe_Msg),
            To    => Anet.Bcast_HW_Addr,
            Iface => Iface_Name);

         Interval := Duration (DHCP.Random.Get
                               (Low  => Float (Constants.PROBE_MIN),
                                High => Float (Constants.PROBE_MAX)));
      end loop;

      declare
         use type Anet.IPv4_Addr_Type;
         use type Anet.Hardware_Addr_Type;

         ARP_Msg : Anet.ARP.Header_Type;
      begin

         --  Process received ARP messages, if any.

         while not Collision loop
            begin
               Sock_ARP.Receive (Item => Buffer,
                                 Last => Last_Idx);

            exception
               when Anet.Socket_Error =>
                  if GNAT.OS_Lib.Errno = Anet.Constants.Sys.EAGAIN then
                     exit;
                  else
                     raise;
                  end if;
            end;

            exit when Last_Idx = 0;
            if Last_Idx - Buffer'First + 1 >= Anet.ARP.ARP_Header_Length then
               begin
                  ARP_Msg := Anet.ARP.To_Header
                    (Buffer => Buffer
                       (Buffer'First .. Buffer'First +
                            Anet.ARP.ARP_Header_Length - 1));

                  if ARP_Msg.Src_IP = IP_Address then
                     Collision := True;
                     L.Log (Message => "Collision detected - "
                            & Anet.To_String (Address => IP_Address)
                            & " already in use by "
                            & Anet.To_String (Address => ARP_Msg.Src_Ether));
                  end if;

                  if ARP_Msg.Dst_IP = IP_Address
                    and then ARP_Msg.Src_IP = Anet.Any_Addr
                    and then ARP_Msg.Src_Ether /= Iface_Info.Mac_Addr
                  then
                     Collision := True;
                     L.Log (Message => "Collision detected - "
                            & Anet.To_String (Address => IP_Address)
                            & " is being probed by "
                            & Anet.To_String (Address => ARP_Msg.Src_Ether));
                  end if;

               exception
                  when E : Anet.ARP.Invalid_ARP_Packet =>
                     L.Log (Message => "Ignoring ARP packet - "
                            & Ada.Exceptions.Exception_Message (X => E));
               end;
            end if;
         end loop;
      end;

      Sock_ARP.Close;

      if Collision then
         Collision_Count := Collision_Count + 1;
         Last_Collision  := Ada.Real_Time.Clock;
      else
         Collision_Count := 0;
         Last_Collision  := Ada.Real_Time.Time_Last;
      end if;

   exception
      when others =>
         Sock_ARP.Close;
         raise;
   end Probe;

   -------------------------------------------------------------------------

   procedure Setup_Socket (Iface : Anet.Types.Iface_Name_Type)
   is
   begin
      Sock_ARP.Init (Protocol => Anet.Sockets.Packet.Proto_Packet_Arp);
      Sock_ARP.Set_Nonblocking_Mode;
      Sock_ARP.Bind (Iface => Iface);
   end Setup_Socket;

   -------------------------------------------------------------------------

   procedure Sleep (Interval : Duration) is separate;

end DHCPv4.ACD;
