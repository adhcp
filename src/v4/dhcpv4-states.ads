--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.States;

with DHCPv4.Message;

package DHCPv4.States is

   type State_Type (<>) is
     abstract new DHCP.States.Root_State_Type with private;

   type Transaction_Type is
     abstract new DHCP.States.Root_Context_Type with private;

   procedure Set_ID
     (Transaction : in out Transaction_Type;
      XID         :        Transaction_ID_Type)
   is abstract;
   --  Set ID of transaction.

   function Get_ID
     (Transaction : Transaction_Type)
      return Transaction_ID_Type
      is abstract;
   --  Get ID of transaction.

   procedure Start (Transaction : in out Transaction_Type) is abstract;
   --  Start DHCP transaction.

   procedure Prepare_Message
     (Transaction :        Transaction_Type;
      Msg         : in out Message.Message_Type)
   is abstract;
   --  Initialize DHCP message with current transaction parameters.

   procedure Process_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type)
   is abstract;
   --  Process DHCP message.

   procedure Process_Bind_Timeout (Transaction : in out Transaction_Type)
   is abstract;
   --  Process timeout of binding timer.

   procedure Send_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type;
      Dst         :        Anet.IPv4_Addr_Type)
   is abstract;
   --  Send given DHCP message to specified destination IP address.

   function Get_Server_Address
     (Transaction : Transaction_Type)
      return Anet.IPv4_Addr_Type
      is abstract;
   --  Return IP address of current DHCP server. If the transaction has no
   --  associated server Any_Addr_V4 is returned.

   procedure Set_Server_Address
     (Transaction : in out Transaction_Type;
      Address     :        Anet.IPv4_Addr_Type)
   is abstract;
   --  Set IP address of current DHCP server.

   procedure Increase_Retry_Delay (Transaction : in out Transaction_Type)
   is abstract;
   --  Increase retry delay.

   procedure Reset_Retry_Delay (Transaction : in out Transaction_Type)
   is abstract;
   --  Reset the current retry delay to the default initial value.

   procedure Reset (Transaction : in out Transaction_Type)
   is abstract;
   --  Reset transaction data fields to their initial value.

   procedure Start
     (State       : access State_Type;
      Transaction : in out DHCP.States.Root_Context_Type'Class) is null;
   --  Start DHCP transaction.

   procedure Process_Offer
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type) is null;
   --  Process DHCP Offer message.

   procedure Process_Ack
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type) is null;
   --  Process DHCP Acknowledge message.

   procedure Process_Nack
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type) is null;
   --  Process DHCP Not Acknowledge message.

   procedure Process_T1_Expiry
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class) is null;
   --  Process expiration of T1 timer.

   procedure Process_T2_Expiry
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class) is null;
   --  Process expiration of T2 timer.

   procedure Process_Lease_Expiry
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class) is null;
   --  Process expiration of DHCP lease associated with given transaction.

   procedure Process_Bind_Timeout
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class);
   --  Process timeout of binding timer associated with given transaction.

   type Allocation_State_Type is abstract new State_Type with private;
   --  Parent type for all states which allocate a lease (Requesting, Renewing
   --  and Rebinding).

   procedure Process_Ack
     (State       : access Allocation_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type);
   --  Extract DHCP lease information and transition to 'Bound'.

   procedure Process_Nack
     (State       : access Allocation_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type);
   --  Restart lease allocation process and transition to 'Init'.

   procedure Notify_Allocation (State : access Allocation_State_Type)
   is abstract;
   --  Send notification update about lease allocation.

   Invalid_Lease_Time : exception;

private

   type Transaction_Type is
     abstract new DHCP.States.Root_Context_Type with null record;

   type State_Type is
     abstract new DHCP.States.Root_State_Type with null record;

   type Allocation_State_Type is abstract new State_Type with null record;

end DHCPv4.States;
