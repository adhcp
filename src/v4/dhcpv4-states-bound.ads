--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package DHCPv4.States.Bound is

   type Bound_State_Type is new State_Type with private;
   --  A client has a valid lease and is in it's normal operating state.

   overriding
   function Get_Name (State : not null access Bound_State_Type) return String;
   --  Return name of state as string.

   overriding
   procedure Process_T1_Expiry
     (State       : access Bound_State_Type;
      Transaction : in out Transaction_Type'Class);
   --  Transition to 'Renewing'.

   function State return DHCP.States.State_Handle;
   --  Return access to singleton.

private

   type Bound_State_Type is new State_Type with null record;

end DHCPv4.States.Bound;
