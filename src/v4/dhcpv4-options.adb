--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCPv4.Options
is

   Code_To_Name_Map : constant array (Code_Type) of Option_Name
     := (0      => Pad,
         1      => Subnet_Mask,
         2      => Time_Offset,
         3      => Routers,
         4      => Time_Servers,
         5      => Name_Servers,
         6      => Domain_Name_Servers,
         7      => Log_Servers,
         8      => Quotes_Servers,
         9      => LPR_Servers,
         10     => Impress_Servers,
         11     => RLP_Servers,
         12     => Host_Name,
         13     => Boot_File_Size,
         14     => Merit_Dump_File,
         15     => Domain_Name,
         16     => Swap_Server,
         17     => Root_Path,
         18     => Extension_File,
         19     => Forward_On_Off,
         20     => Src_Rte_On_Off,
         21     => Policy_Filter,
         22     => Max_DG_Assembly,
         23     => Default_IP_TTL,
         24     => Timeout_MTU,
         25     => Plateau_MTU,
         26     => Interface_MTU,
         27     => Subnet_MTU,
         28     => Broadcast_Address,
         29     => Mask_Discovery,
         30     => Mask_Supplier,
         31     => Router_Discovery,
         32     => Router_Request,
         33     => Static_Route,
         34     => Trailers,
         35     => ARP_Timeout,
         36     => Ethernet,
         37     => Default_TCP_TTL,
         38     => Keepalive_Time,
         39     => Keepalive_Garbage,
         40     => NIS_Domain,
         41     => NIS_Servers,
         42     => NTP_Servers,
         43     => Vendor_Specific,
         44     => NETBIOS_Name_Servers,
         45     => NETBIOS_Dist_Servers,
         46     => NETBIOS_Node_Type,
         47     => NETBIOS_Scope,
         48     => X_Window_Font_Servers,
         49     => X_Window_Managers,
         50     => Address_Request,
         51     => Address_Time,
         52     => Overload,
         53     => DHCP_Msg_Type,
         54     => DHCP_Server_Id,
         55     => Parameter_List,
         56     => DHCP_Message,
         57     => DHCP_Max_Msg_Size,
         58     => Renewal_Time,
         59     => Rebinding_Time,
         60     => Class_Id,
         61     => Client_Id,
         62     => NetWare_IP_Domain,
         63     => NetWare_IP_Option,
         64     => NIS_Plus_Domain,
         65     => NIS_Plus_Servers,
         66     => TFTP_Server_Name,
         67     => Bootfile_Name,
         68     => Home_Agent_Addrs,
         69     => SMTP_Servers,
         70     => POP3_Servers,
         71     => NNTP_Servers,
         72     => WWW_Servers,
         73     => Finger_Servers,
         74     => IRC_Servers,
         75     => Street_Talk_Servers,
         76     => STDA_Servers,
         77     => User_Class,
         78     => Directory_Agent,
         79     => Service_Scope,
         80     => Rapid_Commit,
         81     => Client_FQDN,
         82     => Relay_Agent_Information,
         83     => ISNS,
         85     => NDS_Servers,
         86     => NDS_Tree_Name,
         87     => NDS_Context,
         88     => BCMCS_Controller_Names,
         89     => BCMCS_Controller_Address,
         90     => Authentication,
         91     => Client_Last_Transaction_Time,
         92     => Associated_IP,
         93     => Client_System,
         94     => Client_NDI,
         95     => LDAP,
         97     => UUID_GUID,
         98     => User_Auth,
         99     => Geo_Conf_Civic,
         100    => P_Code,
         101    => T_Code,
         112    => Netinfo_Address,
         113    => Netinfo_Tag,
         114    => URL,
         116    => Auto_Config,
         117    => Name_Service_Search,
         118    => Subnet_Selection,
         119    => Domain_Search,
         120    => SIP_Servers,
         121    => Classless_Static_Route,
         122    => CCC,
         123    => Geo_Conf,
         124    => VI_VCO,
         125    => VI_VSO,
         128    => PXE_Undefined_1,
         129    => PXE_Undefined_2,
         130    => PXE_Undefined_3,
         131    => PXE_Undefined_4,
         132    => PXE_Undefined_5,
         133    => PXE_Undefined_6,
         134    => PXE_Undefined_7,
         135    => PXE_Undefined_8,
         136    => Pana_Agent,
         137    => V4_Lost,
         138    => Capwap_AC_V4,
         139    => IPv4_Address_MoS,
         140    => IPv4_FQDN_MoS,
         141    => SIP_UA_Conf_Domains,
         142    => IPv4_Address_ANDSF,
         143    => IPv6_Address_ANDSF,
         150    => TFTP_Server,
         175    => Etherboot_1,
         176    => IP_Telephone,
         177    => Etherboot_2,
         208    => PXE_Linux_Magic,
         209    => Configuration_File,
         210    => Path_Prefix,
         211    => Reboot_Time,
         212    => IPv6_RD,
         213    => V4_Access_Domain,
         220    => Subnet_Allocation,
         221    => Virtual_Subnet_Select,
         255    => End_Marker,
         others => Site_Specific);

   -------------------------------------------------------------------------

   function Code_To_Name (Code : Code_Type) return Option_Name
   is
   begin
      return Code_To_Name_Map (Code);
   end Code_To_Name;

   -------------------------------------------------------------------------

   function Code_To_Tag (Code : Code_Type) return Ada.Tags.Tag
   is
      Tag_Map : constant array (Option_Name) of Ada.Tags.Tag
        := (Subnet_Mask           => Inst4.IP_Option_Type'Tag,
            Time_Offset           => Inst.U_Int32_Option_Type'Tag,
            Routers               => Inst.IP_Array_Option_Type'Tag,
            Time_Servers          => Inst.IP_Array_Option_Type'Tag,
            Name_Servers          => Inst.IP_Array_Option_Type'Tag,
            Domain_Name_Servers   => Inst.IP_Array_Option_Type'Tag,
            Log_Servers           => Inst.IP_Array_Option_Type'Tag,
            Quotes_Servers        => Inst.IP_Array_Option_Type'Tag,
            LPR_Servers           => Inst.IP_Array_Option_Type'Tag,
            Impress_Servers       => Inst.IP_Array_Option_Type'Tag,
            RLP_Servers           => Inst.IP_Array_Option_Type'Tag,
            Host_Name             => Inst4.String_Option_Type'Tag,
            Boot_File_Size        => Inst.U_Int16_Option_Type'Tag,
            Merit_Dump_File       => Inst4.String_Option_Type'Tag,
            Domain_Name           => Inst4.String_Option_Type'Tag,
            Swap_Server           => Inst4.IP_Option_Type'Tag,
            Root_Path             => Inst4.String_Option_Type'Tag,
            Extension_File        => Inst4.String_Option_Type'Tag,
            Forward_On_Off        => Inst4.Bool_Option_Type'Tag,
            Src_Rte_On_Off        => Inst4.Bool_Option_Type'Tag,
            Policy_Filter         => Inst4.IP_Pairs_Option_Type'Tag,
            Max_DG_Assembly       => Inst.U_Int16_Option_Type'Tag,
            Default_IP_TTL        => Inst4.Positive_Octet_Option_Type'Tag,
            Timeout_MTU           => Inst.U_Int32_Option_Type'Tag,
            Plateau_MTU           => Inst.U_Int16_Array_Option_Type'Tag,
            Interface_MTU         => Inst4.Iface_MTU_Option_Type'Tag,
            Subnet_MTU            => Inst4.Bool_Option_Type'Tag,
            Broadcast_Address     => Inst4.IP_Option_Type'Tag,
            Mask_Discovery        => Inst4.Bool_Option_Type'Tag,
            Mask_Supplier         => Inst4.Bool_Option_Type'Tag,
            Router_Discovery      => Inst4.Bool_Option_Type'Tag,
            Router_Request        => Inst4.IP_Option_Type'Tag,
            Static_Route          => Inst4.IP_Pairs_Option_Type'Tag,
            Trailers              => Inst4.Bool_Option_Type'Tag,
            ARP_Timeout           => Inst.U_Int32_Option_Type'Tag,
            Ethernet              => Inst4.Bool_Option_Type'Tag,
            Default_TCP_TTL       => Inst4.Positive_Octet_Option_Type'Tag,
            Keepalive_Time        => Inst.U_Int32_Option_Type'Tag,
            Keepalive_Garbage     => Inst4.Bool_Option_Type'Tag,
            NIS_Domain            => Inst4.String_Option_Type'Tag,
            NIS_Servers           => Inst.IP_Array_Option_Type'Tag,
            NTP_Servers           => Inst.IP_Array_Option_Type'Tag,
            NETBIOS_Name_Servers  => Inst.IP_Array_Option_Type'Tag,
            NETBIOS_Dist_Servers  => Inst.IP_Array_Option_Type'Tag,
            NETBIOS_Node_Type     => Inst4.Positive_Octet_Option_Type'Tag,
            NETBIOS_Scope         => Inst4.String_Option_Type'Tag,
            X_Window_Font_Servers => Inst.IP_Array_Option_Type'Tag,
            X_Window_Managers     => Inst.IP_Array_Option_Type'Tag,
            Address_Request       => Inst4.IP_Option_Type'Tag,
            Address_Time          => Inst.U_Int32_Option_Type'Tag,
            Overload              => Inst4.Positive_Octet_Option_Type'Tag,
            DHCP_Msg_Type         => Inst4.Msg_Kind_Option_Type'Tag,
            DHCP_Server_Id        => Inst4.IP_Option_Type'Tag,
            DHCP_Message          => Inst4.String_Option_Type'Tag,
            DHCP_Max_Msg_Size     => Inst.U_Int16_Option_Type'Tag,
            Renewal_Time          => Inst.U_Int32_Option_Type'Tag,
            Rebinding_Time        => Inst.U_Int32_Option_Type'Tag,
            Class_Id              => Inst4.String_Option_Type'Tag,
            NIS_Plus_Domain       => Inst4.String_Option_Type'Tag,
            NIS_Plus_Servers      => Inst.IP_Array_Option_Type'Tag,
            TFTP_Server_Name      => Inst4.String_Option_Type'Tag,
            Bootfile_Name         => Inst4.String_Option_Type'Tag,
            SMTP_Servers          => Inst.IP_Array_Option_Type'Tag,
            POP3_Servers          => Inst.IP_Array_Option_Type'Tag,
            NNTP_Servers          => Inst.IP_Array_Option_Type'Tag,
            WWW_Servers           => Inst.IP_Array_Option_Type'Tag,
            Finger_Servers        => Inst.IP_Array_Option_Type'Tag,
            IRC_Servers           => Inst.IP_Array_Option_Type'Tag,
            Street_Talk_Servers   => Inst.IP_Array_Option_Type'Tag,
            STDA_Servers          => Inst.IP_Array_Option_Type'Tag,
            others                => Inst.Raw_Option_Type'Tag);
   begin
      return Tag_Map (Code_To_Name_Map (Code));
   end Code_To_Tag;

   -------------------------------------------------------------------------

   function Name_To_Code (Name : Option_Name) return Code_Type
   is
      Res : Code_Type := Code_Type'Last;
   begin
      for C in Code_To_Name_Map'Range loop
         if Code_To_Name_Map (C) = Name then
            Res := C;
         end if;
      end loop;

      return Res;
   end Name_To_Code;

end DHCPv4.Options;
