--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet.Types;

package DHCPv4.Relay is

   procedure Run
     (Server_IP    : Anet.IPv4_Addr_Type;
      Client_Iface : Anet.Types.Iface_Name_Type);
   --  Start the DHCP relaying service on given client interface. DHCP messages
   --  are relayed to the specified DHCP server.

   procedure Stop;
   --  Stop the DHCP relay service.

   Config_Error : exception;
   --  Raised if invalid configuration is provided.

end DHCPv4.Relay;
