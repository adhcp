--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet;

package DHCPv4 is

   type Opcode_Type is
     (Boot_Request,
      Boot_Reply);
   --  BOOTP message codes.

   type Hops_Type is range 0 .. 16;
   --  Hop count, see RFC 1542, 4.1.1 and RFC 2131, 2.

   type Hardware_Type is
     (Ethernet,
      IEEE_802,
      ARCNet,
      LocalTalk,
      LocalNet,
      SMDS,
      Frame_Relay,
      A_T_M,
      HDLC,
      Fiber_Channel,
      ATM,
      Serial_Line);
   --  Link-layer hardware types.

   subtype Ether_Addr_Type is Anet.Hardware_Addr_Type (1 .. 6);
   --  Ethernet address.

   subtype Transaction_ID_Type is Anet.Word32;
   --  Transaction identifier.

   subtype Seconds_Type is Anet.Double_Byte;
   --  DHCP seconds type.

   Bootp_Server_Port : constant Anet.Port_Type;
   --  BOOTP server port (67).

   Bootp_Client_Port : constant Anet.Port_Type;
   --  BOOTP client port (68).

   subtype Server_Name_Type is Anet.Byte_Array (1 .. 64);

   subtype File_Name_Type is Anet.Byte_Array (1 .. 128);

   function To_String (XID : Transaction_ID_Type) return String;
   --  Convert given transaction ID to string.

   function To_Server_Name (Str : String) return Server_Name_Type;
   --  Convert given string to server name.

   function To_File_Name (Str : String) return File_Name_Type;
   --  Convert given string to file name.

   Conversion_Error : exception;

private

   for Hops_Type'Size use 8;

   for Opcode_Type use
     (Boot_Request => 1,
      Boot_Reply   => 2);
   for Opcode_Type'Size use 8;

   for Hardware_Type use
     (Ethernet      => 1,
      IEEE_802      => 6,
      ARCNet        => 7,
      LocalTalk     => 11,
      LocalNet      => 12,
      SMDS          => 14,
      Frame_Relay   => 15,
      A_T_M         => 16,
      HDLC          => 17,
      Fiber_Channel => 18,
      ATM           => 19,
      Serial_Line   => 20);
   for Hardware_Type'Size use 8;

   Bootp_Server_Port : constant Anet.Port_Type := 67;
   Bootp_Client_Port : constant Anet.Port_Type := 68;

end DHCPv4;
