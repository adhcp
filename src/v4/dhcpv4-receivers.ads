--
--  Copyright (C) 2012 secunet Security Networks AG
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet.Sockets.Inet;
with Anet.Sockets.Packet;
with Anet.Receivers.Datagram;

pragma Elaborate_All (Anet.Receivers.Datagram);

package DHCPv4.Receivers is

   package Packet is new Anet.Receivers.Datagram
     (Socket_Type  => Anet.Sockets.Packet.UDP_Socket_Type,
      Address_Type => Ether_Addr_Type,
      Receive      => Anet.Sockets.Packet.Receive);

   package UDPv4 is new Anet.Receivers.Datagram
     (Socket_Type  => Anet.Sockets.Inet.UDPv4_Socket_Type,
      Address_Type => Anet.Sockets.Inet.IPv4_Sockaddr_Type,
      Receive      => Anet.Sockets.Inet.Receive);

end DHCPv4.Receivers;
