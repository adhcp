--
--  Copyright (C) 2018 secunet Security Networks AG
--  Copyright (C) 2018 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2018 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet;

with DHCP.Timing_Events;

package DHCPv4.Timing_Events.Announce
is

   type Announce_Type is new DHCP.Timing_Events.Event_Type with private;

   overriding
   procedure Trigger (Event : in out Announce_Type);
   --  Send ARP announcement.

   function Create_Event
     (IP_Address : Anet.IPv4_Addr_Type;
      Count      : Positive)
      return Announce_Type;
   --  Set IP address of announcement.

private

   type Announce_Type is new DHCP.Timing_Events.Event_Type with record
      IP_Address : Anet.IPv4_Addr_Type;
      Count      : Positive;
   end record;

end DHCPv4.Timing_Events.Announce;
