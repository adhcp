   --
--  Copyright (C) 2018 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2018 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package DHCPv4.Constants
is

   --  RFC 2131, section 3.1.
   RESTART_DELAY       : constant := 10;

   --  Constants as specified by RFC 5227, section 1.1.
   PROBE_WAIT          : constant := 1;
   PROBE_NUM           : constant := 3;
   PROBE_MIN           : constant := 1;
   PROBE_MAX           : constant := 2;
   ANNOUNCE_NUM        : constant := 2;
   ANNOUNCE_INTERVAL   : constant := 2;
   MAX_CONFLICTS       : constant := 10;
   RATE_LIMIT_INTERVAL : constant := 60;

end DHCPv4.Constants;
