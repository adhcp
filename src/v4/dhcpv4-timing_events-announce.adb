--
--  Copyright (C) 2018 secunet Security Networks AG
--  Copyright (C) 2018 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2018 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCPv4.ACD;

package body DHCPv4.Timing_Events.Announce
is

   -------------------------------------------------------------------------

   function Create_Event
     (IP_Address : Anet.IPv4_Addr_Type;
      Count      : Positive)
      return Announce_Type
   is
   begin
      return Ev : Announce_Type do
         Ev.IP_Address := IP_Address;
         Ev.Count      := Count;
      end return;
   end Create_Event;

   -------------------------------------------------------------------------

   procedure Trigger (Event : in out Announce_Type)
   is
   begin
      ACD.Announce (IP_Address         => Event.IP_Address,
                    Announcement_Count => Event.Count);
   end Trigger;

end DHCPv4.Timing_Events.Announce;
