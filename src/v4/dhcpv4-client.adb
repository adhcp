--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;
with Ada.Exceptions;

with Anet.UDP;
with Anet.IPv4;
with Anet.Sockets.Net_Ifaces;

with DHCP.Net;
with DHCP.Timer;
with DHCP.Logger;
with DHCP.Notify;
with DHCP.Observers;
with DHCP.Timing_Events.Start;

with DHCPv4.ACD;
with DHCPv4.Message;
with DHCPv4.Transactions;
with DHCPv4.Timing_Events.Handle_Msg;
with DHCPv4.Transmission;
with DHCPv4.Database.IO;

pragma Elaborate_All (DHCPv4.Transactions);

package body DHCPv4.Client is

   package L renames DHCP.Logger;

   Trans : aliased Transactions.Transaction_Type;
   --  Current DHCP transaction.

   procedure Process_Server_Reply
     (Buffer : Ada.Streams.Stream_Element_Array;
      Src    : Ether_Addr_Type);
   --  Process replies from DHCP server.

   procedure Register_Observers (One_Shot_Mode : Boolean);
   --  Register mode-specific observer callbacks for notifications.

   procedure Call_External_Notify is new DHCP.Observers.Call_External_Notify
     (Get_Interface_Name => Transmission.Get_Iface_Name,
      Write_Database     => Database.IO.Write);

   -------------------------------------------------------------------------

   procedure Process_Server_Reply
     (Buffer : Ada.Streams.Stream_Element_Array;
      Src    : Ether_Addr_Type)
   is
      pragma Unreferenced (Src);

      use type Anet.Word32;

      Msg : Message.Message_Type;
   begin
      Msg := Message.Deserialize
        (Buffer => Anet.IPv4.Validate_And_Strip (Packet => Buffer));

      if Msg.Get_Transaction_ID /= Transactions.Get_ID (Transaction => Trans)
        or else Msg.Get_Opcode = Boot_Request
      then

         --  Silently discard non-matching XID's and requests.

         return;
      end if;

      L.Log (Message => "Received " & Msg.Get_Kind'Img
             & " message from server "
             & Anet.To_String (Address => Msg.Get_Server_ID));
      declare
         Ev : Timing_Events.Handle_Msg.Handle_Msg_Type (T => Trans'Access);
      begin
         Ev.Set_Message (Msg => Msg);
         DHCP.Timer.Schedule (Event => Ev);
      end;

   exception
      when E : Anet.IPv4.Invalid_IP_Packet | Anet.UDP.Invalid_UDP_Packet =>
         L.Log (Level   => L.Info,
                Message => "Ignoring packet ("
                & Ada.Exceptions.Exception_Message (X => E) & ")");

      when E : Message.Invalid_Message =>

         --  Discard invalid messages, see RFC 1542; section 2.1.

         L.Log
           (Level   => L.Info,
            Message => "Discarding message (XID" & Msg.Get_Transaction_ID'Img
            & "): " & Ada.Exceptions.Exception_Message (X => E));
   end Process_Server_Reply;

   -------------------------------------------------------------------------

   procedure Register_Observers (One_Shot_Mode : Boolean)
   is
      use DHCP.Notify;
   begin
      if One_Shot_Mode then
         L.Log (Message => "Running in one-shot mode");

         Register (Reasons => (Bound, Timeout),
                   Handler => DHCP.Observers.One_Shot_Mode'Access);
      end if;

      Register (Reasons => (Bound, Preinit, Rebind, Renew),
                Handler => Call_External_Notify'Access);
   end Register_Observers;

   -------------------------------------------------------------------------

   procedure Run
     (Client_Iface  : Anet.Types.Iface_Name_Type;
      One_Shot_Mode : Boolean;
      ACD_Enable    : Boolean)
   is
      Cli_Iface : DHCP.Net.Iface_Type (Addrtype => DHCP.Net.Link_Layer);
   begin
      DHCP.Net.Discover_Ifaces;

      Cli_Iface := DHCP.Net.Get_Iface
        (Name     => Client_Iface,
         Addrtype => DHCP.Net.Link_Layer);

      L.Log (Message => "Configuring interface " & String (Client_Iface));

      if not Anet.Sockets.Net_Ifaces.Is_Iface_Up (Name => Client_Iface) then
         L.Log (Message => "Bringing up interface "
                & String (Client_Iface));
         Anet.Sockets.Net_Ifaces.Set_Iface_State
           (Name  => Client_Iface,
            State => True);
      end if;

      DHCP.Timer.Start;
      Register_Observers (One_Shot_Mode);
      Transmission.Initialize (Client_Iface => Cli_Iface,
                               Listen_Cb    => Process_Server_Reply'Access);

      if ACD_Enable then
         ACD.Initialize (Iface => Cli_Iface);
      end if;

      --  Disable exponential backoff retry if running in one-shot mode.

      if One_Shot_Mode then
         Transactions.Set_Exp_Backoff (Transaction => Trans,
                                       New_State   => False);
      end if;

      L.Log (Message => "Initiating DHCP lease allocation on interface "
             & String (Client_Iface));
      declare
         Ev : DHCP.Timing_Events.Start.Start_Type (T => Trans'Access);
      begin
         DHCP.Timer.Schedule (Event => Ev);
      end;
   end Run;

   -------------------------------------------------------------------------

   procedure Stop
   is
   begin
      Transmission.Stop;
      DHCP.Timer.Stop;
      DHCP.Logger.Stop;
   end Stop;

end DHCPv4.Client;
