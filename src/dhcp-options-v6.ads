--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet;

with DHCP.Types;

generic
package DHCP.Options.V6
is

   -----------------
   -- Option list --
   -----------------

   type Option_List is new DHCP.Options.Option_List with private;
   --  List of DHCP options.

   overriding
   function Serialize
     (List : Option_List)
      return Ada.Streams.Stream_Element_Array;
   --  Serialize given DHCPv6 option list to stream element array.

   overriding
   function Deserialize
     (Buffer : Ada.Streams.Stream_Element_Array)
      return Option_List;
   --  Create a DHCPv6 option list from given stream element array.

   type Option_Code_Array is array (Positive range <>) of Code_Type;

   function Get_Codes (List : Option_List) return Option_Code_Array;
   --  Return all option codes stored in given list.

   Empty_List : constant Option_List;

   ---------------------
   -- IP array option --
   ---------------------

   type IP_Array_Option_Type is new
     DHCP.Options.IP_Array_Option_Type with private;
   --  Option type containing an array of IPv6 addresses.

   overriding
   function Get_Addrlen (Option : IP_Array_Option_Type) return Positive;
   --  Return length of stored address.

   overriding
   function To_IP_String
     (Option : IP_Array_Option_Type;
      Octets : Ada.Streams.Stream_Element_Array)
      return String
   with
      Pre => Octets'Length = 16;
   --  Return string representation of IP given as stream.

   ------------------------
   -- Status code option --
   ------------------------

   type Status_Code_Type is
     (Status_Success,
      Status_Unspec_Fail,
      Status_No_Addrs_Avail,
      Status_No_Binding,
      Status_Not_On_Link,
      Status_Use_Multicast);
   --  Valid status codes.

   type Status_Code_Option_Type is new Raw_Option_Type with private;
   --  Option type containing a status code and an (optional) message.

   overriding
   procedure Validate (Option : Status_Code_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   function Get_Status
     (Option : Status_Code_Option_Type)
      return Status_Code_Type;
   --  Return status code.

   function Get_Status_Message
     (Option : Status_Code_Option_Type)
      return String;
   --  Return status message.

   -----------------------
   -- IA address option --
   -----------------------

   type IA_Address_Option_Type is new Raw_Option_Type with private;
   --  IA address option type.

   overriding
   procedure Validate (Option : IA_Address_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   function Get_Address
     (Option : IA_Address_Option_Type)
      return Anet.IPv6_Addr_Type;
   --  Return IPv6 address.

   procedure Get_Lifetimes
     (Option    :     IA_Address_Option_Type;
      Preferred : out Duration;
      Valid     : out Duration);
   --  Return preferred and valid lifetimes of IA address.

   function Get_Options
     (Option : IA_Address_Option_Type)
      return V6.Option_List'Class;
   --  Return embedded option list.

   ------------------
   -- IA_NA option --
   ------------------

   type IA_NA_Option_Type is new Raw_Option_Type with private;
   --  Identity Association for Non-temporary Addresses Option.

   overriding
   procedure Validate (Option : IA_NA_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   function Get_IAID (Option : IA_NA_Option_Type) return Types.IAID_Type;
   --  Return IAID of given IA_NA option.

   procedure Get_Ts
     (Option :     IA_NA_Option_Type;
      T1     : out Duration;
      T2     : out Duration);
   --  Returns the T1 and T2 duration of the given IA_NA option.

   function Get_Options
     (Option : IA_NA_Option_Type)
      return V6.Option_List'Class;
   --  Return embedded option list.

   Default_Request_Option : constant Raw_Option_Type;
   --  Default request option (6).
   --  Requests the following options from the server:
   --  - DNS recursive name servers

   Inf_Default_Request_Option : constant Raw_Option_Type;
   --  Default request option (6).
   --  Requests the following options from the server:
   --  - DNS recursive name servers
   --  - Information refresh time

private

   for Status_Code_Type use
     (Status_Success        => 0,
      Status_Unspec_Fail    => 1,
      Status_No_Addrs_Avail => 2,
      Status_No_Binding     => 3,
      Status_Not_On_Link    => 4,
      Status_Use_Multicast  => 5);

   type IP_Array_Option_Type is new
     DHCP.Options.IP_Array_Option_Type with null record;

   type Status_Code_Option_Type is new Raw_Option_Type with null record;

   type IA_Address_Option_Type is new Raw_Option_Type with null record;

   type IA_NA_Option_Type is new Raw_Option_Type with null record;

   type Option_List is new DHCP.Options.Option_List with null record;

   Empty_List : constant Option_List := (others => <>);

   Default_Request_Option : constant Raw_Option_Type
     := (Size => 2,
         Code => 6,
         Data => (16#00#, 16#17#));

   Inf_Default_Request_Option : constant Raw_Option_Type
     := (Size => 4,
         Code => 6,
         Data => (16#00#, 16#17#, 16#00#, 16#20#));

end DHCP.Options.V6;
