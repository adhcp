--
--  Copyright (C) 2011-2015 secunet Security Networks AG
--  Copyright (C) 2011-2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011-2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Interfaces.C;

with Ada.Exceptions;
with Ada.Strings.Unbounded;
with Ada.Text_IO;

package body DHCP.OS is

   -------------------------------------------------------------------------

   function Get_Pgid return Positive
   is
      function C_Getpgrp return Interfaces.C.int
      with
         Import,
         Convention => C,
         Link_Name  => "getpgrp";
   begin
      return Positive (C_Getpgrp);
   end Get_Pgid;

   -------------------------------------------------------------------------

   function Get_Pid return Positive
   is
      function C_Getpid return Interfaces.C.int
      with
         Import,
         Convention => C,
         Link_Name => "getpid";
   begin
      return Positive (C_Getpid);
   end Get_Pid;

   -------------------------------------------------------------------------

   function Read_File (Filename : String) return String
   is
      File : Ada.Text_IO.File_Type;
   begin
      begin
         Ada.Text_IO.Open
           (File => File,
            Mode => Ada.Text_IO.In_File,
            Name => Filename,
            Form => "shared=no");

      exception
         when E : others =>
            raise IO_Error with "Error opening file - "
              & Ada.Exceptions.Exception_Message (E);
      end;

      declare
         use Ada.Strings.Unbounded;

         Data : Unbounded_String;
      begin
         Read_Data :
         loop
            Data := Data & Ada.Text_IO.Get_Line (File => File);

            exit Read_Data when Ada.Text_IO.End_Of_File (File => File);

            Data := Data & ASCII.LF;
         end loop Read_Data;

         Ada.Text_IO.Close (File);
         return To_String (Source => Data);

      exception
         when others =>
            Ada.Text_IO.Close (File);
            raise IO_Error with "Unable to read data from file '"
              & Filename & "'";
      end;
   end Read_File;

end DHCP.OS;
