--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;

with DHCP.Logger;
with DHCP.Utils;

package body DHCP.Options.V6
is

   pragma Assert (Code_Type'Size = 16);
   pragma Assert (Len_Byte_Count = 2);
   --  DHCPv6 code and option-len fields are 2 bytes.

   use Ada.Streams;

   -------------------------------------------------------------------------

   function Deserialize
     (Buffer : Ada.Streams.Stream_Element_Array)
      return Option_List
   is
      Current_Idx : Stream_Element_Offset := Buffer'First;
      List        : Option_List;
   begin

      --  Allow empty list

      if Buffer'Length = 0 then
         return List;
      end if;

      loop
         declare
            Optlen : constant Stream_Element_Offset
              := Stream_Element_Offset
                (Utils.To_Value
                   (A => (Buffer
                          (Current_Idx + Bc .. Current_Idx + Bc + 1))));
         begin
            begin
               List.Data.Append
                 (New_Item => Deserialize
                    (Buffer => Buffer
                         (Current_Idx .. Current_Idx + 2 * Bc + Optlen - 1)));

            exception
               when E : Invalid_Option =>
                  Logger.Log (Level   => Logger.Warning,
                              Message => "Skipping invalid option - "
                              & Ada.Exceptions.Exception_Message (X => E));
            end;

            --  Move current index to the beginning of the next option.

            Current_Idx := Current_Idx + 2 * Bc + Optlen;
         end;

         exit when Current_Idx >= Buffer'Last;
      end loop;

      return List;

   exception
      when Constraint_Error =>
         raise Invalid_Option with "Could not parse malformed options";
   end Deserialize;

   -------------------------------------------------------------------------

   function Get_Address
     (Option : IA_Address_Option_Type)
      return Anet.IPv6_Addr_Type
   is
      Addr : Anet.IPv6_Addr_Type;
   begin
      for I in Option.Data'First .. Option.Data'First + 15 loop
         Addr (Positive (I)) := Anet.Byte (Option.Data (I));
      end loop;

      return Addr;
   end Get_Address;

   -------------------------------------------------------------------------

   function Get_Addrlen (Option : IP_Array_Option_Type) return Positive
   is (16);

   -------------------------------------------------------------------------

   function Get_Codes (List : Option_List) return Option_Code_Array
   is
      Res : Option_Code_Array (1 .. List.Get_Count);
      Idx : Positive := Res'First;

      procedure Add_Code (Opt : Option_Type'Class);
      --  Add code of given option to result.

      procedure Add_Code (Opt : Option_Type'Class)
      is
      begin
         Res (Idx) := Opt.Get_Code;
         Idx := Idx + 1;
      end Add_Code;
   begin
      List.Iterate (Process => Add_Code'Access);
      return Res;
   end Get_Codes;

   -------------------------------------------------------------------------

   function Get_IAID (Option : IA_NA_Option_Type) return Types.IAID_Type
   is
   begin
      return Types.IAID_Type
        (Utils.To_Value
           (A => Option.Data
                (Option.Data'First .. Option.Data'First + 3)));
   end Get_IAID;

   -------------------------------------------------------------------------

   procedure Get_Lifetimes
     (Option    :     IA_Address_Option_Type;
      Preferred : out Duration;
      Valid     : out Duration)
   is
      F  : constant Stream_Element_Offset := Option.Data'First;
      PO : constant := 16;
      VO : constant := 16 + 4;
   begin
      Preferred := Duration
        (Utils.To_Value (A => Option.Data (F + PO .. F + PO + 3)));
      Valid := Duration
        (Utils.To_Value (A => Option.Data (F + VO .. F + VO + 3)));
   end Get_Lifetimes;

   -------------------------------------------------------------------------

   function Get_Options
     (Option : IA_Address_Option_Type)
      return V6.Option_List'Class
   is
      L  : Option_List;
      F  : constant Stream_Element_Offset := Option.Data'First;
      OO : constant := 24;
   begin
      if Option.Data'Length > 24 then
         L := Deserialize (Buffer => Option.Data (F + OO .. Option.Data'Last));
      end if;

      return L;
   end Get_Options;

   -------------------------------------------------------------------------

   function Get_Options
     (Option : IA_NA_Option_Type)
      return V6.Option_List'Class
   is
      L  : Option_List;
      F  : constant Stream_Element_Offset := Option.Data'First;
      OO : constant := 12;
   begin
      if Option.Data'Length > 12 then
         L := Deserialize (Buffer => Option.Data (F + OO .. Option.Data'Last));
      end if;

      return L;
   end Get_Options;

   -------------------------------------------------------------------------

   function Get_Status
     (Option : Status_Code_Option_Type)
      return Status_Code_Type
   is
   begin
      return Status_Code_Type'Val
        (Utils.To_Value
           (A => Option.Data
                (Option.Data'First .. Option.Data'First + 1)));
   end Get_Status;

   -------------------------------------------------------------------------

   function Get_Status_Message
     (Option : Status_Code_Option_Type)
      return String
   is
   begin
      if Option.Size = 2 then
         return "";
      end if;

      return Utils.To_String
        (A => Option.Data
           (Option.Data'First + 2 .. Option.Data'Last));
   end Get_Status_Message;

   -------------------------------------------------------------------------

   procedure Get_Ts
     (Option :     IA_NA_Option_Type;
      T1     : out Duration;
      T2     : out Duration)
   is
      use DHCP.Utils;

      F   : constant Stream_Element_Offset := Option.Data'First;
      T1O : constant := 4;
      T2O : constant := 4 + 4;
   begin
      T1 := Duration (To_Value (A => Option.Data (F + T1O .. F + T1O + 3)));
      T2 := Duration (To_Value (A => Option.Data (F + T2O .. F + T2O + 3)));
   end Get_Ts;

   -------------------------------------------------------------------------

   function Serialize
     (List : Option_List)
      return Ada.Streams.Stream_Element_Array
   is
      Upper  : constant Stream_Element_Offset := Stream_Element_Offset
        (List.Get_Data_Size) + Stream_Element_Offset (List.Get_Count) * 2 * Bc;
      Buffer : Stream_Element_Array (1 .. Upper);
      Idx    : Stream_Element_Offset := Buffer'First;

      procedure Serialize (Option : DHCP.Options.Option_Type'Class);
      --  Serialize an option to the buffer.

      procedure Serialize (Option : DHCP.Options.Option_Type'Class)
      is
         Size : constant Size_Type := Option.Get_Size;
      begin
         Buffer (Idx .. Idx + Size + 2 * Bc - 1) := Option.Serialize;
         Idx := Idx + Size + 2 * Bc;
      end Serialize;
   begin
      List.Iterate (Process => Serialize'Access);
      return Buffer;
   end Serialize;

   -------------------------------------------------------------------------

   function To_IP_String
     (Option : IP_Array_Option_Type;
      Octets : Ada.Streams.Stream_Element_Array)
      return String
   is
      pragma Unreferenced (Option);

      Addr : Anet.IPv6_Addr_Type;
      Idx  : Ada.Streams.Stream_Element_Offset := Octets'First;
   begin
      for B of Addr loop
         B   := Anet.Byte (Octets (Idx));
         Idx := Idx + 1;
      end loop;

      return Anet.To_String (Address => Addr);
   end To_IP_String;

   -------------------------------------------------------------------------

   procedure Validate (Option : Status_Code_Option_Type)
   is
   begin
      if Option.Data'Length < 2 then
         raise Invalid_Option with To_Name (Code => Option.Code)'Img
           & " option: Invalid format";
      end if;

      declare
         Status_A : constant Code_Type := Code_Type
           (Utils.To_Value
              (A => Option.Data
                   (Option.Data'First .. Option.Data'First + 1)));
         Status   : Status_Code_Type;
         pragma Unreferenced (Status);
      begin
         Status := Status_Code_Type'Val (Status_A);

      exception
         when Constraint_Error =>
            raise Invalid_Option with To_Name (Code => Option.Code)'Img
              & " option: Invalid status code" & Status_A'Img;
      end;
   end Validate;

   -------------------------------------------------------------------------

   procedure Validate (Option : IA_Address_Option_Type)
   is
      Pref, Valid : Duration;
   begin
      if Option.Data'Length < 24 then
         raise Invalid_Option with To_Name (Code => Option.Code)'Img
           & " option: Data payload too short";
      end if;

      Option.Get_Lifetimes (Preferred => Pref,
                            Valid     => Valid);

      if Pref > Valid then
         raise Invalid_Option with To_Name (Code => Option.Code)'Img
           & " option: Preferred lifetime of" & Pref'Img
           & " exceeds valid lifetime of" & Valid'Img;
      end if;

      declare
         Opts  : constant Option_List'Class := Option.Get_Options;
         Codes : constant Option_Code_Array := Opts.Get_Codes;
      begin
         for C of Codes loop

            --  Only status code (13) options are allowed.

            if C /= 13 then
               raise Invalid_Option with To_Name (Code => Option.Code)'Img
                 & " option: contains invalid option" & C'Img;
            end if;
         end loop;
      end;
   end Validate;

   -------------------------------------------------------------------------

   procedure Validate (Option : IA_NA_Option_Type)
   is
      T1, T2 : Duration;
   begin
      if Option.Data'Length < 12 then
         raise Invalid_Option with To_Name (Code => Option.Code)'Img
           & " option: Data payload too short";
      end if;

      Option.Get_Ts (T1 => T1,
                     T2 => T2);

      if T1 > 0.0 and then T2 > 0.0 and then T1 > T2 then
         raise Invalid_Option with To_Name (Code => Option.Code)'Img
           & " option: T1" & T1'Img & " > T2" & T2'Img;
      end if;

      declare
         Opts  : constant Option_List'Class := Option.Get_Options;
         Codes : constant Option_Code_Array := Opts.Get_Codes;
      begin
         for C of Codes loop

            --  Only IA address (5) and status code (13) options are allowed.

            if C /= 5 and then C /= 13 then
               raise Invalid_Option with To_Name (Code => Option.Code)'Img
                 & " option: contains invalid option" & C'Img;
            end if;
         end loop;
      end;
   end Validate;

end DHCP.Options.V6;
