--
--  Copyright (C) 2011, 2015 secunet Security Networks AG
--  Copyright (C) 2011, 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package DHCP.Notify
is

   type Reason_Type is
     (Bound,
      Preinit,
      Rebind,
      Release,
      Renew,
      Timeout);
   --  Notification reason.

   type Reason_Array is array (Positive range <>) of Reason_Type;
   --  Array of notification reasons.

   type Notfication_Handler is not null access procedure (R : Reason_Type);
   --  Notification handler.

   procedure Register
     (Reasons : Reason_Array;
      Handler : Notfication_Handler);
   --  Register handler for specified reason. The handler will be executed when
   --  a notification update matching the reason occurs.

   function Handler_Count (Reason : Reason_Type) return Natural;
   --  Return number of notification handlers registered for the specified
   --  reason.

   procedure Clear;
   --  Clear all registered notification handlers.

   procedure Update (Reason : Reason_Type);
   --  Inform observers about notification event for given reason.

end DHCP.Notify;
