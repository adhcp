--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;

package DHCP.Marshaling is

   type Serializable_Object is interface;
   --  The serializable object interface.

   function Serialize
     (Object : Serializable_Object)
      return Ada.Streams.Stream_Element_Array
      is abstract;
   --  Serialize given object to stream element array.

   function Deserialize
     (Buffer : Ada.Streams.Stream_Element_Array)
      return Serializable_Object
      is abstract;
   --  Create a new object from given stream element array.

end DHCP.Marshaling;
