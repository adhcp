--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Strings.Fixed;
with Ada.Strings.Unbounded;
with Ada.Characters.Handling;
with Ada.Tags.Generic_Dispatching_Constructor;

with Anet;

with DHCP.Utils;

package body DHCP.Options
is

   -------------------------------------------------------------------------

   procedure Append
     (List     : in out Option_List;
      New_Item :        Option_Type'Class)
   is
   begin
      List.Data.Append (New_Item => New_Item);
   end Append;

   -------------------------------------------------------------------------

   procedure Clear (List : in out Option_List)
   is
   begin
      List.Data.Clear;
   end Clear;

   -------------------------------------------------------------------------

   function Contains
     (List : Option_List;
      Name : Option_Name_Type)
      return Boolean
   is
      Pos : OLP.Cursor := List.Data.First;
   begin
      while OLP.Has_Element (Position => Pos) loop
         declare
            Opt : constant Option_Type'Class
              := Option_Type'Class (OLP.Element (Position => Pos));
         begin
            if Opt.Code = To_Code (Name => Name) then
               return True;
            end if;
         end;
         OLP.Next (Position => Pos);
      end loop;

      return False;
   end Contains;

   -------------------------------------------------------------------------

   function Create
     (Name : Option_Name_Type;
      Data : Ada.Streams.Stream_Element_Array)
      return Option_Type'Class
   is
   begin
      return Create_Option_Object (Code => To_Code (Name => Name),
                                   Data => Data);
   end Create;

   -------------------------------------------------------------------------

   function Create
     (Name  : Option_Name_Type;
      Value : Interfaces.Unsigned_16)
      return U_Int16_Option_Type
   is
      Len : constant Positive
        := Interfaces.Unsigned_16'Size / Ada.Streams.Stream_Element'Size;
   begin
      return U_Int16_Option_Type
        (Create (Name  => Name,
                 Data  => Utils.To_Array
                   (Value  => Interfaces.Unsigned_64 (Value),
                    Length => Len)));
   end Create;

   -------------------------------------------------------------------------

   function Create
     (Param : not null access Parameter_Record)
      return Raw_Option_Type
   is
   begin
      return Raw_Option_Type'
        (Size => Param.Size,
         Code => Param.Code,
         Data => Param.Data);
   end Create;

   -------------------------------------------------------------------------

   function Create_Option_Object
     (Code : Code_Type;
      Data : Ada.Streams.Stream_Element_Array)
      return Option_Type'Class
   is
      function Make_Option is new Ada.Tags.Generic_Dispatching_Constructor
        (T           => Option_Type,
         Parameters  => Parameter_Record,
         Constructor => Create);

      Tag : constant Ada.Tags.Tag := To_Tag (Code => Code);
   begin
      if Data'Length not in Size_Type'Range then
         raise Invalid_Option
           with "Could not create option: data size not in range";
      end if;

      declare
         Params : aliased Parameter_Record
           := (Size => Data'Length,
               Code => Code,
               Data => Data);
         Option : constant Option_Type'Class
           := Make_Option (The_Tag => Tag,
                           Params  => Params'Access);
      begin
         Option.Validate;
         return Option;
      end;
   end Create_Option_Object;

   -------------------------------------------------------------------------

   function Deserialize
     (Buffer : Ada.Streams.Stream_Element_Array)
      return Option_Type'Class
   is

      use Ada.Streams;

      --  Buffer is expected to be formatted in TLV, see RFC 2132 and RFC 3315:
      --
      --   1        1 + L    2 * L + 1...
      --  +--------+--------+---------
      --  |  Type  | Length | Data ...
      --  +--------+--------+---------

      Buf       : constant Stream_Element_Array (1 .. Buffer'Length) := Buffer;
      Len       : Natural;
      Opt_Code  : Code_Type;
      Data_Size : constant Integer := Buf'Length - Positive (2 * Bc);
   begin
      Opt_Code := Code_Type (Utils.To_Value (A => Buf (1 .. Bc)));

      if Data_Size < 1 then
         raise Invalid_Option with "Option " & To_Name (Code => Opt_Code)'Img
           & " has no data";
      end if;

      Len := Natural (Utils.To_Value (Buf (Bc + 1 .. 2 * Bc)));

      if Data_Size /= Len then
         raise Invalid_Option with "Option " & To_Name (Code => Opt_Code)'Img
           & " has invalid data size:" & Data_Size'Img;
      end if;

      return Create_Option_Object
        (Code => Opt_Code,
         Data => Buf (2 * Bc + 1 ..  Buf'Length));
   end Deserialize;

   -------------------------------------------------------------------------

   function Get
     (List : Option_List;
      Name : Option_Name_Type)
      return Option_Type'Class
   is
      Pos : OLP.Cursor := List.Data.First;
   begin
      while OLP.Has_Element (Position => Pos) loop
         declare
            Opt : constant Option_Type'Class
              := Option_Type'Class (OLP.Element (Position => Pos));
         begin
            if Opt.Code = To_Code (Name => Name) then
               return Opt;
            end if;
         end;
         OLP.Next (Position => Pos);
      end loop;

      raise Option_Not_Found with "No " & Name'Img & " option found";
   end Get;

   -------------------------------------------------------------------------

   function Get_Addrlen (Option : IP_Array_Option_Type) return Positive
   is (4);

   -------------------------------------------------------------------------

   function Get_Code (Option : Option_Type'Class) return Code_Type
   is
   begin
      return Option.Code;
   end Get_Code;

   -------------------------------------------------------------------------

   function Get_Count (List : Option_List) return Natural
   is
   begin
      return Natural (List.Data.Length);
   end Get_Count;

   -------------------------------------------------------------------------

   function Get_Data
     (Option : Option_Type'Class)
      return Ada.Streams.Stream_Element_Array
   is
   begin
      return Option.Data;
   end Get_Data;

   -------------------------------------------------------------------------

   function Get_Data_Size (List : Option_List) return Natural
   is
      Size : Natural := 0;

      procedure Add_Size (Opt : Option_Type'Class);
      --  Add option's data size to result.

      procedure Add_Size (Opt : Option_Type'Class)
      is
      begin
         Size := Size + Positive (Opt.Get_Size);
      end Add_Size;
   begin
      List.Iterate (Process => Add_Size'Access);
      return Size;
   end Get_Data_Size;

   -------------------------------------------------------------------------

   function Get_Data_Str (Option : Raw_Option_Type) return String
   is
   begin
      return Anet.To_Hex (Data => Option.Data);
   end Get_Data_Str;

   -------------------------------------------------------------------------

   function Get_Data_Str (Option : Byte_Option_Type) return String
   is
   begin
      return Ada.Strings.Fixed.Trim
        (Source => Option.Data (Option.Data'First)'Img,
         Side   => Ada.Strings.Left);
   end Get_Data_Str;

   -------------------------------------------------------------------------

   function Get_Data_Str (Option : U_Int16_Array_Option_Type) return String
   is
      use Ada.Streams;
      use Ada.Strings.Unbounded;

      Result : Unbounded_String;
      Count  : constant Stream_Element_Offset := Option.Data'Length / 2;
      Idx    : Stream_Element_Offset          := Option.Data'First;
   begin
      for A in 1 .. Count loop
         Result := Result & To_Unbounded_String
           (Ada.Strings.Fixed.Trim
              (Source => To_U_Int16
                   (Octets => Option.Data (Idx .. Idx + 1))'Img,
               Side   => Ada.Strings.Left));

         Idx := Idx + 2;
         exit when Idx > Option.Data'Length;

         Result := Result & " ";
      end loop;

      return To_String (Result);
   end Get_Data_Str;

   -------------------------------------------------------------------------

   function Get_Data_Str (Option : U_Int32_Option_Type) return String
   is
      use Interfaces;

      Result : Unsigned_32 := 0;
   begin
      for I in Option.Data'Range loop
         Result := Result + Shift_Left
           (Value  => Unsigned_32 (Option.Data (I)),
            Amount => Integer ((Option.Data'Length - I) * 8));
      end loop;

      return Ada.Strings.Fixed.Trim
        (Source => Result'Img,
         Side   => Ada.Strings.Left);
   end Get_Data_Str;

   -------------------------------------------------------------------------

   function Get_Data_Str (Option : IP_Array_Option_Type) return String
   is
      use Ada.Streams;
      use Ada.Strings.Unbounded;

      Result  : Unbounded_String;
      Addrlen : constant Stream_Element_Offset
        := Stream_Element_Offset (IP_Array_Option_Type'Class
                                  (Option).Get_Addrlen);
      Count   : constant Stream_Element_Offset := Option.Data'Length / Addrlen;
      Idx     : Stream_Element_Offset          := Option.Data'First;
   begin
      for A in 1 .. Count loop
         Result := Result & To_Unbounded_String
           (IP_Array_Option_Type'Class (Option).To_IP_String
            (Octets => Option.Data (Idx .. Idx + Addrlen - 1)));

         Idx := Idx + Addrlen;
         exit when Idx > Option.Data'Length;

         Result := Result & " ";
      end loop;

      return To_String (Result);
   end Get_Data_Str;

   -------------------------------------------------------------------------

   function Get_Name (Option : Option_Type'Class) return Option_Name_Type
   is
   begin
      return To_Name (Code => Option.Code);
   end Get_Name;

   -------------------------------------------------------------------------

   function Get_Name_Str (Option : Option_Type) return String
   is
   begin
      return Ada.Characters.Handling.To_Lower (Item => Option.Get_Name'Img);
   end Get_Name_Str;

   -------------------------------------------------------------------------

   function Get_Size (Option : Option_Type'Class) return Size_Type
   is
   begin
      return Option.Size;
   end Get_Size;

   -------------------------------------------------------------------------

   function Get_Value
     (Option : U_Int16_Option_Type)
      return Interfaces.Unsigned_16
   is
   begin
      return To_U_Int16 (Octets => Option.Data);
   end Get_Value;

   -------------------------------------------------------------------------

   function Get_Value
     (Option : U_Int32_Option_Type)
      return Interfaces.Unsigned_32
   is
   begin
      return Interfaces.Unsigned_32 (Utils.To_Value (A => Option.Data));
   end Get_Value;

   -------------------------------------------------------------------------

   function Is_Empty (List : Option_List) return Boolean
   is
   begin
      return List.Data.Is_Empty;
   end Is_Empty;

   -------------------------------------------------------------------------

   procedure Iterate
     (List    : Option_List;
      Process : not null access procedure (Option : Option_Type'Class))
   is
      procedure Call_Process (Position : OLP.Cursor);
      --  Call the process procedure for given container position.

      procedure Call_Process (Position : OLP.Cursor)
      is
         Opt : constant Option_Type'Class := OLP.Element (Position);
      begin
         Process (Option => Opt);
      end Call_Process;
   begin
      List.Data.Iterate (Process => Call_Process'Access);
   end Iterate;

   -------------------------------------------------------------------------

   procedure Remove
     (List : in out Option_List;
      Name :        Option_Name_Type)
   is
      Pos : OLP.Cursor := List.Data.First;
   begin
      while OLP.Has_Element (Position => Pos) loop
         declare
            Opt : constant Option_Type'Class
              := Option_Type'Class (OLP.Element (Position => Pos));
         begin
            if Opt.Code = To_Code (Name => Name) then
               List.Data.Delete (Position => Pos);
               return;
            end if;
         end;
         OLP.Next (Position => Pos);
      end loop;

      raise Option_Not_Found with "No " & Name'Img & " option found";
   end Remove;

   -------------------------------------------------------------------------

   function Serialize
     (Option : Option_Type'Class)
      return Ada.Streams.Stream_Element_Array
   is
      use Ada.Streams;
   begin
      return Opt : Stream_Element_Array (1 .. Option.Data'Length +  2 * Bc) do
         Opt (1 .. Bc) := Utils.To_Array
           (Value  => Interfaces.Unsigned_64 (Option.Code),
            Length => Positive (Bc));
         Opt (Bc + 1 .. 2 * Bc) := Utils.To_Array
           (Value  => Option.Data'Length,
            Length => Positive (Bc));
         Opt (2 * Bc + 1 .. Opt'Last) := Option.Data;
      end return;
   end Serialize;

   -------------------------------------------------------------------------

   function To_IP_String
     (Option : IP_Array_Option_Type;
      Octets : Ada.Streams.Stream_Element_Array)
      return String
   is
      pragma Unreferenced (Option);

      Addr : Anet.IPv4_Addr_Type;
      Idx  : Ada.Streams.Stream_Element_Offset := Octets'First;
   begin
      for B of Addr loop
         B   := Anet.Byte (Octets (Idx));
         Idx := Idx + 1;
      end loop;

      return Anet.To_String (Address => Addr);
   end To_IP_String;

   -------------------------------------------------------------------------

   function To_U_Int16
     (Octets : Ada.Streams.Stream_Element_Array)
      return Interfaces.Unsigned_16
   is
      use Interfaces;

      Result : Unsigned_16 := 0;
   begin
      for I in Octets'Range loop
         Result := Result + Shift_Left
           (Value  => Unsigned_16 (Octets (I)),
            Amount => Integer ((Octets'Length -
              (I - Octets'First + 1)) * 8));
      end loop;

      return Result;
   end To_U_Int16;

   -------------------------------------------------------------------------

   procedure Validate (Option : Byte_Option_Type)
   is
      Length : constant Natural := Option.Data'Length;
   begin
      if Length /= 1 then
         raise Invalid_Option with "Byte based option "
           & To_Name (Code => Option.Code)'Img & ": data size" & Length'Img
           & ", expected 1";
      end if;
   end Validate;

   -------------------------------------------------------------------------

   procedure Validate (Option : U_Int16_Array_Option_Type)
   is
      Length : constant Natural := Option.Data'Length;
   begin
      if Length mod 2 /= 0 then
         raise Invalid_Option with "Unsigned 16-bit array option "
           & To_Name (Code => Option.Code)'Img
           & ": data size" & Length'Img & ", expected multiple of 2";
      end if;
   end Validate;

   -------------------------------------------------------------------------

   procedure Validate (Option : U_Int16_Option_Type)
   is
      Length : constant Natural := Option.Data'Length;
   begin
      if Length /= 2 then
         raise Invalid_Option with "Unsigned 16-bit option "
           & To_Name (Code => Option.Code)'Img & ": data size" & Length'Img
           & ", expected 2";
      end if;
   end Validate;

   -------------------------------------------------------------------------

   procedure Validate (Option : U_Int32_Option_Type)
   is
      Length : constant Natural := Option.Data'Length;
   begin
      if Length /= 4 then
         raise Invalid_Option with "Unsigned 32-bit option "
           & To_Name (Code => Option.Code)'Img & ": data size" & Length'Img
           & ", expected 4";
      end if;
   end Validate;

   -------------------------------------------------------------------------

   procedure Validate (Option : IP_Array_Option_Type)
   is
      Len : constant Positive   := IP_Array_Option_Type'Class
        (Option).Get_Addrlen;
      Length : constant Natural := Option.Data'Length;
   begin
      if Length mod Len /= 0 then
         raise Invalid_Option with "IP array option "
           & To_Name (Code => Option.Code)'Img
           & ": data size" & Length'Img & ", expected multiple of" & Len'Img;
      end if;
   end Validate;

end DHCP.Options;
