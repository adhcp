--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;

with Anet;

generic
package DHCP.Options.V4
is

   -------------------
   -- String option --
   -------------------

   type String_Option_Type is new Raw_Option_Type with private;
   --  Option type containing a string.

   function Get_Data_Str (Option : String_Option_Type) return String;
   --  Return string representation of option data.

   ---------------
   -- IP option --
   ---------------

   type IP_Option_Type is new IP_Array_Option_Type with private;
   --  Option type containing an IP address.

   function Create
     (Name    : Option_Name_Type;
      Address : Anet.IPv4_Addr_Type)
      return IP_Option_Type;
   --  Create a new IP option with given name and IPv4 address.

   overriding
   procedure Validate (Option : IP_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   function Get_Value (Option : IP_Option_Type) return Anet.IPv4_Addr_Type;
   --  Return the IP address stored in the option.

   ---------------------
   -- IP pairs option --
   ---------------------

   type IP_Pairs_Option_Type is new IP_Array_Option_Type with private;
   --  Option type containing an array of IP address pairs.

   overriding
   procedure Validate (Option : IP_Pairs_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   --------------------
   -- Boolean option --
   --------------------

   type Bool_Option_Type is new Byte_Option_Type with private;
   --  Option type containing a boolean.

   overriding
   procedure Validate (Option : Bool_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   ---------------------------
   -- Positive octet option --
   ---------------------------

   type Positive_Octet_Option_Type is new Byte_Option_Type with private;
   --  Option type containing an octet with a value of range 1 .. 255.

   overriding
   procedure Validate (Option : Positive_Octet_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   --------------------------
   -- Interface MTU option --
   --------------------------

   type Iface_MTU_Option_Type is new U_Int16_Option_Type with private;
   --  Option type containing the interface MTU.

   overriding
   procedure Validate (Option : Iface_MTU_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   --------------------------
   -- DHCP msg kind option --
   --------------------------

   type Msg_Kind_Option_Type is new Positive_Octet_Option_Type with private;
   --  Option type containing the DHCP message kind.

   overriding
   procedure Validate (Option : Msg_Kind_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   -----------------
   -- Option list --
   -----------------

   type Option_List is new DHCP.Options.Option_List with private;
   --  List of DHCP options.

   overriding
   function Serialize
     (List : Option_List)
      return Ada.Streams.Stream_Element_Array;
   --  Serialize given DHCP option list to stream element array.

   overriding
   function Deserialize
     (Buffer : Ada.Streams.Stream_Element_Array)
      return Option_List;
   --  Create a DHCP option list from given stream element array.

   Default_Param_Req_List : constant Raw_Option_Type;
   --  Default request parameter list option (Parameter_List, option nr. 55).
   --  Requests the following options from the server: Subnet_Mask,
   --  Broadcast_Address, Time_Offset, Routers, Domain_Name,
   --  Domain_Name_Servers, Domain_Search, Host_Name, NETBIOS_Name_Servers,
   --  NETBIOS_Scope, Interface_MTU, Classless_Static_Route, NTP_Servers.

   Invalid_Cookie : exception;

private

   type String_Option_Type is new Raw_Option_Type with null record;

   type IP_Option_Type is new IP_Array_Option_Type with null record;

   type IP_Pairs_Option_Type is new IP_Array_Option_Type with null record;

   type Bool_Option_Type is new Byte_Option_Type with null record;

   type Positive_Octet_Option_Type is new Byte_Option_Type with null record;

   type Iface_MTU_Option_Type is new U_Int16_Option_Type with null record;

   type Msg_Kind_Option_Type is new
     Positive_Octet_Option_Type with null record;

   type Option_List is new DHCP.Options.Option_List with null record;

   Default_Param_Req_List : constant Raw_Option_Type
     := (Size => 13,
         Code => 55,
         Data => (16#01#, 16#1c#, 16#02#, 16#03#, 16#0f#, 16#06#, 16#77#,
                  16#0c#, 16#2c#, 16#2f#, 16#1a#, 16#79#, 16#2a#));

end DHCP.Options.V4;
