--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Strings.Unbounded;

package body DHCP.Utils
is

   use Ada.Strings.Unbounded;

   -------------------------------------------------------------------------

   function Strip
     (Str     : String;
      Pattern : String)
      return String
   is
      Result : Unbounded_String
        := To_Unbounded_String (Source => Str);
      Cur_Idx : Natural := Str'First;
   begin
      if Pattern = "" or Str = "" then
         return Str;
      end if;

      loop
         Cur_Idx := Index
           (Source  => Result,
            Pattern => Pattern);
         exit when Cur_Idx = 0;
         Delete (Source  => Result,
                 From    => Cur_Idx,
                 Through => Cur_Idx + Pattern'Length - 1);
      end loop;
      return To_String (Result);
   end Strip;

   -------------------------------------------------------------------------

   function To_Array
     (Value  : Interfaces.Unsigned_64;
      Length : Positive)
      return Ada.Streams.Stream_Element_Array
   is
      use Ada.Streams;

      Res : Stream_Element_Array (1 .. Stream_Element_Offset (Length));
      Idx : Integer := Length - 1;
   begin
      for I in Res'Range loop
         Res (I) := Stream_Element'Mod (Value / 2 ** (Idx * 8));
         Idx     := Idx - 1;
      end loop;

      return Res;
   end To_Array;

   -------------------------------------------------------------------------

   function To_String (A : Ada.Streams.Stream_Element_Array) return String
   is
      Result : String (1 .. A'Length) := (others => ASCII.NUL);
      Idx    : Positive := Result'First;
   begin
      for C in A'Range loop
         Result (Idx) := Character'Val (A (C));
         Idx := Idx + 1;
      end loop;

      return Result;
   end To_String;

   -------------------------------------------------------------------------

   function To_Value
     (A : Ada.Streams.Stream_Element_Array)
      return Interfaces.Unsigned_64
   is
      use Interfaces;

      Res : Unsigned_64 := 0;
      Idx : Natural     := 0;
   begin
      for I in reverse A'Range loop
         Res := Res + Shift_Left
           (Value  => Unsigned_64 (A (I)),
            Amount => Idx * 8);
         Idx := Idx + 1;
      end loop;

      return Res;
   end To_Value;

end DHCP.Utils;
