--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

with DHCP.Logger;
with DHCP.Notify;
with DHCP.Timer;
with DHCP.Timing_Events.Start;

with DHCPv6.Constants;
with DHCPv6.Database;
with DHCPv6.Options;
with DHCPv6.States.Stateless_Init;

package body DHCPv6.States.Stateless_Config
is

   package L renames DHCP.Logger;

   Instance : aliased Config_State_Type;

   -------------------------------------------------------------------------

   function Get_Name (State : not null access Config_State_Type) return String
   is ("Stateless_Config");

   -------------------------------------------------------------------------

   procedure Process_Reply_Msg
     (State       : access Config_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      pragma Unreferenced (State);

      Opts : constant Options.Inst6.Option_List := Msg.Get_Options;

      Refresh_Time : Duration := Constants.IRT_DEFAULT;

      procedure Handle_Option (Option : Options.Inst.Option_Type'Class);
      --  Store option in database if it is a global config option.

      ----------------------------------------------------------------------

      procedure Handle_Option (Option : Options.Inst.Option_Type'Class)
      is
         use type Options.Option_Name;
      begin
         if Options.Is_Config_Option (Name => Option.Get_Name) then
            Database.Add_Global_Option (Opt => Option);
         elsif Option.Get_Name = Options.Information_Refresh_Time then
            declare
               IRT : Duration := Duration
                 (Options.Inst.U_Int32_Option_Type (Option).Get_Value);
            begin
               if IRT < Constants.IRT_MINIMUM then
                  IRT := Constants.IRT_MINIMUM;
               end if;

               Refresh_Time := IRT;
            end;
         end if;
      end Handle_Option;
   begin
      Opts.Iterate (Process => Handle_Option'Access);

      DHCP.Notify.Update (Reason => DHCP.Notify.Bound);

      if Refresh_Time /= Constants.Infinity then
         L.Log (Message => "Refreshing DHCPv6 configuration information in"
                & Long_Integer (Refresh_Time)'Img & " seconds");
         declare
            use Ada.Real_Time;

            Ev : DHCP.Timing_Events.Start.Start_Type (T => Transaction'Access);
         begin
            Ev.Set_Time (At_Time => Clock + To_Time_Span (D => Refresh_Time));
            DHCP.Timer.Schedule (Event => Ev);
         end;
      end if;

      Transaction.Reset_Retransmit;
      Transaction.Reset;
      Transaction.Set_State (State => Stateless_Init.State);
   end Process_Reply_Msg;

   -------------------------------------------------------------------------

   function State return DHCP.States.State_Handle
   is (Instance'Access);

end DHCPv6.States.Stateless_Config;
