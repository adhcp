--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

private with Ada.Containers.Indefinite_Hashed_Maps;

with Anet;

with DHCP.Types;

with DHCPv6.Options;

package DHCPv6.IAs
is

   type Identity_Association_Type is private;
   --  Identity association.

   function Create
     (IAID : DHCP.Types.IAID_Type)
      return Identity_Association_Type;
   --  Create new identity association with given ID.

   function Get_ID
     (IA : Identity_Association_Type)
      return DHCP.Types.IAID_Type;
   --  Return the identifier of the given identity association.

   procedure Set_Timestamp
     (IA        : in out Identity_Association_Type;
      Timestamp :        Ada.Real_Time.Time);
   --  Set timestamp of given identity association.

   procedure Set_Timeouts
     (IA : in out Identity_Association_Type;
      T1 :        Duration;
      T2 :        Duration)
   with
      Pre => T1 <= T2;
   --  Set T1 and T2 timeouts of given identity association.

   function Get_T1 (IA : Identity_Association_Type) return Duration;
   --  Returns the T1 duration of the given identity association.

   function Get_T2 (IA : Identity_Association_Type) return Duration;
   --  Returns the T2 duration of the given identity association.

   function Get_Expiry (IA : Identity_Association_Type) return Duration;
   --  Returns the duration until valid lifetimes of all addresses have
   --  expired.

   function Get_Timestamp
     (IA : Identity_Association_Type)
      return Ada.Real_Time.Time;
   --  Returns the timestamp of the given identity association.

   procedure Add_Address
     (IA      : in out Identity_Association_Type;
      Address :        Options.Inst6.IA_Address_Option_Type);
   --  Add address with specified parameters to given identity association. If
   --  the IA has an existing entry with the specified address, it is updated
   --  with the new values.

   procedure Remove_Address
     (IA      : in out Identity_Association_Type;
      Address :        Anet.IPv6_Addr_Type);
   --  Remove specified address from given identity association.

   procedure Discard_Expired_Addresses
     (IA            : in out Identity_Association_Type;
      Removed_Addrs :    out Options.Inst6.Option_List);
   --  Remove all addresses that have expired from the given identity
   --  association. Removed addresses are returned as members of the
   --  Removed_Addrs list parameter.

   procedure Iterate
     (IA      : Identity_Association_Type;
      Process : not null access procedure
        (Address : Options.Inst6.IA_Address_Option_Type));
   --  Iterate over all addresses of the given identity association.

   function Get_Addresses
     (IA : Identity_Association_Type)
      return Options.Inst6.Option_List'Class;
   --  Return all addresses that are stored in the identity association.

   function To_Option
     (IA : Identity_Association_Type)
      return Options.Inst6.IA_NA_Option_Type;
   --  Convert identity association type to corresponding IA-NA option.

private

   function Hash_Addr
     (Key : Anet.IPv6_Addr_Type)
      return Ada.Containers.Hash_Type;

   package MOA is new Ada.Containers.Indefinite_Hashed_Maps
     (Key_Type        => Anet.IPv6_Addr_Type,
      Element_Type    => DHCPv6.Options.Inst6.IA_Address_Option_Type,
      Hash            => Hash_Addr,
      Equivalent_Keys => Anet."=",
      "="             => DHCPv6.Options.Inst6."=");

   type Identity_Association_Type is record
      IAID      : DHCP.Types.IAID_Type;
      Timestamp : Ada.Real_Time.Time := Ada.Real_Time.Time_First;
      T1        : Duration           := 0.0;
      T2        : Duration           := 0.0;
      Addresses : MOA.Map            := MOA.Empty_Map;
   end record;

end DHCPv6.IAs;
