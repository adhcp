--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package DHCPv6.States.Rebinding
is

   type Rebinding_State_Type is new Allocation_State_Type with private;
   --  The T2 time of an IA has expired which means it could not be renewed.
   --  The client initiates a Rebind/Reply exchange with any available server.

   overriding
   function Get_Name
     (State : not null access Rebinding_State_Type)
      return String;
   --  Return name of state as string.

   overriding
   procedure Process_Lease_Expiry
     (State       : access Rebinding_State_Type;
      Transaction : in out Transaction_Type'Class);
   --  Restart server discovery and transition to init.

   overriding
   procedure Process_Reply_Msg
     (State       : access Rebinding_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type);
   --  Update server ID on rebind reply.

   overriding
   procedure Notify_Allocation (State : access Rebinding_State_Type);
   --  Signal rebinding of lease information.

   function State return DHCP.States.State_Handle;
   --  Return access to singleton.

private

   type Rebinding_State_Type is new Allocation_State_Type with null record;

end DHCPv6.States.Rebinding;
