--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;

with DHCP.Marshaling;

with DHCPv6.Types;
with DHCPv6.Options;

package DHCPv6.Message
is

   type Message_Kind is
     (Solicit,
      Advertise,
      Request,
      Confirm,
      Renew,
      Rebind,
      Reply,
      Release,
      Decline,
      Reconfigure,
      Information_Request);
   --  DHCPv6 message kind.

   type Message_Type is new DHCP.Marshaling.Serializable_Object with private;
   --  DHCPv6 message.

   overriding
   function Serialize
     (Message : Message_Type)
      return Ada.Streams.Stream_Element_Array;
   --  Serialize given DHCPv6 message to stream element array.

   overriding
   function Deserialize
     (Buffer : Ada.Streams.Stream_Element_Array)
      return Message_Type;
   --  Create a DHCPv6 message from given stream element array.

   function Create (Kind : Message_Kind) return Message_Type;
   --  Create a DHCPv6 message of given kind.

   function Get_Kind (Msg : Message_Type) return Message_Kind;
   --  Return the kind of the DHCPv6 message.

   procedure Set_Transaction_ID
     (Msg : in out Message_Type;
      XID :        Types.Transaction_ID_Type);
   --  Set transaction ID.

   function Get_Transaction_ID
     (Msg : Message_Type)
      return Types.Transaction_ID_Type;
   --  Return transaction ID.

   procedure Add_Option
     (Msg : in out Message_Type;
      Opt :        Options.Inst.Option_Type'Class);
   --  Add given option to DHCPv6 message.

   function Get_Options (Msg : Message_Type) return Options.Inst6.Option_List;
   --  Return DHCPv6 options.

   Invalid_Message : exception;

private

   for Message_Kind use
     (Solicit             => 1,
      Advertise           => 2,
      Request             => 3,
      Confirm             => 4,
      Renew               => 5,
      Rebind              => 6,
      Reply               => 7,
      Release             => 8,
      Decline             => 9,
      Reconfigure         => 10,
      Information_Request => 11);

   type Message_Type is new DHCP.Marshaling.Serializable_Object with record
      Kind : Message_Kind              := Message_Kind'First;
      X_ID : Types.Transaction_ID_Type := 0;
      Opts : Options.Inst6.Option_List;
   end record;

end DHCPv6.Message;
