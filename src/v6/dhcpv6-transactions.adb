--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;
with Ada.Strings.Unbounded;

with Interfaces;

with DHCP.Logger;
with DHCP.Timer;
with DHCP.Random;

with DHCPv6.Database;
with DHCPv6.Transmission;

package body DHCPv6.Transactions
is

   package L renames DHCP.Logger;

   use Ada.Strings.Unbounded;
   use type DHCPv6.Message.Message_Kind;

   function Msg_Valid
     (Msg          : Message.Message_Type;
      Invalid_Opts : Option_Names)
      return Boolean;
   --  Returns True if the given message contains a client and a server
   --  identifier option as well as none of the given invalid options.
   --  Additionally the client identifier is matched against the one stored in
   --  the database.

   function Advertise_Msg_Valid (Msg : Message.Message_Type) return Boolean
   with
      Pre => Msg.Get_Kind = Message.Advertise;
   --  Returns True if the given advertise message is valid.

   -------------------------------------------------------------------------

   function Advertise_Msg_Valid (Msg : Message.Message_Type) return Boolean
   is
      Opts : constant Options.Inst6.Option_List := Msg.Get_Options;
   begin
      if not Msg_Valid
        (Msg           => Msg,
         Invalid_Opts  => (Options.Option_Request,
                           Options.Elapsed_Time,
                           Options.Relay_Message,
                           Options.Server_Unicast,
                           Options.Rapid_Commit,
                           Options.Interface_ID,
                           Options.Reconfigure_Message))
      then
         return False;
      end if;

      --  Check status code value if option is present, see RFC 3315, section
      --  17.1.3.

      declare
         package OI6 renames DHCPv6.Options.Inst6;

         use type OI6.Status_Code_Type;

         Found      : Boolean := False;
         Status_Msg : Unbounded_String;
      begin
         Options.Has_Status_Code
           (Opts       => Opts,
            Status     => OI6.Status_No_Addrs_Avail,
            Found      => Found,
            Status_Msg => Status_Msg);

         if Found then
            L.Log (Message => "Discarding " & Msg.Get_Kind'Img
                   & " message with XID" & Msg.Get_Transaction_ID'Img
                   & ": " & OI6.Status_No_Addrs_Avail'Img
                   & (if Length (Status_Msg) > 0 then
                        " - " & To_String (Status_Msg)
                     else ""));
            return False;
         end if;
      end;

      return True;
   end Advertise_Msg_Valid;

   -------------------------------------------------------------------------

   function Get_Elapsed_Time
     (Transaction : Transaction_Type)
      return Elapsed_Duration
   is
      use type Ada.Real_Time.Time;

      Elapsed_Time : Duration := 0.0;
   begin
      if Transaction.RT_Count > 0 then
         Elapsed_Time := Ada.Real_Time.To_Duration
           (TS => Ada.Real_Time.Clock - Transaction.Get_Start_Time);

         if Elapsed_Time > Elapsed_Duration'Last / 100 then
            Elapsed_Time := Elapsed_Duration'Last;
         else
            Elapsed_Time := Elapsed_Time * 100;
         end if;
      end if;

      return Elapsed_Time;
   end Get_Elapsed_Time;

   -------------------------------------------------------------------------

   function Get_ID
     (Transaction : Transaction_Type)
      return Types.Transaction_ID_Type
   is (Transaction.ID);

   -------------------------------------------------------------------------

   procedure Increase_Retransmission_Params
     (Transaction : in out Transaction_Type;
      Random      :        Types.Rand_Duration)
   is
   begin
      if Transaction.RT_Count = 0 then
         Transaction.RT := Transaction.IRT + Random * Transaction.IRT;
      else
         Transaction.RT := 2 * Transaction.RT + Random * Transaction.RT;
      end if;

      if Transaction.MRT > 0.0 and then Transaction.RT > Transaction.MRT then
         Transaction.RT := Transaction.MRT + Random * Transaction.MRT;
      end if;

      Transaction.RT_Count := Transaction.RT_Count + 1;
   end Increase_Retransmission_Params;

   -------------------------------------------------------------------------

   function Max_Retransmit_Count_Reached
     (Transaction : Transaction_Type)
      return Boolean
   is (Transaction.MRC > 0 and then Transaction.RT_Count >= Transaction.MRC);

   -------------------------------------------------------------------------

   function Max_Retransmit_Duration_Reached
     (Transaction : Transaction_Type)
      return Boolean
   is
      use Ada.Real_Time;
   begin
      return Transaction.MRD > 0.0 and then
        Transaction.Get_Start_Time <= Clock - To_Time_Span
          (D => Transaction.MRD);
   end Max_Retransmit_Duration_Reached;

   -------------------------------------------------------------------------

   function Msg_Valid
     (Msg          : Message.Message_Type;
      Invalid_Opts : Option_Names)
      return Boolean
   is
      use type Options.Inst.Option_Type'Class;

      Opts : constant Options.Inst6.Option_List := Msg.Get_Options;
   begin
      if not Valid_Message_Options
        (Msg           => Msg,
         Required_Opts => (Options.Server_Identifier,
                           Options.Client_Identifier),
         Invalid_Opts  => Invalid_Opts)
      then
         return False;
      end if;

      if Opts.Get (Name => Options.Client_Identifier)
        /= Database.Get_Client_ID
      then
         L.Log (Message => "Discarding " & Msg.Get_Kind'Img
                & " message with XID" & Msg.Get_Transaction_ID'Img
                & ": Client identifier mismatch");
         return False;
      end if;

      return True;
   end Msg_Valid;

   -------------------------------------------------------------------------

   procedure Process_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type)
   is
      Msg_Kind : constant Message.Message_Kind := Msg.Get_Kind;
   begin
      case Msg_Kind is
         when Message.Reply =>
            if not Msg_Valid
              (Msg          => Msg,
               Invalid_Opts => (Options.Option_Request,
                                Options.Elapsed_Time,
                                Options.Relay_Message,
                                Options.Interface_ID,
                                Options.Reconfigure_Message))
            then
               return;
            end if;

            States.State_Type'Class (Transaction.State.all).Process_Reply_Msg
              (Transaction => Transaction,
               Msg         => Msg);
         when Message.Advertise =>
            if not Advertise_Msg_Valid (Msg => Msg) then
               return;
            end if;

            States.State_Type'Class
              (Transaction.State.all).Process_Advertise_Msg
              (Transaction => Transaction,
               Msg         => Msg);
         when others        =>
            L.Log (Message => "Not processing unsupported " & Msg_Kind'Img
                   & " message");
      end case;
   end Process_Message;

   -------------------------------------------------------------------------

   procedure Process_Timer_Expiry
     (Transaction : in out Transaction_Type;
      Timer_Kind  :        DHCP.Types.DHCP_Timer_Kind)
   is
      use DHCP.Types;
   begin
      L.Log (Message => "Timer " & Timer_Kind'Img & " expired while in state "
             & Transaction.State.Get_Name);

      case Timer_Kind is
         when Retransmit   =>
            if Transaction.Max_Retransmit_Count_Reached
              or else Transaction.Max_Retransmit_Duration_Reached
            then
               States.State_Type'Class
                 (Transaction.State.all).Process_Transmission_Failure
                 (Transaction => Transaction,
                  Msg         => Transaction.Last_Msg);
            else
               L.Log (Message => "Retransmission" & Transaction.RT_Count'Img
                      & (if Transaction.MRC = 0 then ""
                        else " of" & Transaction.MRC'Img)
                      & " for " & Transaction.Last_Msg.Get_Kind'Img
                      & " message, XID" & Transaction.Get_ID'Img);
               Transaction.Send_Message (Msg => Transaction.Last_Msg);
            end if;
         when T1           =>
            States.State_Type'Class
              (Transaction.State.all).Process_T1_Expiry
              (Transaction => Transaction);
         when T2           =>
            States.State_Type'Class
              (Transaction.State.all).Process_T2_Expiry
              (Transaction => Transaction);
         when Lease_Expiry =>
            States.State_Type'Class
              (Transaction.State.all).Process_Lease_Expiry
              (Transaction => Transaction);
      end case;
   end Process_Timer_Expiry;

   -------------------------------------------------------------------------

   procedure Reset (Transaction : in out Transaction_Type)
   is
   begin
      Transaction.Set_Start_Time (Time => Ada.Real_Time.Time_First);

      Transaction.ID       := Types.Transaction_ID_Type'First;
      Transaction.RT_Count := 0;
      Transaction.RT       := 0.0;
      Transaction.IRT      := 0.0;
      Transaction.MRC      := 0;
      Transaction.MRT      := 0.0;
      Transaction.MRD      := 0.0;
   end Reset;

   -------------------------------------------------------------------------

   procedure Reset_Retransmit (Transaction : in out Transaction_Type)
   is
   begin
      DHCP.Timer.Cancel (Event => Transaction.Retransmit_Ev);
      Transaction.RT_Count := 0;
      Transaction.RT       := Transaction.IRT;
   end Reset_Retransmit;

   -------------------------------------------------------------------------

   procedure Schedule_Retransmit
     (Transaction : in out Transaction_Type;
      In_Time     :        Duration)
   is
      use Ada.Real_Time;

      New_Time : constant Time := Clock + To_Time_Span (D => In_Time);
   begin

      --  Cancel any potentially pending retransmission.

      DHCP.Timer.Cancel (Event => Transaction.Retransmit_Ev);

      Transaction.Retransmit_Ev.Set_Time (At_Time => New_Time);

      L.Log (Message => "Retransmission timer in" & Long_Integer (In_Time)'Img
             & " seconds");

      DHCP.Timer.Schedule (Event => Transaction.Retransmit_Ev);
   end Schedule_Retransmit;

   -------------------------------------------------------------------------

   procedure Send_Message
     (Transaction : in out Transaction_Type;
      Msg         :         Message.Message_Type)
   is
   begin
      Transaction.Last_Msg := Msg;

      declare
         Out_Msg : Message.Message_Type := Msg;
      begin
         Out_Msg.Add_Option
           (Opt => Options.Inst.Create
              (Name  => Options.Elapsed_Time,
               Value => Interfaces.Unsigned_16
                 (Transaction.Get_Elapsed_Time)));
         Transmission.Send (Message => Out_Msg);
      end;

      declare
         Rand : Types.Rand_Duration
           := Types.Rand_Duration
             (DHCP.Random.Get
                (Low  => Float (Types.Rand_Duration'First),
                 High => Float (Types.Rand_Duration'Last)));
      begin
         if Msg.Get_Kind = Message.Solicit and then Transaction.RT_Count = 0
         then

            --  For the first Solicit message RAND must be *strictly* greater
            --  than 0, see RFC 3315, section 17.1.2.

            Rand := (if Rand = 0.0 then Types.Rand_Duration'Delta
                     else abs Rand);
         end if;

         Transaction.Increase_Retransmission_Params (Random => Rand);
      end;

      Schedule_Retransmit (Transaction => Transaction,
                           In_Time     => Transaction.RT);
   end Send_Message;

   -------------------------------------------------------------------------

   procedure Set_ID
     (Transaction : in out Transaction_Type;
      XID         :        Types.Transaction_ID_Type)
   is
   begin
      Transaction.ID := XID;
   end Set_ID;

   -------------------------------------------------------------------------

   procedure Set_Retransmission_Params
     (Transaction : in out Transaction_Type;
      IRT         :        Duration;
      MRC         :        Natural;
      MRT         :        Duration;
      MRD         :        Duration)
   is
   begin
      Transaction.RT_Count := 0;
      Transaction.RT       := IRT;
      Transaction.IRT      := IRT;
      Transaction.MRC      := MRC;
      Transaction.MRT      := MRT;
      Transaction.MRD      := MRD;
   end Set_Retransmission_Params;

   -------------------------------------------------------------------------

   procedure Start (Transaction : in out Transaction_Type)
   is
   begin
      States.State_Type'Class
        (Transaction.State.all).Start (Transaction => Transaction);
   end Start;

   -------------------------------------------------------------------------

   procedure Stop (Transaction : in out Transaction_Type)
   is
   begin
      States.State_Type'Class
        (Transaction.State.all).Stop (Transaction => Transaction);
   end Stop;

   -------------------------------------------------------------------------

   function Valid_Message_Options
     (Msg           : Message.Message_Type;
      Required_Opts : Option_Names;
      Invalid_Opts  : Option_Names)
      return Boolean
   is
      Opts : constant Options.Inst6.Option_List := Msg.Get_Options;
   begin
      for O of Required_Opts loop
         if not Opts.Contains (Name => O) then
            L.Log (Message => "Discarding " & Msg.Get_Kind'Img
                   & " message with XID" & Msg.Get_Transaction_ID'Img
                   & ": " & O'Img & " option missing");
            return False;
         end if;
      end loop;

      for O of Invalid_Opts loop
         if Opts.Contains (Name => O) then
            L.Log (Message => "Discarding " & Msg.Get_Kind'Img
                   & " message with XID" & Msg.Get_Transaction_ID'Img
                   & ": Invalid " & O'Img & " option present");
            return False;
         end if;
      end loop;

      return True;
   end Valid_Message_Options;

end DHCPv6.Transactions;
