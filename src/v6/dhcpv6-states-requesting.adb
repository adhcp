--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Notify;

package body DHCPv6.States.Requesting
is

   Instance : aliased Requesting_State_Type;

   -------------------------------------------------------------------------

   function Get_Name
     (State : not null access Requesting_State_Type)
      return String
   is ("Requesting");

   -------------------------------------------------------------------------

   procedure Notify_Allocation (State : access Requesting_State_Type)
   is
      pragma Unreferenced (State);
   begin
      DHCP.Notify.Update (Reason => DHCP.Notify.Bound);
   end Notify_Allocation;

   -------------------------------------------------------------------------

   function State return DHCP.States.State_Handle
   is (Instance'Access);

end DHCPv6.States.Requesting;
