--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

private with Ada.Containers.Hashed_Maps;

with DHCP.Types;

with DHCPv6.IAs;
with DHCPv6.Options;

package DHCPv6.Database
is

   use type DHCPv6.Options.Option_Name;

   procedure Initialize (Client_ID : Options.Inst.Option_Type'Class)
   with
      Pre  => not Is_Initialized,
      Post => Is_Initialized;
   --  Initialized database with given client identifier option used in
   --  outgoing DHCPv6 messages.

   function Is_Initialized return Boolean;
   --  Returns True if the database has been initialized;

   function Get_Client_ID return Options.Inst.Option_Type'Class
   with
      Pre => Is_Initialized;
   --  Return current client identifier option.

   procedure Reset
   with
      Pre => Is_Initialized;
   --  Reset database. The existing client identifier is retained.

   procedure Add_Global_Option (Opt : Options.Inst.Option_Type'Class)
   with
      Pre => Opt.Get_Name /= Options.Client_Identifier;
   --  Add given global configuration option to database. If an option with the
   --  same name is already present its value is updated.

   procedure Remove_Global_Option (Name : Options.Option_Name)
   with
      Pre => Name /= Options.Client_Identifier;
   --  Remove global configuration option with given name from the database.
   --  If no such option is found, nothing is done.

   function Get_Global_Options return Options.Inst6.Option_List;
   --  Return global configurations options currently stored in database.

   function Contains_Option (Name : Options.Option_Name) return Boolean;
   --  Returns True if the global configuration option with given name is
   --  stored in the database.

   procedure Add_IA (IA : IAs.Identity_Association_Type);
   --  Add identity association to database.

   function Contains (IA : DHCP.Types.IAID_Type) return Boolean;
   --  Returns True if an identity association with given ID exists in the
   --  database.

   function Get_IA
     (ID : DHCP.Types.IAID_Type)
      return IAs.Identity_Association_Type;
   --  Return identity association with given ID from database. If no IA with
   --  specified ID is present an exception is raised.

   procedure Remove_IA (ID : DHCP.Types.IAID_Type);
   --  Remove identity association with given ID from database. If no IA with
   --  specified ID is present an exception is raised.

   procedure Update_IA
     (ID     : DHCP.Types.IAID_Type;
      Update : not null access procedure
        (ID :        DHCP.Types.IAID_Type;
         IA : in out IAs.Identity_Association_Type));
   --  Updates the identity association with specified ID by invoking the given
   --  procedure. If no IA with specified ID is present an exception is raised.

   procedure Iterate
     (Process : not null access procedure
        (IA : IAs.Identity_Association_Type));
   --  Call the process procedure for each identity association in the
   --  database.

   function Get_IA_Count return Natural;
   --  Returns the number of IAs currently stored in the database.

   IA_Present : exception;
   IA_Missing : exception;

private

   use type DHCP.Types.IAID_Type;

   function ID_To_Hash
     (Key : DHCP.Types.IAID_Type)
      return Ada.Containers.Hash_Type
   is (Ada.Containers.Hash_Type (Key));

   function Equivalent_Keys
     (Left, Right : DHCP.Types.IAID_Type)
      return Boolean
   is (Left = Right);

   package MOIA is new Ada.Containers.Hashed_Maps
     (Key_Type        => DHCP.Types.IAID_Type,
      Element_Type    => IAs.Identity_Association_Type,
      Hash            => ID_To_Hash,
      Equivalent_Keys => Equivalent_Keys,
      "="             => IAs."=");

   type Database_Type is record
      Global_Opts : Options.Inst6.Option_List;
      IAs         : MOIA.Map;
   end record;

   Instance : Database_Type;

   function Is_Initialized return Boolean
   is (Instance.Global_Opts.Contains (Name => Options.Client_Identifier));

end DHCPv6.Database;
