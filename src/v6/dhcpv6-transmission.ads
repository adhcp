--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet.Types;

with DHCP.Net;

with DHCPv6.Message;
with DHCPv6.Receivers;

package DHCPv6.Transmission
is

   use type DHCP.Net.Iface_Addr_Type;

   procedure Initialize
     (Client_Iface : DHCP.Net.Iface_Type;
      Listen_Cb    : Receivers.UDPv6.Rcv_Item_Callback)
   with
      Pre => Client_Iface.Addrtype = DHCP.Net.IPv6_Address;
   --  Initialize DHCPv6 transmission layer. Use the specified client
   --  interface for sending messages and register the specified callback with
   --  the receiver.

   procedure Stop;
   --  Stop listening for DHCPv6 messages.

   procedure Send (Message : DHCPv6.Message.Message_Type);
   --  Send given DHCP message to specified destination.

   function Get_Iface_Name return Anet.Types.Iface_Name_Type;
   --  Return name of currently used interface.

private

   Iface_Info : DHCP.Net.Iface_Type (Addrtype => DHCP.Net.IPv6_Address);
   --  Information about currently used network interface.

end DHCPv6.Transmission;
