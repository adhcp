--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

private with Ada.Streams;
private with Anet.Sockets.Inet;

private with DHCPv6.States;
private with DHCPv6.Transactions;

package DHCPv6.Client
is

   procedure Run;
   --  Start the DHCPv6 client service.

   procedure Stop;
   --  Stop the DHCPv6 client service.

private

   Instance : aliased Transactions.Transaction_Type;

   Transaction : not null access  States.Transaction_Type'Class
     := Instance'Access;
   --  Transaction handle required for improved testability to make transaction
   --  instance replaceable during testing.

   procedure Process_Server_Reply
     (Buffer : Ada.Streams.Stream_Element_Array;
      Src    : Anet.Sockets.Inet.IPv6_Sockaddr_Type);
   --  Process replies from DHCP server.

end DHCPv6.Client;
