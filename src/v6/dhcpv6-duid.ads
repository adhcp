--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;

with DHCPv6.Options;

package DHCPv6.DUID
is

   type DUID_Kind_Type is mod 2 ** 16
     with Size => 16;
   --  DUID kind represents the DUID two-octet type code, see RFC 3315,
   --  section 9.1.

   DUID_UUID : constant DUID_Kind_Type := 4;

   function Parse_UUID (Str : String) return Ada.Streams.Stream_Element_Array;
   --  Parse given UUID string and return as stream element array.

   function Read_DUID_From_File
     (Path      : String;
      DUID_Type : DUID_Kind_Type)
      return Options.Inst.Raw_Option_Type;
   --  Read DUID of specified type from file with given path and return as
   --  DHCPv6 raw option with client identifier code.

private

   function To_Array
     (DUID_Type : DUID_Kind_Type)
      return Ada.Streams.Stream_Element_Array;
   --  Return given DUID type as stream element array.

   Invalid_DUID : exception;

end DHCPv6.DUID;
