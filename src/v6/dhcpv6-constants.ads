--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet;

package DHCPv6.Constants
is

   All_DHCP_Relay_Agents_and_Servers : constant Anet.IPv6_Addr_Type;

   DHCPv6_Client_Port : constant Anet.Port_Type;
   DHCPv6_Server_Port : constant Anet.Port_Type;

   --  Representation of "infinity" as time value, see RFC 3315, section 5.6.

   Infinity : constant Duration := Duration (16#ffff_ffff#);

   --  Transmission and Retransmission parameters, RFC 3315, section 5.5.

   SOL_MAX_DELAY : constant Duration := 1.0;    --  Max delay of first Solicit
   SOL_TIMEOUT   : constant Duration := 1.0;    --  Initial Solicit timeout
   SOL_MAX_RT    : constant Duration := 120.0;  --  Max Solicit timeout value
   REQ_TIMEOUT   : constant Duration := 1.0;    --  Initial Request timeout
   REQ_MAX_RT    : constant Duration := 30.0;   --  Max Request timeout value
   REQ_MAX_RC    : constant          := 10;     --  Max Request retry attempts
   CNF_MAX_DELAY : constant Duration := 1.0;    --  Max delay of first Confirm
   CNF_TIMEOUT   : constant Duration := 1.0;    --  Initial Confirm timeout
   CNF_MAX_RT    : constant Duration := 4.0;    --  Max Confirm timeout
   CNF_MAX_RD    : constant Duration := 10.0;   --  Max Confirm duration
   REN_TIMEOUT   : constant Duration := 10.0;   --  Initial Renew timeout
   REN_MAX_RT    : constant Duration := 600.0;  --  Max Renew timeout value
   REB_TIMEOUT   : constant Duration := 10.0;   --  Initial Rebind timeout
   REB_MAX_RT    : constant Duration := 600.0;  --  Max Rebind timeout value
   INF_MAX_DELAY : constant Duration := 1.0;    --  Max delay of first Inf-req
   INF_TIMEOUT   : constant Duration := 1.0;    --  Initial Inf-request timeout
   INF_MAX_RT    : constant Duration := 120.0;  --  Max Inf-req timeout value
   REL_TIMEOUT   : constant Duration := 1.0;    --  Initial Release timeout
   REL_MAX_RC    : constant          := 5;      --  Max Release attempts
   DEC_TIMEOUT   : constant Duration := 1.0;    --  Initial Decline timeout
   DEC_MAX_RC    : constant          := 5;      --  Max Decline attempts
   REC_TIMEOUT   : constant Duration := 2.0;    --  Initial Reconfigure timeout
   REC_MAX_RC    : constant          := 8;      --  Max Reconfigure attempts

   --  Information Refresh Time constants, RFC 4242, section 3.1.

   IRT_DEFAULT   : constant Duration := 86400.0;
   IRT_MINIMUM   : constant Duration := 600.0;

private

   DHCPv6_Client_Port : constant Anet.Port_Type := 546;
   DHCPv6_Server_Port : constant Anet.Port_Type := 547;

   All_DHCP_Relay_Agents_and_Servers : constant Anet.IPv6_Addr_Type
     := (16#ff#, 16#02#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#00#,
         16#00#, 16#00#, 16#00#, 16#00#, 16#00#, 16#01#, 16#00#, 16#02#);

end DHCPv6.Constants;
