--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;

with Interfaces;

with DHCP.Utils;

package body DHCPv6.Message
is

   HDR_Bytes : constant := 4;

   -------------------------------------------------------------------------

   procedure Add_Option
     (Msg : in out Message_Type;
      Opt :        Options.Inst.Option_Type'Class)
   is
   begin
      Msg.Opts.Append (New_Item => Opt);
   end Add_Option;

   -------------------------------------------------------------------------

   function Create (Kind : Message_Kind) return Message_Type
   is
   begin
      return Message_Type'
        (Kind   => Kind,
         others => <>);
   end Create;

   -------------------------------------------------------------------------

   function Deserialize
     (Buffer : Ada.Streams.Stream_Element_Array)
      return Message_Type
   is
      use Interfaces;
      use type Ada.Streams.Stream_Element_Offset;

      Message    : Message_Type;
      Kind_Value : constant Unsigned_8 := Unsigned_8
        (DHCP.Utils.To_Value (A => Buffer (Buffer'First .. Buffer'First)));
   begin
      if Buffer'Length < HDR_Bytes then
         raise Invalid_Message with "Could not deserialize message - "
           & "invalid buffer size:" & Buffer'Length'Img & " byte(s)";
      end if;

      begin
         Message.Kind := Message_Kind'Enum_Val (Kind_Value);

      exception
         when Constraint_Error =>
            raise Invalid_Message with "Could not deserialize message - "
              & "invalid kind value" & Kind_Value'Img;
      end;

      Message.X_ID := Types.Transaction_ID_Type
        (DHCP.Utils.To_Value
           (A => Buffer (Buffer'First + 1 .. Buffer'First + 3)));

      begin
         Message.Opts := Options.Inst6.Deserialize
           (Buffer => Buffer (Buffer'First + HDR_Bytes .. Buffer'Last));

      exception
         when E : Options.Inst.Invalid_Option =>
            raise Invalid_Message with "Could not deserialize message - "
              & Ada.Exceptions.Exception_Message (X => E);
      end;

      --  Check appearance of options in DHCPv6 message, see RFC 3315,
      --  Appendix B.

      Validate_Opts :
      declare
         Invalid_Options : constant array (1 .. 3) of Options.Option_Name
           := (Options.IA_Address,
               Options.Relay_Message,
               Options.Interface_ID);
      begin
         for O of Invalid_Options loop
            if Message.Opts.Contains (Name => O) then
               raise Invalid_Message with "Could not deserialize message - "
                 & "Illegal option " & O'Img & " present";
            end if;
         end loop;
      end Validate_Opts;

      return Message;
   end Deserialize;

   -------------------------------------------------------------------------

   function Get_Kind (Msg : Message_Type) return Message_Kind is (Msg.Kind);

   -------------------------------------------------------------------------

   function Get_Options (Msg : Message_Type) return Options.Inst6.Option_List
   is (Msg.Opts);

   -------------------------------------------------------------------------

   function Get_Transaction_ID
     (Msg : Message_Type)
      return Types.Transaction_ID_Type
   is (Msg.X_ID);

   -------------------------------------------------------------------------

   function Serialize
     (Message : Message_Type)
      return Ada.Streams.Stream_Element_Array
   is
      use Ada.Streams;

      Opts : constant Stream_Element_Array := Message.Opts.Serialize;
   begin
      return Stream : Stream_Element_Array (1 .. HDR_Bytes + Opts'Length) do
         Stream (1 .. 1) := DHCP.Utils.To_Array
           (Value  => Message_Kind'Enum_Rep (Message.Kind),
            Length => 1);
         Stream (2 .. HDR_Bytes) := DHCP.Utils.To_Array
           (Value  => Interfaces.Unsigned_64 (Message.X_ID),
            Length => 3);
         Stream (1 + HDR_Bytes .. Stream'Last) := Opts;
      end return;
   end Serialize;

   -------------------------------------------------------------------------

   procedure Set_Transaction_ID
     (Msg : in out Message_Type;
      XID :        Types.Transaction_ID_Type)
   is
   begin
      Msg.X_ID := XID;
   end Set_Transaction_ID;

end DHCPv6.Message;
