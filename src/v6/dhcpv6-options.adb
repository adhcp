--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Interfaces;

with Ada.Streams;

with DHCP.Utils;

package body DHCPv6.Options
is

   use Ada.Strings.Unbounded;

   Name_To_Code_Map : constant array (Option_Name) of Code_Type
     := (Client_Identifier        => 1,
         Server_Identifier        => 2,
         IA_NA                    => 3,
         IA_TA                    => 4,
         IA_Address               => 5,
         Option_Request           => 6,
         Preference               => 7,
         Elapsed_Time             => 8,
         Relay_Message            => 9,
         Authentication           => 11,
         Server_Unicast           => 12,
         Status_Code              => 13,
         Rapid_Commit             => 14,
         User_Class               => 15,
         Vendor_Class             => 16,
         Vendor_Information       => 17,
         Interface_ID             => 18,
         Reconfigure_Message      => 19,
         Reconfigure_Accept       => 20,
         Name_Servers             => 23,
         Information_Refresh_Time => 32,
         Unknown                  => Code_Type'Last);

   -------------------------------------------------------------------------

   function Code_To_Name (Code : Code_Type) return Option_Name
   is
      Res : Option_Name := Unknown;
   begin
      Name_To_Code :
      for N in Name_To_Code_Map'Range loop
         if Name_To_Code_Map (N) = Code then
            Res := N;
            exit Name_To_Code;
         end if;
      end loop Name_To_Code;

      return Res;
   end Code_To_Name;

   -------------------------------------------------------------------------

   function Code_To_Tag (Code : Code_Type) return Ada.Tags.Tag
   is
      Tag_Map : constant array (Option_Name) of Ada.Tags.Tag
        := (Preference               => Inst.Byte_Option_Type'Tag,
            IA_NA                    => Inst6.IA_NA_Option_Type'Tag,
            IA_Address               => Inst6.IA_Address_Option_Type'Tag,
            Elapsed_Time             => Inst.U_Int16_Option_Type'Tag,
            Status_Code              => Inst6.Status_Code_Option_Type'Tag,
            Name_Servers             => Inst6.IP_Array_Option_Type'Tag,
            Information_Refresh_Time => Inst.U_Int32_Option_Type'Tag,
            others                   => Inst.Raw_Option_Type'Tag);
   begin
      return Tag_Map (Code_To_Name (Code => Code));
   end Code_To_Tag;

   -------------------------------------------------------------------------

   function Create
     (Address   : Anet.IPv6_Addr_Type;
      Preferred : Duration;
      Valid     : Duration;
      Opts      : Inst6.Option_List'Class := Inst6.Empty_List)
      return Inst6.IA_Address_Option_Type
   is
      use Ada.Streams;

      Data : Ada.Streams.Stream_Element_Array (1 .. 24);
      Idx  : Ada.Streams.Stream_Element_Offset := Data'First;
   begin
      for B of Address loop
         Data (Idx) := Ada.Streams.Stream_Element (B);
         Idx := Idx + 1;
      end loop;
      Data (17 .. 20) := DHCP.Utils.To_Array
        (Value  => Interfaces.Unsigned_64 (Preferred),
         Length => 4);
      Data (21 .. 24) := DHCP.Utils.To_Array
        (Value  => Interfaces.Unsigned_64 (Valid),
         Length => 4);
      return Inst6.IA_Address_Option_Type
        (Inst.Create (Name => IA_Address,
                      Data => Data & Opts.Serialize));
   end Create;

   -------------------------------------------------------------------------

   function Create
     (IAID : DHCP.Types.IAID_Type;
      T1   : Duration;
      T2   : Duration;
      Opts : Inst6.Option_List'Class := Inst6.Empty_List)
      return Inst6.IA_NA_Option_Type
   is
      use type Ada.Streams.Stream_Element_Array;

      Data : Ada.Streams.Stream_Element_Array (1 .. 12);
   begin
      Data (1 .. 4) := DHCP.Utils.To_Array
        (Value  => Interfaces.Unsigned_64 (IAID),
         Length => 4);
      Data (5 .. 8) := DHCP.Utils.To_Array
        (Value  => Interfaces.Unsigned_64 (T1),
         Length => 4);
      Data (9 .. 12) := DHCP.Utils.To_Array
        (Value  => Interfaces.Unsigned_64 (T2),
         Length => 4);
      return Inst6.IA_NA_Option_Type
        (Inst.Create (Name => IA_NA,
                      Data => Data & Opts.Serialize));
   end Create;

   -------------------------------------------------------------------------

   procedure Has_Status_Code
     (Opt        :        Inst.Option_Type'Class;
      Status     :        Inst6.Status_Code_Type;
      Found      : in out Boolean;
      Status_Msg :    out Ada.Strings.Unbounded.Unbounded_String)
   is
   begin
      if Found then
         return;
      end if;

      if Opt.Get_Name = Status_Code then
         declare
            use type Inst6.Status_Code_Type;

            SO : constant Inst6.Status_Code_Option_Type
              := Inst6.Status_Code_Option_Type (Opt);
         begin
            if SO.Get_Status = Status then
               Status_Msg := To_Unbounded_String (SO.Get_Status_Message);
               Found      := True;
               return;
            end if;
         end;
      elsif Opt.Get_Name = IA_Address then
         declare
            Opts : constant Inst6.Option_List'Class
              := Inst6.IA_Address_Option_Type (Opt).Get_Options;
         begin
            Has_Status_Code
              (Opts       => Opts,
               Status     => Status,
               Found      => Found,
               Status_Msg => Status_Msg);
         end;
      elsif Opt.Get_Name = IA_NA then
         declare
            Opts : constant Inst6.Option_List'Class
              := Inst6.IA_NA_Option_Type (Opt).Get_Options;
         begin
            Has_Status_Code
              (Opts       => Opts,
               Status     => Status,
               Found      => Found,
               Status_Msg => Status_Msg);
         end;
      end if;
   end Has_Status_Code;

   -------------------------------------------------------------------------

   procedure Has_Status_Code
     (Opts       :        Inst6.Option_List'Class;
      Status     :        Inst6.Status_Code_Type;
      Found      : in out Boolean;
      Status_Msg :    out Ada.Strings.Unbounded.Unbounded_String)
   is
      procedure Check_Status (Opt : Options.Inst.Option_Type'Class);
      --  Recursively check if the option contains the status code.

      -------------------------------------------------------------------

      procedure Check_Status (Opt : Options.Inst.Option_Type'Class)
      is
      begin
         if Found then
            return;
         end if;

         Options.Has_Status_Code
           (Opt        => Opt,
            Status     => Status,
            Found      => Found,
            Status_Msg => Status_Msg);
      end Check_Status;
   begin
      Opts.Iterate (Process => Check_Status'Access);
   end Has_Status_Code;

   -------------------------------------------------------------------------

   function Is_Config_Option (Name : Option_Name) return Boolean
   is
      Result : Boolean;
   begin
      case Name is
         when Name_Servers
            | Vendor_Information =>
            Result := True;
         when others =>
            Result := False;
      end case;
      return Result;
   end Is_Config_Option;

   -------------------------------------------------------------------------

   function Name_To_Code (Name : Option_Name) return Code_Type
   is (Name_To_Code_Map (Name));

end DHCPv6.Options;
