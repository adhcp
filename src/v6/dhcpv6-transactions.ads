--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCPv6.States.Init;

private with DHCP.Types;
private with DHCP.Timing_Events.Timer_Expiry;
private with DHCPv6.Message;
private with DHCPv6.Types;
private with DHCPv6.Options;

package DHCPv6.Transactions
is

   type Transaction_Type is new States.Transaction_Type
     (Initial_State => States.Init.State) with private;
   --  The transaction type implements a DHCPv6 transaction encompassing it's
   --  associated data and current state.

private

   type Transaction_Type is new States.Transaction_Type
     (Initial_State => States.Init.State) with record
      ID            : Types.Transaction_ID_Type := 0;
      RT_Count      : Natural                   := 0;
      RT            : Duration                  := 0.0;
      IRT           : Duration                  := 0.0;
      MRC           : Natural                   := 0;
      MRT           : Duration                  := 0.0;
      MRD           : Duration                  := 0.0;
      Last_Msg      : Message.Message_Type;
      Retransmit_Ev : DHCP.Timing_Events.Timer_Expiry.Expiry_Type
        (T          => Transaction_Type'Access,
         Timer_Kind => DHCP.Types.Retransmit);
   end record;

   procedure Schedule_Retransmit
     (Transaction : in out Transaction_Type;
      In_Time     :        Duration);
   --  Schedule retransmission event of the transaction to expire in the given
   --  amount of time from now.

   overriding
   procedure Reset_Retransmit (Transaction : in out Transaction_Type)
   with
      Post => Transaction.RT_Count = 0 and Transaction.RT = Transaction.IRT;

   subtype Elapsed_Duration is Duration range 0.0 .. Duration (2 ** 16 - 1);

   function Get_Elapsed_Time
     (Transaction : Transaction_Type)
      return Elapsed_Duration
   with
      Post =>
        Get_Elapsed_Time'Result <= Duration (2 ** 16 - 1) and
        (if Transaction.RT_Count = 0 then Get_Elapsed_Time'Result = 0.0);
   --  Return the time elapsed since the transaction was started in 100th of a
   --  second. If no message has been sent yet, then zero is returned.

   overriding
   procedure Start (Transaction : in out Transaction_Type);

   overriding
   procedure Stop (Transaction : in out Transaction_Type);

   overriding
   procedure Set_ID
     (Transaction : in out Transaction_Type;
      XID         :        Types.Transaction_ID_Type);

   overriding
   function Get_ID
     (Transaction : Transaction_Type)
      return Types.Transaction_ID_Type;

   overriding
   procedure Reset (Transaction : in out Transaction_Type);

   overriding
   procedure Send_Message
     (Transaction : in out Transaction_Type;
      Msg         :         Message.Message_Type);

   use type DHCPv6.Types.Transaction_ID_Type;

   overriding
   procedure Process_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type)
   with
      Pre => Transaction.Get_ID = Msg.Get_Transaction_ID;

   overriding
   procedure Process_Timer_Expiry
     (Transaction : in out Transaction_Type;
      Timer_Kind  :        DHCP.Types.DHCP_Timer_Kind);

   overriding
   procedure Set_Retransmission_Params
     (Transaction : in out Transaction_Type;
      IRT         :        Duration;
      MRC         :        Natural;
      MRT         :        Duration;
      MRD         :        Duration);

   overriding
   procedure Increase_Retransmission_Params
     (Transaction : in out Transaction_Type;
      Random      :        Types.Rand_Duration);

   overriding
   function Max_Retransmit_Count_Reached
     (Transaction : Transaction_Type)
      return Boolean;

   overriding
   function Max_Retransmit_Duration_Reached
     (Transaction : Transaction_Type)
      return Boolean;

   use type DHCPv6.Options.Option_Name;

   type Option_Names is array (Positive range <>) of Options.Option_Name;

   No_Opts : constant Option_Names (2 .. 1) := (others => <>);

   function Valid_Message_Options
     (Msg           : Message.Message_Type;
      Required_Opts : Option_Names;
      Invalid_Opts  : Option_Names)
      return Boolean;
   --  Returns True if the message contains each required and no invalid
   --  option.

end DHCPv6.Transactions;
