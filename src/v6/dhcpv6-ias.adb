--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Strings.Hash;

with DHCPv6.Constants;

package body DHCPv6.IAs
is

   -------------------------------------------------------------------------

   procedure Add_Address
     (IA      : in out Identity_Association_Type;
      Address :        Options.Inst6.IA_Address_Option_Type)
   is
      use type MOA.Cursor;

      Addr : constant Anet.IPv6_Addr_Type := Address.Get_Address;
      Pos  : constant MOA.Cursor          := IA.Addresses.Find (Key => Addr);
   begin
      if Pos = MOA.No_Element then
         IA.Addresses.Insert (Key      => Addr,
                              New_Item => Address);
      else
         IA.Addresses.Replace_Element
           (Position => Pos,
            New_Item => Address);
      end if;
   end Add_Address;

   -------------------------------------------------------------------------

   function Create
     (IAID : DHCP.Types.IAID_Type)
      return Identity_Association_Type
   is
   begin
      return Identity_Association_Type'
        (IAID   => IAID,
         others => <>);
   end Create;

   -------------------------------------------------------------------------

   procedure Discard_Expired_Addresses
     (IA            : in out Identity_Association_Type;
      Removed_Addrs :    out Options.Inst6.Option_List)
   is
      use Ada.Real_Time;
      use type MOA.Cursor;

      Now : constant Time := Clock;
      Pos : MOA.Cursor    := IA.Addresses.First;
   begin
      Removed_Addrs := Options.Inst6.Empty_List;
      while Pos /= MOA.No_Element loop
         declare
            Cur_Pos  : MOA.Cursor := Pos;
            Cur_Addr : constant Options.Inst6.IA_Address_Option_Type
              := MOA.Element (Position => Pos);
            Valid, Pref_Unused : Duration;
         begin
            MOA.Next (Position => Pos);
            Cur_Addr.Get_Lifetimes
              (Preferred => Pref_Unused,
               Valid     => Valid);
            if Valid = 0.0 or
              (Valid /= Constants.Infinity
               and then IA.Timestamp < Now - To_Time_Span (D => Valid))
            then
               IA.Addresses.Delete (Position => Cur_Pos);
               Removed_Addrs.Append (New_Item => Cur_Addr);
            end if;
         end;
      end loop;
   end Discard_Expired_Addresses;

   -------------------------------------------------------------------------

   function Get_Addresses
     (IA : Identity_Association_Type)
      return Options.Inst6.Option_List'Class
   is
      use type MOA.Cursor;

      Addrs : Options.Inst6.Option_List;
      Pos   : MOA.Cursor := IA.Addresses.First;
   begin
      while Pos /= MOA.No_Element loop
         Addrs.Append (New_Item => MOA.Element (Position => Pos));
         MOA.Next (Position => Pos);
      end loop;

      return Addrs;
   end Get_Addresses;

   -------------------------------------------------------------------------

   function Get_Expiry (IA : Identity_Association_Type) return Duration
   is
      use type MOA.Cursor;

      Time_Max : Duration := 0.0;

      Pos : MOA.Cursor := IA.Addresses.First;
   begin
      while Pos /= MOA.No_Element loop
         declare
            Valid, Pref_Unused : Duration;
         begin
            MOA.Element (Position => Pos).Get_Lifetimes
              (Preferred => Pref_Unused,
               Valid     => Valid);

            if Valid > Time_Max then
               Time_Max := Valid;
            end if;
         end;
         MOA.Next (Position => Pos);
      end loop;

      return Time_Max;
   end Get_Expiry;

   -------------------------------------------------------------------------

   function Get_ID
     (IA : Identity_Association_Type)
      return DHCP.Types.IAID_Type
   is (IA.IAID);

   -------------------------------------------------------------------------

   function Get_T1 (IA : Identity_Association_Type) return Duration
   is (IA.T1);

   -------------------------------------------------------------------------

   function Get_T2 (IA : Identity_Association_Type) return Duration
   is (IA.T2);

   -------------------------------------------------------------------------

   function Get_Timestamp
     (IA : Identity_Association_Type)
      return Ada.Real_Time.Time
   is (IA.Timestamp);

   -------------------------------------------------------------------------

   function Hash_Addr
     (Key : Anet.IPv6_Addr_Type)
      return Ada.Containers.Hash_Type
   is (Ada.Strings.Hash (Key => Anet.To_String (Address => Key)));

   -------------------------------------------------------------------------

   procedure Iterate
     (IA      : Identity_Association_Type;
      Process : not null access procedure
        (Address : Options.Inst6.IA_Address_Option_Type))
   is
      procedure Call_Process (Position : MOA.Cursor);
      --  Call process procedure for address at given list position.

      ----------------------------------------------------------------------

      procedure Call_Process (Position : MOA.Cursor)
      is
      begin
         Process (Address => MOA.Element (Position => Position));
      end Call_Process;
   begin
      IA.Addresses.Iterate (Process => Call_Process'Access);
   end Iterate;

   -------------------------------------------------------------------------

   procedure Remove_Address
     (IA      : in out Identity_Association_Type;
      Address :        Anet.IPv6_Addr_Type)
   is
      use type MOA.Cursor;

      Pos : MOA.Cursor  := IA.Addresses.Find (Key => Address);
   begin
      if Pos /= MOA.No_Element then
         IA.Addresses.Delete (Position => Pos);
      end if;
   end Remove_Address;

   -------------------------------------------------------------------------

   procedure Set_Timeouts
     (IA : in out Identity_Association_Type;
      T1 :        Duration;
      T2 :        Duration)
   is
   begin
      IA.T1 := T1;
      IA.T2 := T2;
   end Set_Timeouts;

   -------------------------------------------------------------------------

   procedure Set_Timestamp
     (IA        : in out Identity_Association_Type;
      Timestamp :        Ada.Real_Time.Time)
   is
   begin
      IA.Timestamp := Timestamp;
   end Set_Timestamp;

   -------------------------------------------------------------------------

   function To_Option
     (IA : Identity_Association_Type)
      return Options.Inst6.IA_NA_Option_Type
   is
   begin
      return Options.Create
        (IAID => IA.IAID,
         T1   => IA.T1,
         T2   => IA.T2,
         Opts => Get_Addresses (IA => IA));
   end To_Option;

end DHCPv6.IAs;
