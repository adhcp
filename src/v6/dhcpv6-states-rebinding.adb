--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Notify;
with DHCP.Logger;

with DHCPv6.Database;
with DHCPv6.Options;

package body DHCPv6.States.Rebinding
is

   package L renames DHCP.Logger;

   Instance : aliased Rebinding_State_Type;

   -------------------------------------------------------------------------

   function Get_Name
     (State : not null access Rebinding_State_Type)
      return String
   is ("Rebinding");

   -------------------------------------------------------------------------

   procedure Notify_Allocation (State : access Rebinding_State_Type)
   is
      pragma Unreferenced (State);
   begin
      DHCP.Notify.Update (Reason => DHCP.Notify.Rebind);
   end Notify_Allocation;

   -------------------------------------------------------------------------

   procedure Process_Lease_Expiry
     (State       : access Rebinding_State_Type;
      Transaction : in out Transaction_Type'Class)
   is
      pragma Unreferenced (State);
   begin
      L.Log (Message => "Lease expired, restarting");
      Transaction.Restart_Server_Discovery;
   end Process_Lease_Expiry;

   -------------------------------------------------------------------------

   procedure Process_Reply_Msg
     (State       : access Rebinding_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      Opts      : constant Options.Inst6.Option_List := Msg.Get_Options;
      Server_ID : constant Options.Inst.Option_Type'Class
        := Opts.Get (Name => Options.Server_Identifier);
   begin
      L.Log (Message => "Rebinding with server '"
             & Server_ID.Get_Data_Str & "'");

      Database.Add_Global_Option (Opt => Server_ID);

      Allocation_State_Type (State.all).Process_Reply_Msg
        (Transaction => Transaction,
         Msg         => Msg);
   end Process_Reply_Msg;

   -------------------------------------------------------------------------

   function State return DHCP.States.State_Handle
   is (Instance'Access);

end DHCPv6.States.Rebinding;
