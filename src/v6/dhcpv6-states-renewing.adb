--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

with DHCP.Logger;
with DHCP.Notify;
with DHCP.Random;

with DHCPv6.Constants;
with DHCPv6.Database;
with DHCPv6.IAs;
with DHCPv6.Message;
with DHCPv6.Options;
with DHCPv6.Types;
with DHCPv6.States.Rebinding;

package body DHCPv6.States.Renewing
is

   package L renames DHCP.Logger;

   Instance : aliased Renewing_State_Type;

   -------------------------------------------------------------------------

   function Get_Name
     (State : not null access Renewing_State_Type)
      return String
   is ("Renewing");

   -------------------------------------------------------------------------

   procedure Notify_Allocation (State : access Renewing_State_Type)
   is
      pragma Unreferenced (State);
   begin
      DHCP.Notify.Update (Reason => DHCP.Notify.Renew);
   end Notify_Allocation;

   -------------------------------------------------------------------------

   procedure Process_T2_Expiry
     (State       : access Renewing_State_Type;
      Transaction : in out Transaction_Type'Class)
   is
      pragma Unreferenced (State);

      Now : constant Ada.Real_Time.Time := Ada.Real_Time.Clock;
      XID : constant Types.Transaction_ID_Type
        := Types.Transaction_ID_Type'Mod (DHCP.Random.Get);
      Req : Message.Message_Type := Message.Create (Kind => Message.Rebind);

      MRD : Duration := 0.0;

      procedure Add_IA_Option (IA : IAs.Identity_Association_Type);
      --  Check if IA has expired and add corresponding option to renew
      --  message.

      ----------------------------------------------------------------------

      procedure Add_IA_Option (IA : IAs.Identity_Association_Type)
      is
         use Ada.Real_Time;

         Timeout : constant Ada.Real_Time.Time := IAs.Get_Timestamp
           (IA => IA) + To_Time_Span (D => IAs.Get_T2 (IA => IA));
         Expiry  : constant Ada.Real_Time.Time := IAs.Get_Timestamp (IA => IA)
           + To_Time_Span (D => IAs.Get_Expiry (IA => IA));
      begin
         if Timeout < Now then
            Req.Add_Option (Opt => IAs.To_Option (IA => IA));
         end if;

         if Expiry > Now + To_Time_Span (MRD) then
            MRD := To_Duration (Expiry - Now);
         end if;
      end Add_IA_Option;
   begin
      L.Log (Message => "Rebinding IA(s) using transaction with ID"
             & XID'Img);

      Transaction.Set_ID (XID => XID);

      --  Rebind retransmission params, RFC 3315, section 18.1.4.

      Transaction.Set_Retransmission_Params
        (IRT => Constants.REB_TIMEOUT,
         MRT => Constants.REB_MAX_RT,
         MRC => 0,
         MRD => (if MRD = Constants.Infinity then 0.0 else MRD));

      Req.Set_Transaction_ID (XID => XID);
      Req.Add_Option (Opt => Database.Get_Client_ID);
      Req.Add_Option (Opt => Options.Inst6.Default_Request_Option);

      Database.Iterate (Process => Add_IA_Option'Access);

      Transaction.Set_Start_Time;
      Transaction.Send_Message (Msg => Req);
      Transaction.Set_State (State => Rebinding.State);
   end Process_T2_Expiry;

   -------------------------------------------------------------------------

   function State return DHCP.States.State_Handle
   is (Instance'Access);

end DHCPv6.States.Renewing;
