--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with System.Assertions;

with Ada.Characters.Handling;

with DHCP.OS;
with DHCP.Utils;

package body DHCPv6.DUID
is

   -------------------------------------------------------------------------

   function Parse_UUID (Str : String) return Ada.Streams.Stream_Element_Array
   is
      use type Ada.Streams.Stream_Element_Offset;

      subtype Hex_String is String
        with Dynamic_Predicate =>
          (for all Char of Hex_String
           => Ada.Characters.Handling.Is_Hexadecimal_Digit (Item => Char));

      Hex_Str : constant Hex_String
        := (if Str'Length mod 2 /= 0 then "0" else "")
        & DHCP.Utils.Strip
        (Str     => Str,
         Pattern => "-");

      Result : Ada.Streams.Stream_Element_Array (1 .. Hex_Str'Length / 2);
      Idx    : Natural := Hex_Str'First;
   begin
      for I in Result'Range loop
         Result (I) := Ada.Streams.Stream_Element'Value
           ("16#" & Hex_Str (Idx .. Idx + 1) & "#");
         Idx        := Idx + 2;
      end loop;

      return Result;
   end Parse_UUID;

   -------------------------------------------------------------------------

   function Read_DUID_From_File
     (Path      : String;
      DUID_Type : DUID_Kind_Type)
      return Options.Inst.Raw_Option_Type
   is
      use type Ada.Streams.Stream_Element_Offset;

      subtype DUID_Data_Array is Ada.Streams.Stream_Element_Array
        with Predicate => DUID_Data_Array'Length <= 128;
   begin
      if DUID_Type /= DUID_UUID then
         declare
            DUID_Data : constant DUID_Data_Array
              := DHCP.OS.Read_File (Filename => Path);
            Data      : Ada.Streams.Stream_Element_Array
              (1 .. DUID_Data'Length + 2);
         begin
            Data (1 .. 2) := To_Array (DUID_Type => DUID_Type);
            Data (3 .. DUID_Data'Length + 2) := DUID_Data;

            return Options.Inst.Raw_Option_Type
              (Options.Inst.Create
                 (Name => Options.Client_Identifier,
                  Data => Data));
         end;
      end if;

      declare
         DUID_Data : constant DUID_Data_Array
           := Parse_UUID (Str => DHCP.OS.Read_File (Filename => Path));
         Data      : Ada.Streams.Stream_Element_Array
           (1 .. DUID_Data'Length + 2);
      begin
         Data (1 .. 2) := To_Array (DUID_Type => DUID_Type);
         Data (3 .. DUID_Data'Length + 2) := DUID_Data;

         return Options.Inst.Raw_Option_Type
           (Options.Inst.Create
              (Name => Options.Client_Identifier,
               Data => Data));
      end;

   exception
      when System.Assertions.Assert_Failure =>
         raise Invalid_DUID with "Invalid data for DUID type" & DUID_Type'Img
           & " in file '" & Path & "'";
   end Read_DUID_From_File;

   --------------------------------------------------------------------------

   function To_Array
     (DUID_Type : DUID_Kind_Type)
      return Ada.Streams.Stream_Element_Array
   is
   begin
      return (1 => Ada.Streams.Stream_Element'Mod (DUID_Type / 2 ** 8),
              2 => Ada.Streams.Stream_Element'Mod (DUID_Type));
   end To_Array;

end DHCPv6.DUID;
