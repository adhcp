--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package DHCPv6.States.Renewing
is

   type Renewing_State_Type is new Allocation_State_Type with private;
   --  The T1 time of an IA has expired and the client is trying to renew
   --  associated addresses by sending Renew messages to the server that issued
   --  the lease.

   overriding
   function Get_Name
     (State : not null access Renewing_State_Type)
      return String;
   --  Return name of state as string.

   overriding
   procedure Process_T2_Expiry
     (State       : access Renewing_State_Type;
      Transaction : in out Transaction_Type'Class);
   --  Start Rebind/Reply exchange to extend lifetime of IAs with any available
   --  server.

   overriding
   procedure Notify_Allocation (State : access Renewing_State_Type);
   --  Signal renewal of lease information.

   function State return DHCP.States.State_Handle;
   --  Return access to singleton.

private

   type Renewing_State_Type is new Allocation_State_Type with null record;

end DHCPv6.States.Renewing;
