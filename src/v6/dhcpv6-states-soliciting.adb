--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Logger;
with DHCP.Random;

with DHCPv6.Constants;
with DHCPv6.Database;
with DHCPv6.Options;
with DHCPv6.States.Requesting;

package body DHCPv6.States.Soliciting
is
   package L   renames DHCP.Logger;
   package OI6 renames DHCPv6.Options.Inst6;

   Instance : aliased Soliciting_State_Type;

   -------------------------------------------------------------------------

   function Get_Name
     (State : not null access Soliciting_State_Type)
      return String
   is ("Soliciting");

   -------------------------------------------------------------------------

   procedure Process_Advertise_Msg
     (State       : access Soliciting_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      pragma Unreferenced (State);

      Opts      : constant OI6.Option_List := Msg.Get_Options;
      XID       : constant Types.Transaction_ID_Type
        := Types.Transaction_ID_Type'Mod (DHCP.Random.Get);
      Server_ID : constant Options.Inst.Option_Type'Class
        := Opts.Get (Name => Options.Server_Identifier);
      Req       : Message.Message_Type
        := Message.Create (Kind => Message.Request);
   begin
      L.Log (Message => "Requesting IA(s) from server '"
             & Server_ID.Get_Data_Str & "' using transaction with ID"
             & XID'Img);

      Database.Add_Global_Option (Opt => Server_ID);

      Transaction.Set_ID (XID => XID);

      --  Request retransmission params, RFC 3315, section 18.1.1.

      Transaction.Set_Retransmission_Params
        (IRT => Constants.REQ_TIMEOUT,
         MRT => Constants.REQ_MAX_RT,
         MRC => Constants.REQ_MAX_RC,
         MRD => 0.0);

      Req.Set_Transaction_ID (XID => XID);
      Req.Add_Option (Opt => Database.Get_Client_ID);
      Req.Add_Option (Opt => Server_ID);
      Req.Add_Option (Opt => Options.Inst6.Default_Request_Option);

      declare
         procedure Add_IA_Option (Opt : Options.Inst.Option_Type'Class);
         --  Check if option is IA_NA and add to request message if so.

         procedure Add_IA_Option (Opt : Options.Inst.Option_Type'Class)
         is
            use type Options.Option_Name;
         begin
            if Opt.Get_Name = Options.IA_NA
              and then Database.Contains
                (IA => OI6.IA_NA_Option_Type (Opt).Get_IAID)
            then
               Req.Add_Option (Opt => Opt);
            end if;
         end Add_IA_Option;
      begin
         Opts.Iterate (Process => Add_IA_Option'Access);
      end;

      Transaction.Set_Start_Time;
      Transaction.Send_Message (Msg => Req);
      Transaction.Set_State (State => Requesting.State);
   end Process_Advertise_Msg;

   -------------------------------------------------------------------------

   function State return DHCP.States.State_Handle
   is (Instance'Access);

end DHCPv6.States.Soliciting;
