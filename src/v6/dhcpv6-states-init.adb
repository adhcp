--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Logger;
with DHCP.Notify;
with DHCP.Random;
with DHCP.Types;

with DHCPv6.Constants;
with DHCPv6.Database;
with DHCPv6.IAs;
with DHCPv6.Message;
with DHCPv6.Options;
with DHCPv6.States.Soliciting;

package body DHCPv6.States.Init
is

   package L renames DHCP.Logger;

   Instance : aliased Init_State_Type;

   -------------------------------------------------------------------------

   function Get_Name (State : not null access Init_State_Type) return String
   is ("Init");

   -------------------------------------------------------------------------

   procedure Start
     (State       : access Init_State_Type;
      Transaction : in out DHCP.States.Root_Context_Type'Class)
   is
      pragma Unreferenced (State);

      ID         : constant Types.Transaction_ID_Type
        := Types.Transaction_ID_Type'Mod (DHCP.Random.Get);
      Init_Delay : constant Duration
        := Duration (DHCP.Random.Get
          (Low  => 0.0,
           High => Float (Constants.SOL_MAX_DELAY)));
      Req        : Message.Message_Type
        := Message.Create (Kind => Message.Solicit);
   begin
      L.Log (Message => "Starting new stateful transaction with ID" & ID'Img
             & ", delay" & Long_Integer (Init_Delay)'Img);
      Transaction_Type'Class (Transaction).Set_ID (XID => ID);

      delay Init_Delay;

      --  Solicit retransmission params, RFC 3315, section 17.1.2.

      Transaction_Type'Class (Transaction).Set_Retransmission_Params
        (IRT => Constants.SOL_TIMEOUT,
         MRT => Constants.SOL_MAX_RT,
         MRC => 0,
         MRD => 0.0);
      Transaction_Type'Class (Transaction).Set_Start_Time;

      DHCP.Notify.Update (Reason => DHCP.Notify.Preinit);

      Req.Set_Transaction_ID (XID => ID);
      Req.Add_Option (Opt => Database.Get_Client_ID);
      Req.Add_Option (Opt => Options.Inst6.Default_Request_Option);

      declare
         IAID : constant DHCP.Types.IAID_Type
           := DHCP.Types.IAID_Type (DHCP.Random.Get);
         IA   : constant IAs.Identity_Association_Type
           := IAs.Create (IAID => IAID);
      begin
         Database.Add_IA (IA => IA);
         Req.Add_Option (Opt => IAs.To_Option (IA => IA));
      end;

      Transaction_Type'Class (Transaction).Send_Message (Msg => Req);

      Transaction.Set_State (State => Soliciting.State);
   end Start;

   -------------------------------------------------------------------------

   function State return DHCP.States.State_Handle
   is
   begin
      return Instance'Access;
   end State;

end DHCPv6.States.Init;
