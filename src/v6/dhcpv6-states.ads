--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.States;

with DHCPv6.Types;
with DHCPv6.Message;

package DHCPv6.States
is

   use type Message.Message_Kind;
   use type Types.Transaction_ID_Type;

   type State_Type (<>) is
     abstract new DHCP.States.Root_State_Type with private;

   type Transaction_Type is
     abstract new DHCP.States.Root_Context_Type with private;

   procedure Set_ID
     (Transaction : in out Transaction_Type;
      XID         :        Types.Transaction_ID_Type)
   is abstract;
   --  Set ID of transaction.

   function Get_ID
     (Transaction : Transaction_Type)
      return Types.Transaction_ID_Type
   is abstract;
   --  Get ID of transaction.

   procedure Reset (Transaction : in out Transaction_Type)
   is abstract;
   --  Reset transaction data fields to their initial value.

   procedure Stop (Transaction : in out Transaction_Type)
   is abstract;
   --  Stop transaction and release lease information if necessary.

   procedure Send_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type)
   is abstract;
   --  Send given DHCPv6 message.

   procedure Process_Message
     (Transaction : in out Transaction_Type;
      Msg         :        Message.Message_Type)
   is abstract;
   --  Process DHCPv6 message.

   procedure Set_Retransmission_Params
     (Transaction : in out Transaction_Type;
      IRT         :        Duration;
      MRC         :        Natural;
      MRT         :        Duration;
      MRD         :        Duration)
   is abstract;
   --  Set message retransmission parameters of transaction to given values.

   procedure Increase_Retransmission_Params
     (Transaction : in out Transaction_Type;
      Random      :        Types.Rand_Duration)
   is abstract;
   --  Increment transaction retransmission parameters with given randomization
   --  value according to the algorithm specified in RFC 3315, section 14.

   function Max_Retransmit_Count_Reached
     (Transaction : Transaction_Type)
      return Boolean
      is abstract;
   --  Returns True if the maximum retransmission count has been reached for
   --  the given transaction.

   function Max_Retransmit_Duration_Reached
     (Transaction : Transaction_Type)
      return Boolean
      is abstract;
   --  Returns True if the maximum retransmission duration has been reached for
   --  the given transaction.

   procedure Reset_Retransmit (Transaction : in out Transaction_Type)
   is abstract;
   --  Cancel message retransmission and reset retransmission parameters to
   --  initial values.

   procedure Restart_Server_Discovery (Transaction : in out Transaction_Type);
   --  Reset transaction, transition to initial state and execute Start
   --  procedure to restart server solicitation.

   overriding
   procedure Start
     (State       : access State_Type;
      Transaction : in out DHCP.States.Root_Context_Type'Class) is null;
   --  Start DHCP transaction.

   procedure Stop
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class);
   --  Stop DHCP transaction. Release any bound IAs.

   procedure Process_Advertise_Msg
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type) is null
   with
      Pre'Class =>
        Msg.Get_Kind = Message.Advertise and
        Msg.Get_Transaction_ID = Transaction.Get_ID;
   --  Process DHCPv6 advertise message.

   procedure Process_Reply_Msg
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type) is null
   with
      Pre'Class =>
        Msg.Get_Kind = Message.Reply and
        Msg.Get_Transaction_ID = Transaction.Get_ID;
   --  Process DHCPv6 reply message.

   procedure Process_Transmission_Failure
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type) is null;
   --  Process failed transmission (incl. retransmissions) of given message.

   procedure Process_T1_Expiry
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class) is null;
   --  Process expiration of T1 timer.

   procedure Process_T2_Expiry
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class) is null;
   --  Process expiration of T2 timer.

   procedure Process_Lease_Expiry
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class) is null;
   --  Process expiration of DHCP lease associated with given transaction.

   type Allocation_State_Type is abstract new State_Type with private;
   --  Parent type for all states which allocate a lease (Requesting, Renewing
   --  and Rebinding).

   overriding
   procedure Process_Reply_Msg
     (State       : access Allocation_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type);
   --  Extract IA and configuration information from reply message and store it
   --  in the database. Transition to Bound if message was processed
   --  successfully.

   procedure Notify_Allocation (State : access Allocation_State_Type)
   is abstract;
   --  Send notification update about lease allocation.

private

   type Transaction_Type is
     abstract new DHCP.States.Root_Context_Type with null record;

   type State_Type is
     abstract new DHCP.States.Root_State_Type with null record;

   type Allocation_State_Type is abstract new State_Type with null record;

end DHCPv6.States;
