--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

private with Ada.Strings.Unbounded;

private with GNAT.Command_Line;

with DHCPv6.DUID;

package DHCPv6.Cmd_Line
is

   function Get_Interface_Name return String;
   --  Get name of network interface specified on command line.

   function Get_DUID_Path return String;
   --  Return the path to the file containing the client DUID. The default path
   --  is '/sys/class/dmi/id/product_uuid'.

   function Get_DUID_Type return DUID.DUID_Kind_Type;
   --  Return the DUID type of the client DUID contained in the file specified
   --  by the DUID path. Default type is 4.

   function In_Stateless_Mode return Boolean;
   --  Returns True if DHCPv6 stateless mode is enabled.

   procedure Parse;
   --  Parse command line arguments.

   Invalid_Cmd_Line : exception;

private

   use Ada.Strings.Unbounded;

   type Cmd_Line_Data_Type is record
      Iface_Name : Unbounded_String;
      --  Name of network interface to configure.

      DUID_Path  : Unbounded_String := To_Unbounded_String
        (Source => "/sys/class/dmi/id/product_uuid");
      --  Path of client DUID file.

      DUID_Type : DUID.DUID_Kind_Type := 4;
      --  Type of the client DUID.

      Stateless : Boolean := False;
      --  Stateless mode operation flag.

      Cmd_Parser : GNAT.Command_Line.Opt_Parser
        := GNAT.Command_Line.Command_Line_Parser;
   end record;

   Instance : Cmd_Line_Data_Type;

end DHCPv6.Cmd_Line;
