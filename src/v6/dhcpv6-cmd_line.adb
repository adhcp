--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Finalization;

with GNAT.Strings;

with Anet.Types;

with DHCP.Version;
with DHCP.Cmd_Line;

package body DHCPv6.Cmd_Line
is

   Default_Notifier : constant String := "/usr/local/sbin/adhcp_notify6_dbus";

   type Config_Type is new
     Ada.Finalization.Limited_Controlled with record
      Data : GNAT.Command_Line.Command_Line_Configuration;
   end record;

   overriding
   procedure Finalize (Config : in out Config_Type);

   -------------------------------------------------------------------------

   procedure Finalize (Config : in out Config_Type)
   is
   begin
      GNAT.Command_Line.Free (Config => Config.Data);
   end Finalize;

   -------------------------------------------------------------------------

   function Get_DUID_Path return String
   is (Ada.Strings.Unbounded.To_String (Source => Instance.DUID_Path));

   -------------------------------------------------------------------------

   function Get_DUID_Type return DUID.DUID_Kind_Type
   is (Instance.DUID_Type);

   -------------------------------------------------------------------------

   function Get_Interface_Name return String
   is (Ada.Strings.Unbounded.To_String (Source => Instance.Iface_Name));

   -------------------------------------------------------------------------

   function In_Stateless_Mode return Boolean
   is (Instance.Stateless);

   -------------------------------------------------------------------------

   procedure Parse
   is
      Cmdline         : Config_Type;
      Cmd_Iface_Name  : aliased GNAT.Strings.String_Access := null;
      Cmd_Notify_Path : aliased GNAT.Strings.String_Access := null;
      Cmd_DUID_Path   : aliased GNAT.Strings.String_Access := null;
      Cmd_DUID_Type   : aliased Integer;
      Cmd_Stateless   : aliased Boolean;

      procedure Free_Strings;
      --  Free memory of command argument strings.

      ----------------------------------------------------------------------

      procedure Free_Strings
      is
         use type GNAT.Strings.String_Access;
      begin
         if Cmd_Iface_Name /= null then
            GNAT.Strings.Free (X => Cmd_Iface_Name);
         end if;
         if Cmd_Notify_Path /= null then
            GNAT.Strings.Free (X => Cmd_Notify_Path);
         end if;
         if Cmd_DUID_Path /= null then
            GNAT.Strings.Free (X => Cmd_DUID_Path);
         end if;
      end Free_Strings;
   begin
      DHCP.Cmd_Line.Set_Ext_Notify_Binary (B => Default_Notifier);

      GNAT.Command_Line.Set_Usage
        (Config => Cmdline.Data,
         Usage  => "-i <interface name>",
         Help   => "DHCPv6 client, version " & DHCP.Version.Version_String);
      GNAT.Command_Line.Define_Switch
        (Config      => Cmdline.Data,
         Switch      => "-h",
         Long_Switch => "--help",
         Help        => "Display usage and exit");
      GNAT.Command_Line.Define_Switch
        (Config => Cmdline.Data,
         Output => Cmd_Iface_Name'Access,
         Switch => "-i:",
         Help   => "interface to configure");
      GNAT.Command_Line.Define_Switch
        (Config => Cmdline.Data,
         Output => Cmd_Notify_Path'Access,
         Switch => "-e:",
         Help   => "external notification binary to call");
      GNAT.Command_Line.Define_Switch
        (Config => Cmdline.Data,
         Output => Cmd_DUID_Path'Access,
         Switch => "-u:",
         Help   => "Path to file containing DUID (default: "
         & To_String (Instance.DUID_Path) & ")");
      GNAT.Command_Line.Define_Switch
        (Config  => Cmdline.Data,
         Output  => Cmd_DUID_Type'Access,
         Switch  => "-t:",
         Help    => "DUID type (default:" & Instance.DUID_Type'Img & ")",
         Initial => Integer (Instance.DUID_Type),
         Default => Integer (Instance.DUID_Type));
      GNAT.Command_Line.Define_Switch
        (Config => Cmdline.Data,
         Output => Cmd_Stateless'Access,
         Switch => "-s",
         Help   => "Stateless mode");
      begin
         GNAT.Command_Line.Getopt
           (Config => Cmdline.Data,
            Parser => Instance.Cmd_Parser);
         if Anet.Types.Is_Valid_Iface (Name => Cmd_Iface_Name.all) then
            Instance.Iface_Name := To_Unbounded_String (Cmd_Iface_Name.all);
         else
            raise Invalid_Cmd_Line with "No valid interface specified";
         end if;

         if Cmd_Notify_Path'Length /= 0 then
            DHCP.Cmd_Line.Set_Ext_Notify_Binary (B => Cmd_Notify_Path.all);
         end if;

         if Cmd_DUID_Type /= Integer (Instance.DUID_Type) then
            if Cmd_DUID_Type > Integer (DUID.DUID_Kind_Type'Last)
              or else Cmd_DUID_Type < Integer (DUID.DUID_Kind_Type'First)
            then
               raise Invalid_Cmd_Line with "DUID type not "
                 & "in allowed range" & DUID.DUID_Kind_Type'First'Img & " .."
                 & DUID.DUID_Kind_Type'Last'Img;
            end if;
            if Cmd_DUID_Path'Length = 0 then
               raise Invalid_Cmd_Line with "DUID type specified but no path to"
                 & " DUID file given";
            end if;
            Instance.DUID_Type := DUID.DUID_Kind_Type (Cmd_DUID_Type);
         end if;

         if Cmd_DUID_Path'Length /= 0 then
            Instance.DUID_Path := To_Unbounded_String (Cmd_DUID_Path.all);
         end if;

         Instance.Stateless := Cmd_Stateless;

         Free_Strings;

      exception
         when GNAT.Command_Line.Invalid_Parameter  =>
            GNAT.Command_Line.Display_Help (Config => Cmdline.Data);
            raise;
      end;

   exception
      when others =>
         Free_Strings;
         raise;
   end Parse;

end DHCPv6.Cmd_Line;
