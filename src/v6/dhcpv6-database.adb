--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCPv6.Database
is

   -------------------------------------------------------------------------

   procedure Add_Global_Option (Opt : Options.Inst.Option_Type'Class)
   is
   begin
      Remove_Global_Option (Name => Opt.Get_Name);
      Instance.Global_Opts.Append (New_Item => Opt);
   end Add_Global_Option;

   -------------------------------------------------------------------------

   procedure Add_IA (IA : IAs.Identity_Association_Type)
   is
      ID       : constant DHCP.Types.IAID_Type := IAs.Get_ID (IA => IA);
      Pos      : MOIA.Cursor;
      Inserted : Boolean;
   begin
      Instance.IAs.Insert
        (Key      => IAs.Get_ID (IA => IA),
         New_Item => IA,
         Position => Pos,
         Inserted => Inserted);

      if not Inserted then
         raise IA_Present with "Existing identity association with ID"
           & ID'Img;
      end if;
   end Add_IA;

   -------------------------------------------------------------------------

   function Contains (IA : DHCP.Types.IAID_Type) return Boolean
   is
   begin
      return Instance.IAs.Contains (Key => IA);
   end Contains;

   -------------------------------------------------------------------------

   function Contains_Option (Name : Options.Option_Name) return Boolean
   is (Instance.Global_Opts.Contains (Name => Name));

   -------------------------------------------------------------------------

   function Get_Client_ID return Options.Inst.Option_Type'Class
   is
   begin
      return Instance.Global_Opts.Get (Name => Options.Client_Identifier);
   end Get_Client_ID;

   -------------------------------------------------------------------------

   function Get_Global_Options return Options.Inst6.Option_List
   is (Instance.Global_Opts);

   -------------------------------------------------------------------------

   function Get_IA
     (ID : DHCP.Types.IAID_Type)
      return IAs.Identity_Association_Type
   is
      use type MOIA.Cursor;

      Pos : constant MOIA.Cursor := Instance.IAs.Find (Key => ID);
   begin
      if Pos = MOIA.No_Element then
         raise IA_Missing with "No identity association with ID" & ID'Img;
      end if;

      return MOIA.Element (Position => Pos);
   end Get_IA;

   -------------------------------------------------------------------------

   function Get_IA_Count return Natural
   is
   begin
      return Natural (Instance.IAs.Length);
   end Get_IA_Count;

   -------------------------------------------------------------------------

   procedure Initialize (Client_ID : Options.Inst.Option_Type'Class)
   is
   begin
      Instance.Global_Opts.Append (New_Item => Client_ID);
   end Initialize;

   -------------------------------------------------------------------------

   procedure Iterate
     (Process : not null access procedure
        (IA : IAs.Identity_Association_Type))
   is
      procedure Call_Process (Pos : MOIA.Cursor);
      --  Invoke process procedure for identity association at given position.

      ----------------------------------------------------------------------

      procedure Call_Process (Pos : MOIA.Cursor)
      is
      begin
         Process (MOIA.Element (Position => Pos));
      end Call_Process;
   begin
      Instance.IAs.Iterate (Process => Call_Process'Access);
   end Iterate;

   -------------------------------------------------------------------------

   procedure Remove_Global_Option (Name : Options.Option_Name)
   is
   begin
      if Instance.Global_Opts.Contains (Name => Name) then
         Instance.Global_Opts.Remove (Name => Name);
      end if;
   end Remove_Global_Option;

   -------------------------------------------------------------------------

   procedure Remove_IA (ID : DHCP.Types.IAID_Type)
   is
      use type MOIA.Cursor;

      Pos : MOIA.Cursor := Instance.IAs.Find (Key => ID);
   begin
      if Pos = MOIA.No_Element then
         raise IA_Missing with "No identity association with ID" & ID'Img;
      end if;

      Instance.IAs.Delete (Position => Pos);
   end Remove_IA;

   -------------------------------------------------------------------------

   procedure Reset
   is
      CID : constant Options.Inst.Option_Type'Class
        := Instance.Global_Opts.Get (Name => Options.Client_Identifier);
   begin
      Instance.IAs.Clear;
      Instance.Global_Opts.Clear;
      Instance.Global_Opts.Append (New_Item => CID);
   end Reset;

   -------------------------------------------------------------------------

   procedure Update_IA
     (ID     : DHCP.Types.IAID_Type;
      Update : not null access procedure
        (ID :        DHCP.Types.IAID_Type;
         IA : in out IAs.Identity_Association_Type))
   is
      use type MOIA.Cursor;

      Pos : constant MOIA.Cursor := Instance.IAs.Find (Key => ID);
   begin
      if Pos = MOIA.No_Element then
         raise IA_Missing with "No identity association with ID" & ID'Img;
      end if;

      Instance.IAs.Update_Element
        (Position => Pos,
         Process  => Update);
   end Update_IA;

end DHCPv6.Database;
