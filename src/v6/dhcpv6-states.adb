--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;
with Ada.Strings.Unbounded;

with Anet;

with DHCP.Random;
with DHCP.Logger;
with DHCP.Timer;
with DHCP.Timing_Events.Timer_Expiry;
with DHCP.Types;
with DHCP.Notify;

with DHCPv6.Constants;
with DHCPv6.Database;
with DHCPv6.IAs;
with DHCPv6.Options;
with DHCPv6.States.Init;
with DHCPv6.States.Bound;

package body DHCPv6.States
is

   use Ada.Strings.Unbounded;

   package L   renames DHCP.Logger;
   package OI6 renames DHCPv6.Options.Inst6;

   procedure Process_IA_NA
     (IA_Option         :        OI6.IA_NA_Option_Type;
      Timestamp         :        Ada.Real_Time.Time;
      Updated           : in out Boolean;
      Next_T1           : in out Duration;
      Next_T2           : in out Duration;
      Next_Lease_Expiry : in out Duration);
   --  Process given identity association NA option. Update is set to True if
   --  IA information in the database was updated while handling the IA option.
   --  T1, T2 and lease expiry are set to the next timeout values.

   procedure Schedule_Timer
     (Transaction : in out Transaction_Type'Class;
      Start_Time  :        Ada.Real_Time.Time;
      Timer_Kind  :        DHCP.Types.DHCP_Timer_Kind;
      In_Time     :        Duration);
   --  Schedule timer of given kind to expire in specified amount from start
   --  time.

   -------------------------------------------------------------------------

   procedure Process_IA_NA
     (IA_Option         :        OI6.IA_NA_Option_Type;
      Timestamp         :        Ada.Real_Time.Time;
      Updated           : in out Boolean;
      Next_T1           : in out Duration;
      Next_T2           : in out Duration;
      Next_Lease_Expiry : in out Duration)
   is
      use type OI6.Status_Code_Type;

      IAID : constant DHCP.Types.IAID_Type  := IA_Option.Get_IAID;
      Opts : constant OI6.Option_List'Class := IA_Option.Get_Options;

      procedure Update_IA
        (ID :        DHCP.Types.IAID_Type;
         IA : in out IAs.Identity_Association_Type);
      --  Update IA entry in database with information from IA option.

      ----------------------------------------------------------------------

      procedure Update_IA
        (ID :        DHCP.Types.IAID_Type;
         IA : in out IAs.Identity_Association_Type)
      is
         T1, Lease_Expiry  : Duration := Duration'Last;
         T2, Pref_Lifetime : Duration := Duration'Last;
         Removed_Addrs     : OI6.Option_List;

         procedure Add_Address (Opt : Options.Inst.Option_Type'Class);
         --  Add information of address options to identity association.

         procedure Log_Address_Removal (Opt : Options.Inst.Option_Type'Class);
         --  Log removal of given address.

         procedure Update_Expiry_Time
           (Address : Options.Inst6.IA_Address_Option_Type);
         --  Update lease expiration time depending on the address' valid
         --  lifetime.

         -------------------------------------------------------------------

         procedure Add_Address (Opt : Options.Inst.Option_Type'Class)
         is
            use type Options.Option_Name;
         begin
            if Opt.Get_Name = Options.IA_Address then
               L.Log (Message => "Adding address " & Anet.To_String
                      (Address => OI6.IA_Address_Option_Type
                       (Opt).Get_Address));
               IAs.Add_Address (IA      => IA,
                                Address => OI6.IA_Address_Option_Type (Opt));
            end if;
         end Add_Address;

         -------------------------------------------------------------------

         procedure Log_Address_Removal (Opt : Options.Inst.Option_Type'Class)
         is
         begin
            L.Log (Message => "Removing address " & Anet.To_String
                   (Address => OI6.IA_Address_Option_Type
                       (Opt).Get_Address));
         end Log_Address_Removal;

         -------------------------------------------------------------------

         procedure Update_Expiry_Time
           (Address : Options.Inst6.IA_Address_Option_Type)
         is
            Valid, Pref : Duration;
         begin
            Address.Get_Lifetimes (Preferred => Pref,
                                   Valid     => Valid);

            if Pref < Pref_Lifetime then
               Pref_Lifetime := Pref;
            end if;

            if Valid < Lease_Expiry then
               Lease_Expiry := Valid;
            end if;
         end Update_Expiry_Time;
      begin
         L.Log (Message => "Updating IA" & IAID'Img);

         IA_Option.Get_Ts (T1 => T1,
                           T2 => T2);

         Opts.Iterate (Process => Add_Address'Access);

         IAs.Set_Timestamp
           (IA        => IA,
            Timestamp => Timestamp);

         IAs.Discard_Expired_Addresses
           (IA            => IA,
            Removed_Addrs => Removed_Addrs);
         Removed_Addrs.Iterate (Process => Log_Address_Removal'Access);

         IAs.Iterate (IA      => IA,
                      Process => Update_Expiry_Time'Access);

         if T1 = 0.0 or T2 = 0.0 then

            --  Set T1 and T2 to default values if server left it to clients
            --  discretion, see RFC 3315, section 22.3.

            L.Log (Message => "Server left T1/T2 of IA" & ID'Img & " to our "
                   & "discretion: using default values");

            T1 := Pref_Lifetime * 0.5;
            T2 := Pref_Lifetime * 0.8;
         end if;

         IAs.Set_Timeouts (IA => IA,
                           T1 => T1,
                           T2 => T2);

         if T1 < Next_T1 then
            Next_T1 := T1;
         end if;
         if T2 < Next_T2 then
            Next_T2 := T2;
         end if;
         if Lease_Expiry < Next_Lease_Expiry then
            Next_Lease_Expiry := Lease_Expiry;
         end if;
      end Update_IA;
   begin
      if Opts.Contains (Name => Options.Status_Code) then
         declare
            Status : constant OI6.Status_Code_Option_Type
              := OI6.Status_Code_Option_Type
                (Opts.Get (Name => Options.Status_Code));
         begin
            if Status.Get_Status = OI6.Status_No_Addrs_Avail then
               L.Log (Message => "No addresses available for IA" & IAID'Img
                      & (if Status.Get_Status_Message'Length > 0 then
                           ": " & Status.Get_Status_Message else ""));
               if Database.Contains (IA => IAID) then
                  L.Log (Message => "Removing IA " & IAID'Img);
                  Database.Remove_IA (ID => IAID);
                  Updated := True;
                  return;
               end if;
            end if;
         end;
      end if;

      Database.Update_IA
        (ID     => IAID,
         Update => Update_IA'Access);

      Updated := True;
   end Process_IA_NA;

   -------------------------------------------------------------------------

   procedure Process_Reply_Msg
     (State       : access Allocation_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type)
   is
      use type Options.Inst.Option_Type'Class;

      Now       : constant Ada.Real_Time.Time := Ada.Real_Time.Clock;
      Opts      : constant OI6.Option_List    := Msg.Get_Options;
      Server_ID : constant Options.Inst.Option_Type'Class
        := Opts.Get (Name => Options.Server_Identifier);

      Address_Received     : Boolean  := False;
      T1, T2, Lease_Expiry : Duration := Duration'Last;

      procedure Update_Database (Option : Options.Inst.Option_Type'Class);
      --  Process message option and update database with new valid data.

      -------------------------------------------------------------------------

      procedure Update_Database (Option : Options.Inst.Option_Type'Class)
      is
      begin
         case Option.Get_Name is
            when Options.IA_NA =>
               Process_IA_NA
                 (IA_Option         => OI6.IA_NA_Option_Type (Option),
                  Timestamp         => Now,
                  Updated           => Address_Received,
                  Next_T1           => T1,
                  Next_T2           => T2,
                  Next_Lease_Expiry => Lease_Expiry);
            when others        =>
               if Options.Is_Config_Option (Name => Option.Get_Name) then
                  L.Log (Message => "Storing " & Option.Get_Name'Img
                         & " config option in database");
                  Database.Add_Global_Option (Opt => Option);
               end if;
         end case;
      end Update_Database;
   begin
      if Server_ID /= Database.Get_Global_Options.Get
        (Name => Options.Server_Identifier)
      then
         L.Log (Message => "Discarding " & Msg.Get_Kind'Img
                & " message with XID" & Msg.Get_Transaction_ID'Img
                & ": Server identifier mismatch");
         return;
      end if;

      declare
         use type OI6.Status_Code_Type;

         Found      : Boolean;
         Status_Msg : Unbounded_String;
      begin
         for Status_Code in OI6.Status_Code_Type range
           OI6.Status_Unspec_Fail .. OI6.Status_Code_Type'Last
         loop
            Found      := False;
            Status_Msg := Null_Unbounded_String;

            Options.Has_Status_Code (Opts       => Opts,
                                     Status     => Status_Code,
                                     Found      => Found,
                                     Status_Msg => Status_Msg);
            if Found then
               L.Log (Message => "Status of " & Msg.Get_Kind'Img
                      & " message with XID" & Msg.Get_Transaction_ID'Img
                      & ": " & Status_Code'Img
                      & (if Length (Status_Msg) > 0 then " - " &
                          To_String (Status_Msg)
                        else ""));

               if Status_Code = OI6.Status_Not_On_Link then
                  Transaction.Restart_Server_Discovery;
                  return;
               else

                  --  Continue retransmission

                  return;
               end if;
            end if;
         end loop;
      end;

      L.Log (Message => "Processing stateful configuration from server '"
             & Server_ID.Get_Data_Str & "'");

      Opts.Iterate (Process => Update_Database'Access);

      if Address_Received then

         --  Check if lease expiry was actually set. If not, there are no valid
         --  leases. This may happen if the server sends us addresses with a
         --  lifetime of zero.

         if Lease_Expiry = Duration'Last then
            States.State_Type'Class
              (State.all).Process_Lease_Expiry
              (Transaction => Transaction);
            return;
         end if;

         --  Since we have successfully allocated a lease all pending events
         --  are obsolete.

         DHCP.Timer.Clear;

         Schedule_Timer (Transaction => Transaction,
                         Start_Time  => Now,
                         Timer_Kind  => DHCP.Types.T1,
                         In_Time     => T1);
         Schedule_Timer (Transaction => Transaction,
                         Start_Time  => Now,
                         Timer_Kind  => DHCP.Types.T2,
                         In_Time     => T2);
         Schedule_Timer (Transaction => Transaction,
                         Start_Time  => Now,
                         Timer_Kind  => DHCP.Types.Lease_Expiry,
                         In_Time     => Lease_Expiry);

         Allocation_State_Type'Class (State.all).Notify_Allocation;

         Transaction.Reset;
         Transaction.Reset_Retransmit;
         Transaction.Set_State (State => Bound.State);
      end if;
   end Process_Reply_Msg;

   -------------------------------------------------------------------------

   procedure Restart_Server_Discovery (Transaction : in out Transaction_Type)
   is
   begin
      L.Log (Message => "Restarting server solicitation");

      Transaction_Type'Class (Transaction).Stop;
      Transaction_Type'Class (Transaction).Start;
   end Restart_Server_Discovery;

   -------------------------------------------------------------------------

   procedure Schedule_Timer
     (Transaction : in out Transaction_Type'Class;
      Start_Time  :        Ada.Real_Time.Time;
      Timer_Kind  :        DHCP.Types.DHCP_Timer_Kind;
      In_Time     :        Duration)
   is
      use Ada.Real_Time;

      Ev : DHCP.Timing_Events.Timer_Expiry.Expiry_Type
        (T          => Transaction'Access,
         Timer_Kind => Timer_Kind);
      New_Time : Time;
   begin
      New_Time := Start_Time + To_Time_Span (D => In_Time);
      L.Log
        (Level   => L.Info,
         Message => "Timer " & Timer_Kind'Img & " in"
         & Long_Long_Integer (To_Duration (New_Time - Clock))'Img
         & " seconds");

      Ev.Set_Time (At_Time => New_Time);
      DHCP.Timer.Schedule (Event => Ev);
   end Schedule_Timer;

   -------------------------------------------------------------------------

   procedure Stop
     (State       : access State_Type;
      Transaction : in out Transaction_Type'Class)
   is
      pragma Unreferenced (State);

      Count : Natural := 0;
      Req   : Message.Message_Type := Message.Create (Kind => Message.Release);

      procedure Add_IA (IA : IAs.Identity_Association_Type);
      --  Add identity association to release message.

      ----------------------------------------------------------------------

      procedure Add_IA (IA : IAs.Identity_Association_Type)
      is
      begin
         if IAs.Get_Addresses (IA => IA).Get_Count > 0 then
            Req.Add_Option (Opt => IAs.To_Option (IA => IA));
            Count := Count + 1;
         end if;
      end Add_IA;
   begin
      DHCP.Timer.Clear;

      DHCP.Notify.Update (Reason => DHCP.Notify.Release);

      Database.Iterate (Process => Add_IA'Access);

      if Count > 0 then
         declare
            XID       : constant Types.Transaction_ID_Type
              := Types.Transaction_ID_Type'Mod (DHCP.Random.Get);
            Server_ID : constant Options.Inst.Option_Type'Class
              := Database.Get_Global_Options.Get
                (Name => Options.Server_Identifier);
         begin
            L.Log (Message => "Releasing" & Count'Img & " IA(s) from server '"
                   & Server_ID.Get_Data_Str & "' using transaction with ID"
                   & XID'Img);

            Transaction.Set_ID (XID => XID);

            --  Release retransmission params, RFC 3315, section 18.1.1.

            Transaction.Set_Retransmission_Params
              (IRT => Constants.REL_TIMEOUT,
               MRT => 0.0,
               MRC => Constants.REL_MAX_RC,
               MRD => 0.0);

            Req.Set_Transaction_ID (XID => XID);
            Req.Add_Option (Opt => Database.Get_Client_ID);
            Req.Add_Option (Opt => Server_ID);

            Transaction.Set_Start_Time;
            Transaction.Send_Message (Msg => Req);
         end;
      end if;

      L.Log (Message => "Resetting database");
      Database.Reset;

      Transaction.Reset;
      Transaction.Reset_Retransmit;
      Transaction.Set_State (State => Init.State);
   end Stop;

end DHCPv6.States;
