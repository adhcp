--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Object_Streamer;

package body DHCPv6.Database.IO
is

   package OIO is new DHCP.Object_Streamer (Object_Type => Database_Type);

   -------------------------------------------------------------------------

   procedure Load (Filename : String)
   is
   begin
      Instance := OIO.Deserialize (Filename => Filename);
   end Load;

   -------------------------------------------------------------------------

   procedure Write (Filename : String)
   is
   begin
      OIO.Serialize (Filename => Filename,
                     Object   => Instance);
   end Write;

end DHCPv6.Database.IO;
