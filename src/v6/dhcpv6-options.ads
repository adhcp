--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Tags;
with Ada.Strings.Unbounded;

with Anet;

with DHCP.Types;

with DHCP.Options.V6;

package DHCPv6.Options
is

   type Option_Name is
     (Unknown,
      Client_Identifier,
      Server_Identifier,
      IA_NA,
      IA_TA,
      IA_Address,
      Option_Request,
      Preference,
      Elapsed_Time,
      Relay_Message,
      Authentication,
      Server_Unicast,
      Status_Code,
      Rapid_Commit,
      User_Class,
      Vendor_Class,
      Vendor_Information,
      Interface_ID,
      Reconfigure_Message,
      Reconfigure_Accept,
      Name_Servers,
      Information_Refresh_Time);
   --  DHCPv6 option names.

   type Code_Type is range 0 .. 2 ** 16 - 1
     with
       Size => 16;

   function Code_To_Name (Code : Code_Type) return Option_Name;
   --  Convert given code to option name.

   function Name_To_Code (Name : Option_Name) return Code_Type;
   --  Convert given name to option code.

   function Code_To_Tag (Code : Code_Type) return Ada.Tags.Tag;
   --  Return object tag for given option code.

   function Is_Config_Option (Name : Option_Name) return Boolean;
   --  Returns True if the given name designates a configuration option.

   --  Option instances.

   package Inst is new DHCP.Options
     (Code_Type        => Code_Type,
      Option_Name_Type => Option_Name,
      To_Name          => Code_To_Name,
      To_Tag           => Code_To_Tag,
      To_Code          => Name_To_Code);

   package Inst6 is new Inst.V6;

   function Create
     (Address   : Anet.IPv6_Addr_Type;
      Preferred : Duration;
      Valid     : Duration;
      Opts      : Inst6.Option_List'Class := Inst6.Empty_List)
      return Inst6.IA_Address_Option_Type;
   --  Create IA address option with given values and options.

   function Create
     (IAID : DHCP.Types.IAID_Type;
      T1   : Duration;
      T2   : Duration;
      Opts : Inst6.Option_List'Class := Inst6.Empty_List)
      return Inst6.IA_NA_Option_Type;
   --  Create identity association for Non-temporary Addresses Option with
   --  given values and options.

   procedure Has_Status_Code
     (Opts       :        Inst6.Option_List'Class;
      Status     :        Inst6.Status_Code_Type;
      Found      : in out Boolean;
      Status_Msg :    out Ada.Strings.Unbounded.Unbounded_String);
   --  Recursively search the given option list for existence of status code
   --  with specified value. If a match exists, Found is set to True and the
   --  status message is returned.

   procedure Has_Status_Code
     (Opt        :        Inst.Option_Type'Class;
      Status     :        Inst6.Status_Code_Type;
      Found      : in out Boolean;
      Status_Msg :    out Ada.Strings.Unbounded.Unbounded_String);
   --  Recursively search given option and children for existence of status
   --  code with specified value. If a match exists, Found is set to True and
   --  the status message is returned.

end DHCPv6.Options;
