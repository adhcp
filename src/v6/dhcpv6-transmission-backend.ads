--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet.Sockets.Inet;

with DHCPv6.Receivers;

private package DHCPv6.Transmission.Backend
is

   Sock_Mcast : aliased Anet.Sockets.Inet.UDPv6_Socket_Type;
   --  Inet/UDP socket used to transmit DHCPv6 messages.

   Receiver   : Receivers.UDPv6.Receiver_Type (S => Sock_Mcast'Access);
   --  UDPv6 packet receiver.

end DHCPv6.Transmission.Backend;
