--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Strings.Unbounded;

with DHCP.Logger;
with DHCP.Socket_Callbacks;

with DHCPv6.Constants;
with DHCPv6.Transmission.Backend;

package body DHCPv6.Transmission
is

   use Ada.Strings.Unbounded;

   package L renames DHCP.Logger;

   -------------------------------------------------------------------------

   function Get_Iface_Name return Anet.Types.Iface_Name_Type
   is
   begin
      return Anet.Types.Iface_Name_Type (To_String (Iface_Info.Name));
   end Get_Iface_Name;

   -------------------------------------------------------------------------

   procedure Initialize
     (Client_Iface : DHCP.Net.Iface_Type;
      Listen_Cb    : Receivers.UDPv6.Rcv_Item_Callback)
   is
   begin
      Iface_Info := Client_Iface;

      Backend.Sock_Mcast.Init;
      Backend.Sock_Mcast.Bind (Iface => Get_Iface_Name);
      Backend.Sock_Mcast.Bind (Address => Anet.Any_Addr_V6,
                               Port    => Constants.DHCPv6_Client_Port);

      Backend.Receiver.Register_Error_Handler
        (Callback => DHCP.Socket_Callbacks.Handle_Receive_Error'Access);
      Backend.Receiver.Listen (Callback => Listen_Cb);
   end Initialize;

   -------------------------------------------------------------------------

   procedure Send (Message : DHCPv6.Message.Message_Type)
   is
   begin
      L.Log (Message => "Sending " & Message.Get_Kind'Img & " message");

      Backend.Sock_Mcast.Send
        (Item     => Message.Serialize,
         Dst_Addr => Constants.All_DHCP_Relay_Agents_and_Servers,
         Dst_Port => Constants.DHCPv6_Server_Port);
   end Send;

   -------------------------------------------------------------------------

   procedure Stop
   is
   begin
      if Backend.Receiver.Is_Listening then
         Backend.Receiver.Stop;
      end if;
   end Stop;

end DHCPv6.Transmission;
