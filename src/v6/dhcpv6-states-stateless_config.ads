--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package DHCPv6.States.Stateless_Config
is

   type Config_State_Type is new State_Type with private;
   --  Configuration state for stateless DHCPv6 operation.

   overriding
   function Get_Name (State : not null access Config_State_Type) return String;
   --  Return name of state as string.

   overriding
   procedure Process_Reply_Msg
     (State       : access Config_State_Type;
      Transaction : in out Transaction_Type'Class;
      Msg         :        Message.Message_Type);
   --  Extract configuration information from reply message and store it in the
   --  database.

   function State return DHCP.States.State_Handle;
   --  Return access to singleton.

private

   type Config_State_Type is new State_Type with null record;

end DHCPv6.States.Stateless_Config;
