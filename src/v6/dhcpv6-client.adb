--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;

with Anet.Types;
with Anet.Sockets.Net_Ifaces;

with DHCP.Logger;
with DHCP.Net;
with DHCP.Timer;
with DHCP.Timing_Events.Start;
with DHCP.Notify;
with DHCP.Observers;

with DHCPv6.Cmd_Line;
with DHCPv6.DUID;
with DHCPv6.Database.IO;
with DHCPv6.Message;
with DHCPv6.States.Stateless_Init;
with DHCPv6.Timing_Events.Handle_Msg;
with DHCPv6.Transmission;
with DHCPv6.Types;

package body DHCPv6.Client
is

   package L renames DHCP.Logger;

   procedure Call_External_Notify is new DHCP.Observers.Call_External_Notify
     (Get_Interface_Name => Transmission.Get_Iface_Name,
      Write_Database     => Database.IO.Write);

   -------------------------------------------------------------------------

   procedure Process_Server_Reply
     (Buffer : Ada.Streams.Stream_Element_Array;
      Src    : Anet.Sockets.Inet.IPv6_Sockaddr_Type)
   is
      use type Types.Transaction_ID_Type;

      Msg : Message.Message_Type;
   begin
      Msg := Message.Deserialize (Buffer => Buffer);

      if Msg.Get_Transaction_ID /= Transaction.Get_ID then

         --  Silently discard non-matching XID's.

         return;
      end if;

      L.Log (Message => "Received " & Msg.Get_Kind'Img
             & " message with XID"
             & Msg.Get_Transaction_ID'Img & " from "
             & Anet.To_String (Address => Src.Addr));
      declare
         Ev : Timing_Events.Handle_Msg.Handle_Msg_Type (T => Transaction);
      begin
         Ev.Set_Message (Msg => Msg);
         DHCP.Timer.Schedule (Event => Ev);
      end;

   exception
      when E : Message.Invalid_Message =>

         --  Discard invalid messages.

         L.Log
           (Level   => L.Info,
            Message => "Discarding message (XID" & Msg.Get_Transaction_ID'Img
            & "): " & Ada.Exceptions.Exception_Message (X => E));
   end Process_Server_Reply;

   -------------------------------------------------------------------------

   procedure Run
   is
      Client_Iface : constant String := Cmd_Line.Get_Interface_Name;
   begin
      Database.Initialize
        (Client_ID => DUID.Read_DUID_From_File
           (Path      => Cmd_Line.Get_DUID_Path,
            DUID_Type => Cmd_Line.Get_DUID_Type));

      if Cmd_Line.In_Stateless_Mode then
         Transaction.Set_State (State => States.Stateless_Init.State);
         L.Log (Message => "Running client in stateless mode");
      end if;

      L.Log (Message => "Using client identifier '"
             & Database.Get_Client_ID.Get_Data_Str & "'");

      declare
         Cli_Iface : DHCP.Net.Iface_Type (Addrtype => DHCP.Net.IPv6_Address);
      begin
         DHCP.Net.Discover_Ifaces;

         Cli_Iface := DHCP.Net.Get_Iface
           (Name     => Anet.Types.Iface_Name_Type (Client_Iface),
            Addrtype => DHCP.Net.IPv6_Address);

         L.Log (Message => "Configuring interface " & Client_Iface);

         if not Anet.Sockets.Net_Ifaces.Is_Iface_Up
           (Name => Anet.Types.Iface_Name_Type (Client_Iface))
         then
            L.Log (Message => "Bringing up interface " & Client_Iface);
            Anet.Sockets.Net_Ifaces.Set_Iface_State
              (Name  => Anet.Types.Iface_Name_Type (Client_Iface),
               State => True);
         end if;

         DHCP.Notify.Register
           (Reasons => (DHCP.Notify.Bound, DHCP.Notify.Preinit,
                        DHCP.Notify.Rebind, DHCP.Notify.Release,
                        DHCP.Notify.Renew),
            Handler => Call_External_Notify'Access);

         DHCP.Timer.Start;

         Transmission.Initialize
           (Client_Iface => Cli_Iface,
            Listen_Cb    => Process_Server_Reply'Access);

         L.Log (Message => "Initiating DHCPv6 operation on interface "
                & String (Client_Iface));
         declare
            Ev : DHCP.Timing_Events.Start.Start_Type (T => Transaction);
         begin
            DHCP.Timer.Schedule (Event => Ev);
         end;
      end;
   end Run;

   -------------------------------------------------------------------------

   procedure Stop
   is
   begin
      begin
         if not Cmd_Line.In_Stateless_Mode then
            Transaction.Stop;
         end if;
      exception
         when others =>

            --  Ignore issues when stopping transaction since we are shutting
            --  down anyway.

            null;
      end;

      Transmission.Stop;
      DHCP.Timer.Stop;
      DHCP.Logger.Stop;
   end Stop;

end DHCPv6.Client;
