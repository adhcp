--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCP.States
is

   -------------------------------------------------------------------------

   function Get_Start_Time
     (Context : Root_Context_Type)
      return Ada.Real_Time.Time
   is
   begin
      return Context.Start_Time;
   end Get_Start_Time;

   -------------------------------------------------------------------------

   procedure Set_Start_Time
     (Context : in out Root_Context_Type;
      Time    :        Ada.Real_Time.Time := Ada.Real_Time.Clock)
   is
   begin
      Context.Start_Time := Time;
   end Set_Start_Time;

   -------------------------------------------------------------------------

   procedure Set_State
     (Context : in out Root_Context_Type;
      State   :        State_Handle)
   is
   begin
      Context.State := State;
   end Set_State;

   -------------------------------------------------------------------------

   function State (Context : Root_Context_Type'Class) return State_Handle
   is
   begin
      return Context.State;
   end State;

end DHCP.States;
