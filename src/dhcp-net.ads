--
--  Copyright (C) 2011-2015 secunet Security Networks AG
--  Copyright (C) 2011-2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011-2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Strings.Unbounded;

with Anet.Types;

package DHCP.Net
is

   type Iface_Addr_Type is
     (Unknown,
      Link_Layer,
      IPv4_Address,
      IPv6_Address);
   --  Address status of a given interface.

   type Iface_Type (Addrtype : Iface_Addr_Type) is record
      Name : Ada.Strings.Unbounded.Unbounded_String;
      case Addrtype is
         when Link_Layer   =>
            Mac_Addr  : Anet.Hardware_Addr_Type (1 .. 6);
         when IPv4_Address =>
            Address   : Anet.IPv4_Addr_Type;
            Netmask   : Anet.IPv4_Addr_Type;
            Broadcast : Anet.IPv4_Addr_Type;
         when IPv6_Address =>
            Address6  : Anet.IPv6_Addr_Type;
            Netmask6  : Anet.IPv6_Addr_Type;
         when Unknown      => null;
      end case;
   end record;
   --  Network interface.

   procedure Discover_Ifaces;
   --  Discover all interfaces of the local system.

   function Get_Name (Iface : Iface_Type) return Anet.Types.Iface_Name_Type;
   --  Return name of given network interface type.

   function Get_Iface
     (Name     : Anet.Types.Iface_Name_Type;
      Addrtype : Iface_Addr_Type)
      return Iface_Type;
   --  Return network interface with given name and address type. If no such
   --  interface exists an exception is raised.

   function Get_Iface_Count return Natural;
   --  Return the number of discovered network interfaces.

   function Is_Local_Iface (Address : Anet.IPv4_Addr_Type) return Boolean;
   --  Returns True if an interface of the local system with given IP address
   --  exists.

   Network_Error : exception;

end DHCP.Net;
