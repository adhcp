--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.States;

package DHCP.Timing_Events.Start
is

   type Start_Type (T : not null access States.Root_Context_Type'Class)
   is new DHCP.Timing_Events.Event_Type with private;

   overriding
   procedure Trigger (Event : in out Start_Type);
   --  Start the transaction.

private

   type Start_Type (T  : not null access States.Root_Context_Type'Class)
   is new DHCP.Timing_Events.Event_Type with null record;

end DHCP.Timing_Events.Start;
