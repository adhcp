--
--  Copyright (C) 2014 secunet Security Networks AG
--  Copyright (C) 2014 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2014 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Strings.Unbounded;

package body DHCP.Cmd_Line
is

   use Ada.Strings.Unbounded;

   Current_Notifier : Unbounded_String;

   -------------------------------------------------------------------------

   function Get_Ext_Notify_Binary return String
   is
   begin
      return To_String (Current_Notifier);
   end Get_Ext_Notify_Binary;

   -------------------------------------------------------------------------

   procedure Set_Ext_Notify_Binary (B : String)
   is
   begin
      Current_Notifier := To_Unbounded_String (B);
   end Set_Ext_Notify_Binary;

end DHCP.Cmd_Line;
