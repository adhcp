--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

with DHCP.Types;

package DHCP.States
is

   type Root_State_Type is abstract tagged limited private;
   --  Each distinct state must extend the root state type and overwrite the
   --  needed procedures to perform state transitions.
   --  Reference:
   --  http://www.adapower.com/index.php?Command=Class&ClassID=Patterns&CID=277

   type State_Handle is not null access all Root_State_Type'Class;

   function Get_Name
     (State : not null access Root_State_Type)
      return String
      is abstract;
   --  Returns the name of the state as string.

   type Root_Context_Type (Initial_State : State_Handle) is
     abstract tagged limited private;
   --  The context contains the current state and additional information that
   --  is to be passed to state-specific procedures. All data is contained in
   --  the context: the state objects only perform the transition operation and
   --  change the attributes of the context.

   procedure Set_State
     (Context : in out Root_Context_Type;
      State   :        State_Handle);
   --  Set current state of context to given value.

   function State (Context : Root_Context_Type'Class) return State_Handle;
   --  Get current state of context.

   procedure Start (Context : in out Root_Context_Type) is abstract;
   --  Start state machine processing with given context.

   procedure Process_Timer_Expiry
     (Context    : in out Root_Context_Type;
      Timer_Kind :        Types.DHCP_Timer_Kind)
   is abstract;
   --  Process expiration of DHCP timer such as T1, T2 or lease expiration.

   procedure Set_Start_Time
     (Context : in out Root_Context_Type;
      Time    :        Ada.Real_Time.Time := Ada.Real_Time.Clock);
   --  Set start time of context to the given time.

   function Get_Start_Time
     (Context : Root_Context_Type)
      return Ada.Real_Time.Time;
   --  Return start time of context. If the time has not been set,
   --  Ada.Real_Time.Time_First is returned;

   procedure Start
     (State   : access Root_State_Type;
      Context : in out Root_Context_Type'Class) is abstract;
   --  Perform start operation for specified state with given context.

private

   type Root_Context_Type (Initial_State : State_Handle)
   is abstract tagged limited record
      State      : State_Handle       := Initial_State;
      Start_Time : Ada.Real_Time.Time := Ada.Real_Time.Time_First;
   end record;

   type Root_State_Type is abstract tagged limited null record;

end DHCP.States;
