--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Tags;
with Ada.Streams;
with Ada.Containers.Indefinite_Doubly_Linked_Lists;

with Interfaces;

with DHCP.Marshaling;

generic

   type Code_Type is range <>;
   --  Option code range.

   type Option_Name_Type is (<>);
   --  Option name enumeration type.

   with function To_Name
     (Code : Code_Type)
      return Option_Name_Type;

   with function To_Tag
     (Code : Code_Type)
      return Ada.Tags.Tag;

   with function To_Code
     (Name : Option_Name_Type)
      return Code_Type;

package DHCP.Options
is
   use type Ada.Streams.Stream_Element_Offset;

   Len_Byte_Count : constant Positive := Code_Type'Size / 8;
   --  Length in bytes of the option-len field. Derived from code type because
   --  option-len and option-code have the same byte count in DHCPv4 and
   --  DHCPv6.

   ------------
   -- Option --
   ------------

   subtype Size_Type is Ada.Streams.Stream_Element_Offset range
     1 .. 2 ** (Len_Byte_Count * 8) - 1;

   type Option_Type (Size : Size_Type) is abstract tagged private;
   --  Generic DHCP option.

   type Parameter_Record (Size : Size_Type) is private;
   --  Parameter type used to create option objects with generic dispatching.

   function Create
     (Param : not null access Parameter_Record)
      return Option_Type
      is abstract;

   function Create
     (Name : Option_Name_Type;
      Data : Ada.Streams.Stream_Element_Array)
      return Option_Type'Class;
   --  Create DHCP option with given option name and data.

   procedure Validate (Option : Option_Type) is abstract;
   --  Validate option data. Raise invalid option exception if data is not
   --  valid for a specific option.

   function Get_Data_Str (Option : Option_Type) return String is abstract;
   --  Convert option data to string representation.

   function Get_Size (Option : Option_Type'Class) return Size_Type;
   --  Return size of DHCP option.

   function Get_Code (Option : Option_Type'Class) return Code_Type;
   --  Return option code.

   function Get_Data
     (Option : Option_Type'Class)
      return Ada.Streams.Stream_Element_Array;
   --  Return DHCP option data as stream element array.

   function Get_Name (Option : Option_Type'Class) return Option_Name_Type;
   --  Return name of DHCP option.

   function Get_Name_Str (Option : Option_Type) return String;
   --  Return the name of the DHCP option as string.

   function Serialize
     (Option : Option_Type'Class)
      return Ada.Streams.Stream_Element_Array;
   --  Serialize given DHCP option to stream element array.

   function Deserialize
     (Buffer : Ada.Streams.Stream_Element_Array)
      return Option_Type'Class;
   --  Create a DHCP option from given stream element array.

   ----------------
   -- Raw option --
   ----------------

   type Raw_Option_Type is new Option_Type with private;
   --  Option containing raw option data.

   overriding
   function Create
     (Param : not null access Parameter_Record)
      return Raw_Option_Type;

   overriding
   function Get_Data_Str (Option : Raw_Option_Type) return String;
   --  Return hex string of raw option data.

   overriding
   procedure Validate (Option : Raw_Option_Type) is null;
   --  Validate option data. Not needed for raw options.

   -----------------
   -- Byte option --
   -----------------

   type Byte_Option_Type is new Raw_Option_Type with private;
   --  Option type containing one byte of data.

   overriding
   function Get_Data_Str (Option : Byte_Option_Type) return String;
   --  Return string representation of data.

   overriding
   procedure Validate (Option : Byte_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   -------------------------
   -- uint16 array option --
   -------------------------

   type U_Int16_Array_Option_Type is new Raw_Option_Type with private;
   --  Option type containing an array of 16-bit unsigned integers.

   overriding
   function Get_Data_Str (Option : U_Int16_Array_Option_Type) return String;
   --  Return string representation of 16-bit unsigned integer array.

   overriding
   procedure Validate (Option : U_Int16_Array_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   -------------------
   -- uint16 option --
   -------------------

   type U_Int16_Option_Type is new U_Int16_Array_Option_Type with private;
   --  Option type containing a 16-bit unsigned integer.

   function Create
     (Name  : Option_Name_Type;
      Value : Interfaces.Unsigned_16)
      return U_Int16_Option_Type;
   --  Create a new 16-bit unsigned option with given name and value.

   function Get_Value
     (Option : U_Int16_Option_Type)
      return Interfaces.Unsigned_16;
   --  Return 16-bit unsigned value stored in option.

   overriding
   procedure Validate (Option : U_Int16_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   -------------------
   -- uint32 option --
   -------------------

   type U_Int32_Option_Type is new Raw_Option_Type with private;
   --  Option type containing a 32-bit unsigned integer.

   overriding
   function Get_Data_Str (Option : U_Int32_Option_Type) return String;
   --  Return string representation of 32-bit unsigned integer.

   overriding
   procedure Validate (Option : U_Int32_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   function Get_Value
     (Option : U_Int32_Option_Type)
      return Interfaces.Unsigned_32;
   --  Return 32-bit unsigned value stored in option.

   ---------------------
   -- IP array option --
   ---------------------

   type IP_Array_Option_Type is new Raw_Option_Type with private;
   --  Option type containing an array of IP addresses.

   overriding
   function Get_Data_Str (Option : IP_Array_Option_Type) return String;
   --  Return string representation of IP address array.

   overriding
   procedure Validate (Option : IP_Array_Option_Type);
   --  Validate option data. Raise invalid option exception if data is not
   --  valid.

   function Get_Addrlen (Option : IP_Array_Option_Type) return Positive;
   --  Return length of stored address.

   function To_IP_String
     (Option : IP_Array_Option_Type;
      Octets : Ada.Streams.Stream_Element_Array)
      return String
   with
      Pre => Octets'Length = 4;
   --  Return string representation of IP given as stream.

   -----------------
   -- Option list --
   -----------------

   type Option_List is abstract new
     DHCP.Marshaling.Serializable_Object with private;
   --  List of DHCP options.

   function Get_Count (List : Option_List) return Natural;
   --  Return number of options in the list.

   function Get_Data_Size (List : Option_List) return Natural;
   --  Return the size of all option data combined.

   function Is_Empty (List : Option_List) return Boolean;
   --  Returns True if list contains no options.

   procedure Append
     (List     : in out Option_List;
      New_Item :        Option_Type'Class);
   --  Append a new option to the option list.

   procedure Remove
     (List : in out Option_List;
      Name :        Option_Name_Type);
   --  Remove option with given name from list. If no such option is found in
   --  the list, an exception is raised.

   procedure Iterate
     (List    : Option_List;
      Process : not null access procedure (Option : Option_Type'Class));
   --  Call the given procedure for each option in the list.

   function Get
     (List : Option_List;
      Name : Option_Name_Type)
      return Option_Type'Class;
   --  Return option given by name. If no such option is found in the list, an
   --  exception is raised.

   function Contains
     (List : Option_List;
      Name : Option_Name_Type)
      return Boolean;
   --  Returns true if the option specified by name is present in the list.

   procedure Clear (List : in out Option_List);
   --  Clear given list.

   Invalid_Option   : exception;
   Option_Not_Found : exception;

private

   type Option_Type (Size : Size_Type) is abstract tagged record
      Code : Code_Type := Code_Type'Last;
      Data : Ada.Streams.Stream_Element_Array (Size_Type'First .. Size)
        := (others => 0);
   end record;

   type Raw_Option_Type is new Option_Type with null record;

   type Byte_Option_Type is new Raw_Option_Type with null record;

   type U_Int16_Array_Option_Type is new Raw_Option_Type with null record;

   type U_Int16_Option_Type is new U_Int16_Array_Option_Type with null record;

   type U_Int32_Option_Type is new Raw_Option_Type with null record;

   type IP_Array_Option_Type is new Raw_Option_Type with null record;

   type Parameter_Record (Size : Size_Type) is record
      Code : Code_Type;
      Data : Ada.Streams.Stream_Element_Array (Size_Type'First .. Size);
   end record;

   package Opt_List_Package is new
     Ada.Containers.Indefinite_Doubly_Linked_Lists
       (Element_Type => Option_Type'Class);

   package OLP renames Opt_List_Package;

   type Option_List is abstract new
     DHCP.Marshaling.Serializable_Object with record
      Data : OLP.List;
   end record;

   Bc : constant Ada.Streams.Stream_Element_Offset
     := Ada.Streams.Stream_Element_Offset (Len_Byte_Count);
   --  Byte count of option-code/option-len fields as stream offset.

   function Create_Option_Object
     (Code : Code_Type;
      Data : Ada.Streams.Stream_Element_Array)
      return Option_Type'Class;
   --  Create option object using dynamic dispatcher.

   function To_U_Int16
     (Octets : Ada.Streams.Stream_Element_Array)
      return Interfaces.Unsigned_16;
   --  Convert given stream array to it's 16-bit unsigned integer value.

end DHCP.Options;
