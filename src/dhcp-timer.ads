--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Timing_Events;

private with DHCP.Timing_Events.Queues;
pragma Elaborate_All (DHCP.Timing_Events.Queues);

package DHCP.Timer is

   procedure Schedule (Event : Timing_Events.Event_Type'Class);
   --  Schedule given event to be triggered at the event's time.
   --  If the event is runnable it will be executed immediately.

   procedure Cancel (Event : Timing_Events.Event_Type'Class);
   --  Cancel scheduled event designated.

   procedure Clear;
   --  Clear all events.

   procedure Start;
   --  Start timer.

   procedure Stop;
   --  Stop timer.

   function Event_Count return Natural;
   --  Return the number of currently scheduled events.

private

   Event_Queue : Timing_Events.Queues.Protected_Event_Queue;
   --  List containing all scheduled events.

end DHCP.Timer;
