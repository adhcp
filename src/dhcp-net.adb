--
--  Copyright (C) 2011-2015 secunet Security Networks AG
--  Copyright (C) 2011-2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011-2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Containers.Indefinite_Doubly_Linked_Lists;

with System;

with Interfaces.C.Strings;

with Anet.Errno;
with Anet.Socket_Families;
with Anet.Sockets.Thin.Inet;
with Anet.Sockets.Thin.Packet;

with DHCP.Logger;

package body DHCP.Net
is

   package C renames Interfaces.C;
   package L renames DHCP.Logger;

   use Ada.Strings.Unbounded;

   package Iface_List_Package is new
     Ada.Containers.Indefinite_Doubly_Linked_Lists
       (Element_Type => Iface_Type);

   package ILP renames Iface_List_Package;

   System_Ifaces : ILP.List;
   --  Discovered network interfaces of the local system.

   type If_Addrs_Type;

   type If_Addrs_Handle is access If_Addrs_Type;

   type Sockaddr_Storage_Type is record
      Ss_Family : Interfaces.C.unsigned_short;
      Data      : Anet.Byte_Array (1 .. 126);
   end record
     with
       Size      => 128 * 8,
       Alignment => 8 * 8;

   for Sockaddr_Storage_Type use record
      Ss_Family at 0 range 0 .. 15;
      Data      at 2 range 0 .. 126 * 8 - 1;
   end record;

   type If_Addrs_Type is record
      Next      : If_Addrs_Handle;
      Name      : C.Strings.chars_ptr;
      Flags     : C.unsigned;
      Addr      : access Sockaddr_Storage_Type;
      Netmask   : access Sockaddr_Storage_Type;  --  Invalid for link layer
      Broadaddr : access Sockaddr_Storage_Type;  --  Invalid for link layer
      Data      : System.Address;
   end record;
   pragma Convention (C, If_Addrs_Type);

   procedure C_Getifaddrs
     (Retval : out C.int;
      Ifap   : access If_Addrs_Handle);
   pragma Import (C, C_Getifaddrs, "getifaddrs");
   pragma Import_Valued_Procedure (C_Getifaddrs, "getifaddrs");

   procedure C_Freeifaddrs (Ifa : If_Addrs_Handle);
   pragma Import (C, C_Freeifaddrs, "freeifaddrs");

   function Find (Address : Anet.IPv4_Addr_Type) return ILP.Cursor;
   --  This function returns the position of the interface with the given
   --  address present in the system interfaces map. If no match is found
   --  No_Element is returned.

   function To_IPv4 (S : Sockaddr_Storage_Type) return Anet.IPv4_Addr_Type;
   --  Create IPv4 address from given sockaddr storage data.

   function To_IPv6 (S : Sockaddr_Storage_Type) return Anet.IPv6_Addr_Type;
   --  Create IPv6 address from given sockaddr storage data.

   function To_Mac (S : Sockaddr_Storage_Type) return Anet.Hardware_Addr_Type;
   --  Create link layer address from given sockaddr storage data.

   function To_Family
     (Value : Interfaces.C.unsigned_short)
      return Anet.Socket_Families.Family_Type;
   --  Return family type for given value.

   Fam_To_Addrtype : constant array (Anet.Socket_Families.Family_Type)
     of Iface_Addr_Type
       := (Anet.Socket_Families.Family_Packet => Link_Layer,
           Anet.Socket_Families.Family_Inet   => IPv4_Address,
           Anet.Socket_Families.Family_Inet6  => IPv6_Address,
           others                             => Unknown);

   -------------------------------------------------------------------------

   procedure Discover_Ifaces
   is
      use type C.int;

      Ret     : C.int;
      Ifa     : aliased If_Addrs_Handle;
      Current : If_Addrs_Handle;
      Logmsg  : Unbounded_String;
   begin
      C_Getifaddrs (Retval => Ret,
                    Ifap   => Ifa'Access);

      if Ret = Anet.C_Failure then
         raise Network_Error with "Could not discover network interfaces: "
           & Anet.Errno.Get_Errno_String;
      end if;

      Current := Ifa;
      while Current /= null loop

         if Current.Addr /= null then
            declare
               Family    : constant Anet.Socket_Families.Family_Type
                 := To_Family (Value => Current.Addr.Ss_Family);
               Addrtype  : constant Iface_Addr_Type
                 := Fam_To_Addrtype (Family);
               New_Iface : Iface_Type (Addrtype => Addrtype);
            begin
               New_Iface.Name := To_Unbounded_String
                 (Source => C.Strings.Value (Current.Name));
               Logmsg := "Discovered network interface "
                 & New_Iface.Name & " (" & Family'Img;

               case Addrtype is
                  when Link_Layer =>
                     New_Iface.Mac_Addr := To_Mac (S => Current.Addr.all);
                     Logmsg := Logmsg & " - " & Anet.To_String
                       (Address => New_Iface.Mac_Addr);
                  when IPv4_Address =>
                     New_Iface.Address := To_IPv4
                       (S => Current.Addr.all);
                     New_Iface.Broadcast := To_IPv4
                       (S => Current.Broadaddr.all);
                     New_Iface.Netmask := To_IPv4
                       (S => Current.Netmask.all);
                     Logmsg := Logmsg & " - " & Anet.To_String
                       (Address => New_Iface.Address);
                  when IPv6_Address =>
                     New_Iface.Address6 := To_IPv6
                       (S => Current.Addr.all);
                     New_Iface.Netmask6 := To_IPv6
                       (S => Current.Netmask.all);
                     Logmsg := Logmsg & " - " & Anet.To_String
                       (Address => New_Iface.Address6);
                  when Unknown => null;
               end case;

               Logmsg := Logmsg & ")";
               L.Log (Level   => L.Debug,
                      Message => To_String (Logmsg));

               System_Ifaces.Append (New_Item => New_Iface);

            exception
               when others =>
                  C_Freeifaddrs (Ifa => Ifa);
                  raise;
            end;
         end if;

         Current := Current.Next;
      end loop;

      C_Freeifaddrs (Ifa => Ifa);
   end Discover_Ifaces;

   -------------------------------------------------------------------------

   function Find (Address : Anet.IPv4_Addr_Type) return ILP.Cursor
   is
      use type Anet.IPv4_Addr_Type;
      use type ILP.Cursor;

      Position : ILP.Cursor := System_Ifaces.First;
   begin
      while Position /= ILP.No_Element loop
         declare
            E : constant Iface_Type := ILP.Element (Position => Position);
         begin
            if E.Addrtype = IPv4_Address and then E.Address = Address then
               return Position;
            end if;
         end;

         ILP.Next (Position);
      end loop;

      return ILP.No_Element;
   end Find;

   -------------------------------------------------------------------------

   function Get_Iface
     (Name     : Anet.Types.Iface_Name_Type;
      Addrtype : Iface_Addr_Type)
      return Iface_Type
   is
      use type Anet.Types.Iface_Name_Type;
      use type ILP.Cursor;

      Position : ILP.Cursor := System_Ifaces.First;
   begin
      while Position /= ILP.No_Element loop
         declare
            E : constant Iface_Type := ILP.Element (Position);
         begin
            if Get_Name (Iface => E) = Name and then E.Addrtype = Addrtype
            then
               return E;
            end if;
         end;
         ILP.Next (Position);
      end loop;

      raise Network_Error with "Could not find network interface "
        & String (Name);
   end Get_Iface;

   -------------------------------------------------------------------------

   function Get_Iface_Count return Natural
   is
   begin
      return Natural (System_Ifaces.Length);
   end Get_Iface_Count;

   -------------------------------------------------------------------------

   function Get_Name (Iface : Iface_Type) return Anet.Types.Iface_Name_Type
   is
   begin
      return Anet.Types.Iface_Name_Type (To_String (Iface.Name));
   end Get_Name;

   -------------------------------------------------------------------------

   function Is_Local_Iface (Address : Anet.IPv4_Addr_Type) return Boolean
   is
      use type ILP.Cursor;
   begin
      return Find (Address => Address) /= ILP.No_Element;
   end Is_Local_Iface;

   -------------------------------------------------------------------------

   function To_Family
     (Value : Interfaces.C.unsigned_short)
      return Anet.Socket_Families.Family_Type
   is
      use type Interfaces.C.int;
      use Anet.Socket_Families;

      Res : Family_Type := Family_Packet;
   begin
      for F in Families'Range loop
         if Families (F) = Interfaces.C.int (Value) then
            Res := F;
            exit;
         end if;
      end loop;

      return Res;
   end To_Family;

   -------------------------------------------------------------------------

   function To_IPv4 (S : Sockaddr_Storage_Type) return Anet.IPv4_Addr_Type
   is
      Unused : Anet.Sockets.Thin.Inet.Sockaddr_In_Type;
      Res    : Anet.IPv4_Addr_Type;
      Idx    : Positive := S.Data'First + Unused.Sin_Addr'Bit_Position / 8 - 2;
   begin
      for B of Res loop
         B   := S.Data (Idx);
         Idx := Idx + 1;
      end loop;

      return Res;
   end To_IPv4;

   -------------------------------------------------------------------------

   function To_IPv6 (S : Sockaddr_Storage_Type) return Anet.IPv6_Addr_Type
   is
      Unused : Anet.Sockets.Thin.Inet.Sockaddr_In_Type
        (Family => Anet.Socket_Families.Family_Inet6);
      Res    : Anet.IPv6_Addr_Type;
      Idx    : Positive := S.Data'First
        + Unused.Sin6_Addr'Bit_Position / 8 - 2;
   begin
      for B of Res loop
         B   := S.Data (Idx);
         Idx := Idx + 1;
      end loop;

      return Res;
   end To_IPv6;

   -------------------------------------------------------------------------

   function To_Mac (S : Sockaddr_Storage_Type) return Anet.Hardware_Addr_Type
   is
      Unused : Anet.Sockets.Thin.Packet.Sockaddr_Ll_Type;
      Res    : Anet.Hardware_Addr_Type (1 .. 6);
      Idx    : Positive := S.Data'First + Unused.Sa_Addr'Bit_Position / 8 - 2;
   begin
      for B of Res loop
         B   := S.Data (Idx);
         Idx := Idx + 1;
      end loop;

      return Res;
   end To_Mac;

end DHCP.Net;
