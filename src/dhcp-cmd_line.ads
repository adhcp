--
--  Copyright (C) 2014 secunet Security Networks AG
--  Copyright (C) 2014 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2014 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package DHCP.Cmd_Line
is

   procedure Set_Ext_Notify_Binary (B : String);
   --  Set binary to use for external notifications.

   function Get_Ext_Notify_Binary return String
   with
      Post => Get_Ext_Notify_Binary'Result'Length > 0;
   --  Return binary to use for external notifications. Default is
   --  '/usr/local/sbin/adhcp_notify_dbus'.

end DHCP.Cmd_Line;
