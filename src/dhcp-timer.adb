--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;

with System.Assertions;

with DHCP.Logger;
with DHCP.Termination;

pragma Elaborate_All (DHCP.Logger);
pragma Elaborate_All (DHCP.Termination);

package body DHCP.Timer is

   task Timer_Task is
      entry Start;
   end Timer_Task;
   --  Task implementing the timer.

   Started : Boolean := False;
   --  Timer task started flag.

   -------------------------------------------------------------------------

   procedure Cancel (Event : Timing_Events.Event_Type'Class)
   is
   begin
      Event_Queue.Remove (Event => Event);
   end Cancel;

   -------------------------------------------------------------------------

   procedure Clear
   is
   begin
      Event_Queue.Clear;
   end Clear;

   -------------------------------------------------------------------------

   function Event_Count return Natural
   is
   begin
      return Event_Queue.Length;
   end Event_Count;

   -------------------------------------------------------------------------

   procedure Schedule (Event : Timing_Events.Event_Type'Class)
   is
   begin
      Event_Queue.Insert (Event => Event);
   end Schedule;

   -------------------------------------------------------------------------

   procedure Start
   is
   begin
      if not Started then
         Timer_Task.Start;
         Started := True;
      end if;
   end Start;

   -------------------------------------------------------------------------

   procedure Stop
   is
   begin
      if Started then
         Event_Queue.Signal_Stop;
         Started := False;
      end if;
   end Stop;

   -------------------------------------------------------------------------

   task body Timer_Task is
      Shutdown    : Boolean;
      Event_Delay : Duration;
   begin
      loop
         Shutdown    := False;
         Event_Delay := Duration'Last;

         select
            accept Start;
         or
            terminate;
         end select;

         Main_Loop :
         loop
            begin
               select

                  ----------------------------------------------------------

                  Event_Queue.Event_Time_Changed
                    (New_Delay => Event_Delay,
                     Stop      => Shutdown);
                  if Shutdown then
                     exit Main_Loop;
                  end if;

               or

                  ----------------------------------------------------------

                  delay Event_Delay;
                  while Event_Queue.Has_Runnable_Event loop
                     declare
                        Current_Event : Timing_Events.Event_Type'Class
                          := Event_Queue.Get_Next_Event;
                     begin
                        Event_Queue.Remove (Event => Current_Event);
                        Current_Event.Trigger;
                     end;
                  end loop;
               end select;

            exception
               when E : System.Assertions.Assert_Failure =>
                  Logger.Log (Level   => Logger.Warning,
                              Message => "Timer: continuing execution after "
                              & "assertion error");
                  Logger.Log
                    (Level   => Logger.Warning,
                     Message => Ada.Exceptions.Exception_Information (X => E));
               when E : others =>
                  Logger.Log
                    (Level   => Logger.Error,
                     Message => "Timer: signaling termination due to error");
                  Logger.Log
                    (Level   => Logger.Error,
                     Message => Ada.Exceptions.Exception_Information (X => E));
                  Termination.Signal (Exit_Status => Termination.Failure);
            end;
         end loop Main_Loop;
         Event_Queue.Reset;
         Logger.Log (Message => "Timer task stopped");
      end loop;
   end Timer_Task;

end DHCP.Timer;
