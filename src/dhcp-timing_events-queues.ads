--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

private with Ada.Containers.Indefinite_Doubly_Linked_Lists;

package DHCP.Timing_Events.Queues is

   type Event_Data_List is private;

   protected type Protected_Event_Queue is

      procedure Insert (Event : Event_Type'Class);
      --  Insert given event into queue and if necessary signal a next event
      --  time change.

      procedure Remove (Event : Event_Type'Class);
      --  Remove given event from queue and if necessary signal a next event
      --  time change.
      --  If the specified event is not present in the queue nothing is done.

      procedure Clear;
      --  Clear all events from the queue and if necessary signal a next event
      --  time change.

      procedure Reset;
      --  Reset event queue to initial state.

      function Length return Natural;
      --  Return number of events in queue.

      function Next_Event_Time return Ada.Real_Time.Time;
      --  Return the trigger time of the next event in the queue. If the queue
      --  is empty Ada.Real_Time.Time_Last is returned.

      function Get_Next_Event return Event_Type'Class;
      --  Return the next event in the queue. If the queue is empty an
      --  exception is raised.

      function Has_Runnable_Event return Boolean;
      --  Returns True if there is an event in the queue ready to be triggered.

      procedure Signal_Stop;
      --  Stop processing of all events and signal an orderly shutdown.

      entry Event_Time_Changed
        (New_Delay : out Duration;
         Stop      : out Boolean);
      --  This entry is opened if the time of the next event changes. Stop is
      --  set to true if callers should perform an orderly shutdown.

   private

      Data         : Event_Data_List;
      Time_Changed : Boolean := False;
      Shutdown     : Boolean := False;

   end Protected_Event_Queue;

   No_Events       : exception;
   Event_Not_Found : exception;

private

   package LOSE is new Ada.Containers.Indefinite_Doubly_Linked_Lists
     (Element_Type => Timing_Events.Event_Type'Class);

   type Event_Data_List is new LOSE.List with null record;

end DHCP.Timing_Events.Queues;
