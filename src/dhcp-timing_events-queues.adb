--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body DHCP.Timing_Events.Queues is

   protected body Protected_Event_Queue is

      ----------------------------------------------------------------------

      procedure Clear
      is
         Was_Empty : constant Boolean := Data.Is_Empty;
      begin
         Data.Clear;

         if not Was_Empty then
            Time_Changed := True;
         end if;
      end Clear;

      ----------------------------------------------------------------------

      entry Event_Time_Changed
        (New_Delay : out Duration;
         Stop      : out Boolean)
        when Time_Changed or Shutdown
      is
         use Ada.Real_Time;
      begin
         if not Data.Is_Empty then
            if Data.First_Element.Get_Time < Clock then
               New_Delay := 0.0;
            else
               New_Delay := To_Duration
                 (TS => Data.First_Element.Get_Time - Clock);
            end if;
         else
            New_Delay := Duration'Last;
         end if;
         Stop         := Shutdown;
         Time_Changed := False;
      end Event_Time_Changed;

      ----------------------------------------------------------------------

      function Get_Next_Event return Event_Type'Class
      is
      begin
         if Data.Is_Empty then
            raise No_Events with "Unable to get event from empty queue";
         end if;

         return Data.First_Element;
      end Get_Next_Event;

      ----------------------------------------------------------------------

      function Has_Runnable_Event return Boolean
      is
         use type Ada.Real_Time.Time;
      begin
         return Next_Event_Time <= Ada.Real_Time.Clock;
      end Has_Runnable_Event;

      ----------------------------------------------------------------------

      procedure Insert (Event : Event_Type'Class)
      is
         use type Ada.Real_Time.Time;

         Pos             : LOSE.Cursor        := Data.First;
         Cur_Wakeup_Time : Ada.Real_Time.Time := Ada.Real_Time.Time_Last;

         procedure Update_Time_Barrier;
         --  Update barrier for event time change if necessary.

         procedure Update_Time_Barrier
         is
         begin
            if Data.First_Element.Get_Time < Cur_Wakeup_Time then
               Time_Changed := True;
            end if;
         end Update_Time_Barrier;
      begin
         if not Data.Is_Empty then
            Cur_Wakeup_Time := LOSE.Element (Position => Pos).Get_Time;
         end if;

         while LOSE.Has_Element (Position => Pos) loop
            if Event.Get_Time < LOSE.Element
              (Position => Pos).Get_Time
            then
               Data.Insert (Before   => Pos,
                            New_Item => Event);
               Update_Time_Barrier;
               return;
            end if;
            Pos := LOSE.Next (Position => Pos);
         end loop;

         Data.Append (New_Item => Event);
         Update_Time_Barrier;
      end Insert;

      ----------------------------------------------------------------------

      function Length return Natural
      is
      begin
         return Natural (Data.Length);
      end Length;

      ----------------------------------------------------------------------

      function Next_Event_Time return Ada.Real_Time.Time
      is
      begin
         if Data.Is_Empty then
            return Ada.Real_Time.Time_Last;
         end if;

         return Data.First_Element.Get_Time;
      end Next_Event_Time;

      ----------------------------------------------------------------------

      procedure Remove (Event : Event_Type'Class)
      is
         use type Ada.Real_Time.Time;
         use type LOSE.Cursor;

         Cur_Wakeup_Time : Ada.Real_Time.Time := Ada.Real_Time.Time_Last;
         Pos : LOSE.Cursor := Data.Find (Item => Event);
      begin
         if Pos = LOSE.No_Element then
            return;
         end if;

         Cur_Wakeup_Time := Data.First_Element.Get_Time;
         Data.Delete (Position => Pos);
         if Data.Is_Empty
           or else Cur_Wakeup_Time /= Data.First_Element.Get_Time
         then
            Time_Changed := True;
         end if;
      end Remove;

      ----------------------------------------------------------------------

      procedure Reset
      is
      begin
         Data.Clear;
         Time_Changed := False;
         Shutdown     := False;
      end Reset;

      ----------------------------------------------------------------------

      procedure Signal_Stop
      is
      begin
         Clear;
         Shutdown := True;
      end Signal_Stop;

   end Protected_Event_Queue;

end DHCP.Timing_Events.Queues;
