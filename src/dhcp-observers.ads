--
--  Copyright (C) 2011, 2015 secunet Security Networks AG
--  Copyright (C) 2011, 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet.Types;

with DHCP.Notify;

package DHCP.Observers
is

   generic
      with function Get_Interface_Name return Anet.Types.Iface_Name_Type;
      --  Function to retrieve name of interface to configure.

      with procedure Write_Database (Destination : String);
      --  Procedure to serialize configuration database to file.

   procedure Call_External_Notify (R : DHCP.Notify.Reason_Type);
   --  This callback executes an external binary passing the reason, interface
   --  name and lease database as parameters.

   procedure One_Shot_Mode (R : DHCP.Notify.Reason_Type);
   --  This observer callback implements the one-shot mode. Program termination
   --  is signaled if the given reason is 'Bound' or if a lease cannot be
   --  acquired.

end DHCP.Observers;
