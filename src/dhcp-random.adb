--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Sequential_IO;
with Ada.Numerics.Float_Random;

with Anet.Thin;

package body DHCP.Random is

   Random_Source : constant String := "/dev/urandom";

   package S_IO is new Ada.Sequential_IO (Element_Type => Anet.Word32);

   Generator : Ada.Numerics.Float_Random.Generator;

   -------------------------------------------------------------------------

   function Get return Anet.Word32
   is
      use S_IO;

      Input_File : File_Type;
      Random     : Anet.Word32 := 0;
   begin
      begin
         Open (File => Input_File,
               Mode => In_File,
               Name => Random_Source,
               Form => "shared=yes");

      exception
         when others =>
            raise Random_Source_Error with "Could not open random source "
              & Random_Source;
      end;

      Read (File => Input_File,
            Item => Random);

      Close (File => Input_File);

      return Random;
   end Get;

   -------------------------------------------------------------------------

   function Get
     (Low  : Float;
      High : Float)
      return Float
   is
      Rand_Max : constant Float
        := Ada.Numerics.Float_Random.Uniformly_Distributed'Last;
      F_Rand   : constant Float
        := Ada.Numerics.Float_Random.Random (Gen => Generator);
   begin
      return Low + (F_Rand * (High - Low) / Rand_Max);
   end Get;

   -------------------------------------------------------------------------

begin
   Ada.Numerics.Float_Random.Reset
     (Gen       => Generator,
      Initiator => Integer (Anet.Thin.C_Getpid));
end DHCP.Random;
