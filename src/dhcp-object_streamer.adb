--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;
with Ada.Streams.Stream_IO;

with DHCP.OS;

package body DHCP.Object_Streamer
is

   -------------------------------------------------------------------------

   function Deserialize (Filename : String) return Object_Type
   is
      File : Ada.Streams.Stream_IO.File_Type;
   begin
      begin
         Ada.Streams.Stream_IO.Open
           (File => File,
            Mode => Ada.Streams.Stream_IO.In_File,
            Name => Filename);

      exception
         when E : others =>
            raise DHCP.OS.IO_Error with "Error opening file - "
              & Ada.Exceptions.Exception_Message (E);
      end;

      begin
         return O : Object_Type do
            O := Object_Type'Input
              (Ada.Streams.Stream_IO.Stream (File => File));
            Ada.Streams.Stream_IO.Close (File => File);
         end return;

      exception
         when others =>
            Ada.Streams.Stream_IO.Close (File);
            raise DHCP.OS.IO_Error with "Unable to read data from file '"
              & Filename & "'";
      end;
   end Deserialize;

   -------------------------------------------------------------------------

   procedure Serialize
     (Filename : String;
      Object   : Object_Type)
   is
      File : Ada.Streams.Stream_IO.File_Type;
   begin
      Ada.Streams.Stream_IO.Create
        (File => File,
         Mode => Ada.Streams.Stream_IO.Out_File,
         Name => Filename);
      Object_Type'Output
        (Ada.Streams.Stream_IO.Stream (File => File), Object);
      Ada.Streams.Stream_IO.Close (File => File);

   exception
      when E : others =>
         raise DHCP.OS.IO_Error with "Error writing object to file - "
           & Ada.Exceptions.Exception_Message (E);
   end Serialize;

end DHCP.Object_Streamer;
