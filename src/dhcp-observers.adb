--
--  Copyright (C) 2011-2015 secunet Security Networks AG
--  Copyright (C) 2011-2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011-2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Strings.Fixed;

with DHCP.OS;
with DHCP.Logger;
with DHCP.Termination;
with DHCP.Cmd_Line;

package body DHCP.Observers
is

   package L renames DHCP.Logger;

   Lease_Filename : constant String := "/tmp/adhcp_client.lease";
   --  Filename to store temporary lease information.

   -------------------------------------------------------------------------

   procedure Call_External_Notify (R : DHCP.Notify.Reason_Type)
   is
      Binary     : constant String
        := DHCP.Cmd_Line.Get_Ext_Notify_Binary;
      Lease_File : constant String  := Lease_Filename & "-"
        & Ada.Strings.Fixed.Trim
        (Source => DHCP.OS.Get_Pid'Img,
         Side   => Ada.Strings.Left);
      Iface_Str  : constant String  := String (Get_Interface_Name);
      Args       : constant String
        := " " & R'Img & " " & Iface_Str & " " & Lease_File;
   begin
      Write_Database (Destination => Lease_File);

      L.Log (Message => "Running external notifier " & Binary
             & " (interface: " & Iface_Str & ", reason: " & R'Img & ")");
      begin
         DHCP.OS.Execute (Command => Binary & Args);

      exception
         when others =>
            DHCP.OS.Delete_File (Filename       => Lease_File,
                                 Ignore_Missing => True);
            raise;
      end;

      DHCP.OS.Delete_File (Filename => Lease_File);
   end Call_External_Notify;

   -------------------------------------------------------------------------

   procedure One_Shot_Mode (R : DHCP.Notify.Reason_Type)
   is
      use type DHCP.Notify.Reason_Type;
   begin
      if R = DHCP.Notify.Bound then
         DHCP.Termination.Signal (Exit_Status => DHCP.Termination.Success);
      else
         DHCP.Termination.Signal (Exit_Status => DHCP.Termination.Failure);
      end if;
   end One_Shot_Mode;

end DHCP.Observers;
