--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Real_Time;

package DHCP.Timing_Events is

   type Event_Type is abstract tagged private;
   --  Events specify a trigger procedure and a time. When an event is put into
   --  an event queue it's procedure will be executed at the specified time.

   function Get_Time (Event : Event_Type'Class) return Ada.Real_Time.Time;
   --  Return the event's time when it is to be triggered.

   procedure Set_Time
     (Event   : in out Event_Type'Class;
      At_Time :        Ada.Real_Time.Time);
   --  Set the event's time.

   procedure Trigger (Event : in out Event_Type) is abstract;
   --  This procedure is called when an event is triggered.

private

   type Event_Type is abstract tagged record
      Time : Ada.Real_Time.Time := Ada.Real_Time.Time_First;
   end record;

end DHCP.Timing_Events;
