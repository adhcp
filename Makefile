BINDIR  = bin
OBJDIR  = obj
COVDIR  = $(OBJDIR)/cov
PROFDIR = $(OBJDIR)/prof

VERSION_SPEC  = src/dhcp-version.ads
VERSION       = $(shell cat .version | sed 's/^v//')
GIT_REV      := $(shell git describe --always 2> /dev/null)

ADHCP   = adhcp-$(VERSION)
TARBALL = $(ADHCP).tar

DESTDIR ?= /usr/local

NUM_CPUS := $(shell getconf _NPROCESSORS_ONLN)

# Set these variables in the environment or on the command line to
# override the default settings.
# ADAFLAGS        : Ada compiler options.
# BUILDER_OPTIONS : gnatmake or gprbuild options.
# LDFLAGS         : linker options.
GMAKE_OPTS = -p -R -j$(NUM_CPUS) \
  $(BUILDER_OPTIONS) $(foreach v,ADAFLAGS LDFLAGS,"-X$(v)=$($(v))")

all: build_tools build_notify build_notify_dbus

.version: FORCE
	@if [ -d .git ]; then \
		if [ -r $@ ]; then \
			if [ "$$(cat $@)" != "$(GIT_REV)" ]; then \
				echo $(GIT_REV) > $@; \
			fi; \
		else \
			echo $(GIT_REV) > $@; \
		fi \
	fi

$(VERSION_SPEC): .version
	@echo "package DHCP.Version is"                 > $@
	@echo "   Version_String : constant String :=" >> $@
	@echo "     \"$(VERSION)\";"                   >> $@
	@echo "end DHCP.Version;"                      >> $@

build_tools build_notify build_notify_dbus build_tests: build_%: $(VERSION_SPEC)
	@gprbuild $(GMAKE_OPTS) -Padhcp_$*.gpr

tests: build_tests
	@obj/tests/test_runner

build_all: build_tests build_tools build_notify build_notify_dbus

cov:
	@rm -f $(COVDIR)/*.gcda
	@gprbuild $(GMAKE_OPTS) -Padhcp_tests.gpr -XBUILD="coverage"
	@$(COVDIR)/test_runner || true
	@lcov -c -d $(COVDIR) -o $(COVDIR)/cov.info
	@lcov -e $(COVDIR)/cov.info "$(PWD)/src/*.adb" -o $(COVDIR)/cov.info
	@genhtml --no-branch-coverage $(COVDIR)/cov.info -o $(COVDIR)

prof:
	@mkdir -p $(PROFDIR)
	@rm -f $(PROFDIR)/callgrind.*
	@gprbuild $(GMAKE_OPTS) -Padhcp_tests.gpr -XBUILD="profiling"
	@cd $(PROFDIR) && \
	valgrind -q --tool=callgrind --dump-instr=yes ./profiler
	@callgrind_annotate $(PROFDIR)/callgrind.* > $(PROFDIR)/profile.txt

install: install_tools install_notify install_notify_dbus

install_tools: build_tools
	install -m 2775 -d $(DESTDIR)/sbin
	install $(BINDIR)/adhcp_dump $(DESTDIR)/sbin
	install $(BINDIR)/adhcp_client $(DESTDIR)/sbin
	install $(BINDIR)/adhcp_client6 $(DESTDIR)/sbin
	install $(BINDIR)/adhcp_client_wrapper $(DESTDIR)/sbin
	install $(BINDIR)/adhcp_relay $(DESTDIR)/sbin

install_notify: build_notify
	install -m 2775 -d $(DESTDIR)/sbin
	install $(BINDIR)/adhcp_notify_simple $(DESTDIR)/sbin
	install $(BINDIR)/adhcp_notify6_isc_script $(DESTDIR)/sbin

install_notify_dbus: build_notify_dbus
	install -m 2775 -d $(DESTDIR)/sbin
	install $(BINDIR)/adhcp_notify_dbus $(DESTDIR)/sbin
	install $(BINDIR)/adhcp_notify6_dbus $(DESTDIR)/sbin

install_tests: build_tests
	install -v -d $(DESTDIR)/tests
	install $(OBJDIR)/tests/test_runner $(DESTDIR)/tests
	cp -vr data $(DESTDIR)/tests

clean:
	@rm -rf $(BINDIR)/*
	@rm -rf $(OBJDIR)/*

distclean: clean
	@rm -rf $(BINDIR)
	@rm -rf $(OBJDIR)
	@$(MAKE) -C doc clean

doc:
	@$(MAKE) -C doc

dist: distclean $(VERSION_SPEC)
	@echo "Creating release tarball $(TARBALL) ... "
	@git archive --format=tar HEAD --prefix $(ADHCP)/ > $(TARBALL) && \
		tar -r -f $(TARBALL) --transform 's,^,$(ADHCP)/,' \
			.version src/dhcp-version.ads && bzip2 -f $(TARBALL)

FORCE:

.PHONY: tests doc
