--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Strings.Fixed;

with D_Bus.Connection;
with D_Bus.Arguments.Basic;

with Anet;

with DHCP.OS;
with DHCP.Logger;

with DHCPv4.Database;

with DHCPv6.IAs;
with DHCPv6.Database;
with DHCPv6.Options;

with Utils_D_Bus;

with Adhcp_Publishers.Dbus_Target;
with Adhcp_Publishers.Dbus_Connection;

package body Adhcp_Publishers.Dbus
is

   use D_Bus.Arguments.Containers;
   use D_Bus.Arguments.Basic;

   procedure Init_Args
     (Args   : out D_Bus.Arguments.Containers.Array_Type;
      Reason :     String;
      Iface  :     String);
   --  Initialize D-Bus args with common lease information.

   -------------------------------------------------------------------------

   procedure Init_Args
     (Args   : out D_Bus.Arguments.Containers.Array_Type;
      Reason :     String;
      Iface  :     String)
   is
   begin
      Args.Append (New_Item => Create
                   (Key   => +"reason",
                    Value => Create
                      (Source => Utils_D_Bus.To_Byte_Array
                         (Str => Reason))));
      Args.Append (New_Item => Create
                   (Key   => +"interface",
                    Value => Create
                      (Source => Utils_D_Bus.To_Byte_Array
                         (Str => Iface))));
      Args.Append (New_Item => Create
                   (Key   => +"pid",
                    Value => Create
                      (Source => Utils_D_Bus.To_Byte_Array
                         (Str => Ada.Strings.Fixed.Trim
                              (Source => DHCP.OS.Get_Pgid'Img,
                               Side   => Ada.Strings.Left)))));
   end Init_Args;

   -------------------------------------------------------------------------

   function Lease_To_Args_V4
     (Reason : DHCP.Notify.Reason_Type;
      Iface  : String)
      return D_Bus.Arguments.Containers.Array_Type
   is
      Args : Array_Type;
   begin
      Init_Args (Args   => Args,
                 Reason => Reason'Img,
                 Iface  => Iface);
      if not DHCPv4.Database.Is_Clear then
         Dbus_Target.Add_V4_Lease_Data (Args => Args);
      end if;

      return Args;
   end Lease_To_Args_V4;

   -------------------------------------------------------------------------

   function Lease_To_Args_V6
     (Reason : DHCP.Notify.Reason_Type;
      Iface  : String)
      return D_Bus.Arguments.Containers.Array_Type
   is
      Args : Array_Type;

      procedure Add_Lease_Data;
      --  Add V6 lease data to D-Bus arguments.

      ----------------------------------------------------------------------

      procedure Add_Lease_Data
      is

         procedure Add_IA (IA : DHCPv6.IAs.Identity_Association_Type);
         --  Iterate over all addresses of an IA.

         procedure Add_Option (Opt : DHCPv6.Options.Inst.Option_Type'Class);
         --  Append DHCPv6 option to D-Bus arguments.

         -------------------------------------------------------------------

         procedure Add_IA (IA : DHCPv6.IAs.Identity_Association_Type)
         is
            procedure Add_Address
              (Addr : DHCPv6.Options.Inst6.IA_Address_Option_Type);
            --  Append IPv6 address to D-Bus arguments.

            procedure Add_Address
              (Addr : DHCPv6.Options.Inst6.IA_Address_Option_Type)
            is
               Pref, Valid : Duration;
            begin
               Addr.Get_Lifetimes (Preferred => Pref,
                                   Valid     => Valid);

               declare
                  Valid_Time : constant String
                    := Ada.Strings.Fixed.Trim
                      (Source => Long_Long_Integer (Valid)'Img,
                       Side   => Ada.Strings.Left);
               begin
                  Args.Append (New_Item => Create
                               (Key   => +"new_max_life",
                                Value => Create
                                  (Source => Utils_D_Bus.To_Byte_Array
                                     (Str => Valid_Time))));
               end;

               declare
                  Pref_Time : constant String
                    := Ada.Strings.Fixed.Trim
                      (Source => Long_Long_Integer (Pref)'Img,
                       Side   => Ada.Strings.Left);
               begin
                  Args.Append (New_Item => Create
                               (Key   => +"new_preferred_life",
                                Value => Create
                                  (Source => Utils_D_Bus.To_Byte_Array
                                     (Str => Pref_Time))));
               end;

               Args.Append (New_Item => Create
                            (Key   => +"new_ip6_address",
                             Value => Create
                               (Source => Utils_D_Bus.To_Byte_Array
                                  (Str => Anet.To_String
                                       (Address => Addr.Get_Address)))));
            end Add_Address;
         begin
            DHCPv6.IAs.Iterate (IA      => IA,
                                Process => Add_Address'Access);
         end Add_IA;

         -------------------------------------------------------------------

         procedure Add_Option (Opt : DHCPv6.Options.Inst.Option_Type'Class)
         is
            Opt_Key : constant String := "new_dhcp6_" & Opt.Get_Name_Str;
         begin
            Args.Append (New_Item => Create
                         (Key   => +Opt_Key,
                          Value => Create
                            (Source => Utils_D_Bus.To_Byte_Array
                               (Str => Opt.Get_Data_Str))));
         end Add_Option;
      begin
         DHCPv6.Database.Iterate (Process => Add_IA'Access);
         DHCPv6.Database.Get_Global_Options.Iterate
           (Process => Add_Option'Access);
      end Add_Lease_Data;
   begin
      Init_Args (Args   => Args,
                 Reason => Reason'Img & "6",
                 Iface  => Iface);
      if DHCPv6.Database.Is_Initialized then
         Add_Lease_Data;
      end if;

      return Args;
   end Lease_To_Args_V6;

   -------------------------------------------------------------------------

   procedure NM_Publisher
     (Reason : DHCP.Notify.Reason_Type;
      Iface  : String)
   is
   begin
      DHCP.Logger.Log (Message => "Sending status notification for interface "
                       & Iface & " (reason: " & Reason'Img & ")");

      declare
         Conn : constant D_Bus.Connection.Connection_Type
           := Dbus_Connection.Create;
      begin

         --  Workaround for Glib Bug #1831
         --  https://gitlab.gnome.org/GNOME/glib/issues/1831

         delay 0.1;

         Dbus_Target.Send (Connection => Conn,
                           Args       => +Create_Args
                             (Reason => Reason,
                              Iface  => Iface));
         D_Bus.Connection.Flush (Connection => Conn);
      end;
   end NM_Publisher;

end Adhcp_Publishers.Dbus;
