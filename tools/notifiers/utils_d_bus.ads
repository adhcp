--
--  Copyright (C) 2017 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2017 Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with D_Bus.Arguments.Containers;

package Utils_D_Bus
is

   function To_Byte_Array
     (Str : String)
      return D_Bus.Arguments.Containers.Array_Type;
   --  Convert given string to D-Bus byte array.

end Utils_D_Bus;
