--
--  Copyright (C) 2017 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2017 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Anet;

with D_Bus.Types;
with D_Bus.Arguments.Basic;

with DHCPv4.Options;
with DHCPv4.Database;

with Utils_D_Bus;

package body Adhcp_Publishers.Dbus_Target
is

   -------------------------------------------------------------------------

   procedure Add_V4_Lease_Data
     (Args : in out D_Bus.Arguments.Containers.Array_Type)
   is
      use D_Bus.Arguments.Basic;
      use D_Bus.Arguments.Containers;

      procedure Add_Option (Opt : DHCPv4.Options.Inst.Option_Type'Class);
      --  Append DHCP option to D-Bus arguments.

      ----------------------------------------------------------------------

      procedure Add_Option (Opt : DHCPv4.Options.Inst.Option_Type'Class)
      is
         Opt_Key : constant String := "new_" & Opt.Get_Name_Str;
      begin
         Args.Append (New_Item => Create
                      (Key   => +Opt_Key,
                       Value => Create
                         (Source => Utils_D_Bus.To_Byte_Array
                            (Str => Opt.Get_Data_Str))));
      end Add_Option;
   begin
      Args.Append
        (New_Item => Create
           (Key   => +"new_ip_address",
            Value => Create
              (Source => Utils_D_Bus.To_Byte_Array
                   (Str => Anet.To_String
                        (Address => DHCPv4.Database.Get_Fixed_Address)))));

      DHCPv4.Database.Get_Options.Iterate (Process => Add_Option'Access);
   end Add_V4_Lease_Data;

   -------------------------------------------------------------------------

   procedure Send
     (Connection : D_Bus.Connection.Connection_Type;
      Args       : D_Bus.Arguments.Argument_List_Type)
   is
      use type D_Bus.Types.Obj_Path;
   begin
      D_Bus.Connection.Send_Signal
        (Connection  => Connection,
         Object_Name => +"/",
         Iface       => "org.freedesktop.nm_dhcp_client",
         Name        => "Event",
         Args        => Args);
   end Send;

end Adhcp_Publishers.Dbus_Target;
