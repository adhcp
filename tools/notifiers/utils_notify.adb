--
--  Copyright (C) 2014, 2015 secunet Security Networks AG
--  Copyright (C) 2014, 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2014, 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Command_Line;

with Anet.Types;

package body Utils_Notify
is

   -------------------------------------------------------------------------

   procedure Get_Cmdline_Args
     (Reason : out DHCP.Notify.Reason_Type;
      Iface  : out Ada.Strings.Unbounded.Unbounded_String;
      Lease  : out Ada.Strings.Unbounded.Unbounded_String)
   is
   begin
      if Ada.Command_Line.Argument_Count /= 3 then
         raise Cmdline_Error with Ada.Command_Line.Command_Name
           & " <reason> <interface> <lease>";
      end if;

      Validate_Reason :
      begin
         Reason := DHCP.Notify.Reason_Type'Value
           (Ada.Command_Line.Argument (1));

      exception
         when others =>
            raise Cmdline_Error with "Invalid reason '"
              & Ada.Command_Line.Argument (1) & "', notify failed";
      end Validate_Reason;

      if not Anet.Types.Is_Valid_Iface
        (Name => Ada.Command_Line.Argument (2))
      then
         raise Cmdline_Error with "Invalid interface '"
           & Ada.Command_Line.Argument (2) & "', notify failed";
      else
         Iface := Ada.Strings.Unbounded.To_Unbounded_String
           (Ada.Command_Line.Argument (2));
      end if;

      Lease := Ada.Strings.Unbounded.To_Unbounded_String
        (Ada.Command_Line.Argument (3));
   end Get_Cmdline_Args;

end Utils_Notify;
