--
--  Copyright (C) 2017 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2017 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with D_Bus.Connection;
with D_Bus.Arguments.Containers;

--  NetworkManager signal target.
private package Adhcp_Publishers.Dbus_Target
is

   procedure Add_V4_Lease_Data
     (Args : in out D_Bus.Arguments.Containers.Array_Type);
   --  Add V4 lease data to D-Bus arguments.

   procedure Send
     (Connection : D_Bus.Connection.Connection_Type;
      Args       : D_Bus.Arguments.Argument_List_Type);
   --  Perform publish operation using signal.

end Adhcp_Publishers.Dbus_Target;
