--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCPv6.Database.IO;

with Adhcp_Publishers.Env;
with Adhcp_Notifier_Tool;

procedure Adhcp_Notify6_ISC_Script is new Adhcp_Notifier_Tool
  (Load_Database         => DHCPv6.Database.IO.Load,
   Publish_Configuration => Adhcp_Publishers.Env.V6_ISC_Script_Publisher);
