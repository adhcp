--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Environment_Variables;

with Anet;

with DHCP.OS;

with DHCPv6.IAs;
with DHCPv6.Options;
with DHCPv6.Database;

package body Adhcp_Publishers.Env
is

   use DHCPv6;

   -------------------------------------------------------------------------

   procedure V6_ISC_Script_Publisher
     (Reason : DHCP.Notify.Reason_Type;
      Iface  : String)
   is
      use type DHCP.Notify.Reason_Type;

      Executed : Boolean := False;
      Prefix   : constant String
        := (if Reason = DHCP.Notify.Release then
               "old_ip6" else "new_ip6");

      procedure Export_IA (IA : IAs.Identity_Association_Type);
      --  Export IA.

      procedure Export_Option (Opt : Options.Inst.Option_Type'Class);
      --  Export given DHCPv6 option.

      ----------------------------------------------------------------------

      procedure Export_IA (IA : IAs.Identity_Association_Type)
      is
         procedure Export_Address
           (Addr : Options.Inst6.IA_Address_Option_Type);
         --  Export an address.

         procedure Export_Address
           (Addr : Options.Inst6.IA_Address_Option_Type)
         is
         begin
            Ada.Environment_Variables.Set
              (Name  => Prefix & "_address",
               Value => Anet.To_String (Address => Addr.Get_Address));
            DHCP.OS.Execute (Command => "/sbin/dhclient-script");
            if not Executed then
               Executed := True;
            end if;
         end Export_Address;
      begin
         IAs.Iterate (IA      => IA,
                      Process => Export_Address'Access);
      end Export_IA;

      ----------------------------------------------------------------------

      procedure Export_Option (Opt : Options.Inst.Option_Type'Class)
      is
      begin
         Ada.Environment_Variables.Set
           (Name  => "new_dhcp6_" & Opt.Get_Name_Str,
            Value => Opt.Get_Data_Str);
      end Export_Option;
   begin
      Ada.Environment_Variables.Set
        (Name  => "reason",
         Value => Reason'Img & "6");
      Ada.Environment_Variables.Set
        (Name  => "interface",
         Value => Iface);
      Ada.Environment_Variables.Set
        (Name  => Prefix & "_prefixlen",
         Value => "64");

      Database.Get_Global_Options.Iterate
        (Process => Export_Option'Access);
      Database.Iterate (Process => Export_IA'Access);

      --  Stateless case, no addresses present to export -> publish stateless
      --  data.

      if not Executed then
         DHCP.OS.Execute (Command => "/sbin/dhclient-script");
      end if;
   end V6_ISC_Script_Publisher;

end Adhcp_Publishers.Env;
