--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with D_Bus.Arguments.Containers;

with DHCP.Notify;

package Adhcp_Publishers.Dbus
is

   function Lease_To_Args_V4
     (Reason : DHCP.Notify.Reason_Type;
      Iface  : String)
      return D_Bus.Arguments.Containers.Array_Type;
   --  Create D-Bus arguments from V4 lease information.

   function Lease_To_Args_V6
     (Reason : DHCP.Notify.Reason_Type;
      Iface  : String)
      return D_Bus.Arguments.Containers.Array_Type;
   --  Create D-Bus arguments from V6 lease information.

   generic

      with function Create_Args
        (Reason : DHCP.Notify.Reason_Type;
         Iface  : String)
         return D_Bus.Arguments.Containers.Array_Type;
      --  Function to create D-Bus args from lease information.

   procedure NM_Publisher
     (Reason : DHCP.Notify.Reason_Type;
      Iface  : String);
   --  Publish lease to NetworkManager via D-Bus.

end Adhcp_Publishers.Dbus;
