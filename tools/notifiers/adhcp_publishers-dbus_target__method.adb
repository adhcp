--
--  Copyright (C) 2017 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2017 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Strings.Unbounded;

with Anet;

with D_Bus.Types;
with D_Bus.Arguments.Basic;

with DHCPv4.Options;
with DHCPv4.Database;

with Utils_D_Bus;

--  NetworkManager sync method call target.
package body Adhcp_Publishers.Dbus_Target
is

   use Ada.Strings.Unbounded;

   function S
     (Source : Unbounded_String)
      return String
      renames To_String;

   function U
     (Source : String)
      return Unbounded_String
      renames To_Unbounded_String;

   Lease_Optnames : constant array
     (DHCPv4.Options.Option_Name) of Unbounded_String
     := (DHCPv4.Options.Address_Time   => U ("new_dhcp_lease_time"),
         DHCPv4.Options.DHCP_Server_Id => U ("new_dhcp_server_identifier"),
         DHCPv4.Options.Domain_Search  => U ("new_dhcp_domain_search"),
         others                        => Null_Unbounded_String);
   --  Option name mangling table for method-based NetworkManager.

   -------------------------------------------------------------------------

   procedure Add_V4_Lease_Data
     (Args : in out D_Bus.Arguments.Containers.Array_Type)
   is
      use D_Bus.Arguments.Basic;
      use D_Bus.Arguments.Containers;

      procedure Add_Option (Opt : DHCPv4.Options.Inst.Option_Type'Class);
      --  Append DHCP option to D-Bus arguments.

      ----------------------------------------------------------------------

      procedure Add_Option (Opt : DHCPv4.Options.Inst.Option_Type'Class)
      is
         Opt_Key  : Unbounded_String;
         Opt_Name : constant Unbounded_String := Lease_Optnames (Opt.Get_Name);
      begin
         if Opt_Name /= Null_Unbounded_String then
            Opt_Key := Opt_Name;
         else
            Opt_Key := U ("new_" & Opt.Get_Name_Str);
         end if;

         Args.Append (New_Item => Create
                      (Key   => +S (Opt_Key),
                       Value => Create
                         (Source => Utils_D_Bus.To_Byte_Array
                            (Str => Opt.Get_Data_Str))));
      end Add_Option;
   begin
      Args.Append
        (New_Item => Create
           (Key   => +"new_ip_address",
            Value => Create
              (Source => Utils_D_Bus.To_Byte_Array
                   (Str => Anet.To_String
                        (Address => DHCPv4.Database.Get_Fixed_Address)))));

      DHCPv4.Database.Get_Options.Iterate (Process => Add_Option'Access);
   end Add_V4_Lease_Data;

   -------------------------------------------------------------------------

   procedure Send
     (Connection : D_Bus.Connection.Connection_Type;
      Args       : D_Bus.Arguments.Argument_List_Type)
   is
      use type D_Bus.Types.Obj_Path;

      Dummy : D_Bus.Arguments.Argument_List_Type;
   begin
      Dummy := D_Bus.Connection.Call_Blocking
        (Connection  => Connection,
         Destination => "org.freedesktop.nm_dhcp_server",
         Path        => +"/org/freedesktop/nm_dhcp_server",
         Iface       => "org.freedesktop.nm_dhcp_server",
         Method      => "Notify",
         Args        => Args);
   end Send;

end Adhcp_Publishers.Dbus_Target;
