--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Logger;

with DHCPv4.Database;
with DHCPv4.Notify_Simple;

package body Adhcp_Publishers.File
is

   -------------------------------------------------------------------------

   procedure Udhcpc_File_Publisher
     (Reason : DHCP.Notify.Reason_Type;
      Iface  : String)
   is
      use type DHCP.Notify.Reason_Type;

      Lease_Filename : constant String := "/tmp/dhcp.lease";
   begin
      if DHCPv4.Database.Is_Clear
        or else (Reason /= DHCP.Notify.Preinit
                 and then Reason /= DHCP.Notify.Bound)
      then
         return;
      end if;

      DHCP.Logger.Log (Message => "Writing lease data for interface "
                       & Iface & " to file '" & Lease_Filename & "' "
                       & "(reason: " & Reason'Img & ")");
      DHCPv4.Notify_Simple.Write_Lease_File (State    => Reason,
                                             Iface    => Iface,
                                             Filename => Lease_Filename);
   end Udhcpc_File_Publisher;

end Adhcp_Publishers.File;
