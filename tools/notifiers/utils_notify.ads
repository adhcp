--
--  Copyright (C) 2014, 2015 secunet Security Networks AG
--  Copyright (C) 2014, 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2014, 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Strings.Unbounded;

with DHCP.Notify;

package Utils_Notify
is

   procedure Get_Cmdline_Args
     (Reason : out DHCP.Notify.Reason_Type;
      Iface  : out Ada.Strings.Unbounded.Unbounded_String;
      Lease  : out Ada.Strings.Unbounded.Unbounded_String);
   --  Validate and return notifier command line arguments. If a command line
   --  argument is invalid, an exception is raised.

   Cmdline_Error : exception;

end Utils_Notify;
