--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with DHCP.Notify;

generic

   with procedure Load_Database (Path : String);
   --  Procedure to initialize the configuration database from the given file.

   with procedure Publish_Configuration
     (Reason : DHCP.Notify.Reason_Type;
      Iface  : String);
   --  The actual notifier procedure for a specific notifier tool.

procedure Adhcp_Notifier_Tool;
