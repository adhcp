--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Command_Line;
with Ada.Exceptions;
with Ada.Strings.Unbounded;

with DHCP.Logger;
with DHCP.Notify;

with Utils_Notify;

procedure Adhcp_Notifier_Tool
is
   use Ada.Strings.Unbounded;
   use DHCP;

   Reason     : Notify.Reason_Type;
   Iface      : Unbounded_String;
   Lease_Path : Unbounded_String;
begin
   Utils_Notify.Get_Cmdline_Args (Reason => Reason,
                                  Iface  => Iface,
                                  Lease  => Lease_Path);

   Load_Database (Path => To_String (Lease_Path));

   Publish_Configuration (Reason => Reason,
                          Iface  => To_String (Iface));
   Logger.Stop;
   Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Success);

exception
   when E : Utils_Notify.Cmdline_Error =>
      DHCP.Logger.Log (Level   => DHCP.Logger.Error,
                       Message => Ada.Exceptions.Exception_Message (X => E));
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
      Logger.Stop;
   when E : others =>
      DHCP.Logger.Log
        (Level   => DHCP.Logger.Error,
         Message => "Terminating due to error");
      DHCP.Logger.Log
        (Level   => DHCP.Logger.Error,
         Message => Ada.Exceptions.Exception_Information (X => E));
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
      Logger.Stop;
end Adhcp_Notifier_Tool;
