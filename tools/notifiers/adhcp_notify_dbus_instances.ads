--
--  Copyright (C) 2011-2015 secunet Security Networks AG
--  Copyright (C) 2011-2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011-2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Adhcp_Publishers.Dbus;

package Adhcp_Notify_Dbus_Instances
is

   procedure Publish_V4 is new Adhcp_Publishers.Dbus.NM_Publisher
     (Create_Args => Adhcp_Publishers.Dbus.Lease_To_Args_V4);

   procedure Publish_V6 is new Adhcp_Publishers.Dbus.NM_Publisher
     (Create_Args => Adhcp_Publishers.Dbus.Lease_To_Args_V6);

end Adhcp_Notify_Dbus_Instances;
