--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

package body Adhcp_Publishers.Dbus_Connection
is

   Socket_Path : constant String
     := "unix:path=/var/run/NetworkManager/private-dhcp";

   -------------------------------------------------------------------------

   function Create return D_Bus.Connection.Connection_Type
   is
   begin
      return D_Bus.Connection.Connect (Address => Socket_Path);
   end Create;

end Adhcp_Publishers.Dbus_Connection;
