--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;
with Ada.Command_Line;
with Ada.Interrupts.Names;

with Anet.Sockets.Filters;
with Anet.Sockets.Packet;

with DHCP.Logger;
with DHCP.Signals;

with DHCPv4.Socket_Callbacks;
with DHCPv4.Receivers;

procedure Adhcp_Dump_LL
is
   use DHCP;
   use DHCPv4;

   pragma Unreserve_All_Interrupts;
   --  Unreserve SIGINT to handle CTRL-C.

   Handler : Signals.Handler_Type (Signal => Ada.Interrupts.Names.SIGINT);
   Sock    : aliased Anet.Sockets.Packet.UDP_Socket_Type;
   Rcvr    : Receivers.Packet.Receiver_Type (S => Sock'Access);
begin
   Logger.Use_Stdout;

   Sock.Init;
   Anet.Sockets.Filters.Set_Filter
     (Socket => Anet.Sockets.Socket_Type (Sock),
      Filter => Anet.Sockets.Filters.Filter_UDP_Port_Bootpc);

   Sock.Bind (Iface => "eth0");

   Rcvr.Listen (Callback => Socket_Callbacks.Print_Raw'Access);

   Logger.Log (Message => "Waiting for DHCP messages...");
   Handler.Wait;

   Rcvr.Stop;

   Logger.Stop;
   Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Success);

exception
   when E : others =>
      Logger.Log (Level   => Logger.Error,
                  Message => "Terminating due to error");
      Logger.Log (Level   => Logger.Error,
                  Message => Ada.Exceptions.Exception_Information (X => E));
      Logger.Stop;
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
end Adhcp_Dump_LL;
