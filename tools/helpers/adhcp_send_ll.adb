--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Text_IO;
with Ada.Exceptions;
with Ada.Command_Line;

with Anet.Sockets.Packet;
with Anet.IPv4;

with DHCP.OS;
with DHCP.Logger;

with DHCPv4.Message;

procedure Adhcp_Send_LL
is
   use DHCP;
   use DHCPv4;

   F : constant String := "data/dhcp-ack2.dat";
   S : Anet.Sockets.Packet.UDP_Socket_Type;
   M : Message.Message_Type;
begin
   Logger.Use_Stdout;

   S.Init;

   M := Message.Deserialize (Buffer => OS.Read_File (Filename => F));

   S.Send
     (Item  => Anet.IPv4.Create_Packet
        (Payload  => M.Serialize,
         Src_IP   => (192, 168, 239, 1),
         Src_Port => 67,
         Dst_IP   => (192, 168, 239, 119),
         Dst_Port => 68),
      To    => M.Get_Hardware_Address,
      Iface => "eth0");

   Logger.Log (Message => "Sent DHCP message to " & Anet.To_String
               (Address => M.Get_Hardware_Address));

   Logger.Stop;
   Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Success);

exception
   when E : others =>
      Logger.Stop;
      Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Message (X => E));
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
end Adhcp_Send_LL;
