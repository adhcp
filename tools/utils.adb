--
--  Copyright (C) 2012 secunet Security Networks AG
--  Copyright (C) 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Command_Line;
with Ada.Text_IO;

with DHCP.Logger;

package body Utils
is

   -------------------------------------------------------------------------

   procedure Error
     (Message     : String;
      Stop_Logger : Boolean := True)
   is
   begin
      DHCP.Logger.Log (Level   => DHCP.Logger.Error,
                       Message => Message);
      Ada.Text_IO.Put_Line
        (File => Ada.Text_IO.Current_Error,
         Item => Message);
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);

      if Stop_Logger then
         DHCP.Logger.Stop;
      end if;
   end Error;

end Utils;
