--
--  Copyright (C) 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Command_Line;
with Ada.Exceptions;

with GNAT.Command_Line;

with Anet;

with DHCP.Net;
with DHCP.Logger;
with DHCP.Version;
with DHCP.Termination;

with DHCPv6.Cmd_Line;
with DHCPv6.Client;

with Utils;

procedure Adhcp_Client6
is
begin
   DHCP.Logger.Log (Message => "Starting DHCPv6 client ("
                    & DHCP.Version.Version_String & ")");

   DHCPv6.Cmd_Line.Parse;

   begin
      DHCPv6.Client.Run;

   exception
      when E : DHCP.Net.Network_Error
         | Anet.Socket_Error =>
         Utils.Error (Message     => "Unable to start DHCPv6 client service: "
                      & Ada.Exceptions.Exception_Message (X => E),
                      Stop_Logger => False);
         DHCPv6.Client.Stop;
         return;
   end;

   DHCP.Logger.Log (Message => "DHCPv6 client service running");
   Ada.Command_Line.Set_Exit_Status (Code => DHCP.Termination.Wait);
   DHCP.Logger.Log (Message => "Shutting down DHCPv6 client");
   DHCPv6.Client.Stop;

exception
   when GNAT.Command_Line.Exit_From_Command_Line =>
      DHCP.Logger.Stop;
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Success);
   when GNAT.Command_Line.Invalid_Switch |
        GNAT.Command_Line.Invalid_Parameter =>
      DHCP.Logger.Stop;
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
   when E : DHCPv6.Cmd_Line.Invalid_Cmd_Line =>
      Utils.Error
        (Message     => "Invalid command line (try --help)",
         Stop_Logger => False);
      Utils.Error
        (Message     => Ada.Exceptions.Exception_Message (X => E),
         Stop_Logger => True);
   when E : others =>
      Utils.Error
        (Message     => "Terminating due to error",
         Stop_Logger => False);
      Utils.Error
        (Message     => Ada.Exceptions.Exception_Information (X => E),
         Stop_Logger => True);
end Adhcp_Client6;
