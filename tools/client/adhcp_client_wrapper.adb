--
--  Copyright (C) 2011, 2015 secunet Security Networks AG
--  Copyright (C) 2011, 2015 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2015 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Exceptions;
with Ada.Command_Line;
with Ada.Interrupts.Names;
with Ada.Strings.Unbounded;

with GNAT.OS_Lib;
with GNAT.Expect;

with DHCP.Logger;
with DHCP.Signals;

procedure Adhcp_Client_Wrapper
is

   use Ada.Strings.Unbounded;
   use DHCP;

   Client_Base : constant String := "/usr/local/sbin/";
   Client_Path : Unbounded_String
     := Client_Base & To_Unbounded_String ("adhcp_client");
   Iface_Name  : Unbounded_String;
   Handler     : Signals.Handler_Type (Signal => Ada.Interrupts.Names.SIGTERM);
   Args        : GNAT.OS_Lib.Argument_List (1 .. 3);
   PID         : GNAT.Expect.Process_Descriptor;

   Invalid_Arguments : exception;

   procedure Free_Args;
   --  Free memory allocated by argument list.

   function Is_V6 return Boolean;
   --  Returns True if the V6 client has been requested.

   -------------------------------------------------------------------------

   procedure Free_Args
   is
   begin
      for A in Args'Range loop
         GNAT.OS_Lib.Free (Args (A));
      end loop;
   end Free_Args;

   -------------------------------------------------------------------------

   function Is_V6 return Boolean
   is
   begin
      for A in 1 .. Ada.Command_Line.Argument_Count loop
         if Ada.Command_Line.Argument (A) = "-6" then
            return True;
         end if;
      end loop;

      return False;
   end Is_V6;

begin
   if Is_V6 then
      Client_Path := Client_Base & To_Unbounded_String ("adhcp_client6");

      Args (3) := new String'("");
   else

      --  Enable IPv4 address collision detection by default.

      Args (3) := new String'("-A");
   end if;

   if (Is_V6 and then Ada.Command_Line.Argument_Count < 2)
     or else Ada.Command_Line.Argument_Count = 0
   then
      raise Invalid_Arguments with "Interface argument missing";
   end if;

   Iface_Name := To_Unbounded_String
     (Ada.Command_Line.Argument (Ada.Command_Line.Argument_Count));

   Logger.Log (Message => "Spawning " & To_String (Client_Path)
               & " for interface " & To_String (Iface_Name));

   Args (1) := new String'("-i");
   Args (2) := new String'(To_String (Iface_Name));
   begin
      GNAT.Expect.Non_Blocking_Spawn
        (Descriptor => PID,
         Command    => To_String (Client_Path),
         Args       => Args);

   exception
      when GNAT.Expect.Invalid_Process =>
         Logger.Log (Level   => Logger.Error,
                     Message => "Unable to spawn " & To_String (Client_Path));
         Logger.Stop;
         Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
         Free_Args;
         return;
   end;
   Free_Args;

   Logger.Log (Message => "Successfully spawned " & To_String (Client_Path)
               & ", pid" & PID.Get_Pid'Img);
   Handler.Wait;

   Logger.Log (Message => "Terminating " & To_String (Client_Path)
               & " with pid" & PID.Get_Pid'Img);
   begin
      GNAT.Expect.Send_Signal
        (Descriptor => PID,
         Signal     => Integer (Ada.Interrupts.Names.SIGTERM));

   exception
      when GNAT.Expect.Invalid_Process =>
         Logger.Log (Level   => Logger.Error,
                     Message => "Unable to terminate "
                     & To_String (Client_Path) & " - process id invalid");
         Logger.Stop;
         Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
         return;
   end;

   GNAT.Expect.Close (Descriptor => PID);
   Logger.Stop;
   Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Success);

exception
   when E : others =>
      Logger.Log (Level   => Logger.Error,
                  Message => "Terminating due to error");
      Logger.Log (Level   => Logger.Error,
                  Message => Ada.Exceptions.Exception_Information (X => E));
      Logger.Stop;
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
end Adhcp_Client_Wrapper;
