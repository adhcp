--
--  Copyright (C) 2011-2014 secunet Security Networks AG
--  Copyright (C) 2011-2014 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011-2014 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Command_Line;
with Ada.Text_IO;
with Ada.Exceptions;
with Ada.Strings.Unbounded;

with GNAT.Command_Line;

with Anet.Types;

with DHCP.Net;
with DHCP.Logger;
with DHCP.Version;
with DHCP.Termination;
with DHCP.Cmd_Line;

with DHCPv4.Client;

with Utils;

procedure Adhcp_Client
is
   use Ada.Strings.Unbounded;
   use DHCP;
   use DHCPv4;

   Default_Notifier : constant String := "/usr/local/sbin/adhcp_notify_dbus";

   procedure Print_Usage (Msg : String);
   --  Print given message to stderr and client usage to stdout. Also set exit
   --  status to failure.

   procedure Print_Usage (Msg : String)
   is
      use Ada.Command_Line;
      use Ada.Text_IO;
   begin
      Put_Line (Item => "DHCPv4 client, version "
                & DHCP.Version.Version_String);
      Utils.Error (Message     => "Invalid arguments: " & Msg,
                   Stop_Logger => False);
      Put_Line (Item => "Usage: " & Command_Name & " -i iface [-1 -A]");
      Put_Line (Item => "  -i interface to configure");
      Put_Line (Item => "  -e external notification binary to call");
      Put_Line (Item => "  -1 one-shot mode");
      Put_Line (Item => "  -A Address Collision Detection");
      Client.Stop;
   end Print_Usage;

   Cli_If     : Unbounded_String;
   One_Shot   : Boolean := False;
   ACD_Enable : Boolean := False;
begin
   Logger.Log (Message => "Starting DHCPv4 client ("
               & Version.Version_String & ")");

   Cmd_Line.Set_Ext_Notify_Binary (B => Default_Notifier);

   begin
      loop
         case GNAT.Command_Line.Getopt ("i: e: 1 A") is
            when ASCII.NUL => exit;
            when 'i'       =>
               Cli_If     := To_Unbounded_String (GNAT.Command_Line.Parameter);
            when 'e'       =>
               Cmd_Line.Set_Ext_Notify_Binary
                 (B => GNAT.Command_Line.Parameter);
            when '1'       =>
               One_Shot   := True;
            when 'A'       =>
               ACD_Enable := True;
            when others    =>
               raise Program_Error;
         end case;
      end loop;

   exception
      when GNAT.Command_Line.Invalid_Switch =>
         Print_Usage (Msg => "Invalid switch -"
                      & GNAT.Command_Line.Full_Switch);
         return;
      when GNAT.Command_Line.Invalid_Parameter =>
         Print_Usage (Msg => "No parameter for -"
                      & GNAT.Command_Line.Full_Switch);
         return;
   end;

   if Cli_If = Null_Unbounded_String then
      Print_Usage (Msg => "No client interface specified");
      return;
   end if;

   if not Anet.Types.Is_Valid_Iface (Name => To_String (Cli_If)) then
      Print_Usage (Msg => "Invalid client interface name: "
                   & To_String (Cli_If));
      return;
   end if;

   begin
      Client.Run
        (Client_Iface  => Anet.Types.Iface_Name_Type (To_String (Cli_If)),
         One_Shot_Mode => One_Shot,
         ACD_Enable    => ACD_Enable);

   exception
      when E : Net.Network_Error
         | Anet.Socket_Error
         | Client.Config_Error =>
         Utils.Error (Message     => "Unable to start DHCP client service: "
                      & Ada.Exceptions.Exception_Message (X => E),
                      Stop_Logger => False);
         Client.Stop;
         return;
   end;

   Logger.Log (Message => "DHCP client service running");
   Ada.Command_Line.Set_Exit_Status (Code => Termination.Wait);
   Logger.Log (Message => "Shutting down DHCP client");
   Client.Stop;

exception
   when E : others =>
      Utils.Error
        (Message     => "Terminating due to error",
         Stop_Logger => False);
      Utils.Error
        (Message     => Ada.Exceptions.Exception_Information (X => E),
         Stop_Logger => False);
      Client.Stop;
end Adhcp_Client;
