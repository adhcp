--
--  Copyright (C) 2011, 2012 secunet Security Networks AG
--  Copyright (C) 2011, 2012 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011, 2012 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Command_Line;
with Ada.Text_IO;
with Ada.Exceptions;

with Anet.Types;

with DHCP.Net;
with DHCP.Logger;
with DHCP.Version;
with DHCP.Termination;

with DHCPv4.Relay;

with Utils;

procedure Adhcp_Relay
is
   use DHCP;
   use DHCPv4;

   procedure Print_Usage;
   --  Print relay agent usage to stdout.

   procedure Print_Usage is
   begin
      Ada.Text_IO.Put_Line ("Usage: " & Ada.Command_Line.Command_Name
                            & " <dhcp server> <client iface>");
   end Print_Usage;

   Srv_IP : Anet.IPv4_Addr_Type;
begin
   Logger.Log (Message => "Starting DHCP relay agent ("
               & Version.Version_String & ")");

   if Ada.Command_Line.Argument_Count /= 2 then
      Utils.Error (Message => "Invalid argument count, aborting");
      Print_Usage;
      return;
   end if;

   Validate_Server_IP :
   begin
      Srv_IP := Anet.To_IPv4_Addr (Str => Ada.Command_Line.Argument (1));

   exception
      when Constraint_Error =>
         Utils.Error (Message => "Invalid server IP: "
                      & Ada.Command_Line.Argument (1));
         return;
   end Validate_Server_IP;

   if not Anet.Types.Is_Valid_Iface
     (Name => Ada.Command_Line.Argument (2))
   then
      Utils.Error (Message => "Invalid client interface name: "
                   & Ada.Command_Line.Argument (2));
      return;
   end if;

   begin
      Relay.Run (Server_IP    => Srv_IP,
                 Client_Iface => Anet.Types.Iface_Name_Type
                   (Ada.Command_Line.Argument (2)));

   exception
      when E : Net.Network_Error
         | Anet.Socket_Error
         | Relay.Config_Error =>
         Utils.Error (Message => "Unable to start relaying service: "
                      & Ada.Exceptions.Exception_Message (X => E));
         return;
   end;

   Logger.Log (Message => "DHCP relaying service running");
   Ada.Command_Line.Set_Exit_Status (Code => DHCP.Termination.Wait);
   Logger.Log (Message => "DHCP relaying service stopped");
   Relay.Stop;
   Logger.Stop;

exception
   when E : others =>
      Utils.Error (Message     => "Terminating due to error",
                   Stop_Logger => False);
      Utils.Error (Message => Ada.Exceptions.Exception_Information (X => E));
end Adhcp_Relay;
