--
--  Copyright (C) 2011 secunet Security Networks AG
--  Copyright (C) 2011 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2011 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--
--  This program is free software; you can redistribute it and/or modify it
--  under the terms of the GNU General Public License as published by the
--  Free Software Foundation; either version 2 of the License, or (at your
--  option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
--
--  This program is distributed in the hope that it will be useful, but
--  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
--  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
--  for more details.
--

with Ada.Streams;

with DHCP.OS;
with DHCP.Logger;

with DHCPv4.Message;

procedure Profiler
is
   Msg_Data : constant Ada.Streams.Stream_Element_Array
     := DHCP.OS.Read_File (Filename => "../../data/dhcp-ack1.dat");
   M : DHCPv4.Message.Message_Type;
   pragma Unreferenced (M);
begin
   for I in 1 .. 1000 loop
      M := DHCPv4.Message.Deserialize (Buffer => Msg_Data);
   end loop;

   DHCP.Logger.Stop;
end Profiler;
